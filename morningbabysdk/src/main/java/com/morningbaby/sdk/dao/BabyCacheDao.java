package com.morningbaby.sdk.dao;

import com.nd.smartcan.datalayer.interfaces.IDataResult;
import com.nd.smartcan.frame.dao.CacheDao;

/**
 * Created by carl on 15/8/8.
 */
public abstract class BabyCacheDao<T> extends CacheDao<T>{
    protected static final String DATASOURCE_NAME = "base";
    public BabyCacheDao(){
    }
    public BabyCacheDao(String id){
        super(id);
    }
    /**
     * 此通用方法暂时加在这里,将来移到底层
     * @return
     */
    public boolean canLoadMore(){
        IDataResult result = getDefaultListDataLayer().getDataLayer().dataSourceForName(DATASOURCE_NAME).allResult();
        if(result==null){
            return false;
        }
        return result.canLoadMore();
    }
}
