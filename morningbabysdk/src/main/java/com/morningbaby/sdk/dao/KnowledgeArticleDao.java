package com.morningbaby.sdk.dao;

import com.morningbaby.sdk.Constant;
import com.morningbaby.sdk.model.KnowledgeArticle;
import com.nd.smartcan.frame.dao.CacheDao;
import com.nd.smartcan.frame.model.DataSourceDefine;

/**
 * Created by carl on 15/8/4.
 */
public class KnowledgeArticleDao extends BabyCacheDao<KnowledgeArticle>{
    public KnowledgeArticleDao(){
    }
    public KnowledgeArticleDao(String id){
        super(id);
    }
    @Override
    protected String getResourceUri() {
        return null;
    }
    @Override
    protected DataSourceDefine getDefaultListDefine() {
        return super.getDefaultListDefine()
                .withApi(Constant.BaseUrl+"/knowledge/article?topic=${topic}&filter=${filter}&page=${__page}&pagesize=${__count}")
                .withClassId(Constant.CacheProxyClass)
                .withPagesize(Constant.PageSize)
                .withKeyField("id")
                .withSortField("id").withExpire(Constant.ttl);
    }
    @Override
    protected DataSourceDefine getDefaultDetailDefine() {
        return super.getDefaultDetailDefine().withApi(Constant.BaseUrl+"/knowledge/article/${id}")
                .withClassId(Constant.CacheProxyClass)
                .withKeyField("id")
                .withExpire(Constant.ttl);
    }
}
