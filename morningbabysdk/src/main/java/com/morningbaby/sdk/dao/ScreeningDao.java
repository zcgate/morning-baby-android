package com.morningbaby.sdk.dao;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.morningbaby.sdk.Constant;
import com.morningbaby.sdk.SdkManager;
import com.morningbaby.sdk.model.ReturnObject;
import com.morningbaby.sdk.model.Screening;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.frame.dao.CacheDao;
import com.nd.smartcan.frame.dao.RestDao;
import com.nd.smartcan.frame.exception.DaoException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by carl on 15/8/3.
 */
public class ScreeningDao extends RestDao<Screening>{

    private static final String PrefName = "Screening";
    private static final String PrefKey = "result";

    public ScreeningDao(String id){
        super(id);
    }
    @Override
    public void setObjId(String id){
        super.setObjId(id);
    }

    @Override
    protected String getResourceUri() {
        return null;
    }

    public boolean submit(Map<String,Object> data){
        String url = Constant.BaseUrl+"/screening/palsy";
        try {
            ReturnObject obj = super.post(url,data,null,ReturnObject.class);
            if(obj.success()){
                saveScreening(""); /// clear local data
                return true;
            }
            return false;
        } catch (DaoException e) {
            e.printStackTrace();
            return false;
        }
    }
    public String getResult(){
        String result = getLocalScreening();
        if(!TextUtils.isEmpty(result)){
            return result;
        }
        return getScreeningFromServer();
    }
    private String getScreeningFromServer(){
        String url = Constant.BaseUrl+"/user/${id}/screening/palsy";
        Map<String,Object> param = new HashMap<String, Object>();
        param.put("id",getObjId());
        try {
            String result = super.get(url, param, String.class);
            saveScreening(result);
            return result;
        } catch (DaoException e) {
            e.printStackTrace();
            return "";
        }
    }
    private String getPrefKey(){
        return PrefKey+"_"+getObjId();
    }
    private String getLocalScreening(){
        SharedPreferences sp = SdkManager.getInstance().getApplication().getSharedPreferences(PrefName, Context.MODE_PRIVATE);
        return sp.getString(getPrefKey(),"");
    }
    private void saveScreening(String result){
        SharedPreferences sp = SdkManager.getInstance().getApplication().getSharedPreferences(PrefName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(getPrefKey(),result);
        editor.commit();
    }

    /*
    public static class ScreeningStruct{
        @JsonProperty("result")
        private String result;
        @JsonProperty("detail")
        private List<Map<String,Integer>> detail;
    }
    */
}
