package com.morningbaby.sdk.dao;

import com.morningbaby.sdk.Constant;
import com.morningbaby.sdk.model.KnowledgeTopic;
import com.nd.smartcan.frame.dao.CacheDao;
import com.nd.smartcan.frame.model.DataSourceDefine;

/**
 * Created by carl on 15/8/4.
 */
public class KnowledgeTopicDao extends BabyCacheDao<KnowledgeTopic>{
    public KnowledgeTopicDao(){
    }
    public KnowledgeTopicDao(String id){
        super(id);
    }
    @Override
    protected String getResourceUri() {
        return null;
    }
    @Override
    protected DataSourceDefine getDefaultListDefine() {
        return super.getDefaultListDefine()
                .withApi(Constant.BaseUrl+"/knowledge/topic?page=${__page}&pagesize=${__count}")
                .withClassId(Constant.CacheProxyClass)
                .withPagesize(50)
                .withKeyField("id")
                .withSortField("id").withExpire(Constant.ttl);
    }
    @Override
    protected DataSourceDefine getDefaultDetailDefine() {
        return super.getDefaultDetailDefine().withApi(Constant.BaseUrl+"/knowledge/topic/${id}")
                .withClassId(Constant.CacheProxyClass)
                .withKeyField("id")
                .withExpire(Constant.ttl);
    }
}
