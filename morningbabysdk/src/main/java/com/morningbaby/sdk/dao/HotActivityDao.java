package com.morningbaby.sdk.dao;

import com.morningbaby.sdk.Constant;
import com.morningbaby.sdk.model.HotActivity;
import com.nd.smartcan.frame.dao.CacheDao;
import com.nd.smartcan.frame.model.DataSourceDefine;

/**
 * Created by carl on 15/8/6.
 */
public class HotActivityDao extends BabyCacheDao<HotActivity> {
    public HotActivityDao(){
    }
    public HotActivityDao(String id){
        super(id);
    }
    @Override
    protected String getResourceUri() {
        return null;
    }
    @Override
    protected DataSourceDefine getDefaultListDefine() {
        return super.getDefaultListDefine()
                .withApi(Constant.BaseUrl+"/activity?top=${top}")
                .withClassId(Constant.CacheProxyClass)
                .withPagesize(Constant.PageSize)
                .withKeyField("id")
                .withSortField("id").withExpire(Constant.ttl);
    }
    @Override
    protected DataSourceDefine getDefaultDetailDefine() {
        return super.getDefaultDetailDefine().withApi(Constant.BaseUrl+"/activity/${id}")
                .withClassId(Constant.CacheProxyClass)
                .withKeyField("id")
                .withExpire(Constant.ttl);
    }
}
