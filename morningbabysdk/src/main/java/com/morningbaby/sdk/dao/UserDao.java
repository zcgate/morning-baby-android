package com.morningbaby.sdk.dao;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Environment;

import com.morningbaby.sdk.Constant;
import com.morningbaby.sdk.SdkManager;
import com.morningbaby.sdk.model.ReturnObject;
import com.morningbaby.sdk.model.User;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.core.restful.ClientResource;
import com.nd.smartcan.core.restful.ResourceException;
import com.nd.smartcan.datalayer.interfaces.IDataLayer;
import com.nd.smartcan.datalayer.network.HttpWrapper;
import com.nd.smartcan.datalayer.tools.SdkEnvironment;
import com.nd.smartcan.frame.dao.CacheDao;
import com.nd.smartcan.frame.exception.DaoException;
import com.nd.smartcan.frame.model.DataSourceDefine;
import com.nd.smartcan.frame.dao.CacheDefine.IDataRetrieveListener;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 用户数据访问
 * Created by carl on 2015/08/02.
 */
public final class UserDao extends BabyCacheDao<User> {
    private static final String BaseUrl = "${BabyBaseUrl}";

    private final static String PREF_NAME = "user";
    private final static String KEY_PREF_UID = "uid";
    private final static String KEY_PREF_SESSION = "session_id";

    public UserDao() {
    }

    @Override
    protected DataSourceDefine getDefaultListDefine() {
        return super.getDefaultListDefine()
                .withApi(BaseUrl+"/users?key=${key}&$offset=${__start}&$limit=${__count}")
                .withPagesize(Constant.PageSize)
                .withKeyField("uid")
                .withSortField("uid")
                .withExpire(SdkManager.getInstance().getCacheTTL());
    }

    @Override
    protected DataSourceDefine getDefaultDetailDefine() {
        return super.getDefaultDetailDefine().withApi(BaseUrl + "/user/${id}")
                .withClassId(Constant.CacheProxyClass)
                .withKeyField("uid")
                .withExpire(SdkManager.getInstance().getCacheTTL());
    }

    /**
     * 取得Post Uri
     * http://www.nd.com/k12app/class/${class}/student/
     * http://www.nd.com/k12app/class/
     */
    @Override
    protected String getResourceUri() {
        return BaseUrl+"/user";
    }

    /**
     * 异步取得用户资料
     * @param uid
     * @param listener
     * @param force
     */
    public void getAsync(String uid,IDataRetrieveListener listener,boolean force){
        Map<String, Object> stringMap = new HashMap<String, Object>();
        stringMap.put("id", uid);
        super.get(listener,stringMap,force);
    }
    @Override
    protected void bindParam(IDataLayer dl){
        dl.bindParam("id",getObjId());
    }
    /**
     * //@see com.nd.smartcan.accountclient.dao.UserDao#get(long, String)
     */
    public User get(String uid ) throws DaoException {
        Map<String, Object> stringMap = new HashMap<String, Object>();
        stringMap.put("id", uid);
        Logger.i(getClass(), "detail cache ns="+getApi().ns+ " name="+getApi().name);
        //bindParam(getDefaultDetailDataLayer());
        return super.get(stringMap);
    }

    public ReturnObject login(String account,String pwd){
        String url = getResourceUri()+"/${account}/session";
        Map<String,String> data = new HashMap<String, String>();
        data.put("key",pwd);
        //todo getDeviceId
        data.put("deviceId", SdkEnvironment.getUUID());
        Map<String,Object> param = new HashMap<String, Object>();
        param.put("account",account);
        try {
            ReturnObject ret = super.post(url, data, param, ReturnObject.class);
            if(ret.success()){
                saveAccount(ret);
            }
            return ret;
        }
        catch (DaoException e){
            e.printStackTrace();
            return new ReturnObject(false);
        }
    }
    private void saveAccount(ReturnObject ret) {
        Map<String, Object> data = ret.getDetail();
        SharedPreferences sp = SdkManager.getInstance().getApplication().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(KEY_PREF_UID, (String) data.get("uid"));
        editor.putString(KEY_PREF_SESSION, (String) data.get("session_id"));
        editor.commit();
        //loadCurrentUser();
    }
    private void clearLoginInfo(){
        SharedPreferences sp = SdkManager.getInstance().getApplication().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(KEY_PREF_UID);
        editor.remove(KEY_PREF_SESSION);
        editor.commit();
    }

    /**
     * 加载当前 用户
     */
    public static String getLoginedSession() {
        SharedPreferences sp = SdkManager.getInstance().getApplication().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return sp.getString(KEY_PREF_SESSION, "");
    }
    public static String getLoginedUid(){
        SharedPreferences sp = SdkManager.getInstance().getApplication().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return sp.getString(KEY_PREF_UID, "");
    }

    public void logout(String account,String session){
        String url = getResourceUri()+"/${account}/session/${session}";
        Map<String,Object> param = new HashMap<String, Object>();
        param.put("account",account);
        param.put("session",session);
        try {
            ReturnObject ret = super.post(url, null, param, ReturnObject.class);
        }
        catch (DaoException e){
            e.printStackTrace();
        }
        clearLoginInfo();
        // logout event
    }
    public ReturnObject register(String phone,String email,String pwd){
        String  url = getResourceUri();
        Map<String,String> data = new HashMap<String, String>();
        data.put("email",email);
        data.put("phone",phone);
        data.put("key",pwd);
        try{
            ReturnObject ret = super.post(url, data, null, ReturnObject.class);
            return ret;
        }
        catch (DaoException e){
            e.printStackTrace();
            return new ReturnObject(false);
        }
    }
    public boolean update(User user){
        String url = getResourceUri()+"/"+user.getUid();
        Logger.i(getClass(),"uid = "+user.getUid());
        try {
            //ReturnObject ret = super.post(user,null,ReturnObject.class);
            ReturnObject ret = super.post(url,user,null,ReturnObject.class);
            if(ret.success()){
                Logger.i(getClass(), "clear detail cache ns="+getApi().ns+ " name="+getApi().name);
                clearDetailCache();
                return true;
            }
            return ret.success();
        } catch (DaoException e) {
            e.printStackTrace();
            return false;
        }
    }
    public String uploadAvata(Bitmap bmp){
        ClientResource cl = new ClientResource(SdkManager.getInstance().getBaseUrl()+"/knowledge/image");
        File avatar = saveAvataToFile(bmp);
        cl.addField("file",avatar);
        try {
            ReturnObject ret = cl.post(ReturnObject.class);
            avatar.delete();
            if(ret.success()){
                return (String) ret.getDetail().get("location");
            }
            else {
                return "";
            }
        } catch (ResourceException e) {
            e.printStackTrace();
        }
        return "";
                /*
        HttpWrapper client = new HttpWrapper(SdkManager.getInstance().getBaseUrl()+"/knowledge/image");
        client.setMethod(HttpWrapper.HTTP_Method.POST);
        client.uploadFile(bmp,"filename");
        Logger.i(getClass(),"===TEST=== bmp size="+bmp.getByteCount());
        if(client.sendRequest()){
            Map<String,Object> result = client.getRetrievedJson();
            if("success".equalsIgnoreCase((String) result.get("result"))){
                Map<String,Object> detail =  (Map<String,Object>) result.get("detail");
                if(detail!=null) {
                    return (String) detail.get("location");
                }
                else {
                    return "";
                }
            }
        }
        */
    }
    private File saveAvataToFile(Bitmap bmp){
        // HttpWrapper 有些问题待解，临时处理
        File sdDir = Environment.getExternalStorageDirectory();
        File dir =  new File(sdDir.toString()+"/httpwrapper_tmp");
        dir.mkdirs();
        String fileName = dir.toString()+"/"+String.valueOf(System.currentTimeMillis()) + ".png";
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        File file = new File(fileName);
        BufferedOutputStream bos = null;
        try {
            bos = new BufferedOutputStream(new FileOutputStream(file));
            bmp.compress(Bitmap.CompressFormat.PNG, 100, bos);
            bos.flush();
            bos.close();
        } catch (FileNotFoundException e) {
            Logger.w(getClass(), ""+e.getMessage());
        } catch (IOException e) {
            Logger.w(getClass(), ""+e.getMessage());
        }
        return file;
    }

}

