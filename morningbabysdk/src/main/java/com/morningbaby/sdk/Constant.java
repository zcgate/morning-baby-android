package com.morningbaby.sdk;

/**
 * Created by carl on 15/8/4.
 */
public class Constant {
    public static final String MockUrl = "http://101.200.89.52:4567/api/v1";
    public static final String BaseUrl = "${BabyBaseUrl}";
    public static final int PageSize = 20;
    public static final String CacheProxyClass = "com.morningbaby.sdk.CacheProxy.BabyCacheProxy";
    public static int ttl= 24*3600;
}
