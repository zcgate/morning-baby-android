package com.morningbaby.sdk;

import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.frame.SmartCanApp;


/**
 * Created by carl on 15/8/4.
 */
public class TestApp extends SmartCanApp{
    @Override
    protected void beforeCreate() {

    }

    @Override
    protected void afterCreate() {
        Logger.i(getClass(),"after set app");
        SdkManager.getInstance().setApplication(this);
        //SdkManager.getInstance().setBaseUrl("http://101.200.89.52:4567/api/v1");
        SdkManager.getInstance().setBaseUrl("http://101.200.89.52:8080/morning-baby-service-0.1.0/api/v1");
        Logger.i(getClass(),"after set app");
    }
}
