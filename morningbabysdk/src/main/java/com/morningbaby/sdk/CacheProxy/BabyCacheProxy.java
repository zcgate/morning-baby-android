package com.morningbaby.sdk.CacheProxy;

import android.util.Log;
import com.nd.smartcan.commons.util.language.Json2Std;
import com.nd.smartcan.commons.util.language.MapHelper;
import com.nd.smartcan.datalayer.cache.CacheProxy;

import java.util.*;

/**
 * Created by carl on 15/8/4.
 */
public class BabyCacheProxy extends CacheProxy{
    private static final String TAG = "BabyCacheProxy";

    @Override
    protected List<Object> getListDataFromResponse() {
        List<Object> objList = new ArrayList<Object>();
        Json2Std json = httpClient().getJson2Std();
        if (json.isMap()) {
            Map<String, Object> map = json.getResultMap();
            if(map.get("detail")!=null){
                objList.addAll((List<?>)map.get("detail"));
                Log.d(TAG, "getListDataFromResponse ");
            }
            return objList;
        } else {
            return json.getResultArray();
        }
    }
    @Override
    protected Map<String,Object> getListGlobalFromResponse(){
        List<Object> objList = new ArrayList<Object>();
        Json2Std json = httpClient().getJson2Std();
        if (json.isMap()) {
            Map<String, Object> map = json.getResultMap();
            if(map.get("paging")!=null){
                return (Map<String,Object>) map.get("paging");
            }
        }
        return new HashMap<String,Object>();
    }
    @Override
    protected String getDetailFromResponse(){
        return mHttpClient.getResponseString();
    }

    @Override
    protected String getErrorMsgFromResponse() {
        return MapHelper.getStringValueByKey(mResponseJson, "Msg", "");
    }

    @Override
    protected long getResponseTick() {
        return MapHelper.getLongValueByKey(mResponseJson, "update_time", 0);
    }
    @Override
    protected String getDeletedFromResponse() {
        return MapHelper.getStringValueByKey(mResponseJson, "delete_items", "");
    }
}
