package com.morningbaby.sdk.model;

import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.morningbaby.sdk.dao.KnowledgeArticleDao;
import com.nd.smartcan.frame.dao.CacheDefine.IDataRetrieveListener;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;
import com.nd.smartcan.frame.exception.DaoException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by carl on 15/8/2.
 */
public class KnowledgeArticle {
    @JsonProperty("id")
    private String id;

    @JsonProperty("time")
    private String time;

    @JsonProperty("title")
    private String title;

    @JsonProperty("author")
    private String author;

    @JsonProperty("tags")
    private String tags;

    @JsonProperty("topic_id")
    private String topicId;

    @JsonProperty("brief")
    private String brief;

    @JsonProperty("content")
    private String content;


    public KnowledgeArticle() {
    }

    public KnowledgeArticle(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }
    private KnowledgeArticleDao dao;
    private synchronized KnowledgeArticleDao getDao(){
        if(dao==null){
            dao = new KnowledgeArticleDao();
        }
        return dao;
    }
    public void getArticleList(String topic,String filter,IListDataRetrieveListener<KnowledgeArticle> listener,boolean force){
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("topic",topic);
        try {
            if(!TextUtils.isEmpty(filter)) {
                param.put("filter", URLEncoder.encode(filter, "UTF-8"));
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        getDao().list(listener, param, force);
    }
    public void nextPage(IListDataRetrieveListener<KnowledgeArticle> listener){
        getDao().nextPage(listener);
    }
    public boolean canLoadMore(){
        return getDao().canLoadMore();
    }
    public static KnowledgeArticle getArticleFromCache(String id){
        return new KnowledgeArticleDao(id).getFromListCache();
    }
    public static KnowledgeArticle getArticle(String id){
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("id",id);
        try {
            return new KnowledgeArticleDao().get(param);
        } catch (DaoException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static void getArticleAsync(String id,IDataRetrieveListener<KnowledgeArticle> listener,boolean force){
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("id",id);
        new KnowledgeArticleDao().get(listener,param,force);
    }
}
