package com.morningbaby.sdk.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.morningbaby.sdk.dao.HotActivityDao;
import com.nd.smartcan.frame.dao.CacheDefine.IDataRetrieveListener;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;
import com.nd.smartcan.frame.exception.DaoException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by carl on 15/8/6.
 */
public class HotActivity {
    @JsonProperty("id")
    private String id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("activityPic")
    private String activityPic;
    @JsonProperty("activityDate")
    private String activityDate;
    @JsonProperty("activityTime")
    private List<Map<String,Integer>> activityTime;
    @JsonProperty("activityAddressProvince")
    private String activityAddressProvince;
    @JsonProperty("activityAddressCity")
    private String activityAddressCity;
    @JsonProperty("activityAddressDistinct")
    private String activityAddressDistinct;
    @JsonProperty("activityAddressDetail")
    private String activityAddressDetail;
    @JsonProperty("activityAddressGeo")
    private String activityAddressGeo;
    @JsonProperty("contactName")
    private String contactAame;
    @JsonProperty("contactEmail")
    private String contactEmail;
    @JsonProperty("contactPhone")
    private String contactPhone;
    @JsonProperty("description")
    private String description;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getActivityPic() {
        return activityPic;
    }

    public void setActivityPic(String activityPic) {
        this.activityPic = activityPic;
    }

    public String getActivityDate() {
        return activityDate;
    }

    public void setActivityDate(String activityDate) {
        this.activityDate = activityDate;
    }

    public List<Map<String, Integer>> getActivityTime() {
        return activityTime;
    }

    public void setActivityTime(List<Map<String, Integer>> activityTime) {
        this.activityTime = activityTime;
    }

    public String getActivityAddressProvince() {
        return activityAddressProvince;
    }

    public void setActivityAddressProvince(String activityAddressProvince) {
        this.activityAddressProvince = activityAddressProvince;
    }

    public String getActivityAddressCity() {
        return activityAddressCity;
    }

    public void setActivityAddressCity(String activityAddressCity) {
        this.activityAddressCity = activityAddressCity;
    }

    public String getActivityAddressDistinct() {
        return activityAddressDistinct;
    }

    public void setActivityAddressDistinct(String activityAddressDistinct) {
        this.activityAddressDistinct = activityAddressDistinct;
    }

    public String getActivityAddressDetail() {
        return activityAddressDetail;
    }

    public void setActivityAddressDetail(String activityAddressDetail) {
        this.activityAddressDetail = activityAddressDetail;
    }

    public String getActivityAddressGeo() {
        return activityAddressGeo;
    }

    public void setActivityAddressGeo(String activityAddressGeo) {
        this.activityAddressGeo = activityAddressGeo;
    }

    public String getContactAame() {
        return contactAame;
    }

    public void setContactAame(String contactAame) {
        this.contactAame = contactAame;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private HotActivityDao dao;
    private synchronized HotActivityDao getDao(){
        if(dao==null){
            dao = new HotActivityDao();
        }
        return dao;
    }
    public void getHotActivityList(IListDataRetrieveListener<HotActivity> listener,int count,boolean force){
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("top",count);
        getDao().list(listener, param, force);
    }
    public void nextPage(IListDataRetrieveListener<HotActivity> listener){
        getDao().nextPage(listener);
    }
    public boolean canLoadMore(){
        return getDao().canLoadMore();
    }
    public static HotActivity getHotActivityFromCache(String id){
        return new HotActivityDao(id).getFromListCache();
    }
    public static HotActivity getHotActivity(String id){
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("id",id);
        try {
            return new HotActivityDao().get(param);
        } catch (DaoException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static void getHotActivityAsync(String id,IDataRetrieveListener<HotActivity> listener,boolean force){
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("id",id);
        new HotActivityDao().get(listener,param,force);
    }
}
