package com.morningbaby.sdk.model;

import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.morningbaby.sdk.dao.HospitalDao;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.frame.dao.CacheDefine.IDataRetrieveListener;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;
import com.nd.smartcan.frame.exception.DaoException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by carl on 15/8/2.
 */
public class Hospital {

    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("phone")
    private String phone;


    @JsonProperty("picture")
    private String picture;

    @JsonProperty("addressProvince")
    private String addressProvince;

    @JsonProperty("addressCity")
    private String addressCity;

    @JsonProperty("addressDistinct")
    private String addressDistinct;

    @JsonProperty("addressDetail")
    private String addressDetail;

    @JsonProperty("addressGeo")
    private String addressGeo;

    @JsonProperty("introduction")
    private String introduction;


    public Hospital() {
    }

    public Hospital(String id) {
        this.id = id;
    }
    /**
     * 获取id
     *
     * @return id
     */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public String getPicture() {
        return this.picture;
    }

    public void setPicture(String pict) {
        this.picture = pict;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddressProvince() {
        return addressProvince;
    }

    public void setAddressProvince(String addressProvince) {
        this.addressProvince = addressProvince;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public String getAddressDistinct() {
        return addressDistinct;
    }

    public void setAddressDistinct(String addressDistinct) {
        this.addressDistinct = addressDistinct;
    }

    public String getAddressDetail() {
        return addressDetail;
    }

    public void setAddressDetail(String addressDetail) {
        this.addressDetail = addressDetail;
    }

    public String getAddressGeo() {
        return addressGeo;
    }

    /**
     * 取得经度
     * @return
     */
    public float getGeoLongitude() {
        if(TextUtils.isEmpty(addressGeo)){
            return 0;
        }
        try {
            //Logger.i(getClass(),"geo Longitude="+Float.parseFloat(addressGeo.substring(0, addressGeo.indexOf(","))));
            return Float.parseFloat(addressGeo.substring(0, addressGeo.indexOf(",")));
        }
        catch (NumberFormatException e){
            return 0;
        }
    }
    public float getGeoLatitude(){
        if(TextUtils.isEmpty(addressGeo)){
            return 0;
        }
        try {
            //Logger.i(getClass(),"geo Latitude="+Float.parseFloat(addressGeo.substring(addressGeo.indexOf(",")+1)));
            return Float.parseFloat(addressGeo.substring(addressGeo.indexOf(",")+1));
        }
        catch (NumberFormatException e){
            return 0;
        }
    }

    public void setAddressGeo(String addressGeo) {
        this.addressGeo = addressGeo;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    private HospitalDao dao;
    private synchronized HospitalDao getDao(){
        if(dao==null){
            dao = new HospitalDao();
        }
        return dao;
    }
    public void getHospitalList(IListDataRetrieveListener<Hospital> listener,boolean force){
        Map<String,Object> param = new HashMap<String,Object>();
        getDao().list(listener, param, force);
    }
    public void nextPage(IListDataRetrieveListener<Hospital> listener){
        getDao().nextPage(listener);
    }
    public boolean canLoadMore(){
        return getDao().canLoadMore();
    }
    public static Hospital getHospitalFromCache(String id){
        return new HospitalDao(id).getFromListCache();
    }
    public static Hospital getHospital(String id){
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("id",id);
        try {
            return new HospitalDao().get(param);
        } catch (DaoException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static void getHospitalAsync(String id,IDataRetrieveListener<Hospital> listener,boolean force){
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("id",id);
        new HospitalDao().get(listener,param,force);
    }
}
