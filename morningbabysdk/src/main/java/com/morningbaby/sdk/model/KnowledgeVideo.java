package com.morningbaby.sdk.model;

import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.morningbaby.sdk.dao.KnowledgeTopicDao;
import com.morningbaby.sdk.dao.KnowledgeVideoDao;
import com.nd.smartcan.frame.dao.CacheDefine.IDataRetrieveListener;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;
import com.nd.smartcan.frame.exception.DaoException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by carl on 15/8/2.
 */
public class KnowledgeVideo {

    @JsonProperty("id")
    private String id;

    @JsonProperty("title")
    private String title;

    @JsonProperty("time")
    private String time;

    @JsonProperty("description")
    private String description;

    @JsonProperty("thumbnail")
    private String thumbnail;

    @JsonProperty("tags")
    private String tags;

    @JsonProperty("topic_id")
    private String topicId;

    @JsonProperty("location")
    private String location;

    public KnowledgeVideo() {
    }

    public KnowledgeVideo(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }


    private KnowledgeVideoDao dao;
    private synchronized KnowledgeVideoDao getDao(){
        if(dao==null){
            dao = new KnowledgeVideoDao();
        }
        return dao;
    }
    public void getVideoList(String topic,String filter,IListDataRetrieveListener<KnowledgeVideo> listener,boolean force){
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("topic",topic);
        try {
            if(!TextUtils.isEmpty(filter)) {
                param.put("filter", URLEncoder.encode(filter, "UTF-8"));
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        getDao().list(listener, param, force);
    }
    public void nextPage(IListDataRetrieveListener<KnowledgeVideo> listener){
        getDao().nextPage(listener);
    }
    public boolean canLoadMore(){
        return getDao().canLoadMore();
    }
    public static KnowledgeVideo getVideoFromCache(String id){
        return new KnowledgeVideoDao(id).getFromListCache();
    }
    public static KnowledgeVideo getVideo(String id){
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("id",id);
        try {
            return new KnowledgeVideoDao().get(param);
        } catch (DaoException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static void getVideoAsync(String id,IDataRetrieveListener<KnowledgeVideo> listener,boolean force){
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("id",id);
        new KnowledgeVideoDao().get(listener,param,force);
    }
}
