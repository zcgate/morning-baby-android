package com.morningbaby.sdk.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.morningbaby.sdk.dao.DoctorDao;
import com.nd.smartcan.frame.dao.CacheDefine.IDataRetrieveListener;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;
import com.nd.smartcan.frame.exception.DaoException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by carl on 15/8/4.
 */
public class Doctor {
    @JsonProperty("id")
    private String id;
    @JsonProperty("hospitalId")
    private String hospitalId;
    @JsonProperty("departmentId")
    private String departmentId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("photo")
    private String photo;
    @JsonProperty("title")
    private String title;
    @JsonProperty("timeVisits")
    private List<Map<String,String>> timeVisits;
    @JsonProperty("timeAvailable")
    private List<Map<String,String>> timeAvailable;
    @JsonProperty("introduction")
    private String introduction;
    public Doctor() {
    }

    public Doctor(String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Map<String, String>> getTimeVisits() {
        return timeVisits;
    }

    public void setTimeVisits(List<Map<String, String>> timeVisits) {
        this.timeVisits = timeVisits;
    }

    public List<Map<String, String>> getTimeAvailable() {
        return timeAvailable;
    }

    public void setTimeAvailable(List<Map<String, String>> time_available) {
        this.timeAvailable = time_available;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    private DoctorDao dao;
    private synchronized DoctorDao getDao(){
        if(dao==null){
            dao = new DoctorDao();
        }
        return dao;
    }
    public void getDoctorList(String hostpitalId,String departmentId,IListDataRetrieveListener<Doctor> listener,boolean force){
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("hospital",hostpitalId);
        param.put("department",departmentId);
        getDao().list(listener, param, force);
    }
    public void nextPage(IListDataRetrieveListener<Doctor> listener){
        getDao().nextPage(listener);
    }
    public boolean canLoadMore(){
        return getDao().canLoadMore();
    }
    public static Doctor getDoctorFromCache(String id){
        return new DoctorDao(id).getFromListCache();
    }
    public static Doctor getDoctor(String id){
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("id",id);
        try {
            return new DoctorDao().get(param);
        } catch (DaoException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static void getDoctorAsync(String id,IDataRetrieveListener<Doctor> listener,boolean force){
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("id",id);
        new DoctorDao().get(listener,param,force);
    }
}
