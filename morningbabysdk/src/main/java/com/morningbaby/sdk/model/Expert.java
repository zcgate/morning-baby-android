package com.morningbaby.sdk.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.morningbaby.sdk.dao.ExpertDao;
import com.nd.smartcan.frame.dao.CacheDefine.IDataRetrieveListener;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;
import com.nd.smartcan.frame.exception.DaoException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by carl on 15/8/5.
 */
public class Expert {
    @JsonProperty("id")
    private String id;
    @JsonProperty("hostpitalId")
    private String hostpitalId;
    @JsonProperty("departmentId")
    private String departmentId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("photo")
    private String photo;
    @JsonProperty("title")
    private String title;
    @JsonProperty("timeVisits")
    private List<Map<String,String>> timeVisits;
    @JsonProperty("timeAvailable")
    private List<Map<String,String>> timeAvailable;
    @JsonProperty("introduction")
    private String introduction;
    public Expert() {
    }

    public Expert(String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHostpitalId() {
        return hostpitalId;
    }

    public void setHostpitalId(String hostpitalId) {
        this.hostpitalId = hostpitalId;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Map<String, String>> getTimeVisits() {
        return timeVisits;
    }

    public void setTimeVisits(List<Map<String, String>> timeVisits) {
        this.timeVisits = timeVisits;
    }

    public List<Map<String, String>> getTimeAvailable() {
        return timeAvailable;
    }

    public void setTimeAvailable(List<Map<String, String>> time_available) {
        this.timeAvailable = time_available;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    private ExpertDao dao;
    private synchronized ExpertDao getDao(){
        if(dao==null){
            dao = new ExpertDao();
        }
        return dao;
    }
    public void getExpertList(IListDataRetrieveListener<Expert> listener,boolean force){
        Map<String,Object> param = new HashMap<String,Object>();
        getDao().list(listener, param, force);
    }
    public void nextPage(IListDataRetrieveListener<Expert> listener){
        getDao().nextPage(listener);
    }
    public boolean canLoadMore(){
        return getDao().canLoadMore();
    }
    public static Expert getExpertFromCache(String id){
        return new ExpertDao(id).getFromListCache();
    }
    public static Expert getExpert(String id){
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("id",id);
        try {
            return new ExpertDao().get(param);
        } catch (DaoException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static void getExpertAsync(String id,IDataRetrieveListener<Expert> listener,boolean force){
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("id",id);
        new ExpertDao().get(listener,param,force);
    }
}
