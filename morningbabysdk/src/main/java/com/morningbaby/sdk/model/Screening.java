package com.morningbaby.sdk.model;

import android.text.TextUtils;
import com.morningbaby.sdk.dao.ScreeningDao;
import com.nd.smartcan.commons.util.language.Json2Std;
import com.nd.smartcan.commons.util.logger.Logger;

import java.util.*;

/**
 * 筛查
 * Created by carl on 15/8/3.
 */
public class Screening {

    private String uid;
    private boolean hasScreening=false;
    private boolean hasLoadData=false;
    private Map<Integer,Integer> screening=new HashMap<Integer, Integer>();

    private ScreeningDao mDao=null;

    public Screening(String uid){
        this.uid = uid;
    }
    public boolean getResult(){
        return isInRisk() ;
    }
    public void setScreening(int seq, int choice){
        this.screening.put(seq,choice);
    }

    /**
     * 取得筛查题目的回答
     * <br>0 代表未选择
     *
     * @param seq 题号
     * @return
     */
    public int getScreening(int seq){
        Integer res = this.screening.get(seq);
        if(res==null){
            return 0;
        }
        return res.intValue();
    }

    private synchronized ScreeningDao getDao(){
        if(mDao==null){
            mDao = new ScreeningDao(this.uid);
        }
        return mDao;
    }

    /**
     * 提交筛查数据
     * @return
     */
    public boolean post(){
        Map<String,Object> data = new HashMap<String, Object>();
        Set<Integer> set = this.screening.keySet();
        Iterator<Integer> it = set.iterator();
        List<Map<String,Integer>> list = new ArrayList<Map<String,Integer>>();
        while(it.hasNext()){
            Map<String,Integer> item = new HashMap<String, Integer>();
            int key = it.next().intValue();
            item.put("question",key);
            item.put("choice",this.screening.get(key));
            list.add(item);
        }
        data.put("uid",this.uid);
        data.put("result",isInRisk());
        data.put("detail",list);
        return getDao().submit(data);
    }

    /**
     * 加载筛查数据
     */
    public void loadScreening(){
        if(hasLoadData){
            return;
        }
        String data = getDao().getResult();
        Logger.i(getClass(),"loadScreening data:"+data);
        if(TextUtils.isEmpty(data)){
            return ;
        }
        hasLoadData=true;
        Json2Std json = new Json2Std(data);
        Map<String,Object> res = json.getResultMap();
        List<Object> list = (List<Object>) res.get("detail");
        Iterator iter = list.iterator();
        Map<String,Integer> item;
        while(iter.hasNext()){
            item = (Map<String,Integer>) iter.next();
            Integer question = item.get("question");
            Integer choice = item.get("choice");
            setScreening(question,choice);
        }
        hasScreening=true;
    }

    /**
     * 是否已经筛查过
     * @return
     */
    public boolean hasScreening(){
        loadScreening();
        return hasScreening;
    }

    /**
     * 是否高危
     * @return
     */
    public boolean isInRisk(){
        if(!hasScreening()){
            return false;
        }
        if(getScreening(1)<=2){
            return true;
        }
        if(getScreening(2)==1){
            return true;
        }
        if(getScreening(3)>=3 ){
            return true;
        }
        if(getScreening(4)==3){
            return true;
        }
        if(getScreening(5)==2){
            return true;
        }
        if(getScreening(6)>=3){
            return true;
        }
        if(getScreening(7)>=2){
            return true;
        }
        if(getScreening(8)>=2){
            return true;
        }
        return false;
    }
}
