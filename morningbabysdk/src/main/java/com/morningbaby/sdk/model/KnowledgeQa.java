package com.morningbaby.sdk.model;

import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.morningbaby.sdk.dao.KnowledgeQaDao;
import com.nd.smartcan.frame.dao.CacheDefine.IDataRetrieveListener;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;
import com.nd.smartcan.frame.exception.DaoException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by carl on 15/8/4.
 */
public class KnowledgeQa {
    @JsonProperty("id")
    private String id;

    @JsonProperty("topic_id")
    private String topicId;

    @JsonProperty("expert_id")
    private String expertId;

    @JsonProperty("title")
    private String title;

    @JsonProperty("tag")
    private String tag;

    @JsonProperty("question")
    private String question;

    @JsonProperty("answer")
    private String answer;
    public KnowledgeQa() {
    }

    public KnowledgeQa(String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicid) {
        this.topicId = topicid;
    }

    public String getExpertId() {
        return expertId;
    }

    public void setExpertId(String expertId) {
        this.expertId = expertId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    private KnowledgeQaDao dao;
    private synchronized KnowledgeQaDao getDao(){
        if(dao==null){
            dao = new KnowledgeQaDao();
        }
        return dao;
    }
    public void getQaList(String topic,String filter,IListDataRetrieveListener<KnowledgeQa> listener,boolean force){
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("topic",topic);
        try {
            if(!TextUtils.isEmpty(filter)) {
                param.put("filter", URLEncoder.encode(filter, "UTF-8"));
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        getDao().list(listener, param, force);
    }
    public void nextPage(IListDataRetrieveListener<KnowledgeQa> listener){
        getDao().nextPage(listener);
    }
    public boolean canLoadMore(){
        return getDao().canLoadMore();
    }
    public static KnowledgeQa getQaFromCache(String id){
        return new KnowledgeQaDao(id).getFromListCache();
    }
    public static KnowledgeQa getQa(String id){
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("id",id);
        try {
            return new KnowledgeQaDao().get(param);
        } catch (DaoException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static void getQaAsync(String id,IDataRetrieveListener<KnowledgeQa> listener,boolean force){
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("id",id);
        new KnowledgeQaDao().get(listener,param,force);
    }
}
