package com.morningbaby.sdk.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.morningbaby.sdk.dao.DepartmentDao;
import com.nd.smartcan.frame.dao.CacheDefine.IDataRetrieveListener;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;
import com.nd.smartcan.frame.exception.DaoException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by carl on 15/8/4.
 */
public class Department {

    @JsonProperty("id")
    private String id;
    @JsonProperty("hospitalId")
    private String hospitalId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("introduction")
    private String introduction;
    public Department() {
    }

    public Department(String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }
    private DepartmentDao dao;
    private synchronized DepartmentDao getDao(){
        if(dao==null){
            dao = new DepartmentDao();
        }
        return dao;
    }
    public void getDepartmentList(String hostpitalId,IListDataRetrieveListener<Department> listener,boolean force){
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("hospital",hostpitalId);
        getDao().list(listener, param, force);
    }
    public void nextPage(IListDataRetrieveListener<Department> listener){
        getDao().nextPage(listener);
    }
    public boolean canLoadMore(){
        return getDao().canLoadMore();
    }
    public static Department getDepartmentFromCache(String id){
        return new DepartmentDao(id).getFromListCache();
    }
    public static Department getDepartment(String id){
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("id",id);
        try {
            return new DepartmentDao().get(param);
        } catch (DaoException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static void getDepartmentAsync(String id,IDataRetrieveListener<Department> listener,boolean force){
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("id",id);
        new DepartmentDao().get(listener,param,force);
    }
}
