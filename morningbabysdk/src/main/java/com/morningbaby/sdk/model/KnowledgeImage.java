package com.morningbaby.sdk.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by carl on 15/8/2.
 */
public class KnowledgeImage {

    @JsonProperty("id")
    private String id;

    @JsonProperty("location")
    private String location;

    public KnowledgeImage(){

    }
    public KnowledgeImage(String id){
        this.id = id;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
