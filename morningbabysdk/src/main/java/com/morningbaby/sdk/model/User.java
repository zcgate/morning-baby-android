package com.morningbaby.sdk.model;

/**
 * Created by carl on 15/8/2.
 */
import android.graphics.Bitmap;
import android.text.TextUtils;

import com.fasterxml.jackson.annotation.*;
import com.morningbaby.sdk.dao.UserDao;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.commons.util.security.MD5;
import com.nd.smartcan.core.restful.ClientResourceUtils;
import com.nd.smartcan.core.restful.ResourceException;
import com.nd.smartcan.frame.exception.DaoException;
import java.io.Serializable;
import java.util.Map;

/**
 * 用户实体类
 * Created by wanghl on 2014/12/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public final class User implements Serializable {


    @JsonIgnore
    private static User currentUser = null;

    @JsonIgnore
    private String uid;
    @JsonIgnore
    private String loginedUid;

    @JsonIgnore
    private String session;
    @JsonIgnore
    private Screening screening;

    @JsonIgnore
    private UserDao mDao = null;

    @JsonProperty("nick")
    private String nick;

    @JsonProperty("email")
    private String email;

    @JsonProperty("gender")
    private String gender;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("address_province")
    private String addressProvince;

    @JsonProperty("address_city")
    private String addressCity;

    @JsonProperty("address_distinct")
    private String addressDistinct;

    @JsonProperty("address_detail")
    private String addressDetail;

    @JsonProperty("baby_name")
    private String babyName;

    @JsonProperty("baby_born_pdd")
    private String babyBornPdd;

    @JsonProperty("baby_born_week")
    private int babyBornWeek;

    @JsonProperty("baby_gender")
    private String babyGender;

    @JsonProperty("baby_premature_reason")
    private String babyPrematureReason;

    @JsonProperty("baby_born_weight")
    private float babyBornWeight;

    @JsonProperty("baby_born_length")
    private float babyBornLength;

    @JsonProperty("baby_born_head")
    private float babyBornHead;

    @JsonProperty("baby_born_date")
    private String babyBornDate;

    @JsonProperty("baby_born_hospital")
    private String babyBornHospital;

    @JsonProperty("baby_apgar_score_one")
    private int babyApgarScoreOne;

    @JsonProperty("baby_apgar_score_five")
    private int babyApgarScoreFive;

    @JsonProperty("baby_apgar_score_ten")
    private int babyApgarScoreTen;


    @JsonProperty("picture")
    private String picture;

    public User() {
    }

    public User(String uid) {
        this.uid = uid;
    }

    /**
     * 取得当前登入的用户
     *
     * @return
     */
    public static User getCurrentUser() {
        if (currentUser == null) {
            synchronized (User.class) {
                currentUser = new User();
                currentUser.loadCurrentUser();
                return currentUser;
            }
        }
        return currentUser;
    }

    /**
     * 载入用户数据
     */
    public void loadUserData(){
        if (TextUtils.isEmpty(getUid())) {
            Logger.w(getClass(),"No uid found for this user , can not load data");
            return ;
        }
        try {
            User usrdata = getDao(true).get(getUid());
            //Logger.w(getClass(),"===TEST=== user baby name="+usrdata.getBabyName()+"score one"+usrdata.getBabyApgarScoreOne()+" score five="+usrdata.getBabyApgarScoreFive());
            copyUserData(usrdata);
        } catch (DaoException e) {
            e.printStackTrace();
        }

    }

    public Screening getScreening(){
        if(this.screening==null){
            //Logger.i(getClass(),"get screening with uid="+this.getUid());
            this.screening = new Screening(this.getUid());
        }
        return this.screening;
    }
    /**
     * 获取用户uid
     *
     * @return 用户uid
     */
    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        uid = uid;
    }

    /**
     * 获取用户昵称
     */
    public String getNick() {
        return nick;
    }

    /**
     * 设置用户昵称
     */
    public void setNick(String nickName) {
        this.nick = nickName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddressProvince() {
        return addressProvince;
    }

    public void setAddressProvince(String addressProvince) {
        this.addressProvince = addressProvince;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public String getAddressDistinct() {
        return addressDistinct;
    }

    public void setAddressDistinct(String addressDistinct) {
        this.addressDistinct = addressDistinct;
    }

    public String getAddressDetail() {
        return addressDetail;
    }

    public void setAddressDetail(String addressDetail) {
        this.addressDetail = addressDetail;
    }

    public String getBabyName() {
        return babyName;
    }

    public void setBabyName(String babyName) {
        this.babyName = babyName;
    }

    public String getBabyBornPdd() {
        return babyBornPdd;
    }

    public void setBabyBornPdd(String babyBornPdd) {
        this.babyBornPdd = babyBornPdd;
    }

    public int getBabyBornWeek() {
        return babyBornWeek;
    }

    public void setBabyBornWeek(int babyBornWeek) {
        this.babyBornWeek = babyBornWeek;
    }

    public String getBabyGender() {
        return babyGender;
    }

    public void setBabyGender(String babyGender) {
        this.babyGender = babyGender;
    }

    public String getBabyPrematureReason() {
        return babyPrematureReason;
    }

    public void setBabyPrematureReason(String babyPrematureReason) {
        this.babyPrematureReason = babyPrematureReason;
    }

    public float getBabyBornWeight() {
        return babyBornWeight;
    }

    public void setBabyBornWeight(float babyBornWeight) {
        this.babyBornWeight = babyBornWeight;
    }

    public float getBabyBornLength() {
        return babyBornLength;
    }

    public void setBabyBornLength(float babyBornLength) {
        this.babyBornLength = babyBornLength;
    }

    public float getBabyBornHead() {
        return babyBornHead;
    }

    public void setBabyBornHead(float babyBornHead) {
        this.babyBornHead = babyBornHead;
    }

    public String getBabyBornDate() {
        return babyBornDate;
    }

    public void setBabyBornDate(String babyBornDate) {
        this.babyBornDate = babyBornDate;
    }

    public String getBabyBornHospital() {
        return babyBornHospital;
    }

    public void setBabyBornHospital(String babyBornHospital) {
        this.babyBornHospital = babyBornHospital;
    }

    public int getBabyApgarScoreOne() {
        return babyApgarScoreOne;
    }

    public void setBabyApgarScoreOne(int babyApgarScoreOne) {
        this.babyApgarScoreOne = babyApgarScoreOne;
    }

    public int getBabyApgarScoreFive() {
        return babyApgarScoreFive;
    }

    public void setBabyApgarScoreFive(int babyApgarScoreFive) {
        this.babyApgarScoreFive = babyApgarScoreFive;
    }

    public int getBabyApgarScoreTen() {
        return babyApgarScoreTen;
    }

    public void setBabyApgarScoreTen(int babyApgarScoreTen) {
        this.babyApgarScoreTen = babyApgarScoreTen;
    }

    public String getPicture() {
        return picture;
    }
    public void setPicture(String url){
        this.picture = url;
    }

    @JsonIgnore
    public boolean setAvatar(Bitmap bmp){
        String url = getDao().uploadAvata(bmp);
        if(!TextUtils.isEmpty(url)) {
            loadUserData();
            this.picture = url;
            return saveUpdate();
        }
        return false;
    }

    private synchronized UserDao getDao() {
        return getDao(false) ;
    }
    private synchronized UserDao getDao(boolean renew) {
        if (mDao == null || renew)  {
            mDao = new UserDao();
        }
        return mDao;
    }

    /**
     * 登入
     *
     * @param account email/mobile phone
     * @param pwd
     * @return
     */
    public boolean login(String account, String pwd) {
        Logger.i(getClass(),"login account="+account+" pwd="+encryptPwd(pwd));
        ReturnObject ret = getDao().login(account, encryptPwd(pwd));
        if(ret.success()){
            loadCurrentUser();
        }
        return ret.success();
    }

    /**
     * 登出
     * @return
     */
    public boolean logout() {
        getDao().logout(uid, session);
        uid="";
        loginedUid="";
        return true;
    }

    private String encryptPwd(String pwd) {
        return MD5.getMD5(pwd);
    }

    /**
     * 注册
     *
     * @param phone
     * @param email
     * @param pwd
     * @return
     */
    public ReturnObject register(String phone, String email, String pwd) {
        return getDao().register(phone, email, encryptPwd(pwd));
    }


    /**
     * 保存用户资料
     *
     * @return
     */
    public boolean saveUpdate() {
        return getDao().update(this);
        /*
        try {
            ReturnObject ret = getDao().post(this,null,ReturnObject.class);
            return ret.success();
        } catch (DaoException e) {
            e.printStackTrace();
            return false;
        }
        */
    }


    /**
     * 是否已经登入
     * @return
     */
    @JsonIgnore
    public boolean isLogin() {
        return !TextUtils.isEmpty(loginedUid);
    }
    private void loadCurrentUser(){
        session = UserDao.getLoginedSession();
        uid = UserDao.getLoginedUid();
        loginedUid = UserDao.getLoginedUid();
    }

    /**
     * 是否需要补全宝宝资料
     * @return
     */
    public boolean needCompleteInfo(){
        Logger.i(getClass()," score one="+this.babyApgarScoreOne+ " score five"+this.babyApgarScoreFive+" ten="+this.babyApgarScoreTen);
        return TextUtils.isEmpty(this.babyName) || TextUtils.isEmpty(this.babyBornDate) ;
    }
    public boolean testReturnObj() {
        String str = "{\n" +
                "\t\t\"result\":\"success\",\n" +
                "\t\t\"detail\":{\n" +
                "\t\t\"uid\" : \"user id\",\n" +
                "\t\t\"session_id\": \"login session id\"\n" +
                "\t\t}\n" +
                "    }";
        ReturnObject obj = null;
        try {
            obj = ClientResourceUtils.stringToObj(str, ReturnObject.class);
            Map<String, Object> res = obj.getDetail();

            Logger.i(getClass(), "uid=" + res.get("uid"));
            Logger.i(getClass(), "sessionid" + res.get("session_id"));
        } catch (ResourceException e) {
            e.printStackTrace();
        }
        return true;
    }

    private void copyUserData(User usr) {
        //this.uid = usr.getUid();
        this.nick = usr.getNick();
        this.email = usr.getEmail();
        this.gender = usr.getGender();
        this.phone = usr.getPhone();
        this.addressProvince = usr.getAddressProvince();
        this.addressCity = usr.getAddressCity();
        this.addressDistinct = usr.getAddressDistinct();
        this.addressDetail = usr.getAddressDetail();
        this.babyName = usr.getBabyName();
        this.babyBornPdd = usr.getBabyBornPdd();
        this.babyBornWeek = usr.getBabyBornWeek();
        this.babyGender = usr.getBabyGender();
        this.babyPrematureReason = usr.getBabyPrematureReason();
        this.babyBornWeight = usr.getBabyBornWeight();
        this.babyBornLength = usr.getBabyBornLength();
        this.babyBornHead = usr.getBabyBornHead();
        this.babyBornDate = usr.getBabyBornDate();
        this.babyBornHospital = usr.getBabyBornHospital();
        this.babyApgarScoreOne = usr.getBabyApgarScoreOne();
        this.babyApgarScoreFive = usr.getBabyApgarScoreFive();
        this.babyApgarScoreTen = usr.getBabyApgarScoreTen();
        this.picture = usr.getPicture();
    }
}
