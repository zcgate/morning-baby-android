package com.morningbaby.sdk.model;

import android.os.Handler;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.morningbaby.sdk.dao.KnowledgeTopicDao;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;

import java.util.List;
import java.util.Map;


/**
 * Created by carl on 15/8/2.
 */
public class KnowledgeTopic {

    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("description")
    private String description;

    public KnowledgeTopic(){

    }

    public KnowledgeTopic(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private KnowledgeTopicDao dao;
    private synchronized KnowledgeTopicDao getDao(){
        if(dao==null){
            dao = new KnowledgeTopicDao();
        }
        return dao;
    }
    public void getTopicList(IListDataRetrieveListener<KnowledgeTopic> listener,boolean force){
        getDao().list(listener, null, force);
    }
    public void nextPage(IListDataRetrieveListener<KnowledgeTopic> listener){
        getDao().nextPage(listener);
    }
    public boolean canLoadMore(){
        return getDao().canLoadMore();
    }
    public static KnowledgeTopic getTopicFromCache(String id){
        return new KnowledgeTopicDao(id).getFromListCache();
    }
}
