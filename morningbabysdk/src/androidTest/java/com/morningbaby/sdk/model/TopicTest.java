package com.morningbaby.sdk.model;

import android.os.Handler;
import android.test.AndroidTestCase;

import com.morningbaby.sdk.Constant;
import com.morningbaby.sdk.SdkManager;
import com.morningbaby.sdk.Tester;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/**
 * Created by carl on 15/8/5.
 */
public class TopicTest extends AndroidTestCase{
    private List<String> keys = new ArrayList<String>();
    private int count=0;
    public void testGetTopicList() throws Exception{
        Tester.init();
        class KnowledgeTopicListener implements IListDataRetrieveListener<KnowledgeTopic> {
            private CountDownLatch counter;
            public void setCounter(CountDownLatch counter){
                this.counter=counter;
            }
            @Override
            public void onCacheDataRetrieve(List<KnowledgeTopic> knowledgeTopics, Map<String, Object> stringObjectMap, boolean b) {
                Logger.i(getClass(),"===TEST=== retrieve topic list from cache.");
                printTopics(knowledgeTopics);
                assertTrue(knowledgeTopics.size()>0);
            }

            @Override
            public void onServerDataRetrieve(List<KnowledgeTopic> knowledgeTopics, Map<String, Object> stringObjectMap) {
                Logger.i(getClass(),"===TEST=== retrieve topic list from server.");
                printTopics(knowledgeTopics);
                assertTrue(knowledgeTopics.size()>0);
            }

            @Override
            public void done() {
                counter.countDown();
            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
                Logger.i(getClass(),"===TEST=== retrieve topic list exception");
                assertTrue(false);
                counter.countDown();
            }

            @Override
            public Handler getCallBackLooperHandler() {
                return null;
            }
        };
        CountDownLatch counter = new CountDownLatch(1);
        KnowledgeTopicListener listener = new KnowledgeTopicListener();
        listener.setCounter(counter);
        KnowledgeTopic topic = new KnowledgeTopic();
        topic.getTopicList(listener,true);
        counter.await();
        if(!topic.canLoadMore()) {
            Logger.i(getClass(),"===TEST=== Can't load more topic");
            return;
        }
        Logger.i(getClass(),"===TEST=== load more topic");
        counter = new CountDownLatch(1);
        listener.setCounter(counter);
        topic.nextPage(listener);
        counter.await();
    }
    private void printTopics(List<KnowledgeTopic> list){
        Iterator iter = list.iterator();
        int i=0;
        Logger.i(getClass(),"===TEST=== count="+count );
        while(iter.hasNext()){
            KnowledgeTopic topic = (KnowledgeTopic) iter.next();
            Logger.i(getClass(),"===TEST=== id="+topic.getId()+" name="+topic.getName());
            // 翻页时，id 不重
            if(count>=1){
                Logger.i(getClass(),"===TEST=== i="+i+ "pagesize="+Constant.PageSize);
                if(i++>=50) {
                    if (!SdkManager.getInstance().getBaseUrl().equalsIgnoreCase(Constant.MockUrl)) {
                        Logger.i(getClass(),"===TEST=== i="+i+ "pagesize="+Constant.PageSize+ " topic="+topic.getId());
                        assertTrue(!keys.contains(topic.getId()));
                    }
                }
                continue;
            }
            keys.add(topic.getId());
        }
        count++;
    }
}
