package com.morningbaby.sdk.model;

import android.test.AndroidTestCase;
import com.morningbaby.sdk.Tester;
import com.nd.smartcan.commons.util.logger.Logger;

import java.util.Random;

/**
 * Created by carl on 15/8/5.
 */
public class ScreeningTest extends AndroidTestCase{

    public void testPostScreening(){
        Tester.init();

        Random rand = new Random();
        int rnd= rand.nextInt(4)+1;

        Screening screening = User.getCurrentUser().getScreening();
        screening.setScreening(1,1);
        screening.setScreening(2,2);
        screening.setScreening(3,2);
        screening.setScreening(4,4);
        screening.setScreening(5,2);
        screening.setScreening(6,2);
        screening.setScreening(7,2);
        screening.setScreening(8,2);
        screening.setScreening(9,2);
        screening.setScreening(10, rnd); // random data
        // submit data success
        assertTrue(screening.post());

        // already screening
        assertTrue(screening.hasScreening());

        // test get data
        screening.loadScreening();
        //Logger.i(getClass(),"===TEST=== ans for 10="+rnd+ " from server="+screening.getScreening(10));
        assertTrue(screening.getScreening(10) == rnd);

    }
}
