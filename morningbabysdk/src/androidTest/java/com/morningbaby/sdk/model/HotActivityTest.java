package com.morningbaby.sdk.model;

import android.os.Handler;
import android.test.AndroidTestCase;

import com.morningbaby.sdk.Constant;
import com.morningbaby.sdk.SdkManager;
import com.morningbaby.sdk.Tester;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.frame.dao.CacheDefine.IDataRetrieveListener;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/**
 * Created by carl on 15/8/6.
 */
public class HotActivityTest extends AndroidTestCase {
    private List<String> keys = new ArrayList<String>();
    private int count=0;
    public void testHotActivityList() throws Exception{
        Tester.init();
        class HotActivityListener implements IListDataRetrieveListener<HotActivity> {

            private CountDownLatch counter;

            public void setCounter(CountDownLatch counter){
                this.counter=counter;
            }
            @Override
            public void onCacheDataRetrieve(List<HotActivity> HotActivitys, Map<String, Object> stringObjectMap, boolean b) {
                Logger.i(getClass(), "===TEST=== retrieve HotActivity list from cache.");
                printHotActivitys(HotActivitys);
                assertTrue(HotActivitys.size()>0);
                getHotActivityDetail(HotActivitys.get(0).getId());
            }

            @Override
            public void onServerDataRetrieve(List<HotActivity> HotActivitys, Map<String, Object> stringObjectMap) {
                Logger.i(getClass(),"===TEST=== retrieve HotActivity list from server.");
                printHotActivitys(HotActivitys);
                assertTrue(HotActivitys.size()>0);
                getHotActivityDetail(HotActivitys.get(0).getId());
            }

            @Override
            public void done() {
                counter.countDown();
            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
                Logger.i(getClass(),"===TEST=== retrieve HotActivity list exception");
                assertTrue(false);
                counter.countDown();
            }

            @Override
            public Handler getCallBackLooperHandler() {
                return null;
            }
        };

        CountDownLatch counter = new CountDownLatch(1);
        HotActivityListener listener = new HotActivityListener() ;
        listener.setCounter(counter);

        HotActivity activity = new HotActivity();
        activity.getHotActivityList(listener,3,true);
        counter.await();

        if(!activity.canLoadMore()) {
            Logger.i(getClass(),"===TEST=== Can't load more activity");
            return;
        }
        Logger.i(getClass(),"===TEST=== load more activity");
        counter = new CountDownLatch(1);
        listener.setCounter(counter);
        activity.nextPage(listener);
        counter.await();

    }
    private void printHotActivitys(List<HotActivity> list){
        Iterator iter = list.iterator();
        int i=0;
        while(iter.hasNext()){
            HotActivity activity = (HotActivity) iter.next();
            Logger.i(getClass(), "===TEST=== id=" + activity.getId() + " title=" + activity.getTitle());
            // 翻页时，id 不重
            if(count>=1){
                if(i++>=Constant.PageSize) {
                    if (!SdkManager.getInstance().getBaseUrl().equalsIgnoreCase(Constant.MockUrl)) {
                        assertTrue(!keys.contains(activity.getId()));
                    }
                }
                continue;
            }
            keys.add(activity.getId());
        }
    }
    public void getHotActivityDetail(String HotActivityId) {
        final CountDownLatch counter = new CountDownLatch(1);
        IDataRetrieveListener<HotActivity> listener = new IDataRetrieveListener<HotActivity>() {
            @Override
            public void onCacheDataRetrieve(HotActivity HotActivity, boolean b) {
                Logger.i(getClass(),"===TEST=== retrieve HotActivity detail from cache.");
                List<Map<String,Integer>> timeList = HotActivity.getActivityTime();
                if(timeList.size()>0) {
                    Map<String,Integer> time = timeList.get(0);
                    Logger.i(getClass(), "===TEST=== activity time from=" + time.get("from")+" to="+time.get("to"));
                }
                Logger.i(getClass(),"===TEST=== HotActivity id="+HotActivity.getId() + "HotActivity name="+HotActivity.getTitle()+ "HotActivity phone="+HotActivity.getContactPhone());
                assertTrue(HotActivity.getId().length()>0);
            }

            @Override
            public void onServerDataRetrieve(HotActivity HotActivity) {
                Logger.i(getClass(),"===TEST=== retrieve HotActivity detail from server.");
                List<Map<String,Integer>> timeList = HotActivity.getActivityTime();
                if(timeList.size()>0) {
                    Map<String,Integer> time = timeList.get(0);
                    Logger.i(getClass(), "===TEST=== activity time from=" + time.get("from")+" to="+time.get("to"));
                }
                Logger.i(getClass(),"===TEST=== HotActivity id="+HotActivity.getId() + "HotActivity name="+HotActivity.getTitle()+ "HotActivity phone="+HotActivity.getContactPhone());
                assertTrue(HotActivity.getId().length()>0);
            }

            @Override
            public void done() {
                counter.countDown();
            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
                Logger.i(getClass(),"===TEST=== retrieve HotActivity detail exception");
                assertTrue(false);
                counter.countDown();
            }

            @Override
            public Handler getCallBackLooperHandler() {
                return null;
            }
        };
        HotActivity.getHotActivityAsync(HotActivityId,listener,false);
        try {
            counter.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
