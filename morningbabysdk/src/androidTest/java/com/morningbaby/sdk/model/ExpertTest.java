package com.morningbaby.sdk.model;

import android.os.Handler;
import android.test.AndroidTestCase;

import com.morningbaby.sdk.Constant;
import com.morningbaby.sdk.SdkManager;
import com.morningbaby.sdk.Tester;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.frame.dao.CacheDefine.IDataRetrieveListener;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/**
 * Created by carl on 15/8/6.
 */
public class ExpertTest extends AndroidTestCase {
    private static final String hospitalId="1";
    private static final String departmentId="111";
    private List<String> keys = new ArrayList<String>();
    int count=0;
    public void testExpertList() throws Exception{
        Tester.init();
        class ExpertListener implements IListDataRetrieveListener<Expert> {

            private CountDownLatch counter;
            public void setCounter(CountDownLatch counter){
                this.counter = counter;
            }
            @Override
            public void onCacheDataRetrieve(List<Expert> Experts, Map<String, Object> stringObjectMap, boolean b) {
                Logger.i(getClass(), "===TEST=== retrieve Expert list from cache.");
                printExperts(Experts);
                assertTrue(Experts.size()>0);
                getExpertDetail(Experts.get(0).getId());
            }

            @Override
            public void onServerDataRetrieve(List<Expert> Experts, Map<String, Object> stringObjectMap) {
                Logger.i(getClass(),"===TEST=== retrieve Expert list from server.");
                printExperts(Experts);
                assertTrue(Experts.size()>0);
                getExpertDetail(Experts.get(0).getId());
            }

            @Override
            public void done() {
                counter.countDown();
            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
                Logger.i(getClass(),"===TEST=== retrieve Expert list exception");
                assertTrue(false);
                counter.countDown();
            }

            @Override
            public Handler getCallBackLooperHandler() {
                return null;
            }
        };
        CountDownLatch counter = new CountDownLatch(1);
        ExpertListener listener = new ExpertListener() ;
        listener.setCounter(counter);

        Expert expert = new Expert();
        expert.getExpertList(listener, true);
        counter.await();

        if(!expert.canLoadMore()) {
            Logger.i(getClass(),"===TEST=== Can't load more expert");
            return;
        }
        Logger.i(getClass(),"===TEST=== load more expert");
        counter = new CountDownLatch(1);
        listener.setCounter(counter);
        expert.nextPage(listener);
        counter.await();
    }
    private void printExperts(List<Expert> list){
        Iterator iter = list.iterator();
        int i=0;
        while(iter.hasNext()){
            Expert expert = (Expert) iter.next();
            Logger.i(getClass(), "===TEST=== id=" + expert.getId() + " title=" + expert.getName());
            // 翻页时，id 不重
            if(count>=1){
                if(i++>=Constant.PageSize) {
                    if (!SdkManager.getInstance().getBaseUrl().equalsIgnoreCase(Constant.MockUrl)) {
                        assertTrue(!keys.contains(expert.getId()));
                    }
                }
                continue;
            }
            keys.add(expert.getId());
        }
        count++;
    }
    public void getExpertDetail(String ExpertId) {
        final CountDownLatch counter = new CountDownLatch(1);
        IDataRetrieveListener<Expert> listener = new IDataRetrieveListener<Expert>() {
            @Override
            public void onCacheDataRetrieve(Expert Expert, boolean b) {
                Logger.i(getClass(),"===TEST=== retrieve Expert detail from cache.");
                Logger.i(getClass(),"===TEST=== Expert id="+Expert.getId() + "Expert name="+Expert.getName()+ "Expert city="+Expert.getTitle());
                assertTrue(Expert.getId().length()>0);
            }

            @Override
            public void onServerDataRetrieve(Expert Expert) {
                Logger.i(getClass(),"===TEST=== retrieve Expert detail from server.");
                Logger.i(getClass(),"===TEST=== Expert id="+Expert.getId() + "Expert name="+Expert.getName()+ "Expert city="+Expert.getTitle());
                assertTrue(Expert.getId().length()>0);
            }

            @Override
            public void done() {
                counter.countDown();
            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
                Logger.i(getClass(),"===TEST=== retrieve Expert detail exception");
                assertTrue(false);
                counter.countDown();
            }

            @Override
            public Handler getCallBackLooperHandler() {
                return null;
            }
        };
        Expert.getExpertAsync(ExpertId,listener,false);
        try {
            counter.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
