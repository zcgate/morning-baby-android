package com.morningbaby.sdk.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.test.AndroidTestCase;

import com.morningbaby.sdk.R;
import com.morningbaby.sdk.Tester;
import junit.framework.Assert;
//import junit.framework.TestCase;
import junit.framework.*;
//import org.junit.Test;

import com.morningbaby.sdk.model.User;
import com.nd.smartcan.commons.util.logger.Logger;

import java.util.Random;

/**
 * Created by carl on 15/8/3.
 */
public class UserTest extends AndroidTestCase {

    /*
    public void testRegister(){
        Tester.init(false);
        Logger.i(getClass(),"testRegister phone="+Tester.phone);
        ReturnObject ret = User.getCurrentUser().register(Tester.phone,Tester.email,Tester.pwd);
        assertTrue(ret.success());

        assertTrue(User.getCurrentUser().login(Tester.phone,Tester.pwd));
        assertTrue(User.getCurrentUser().needCompleteInfo());

    }
    public void testLogin(){
        Tester.init(false);
        // 手机登入
        assertTrue(User.getCurrentUser().login(Tester.phone, Tester.pwd));
        // email登入
        assertTrue(User.getCurrentUser().login(Tester.email,Tester.pwd));
    }
    */

    public void testUpdate(){
        Tester.init(true);
        //1. 随机产生字段 value
        Random rand = new Random();
        String babyName = "BabyName"+rand.nextInt(100);
        float babyWeight = rand.nextFloat()*10;
        float babyLength = rand.nextFloat()*10;
        //2. 修改
        User usr = User.getCurrentUser();
        usr.loadUserData();
        usr.setBabyName(babyName);
        usr.setBabyBornWeight(babyWeight);
        usr.setBabyBornLength(babyLength);
        //3. commit
        usr.saveUpdate();

        //4. load data

        usr.loadUserData();
        //5. test
        Logger.i(getClass(),"===TEST=== babyName="+babyName + " name from server="+usr.getBabyName());
        assertTrue(babyName.equals(usr.getBabyName()));
        assertTrue(babyWeight == usr.getBabyBornWeight());
        assertTrue(babyLength == usr.getBabyBornLength());
    }
    /*
    public void testChangeAvatar(){
        Tester.init(true);
        Bitmap bmp = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.unittest_avatar);
        User user = User.getCurrentUser();
        user.loadUserData();
        String pict = user.getPicture();
        assertTrue(user.setAvatar(bmp));
        assertNotNull(user.getPicture());
        Logger.i(getClass(),"===TEST=== old pict="+pict+ " new pict="+user.getPicture());
        assertTrue(!user.getPicture().equalsIgnoreCase(pict));

    }
    */
    /*
    public void testObj(){
        assertTrue(new User().testReturnObj());
    }
    */
}
