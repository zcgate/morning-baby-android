package com.morningbaby.sdk.model;

import android.os.Handler;
import android.test.AndroidTestCase;

import com.morningbaby.sdk.Constant;
import com.morningbaby.sdk.SdkManager;
import com.morningbaby.sdk.Tester;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.frame.dao.CacheDefine.IDataRetrieveListener;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/**
 * Created by carl on 15/8/5.
 */
public class QATest extends AndroidTestCase {
    //private final String TOPIC_ID="23232-54312";
    //private final String TOPIC_ID="20fb40f3-f865-4519-ae3e-ed2541a8f7d4";
    private final String TOPIC_ID="9d691e45-765c-4c58-af23-e996d75ed37c";
    private List<String> keys = new ArrayList<String>();
    private int count=0;
    public void testQaList() throws Exception{
        Tester.init();
        class KnowledgeQaListener implements IListDataRetrieveListener<KnowledgeQa> {

            private CountDownLatch counter;

            public void setCounter(CountDownLatch counter){
                this.counter=counter;
            }
            @Override
            public void onCacheDataRetrieve(List<KnowledgeQa> knowledgeQas, Map<String, Object> stringObjectMap, boolean b) {
                Logger.i(getClass(), "===TEST=== retrieve Qa list from cache.");
                printQas(knowledgeQas);
                assertTrue(knowledgeQas.size()>0);
                getQaDetail(knowledgeQas.get(0).getId());
            }

            @Override
            public void onServerDataRetrieve(List<KnowledgeQa> knowledgeQas, Map<String, Object> stringObjectMap) {
                Logger.i(getClass(),"===TEST=== retrieve Qa list from server.");
                printQas(knowledgeQas);
                assertTrue(knowledgeQas.size()>0);
                getQaDetail(knowledgeQas.get(0).getId());
            }

            @Override
            public void done() {
                counter.countDown();
            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
                Logger.i(getClass(),"===TEST=== retrieve Qa list exception");
                assertTrue(false);
                counter.countDown();
            }

            @Override
            public Handler getCallBackLooperHandler() {
                return null;
            }
        };

        CountDownLatch counter = new CountDownLatch(1);
        KnowledgeQaListener listener= new KnowledgeQaListener();
        listener.setCounter(counter);

        KnowledgeQa qa = new KnowledgeQa();
        qa.getQaList(TOPIC_ID, "", listener, true);
        counter.await();

        if(!qa.canLoadMore()) {
            Logger.i(getClass(),"===TEST=== Can't load more qa");
            return;
        }
        Logger.i(getClass(),"===TEST=== load more qa");
        counter = new CountDownLatch(1);
        listener.setCounter(counter);
        qa.nextPage(listener);
        counter.await();
    }
    private void printQas(List<KnowledgeQa> list){
        Iterator iter = list.iterator();
        int i=0;
        while(iter.hasNext()){
            KnowledgeQa Qa = (KnowledgeQa) iter.next();
            Logger.i(getClass(), "===TEST=== id=" + Qa.getId() + " title=" + Qa.getTitle());
            // 翻页时，id 不重
            if(count>=1){
                if(i++>=Constant.PageSize) {
                    if (!SdkManager.getInstance().getBaseUrl().equalsIgnoreCase(Constant.MockUrl)) {
                        assertTrue(!keys.contains(Qa.getId()));
                    }
                }
                continue;
            }
            keys.add(Qa.getId());
        }
        count++;
    }
    public void getQaDetail(String qaId) {
        final CountDownLatch counter = new CountDownLatch(1);
        IDataRetrieveListener<KnowledgeQa> listener = new IDataRetrieveListener<KnowledgeQa>() {
            @Override
            public void onCacheDataRetrieve(KnowledgeQa knowledgeQa, boolean b) {
                Logger.i(getClass(),"===TEST=== retrieve Qa detail from cache.");
                Logger.i(getClass(),"===TEST=== Qa id="+knowledgeQa.getId() + "Qa title="+knowledgeQa.getTitle()+ "Qa url="+knowledgeQa.getQuestion());
                assertTrue(knowledgeQa.getId().length()>0);
            }

            @Override
            public void onServerDataRetrieve(KnowledgeQa knowledgeQa) {
                Logger.i(getClass(),"===TEST=== retrieve Qa detail from server.");
                Logger.i(getClass(),"===TEST=== Qa id="+knowledgeQa.getId() + "Qa title="+knowledgeQa.getTitle()+ "Qa url="+knowledgeQa.getQuestion());
                assertTrue(knowledgeQa.getId().length()>0);
            }

            @Override
            public void done() {
                counter.countDown();
            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
                Logger.i(getClass(),"===TEST=== retrieve Qa detail exception");
                assertTrue(false);
                counter.countDown();
            }

            @Override
            public Handler getCallBackLooperHandler() {
                return null;
            }
        };
        KnowledgeQa.getQaAsync(qaId,listener,false);
        try {
            counter.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
