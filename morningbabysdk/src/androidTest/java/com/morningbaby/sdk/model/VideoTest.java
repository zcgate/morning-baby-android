package com.morningbaby.sdk.model;

import android.os.Handler;
import android.test.AndroidTestCase;

import com.morningbaby.sdk.Constant;
import com.morningbaby.sdk.SdkManager;
import com.morningbaby.sdk.Tester;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.frame.dao.CacheDefine.IDataRetrieveListener;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/**
 * Created by carl on 15/8/5.
 */
public class VideoTest extends AndroidTestCase {
    //private final String TOPIC_ID="23232-54312";
    //private final String TOPIC_ID="20fb40f3-f865-4519-ae3e-ed2541a8f7d4";
    private final String TOPIC_ID="9d691e45-765c-4c58-af23-e996d75ed37c";
    private List<String> keys = new ArrayList<String>();
    int count=0;
    public void testVideoList() throws Exception{

        Tester.init();
        //IListDataRetrieveListener<KnowledgeVideo> listener = new IListDataRetrieveListener<KnowledgeVideo>() {
        class KnowledgeVideoListener implements IListDataRetrieveListener<KnowledgeVideo> {

            private CountDownLatch counter;
            public void setCounter(CountDownLatch counter){
                this.counter=counter;
            }
            @Override
            public void onCacheDataRetrieve(List<KnowledgeVideo> knowledgeVideos, Map<String, Object> stringObjectMap, boolean b) {
                Logger.i(getClass(),"===TEST=== retrieve video list from cache.");
                printVideos(knowledgeVideos);
                assertTrue(knowledgeVideos.size()>0);
                getVideoDetail(knowledgeVideos.get(0).getId());
            }

            @Override
            public void onServerDataRetrieve(List<KnowledgeVideo> knowledgeVideos, Map<String, Object> stringObjectMap) {
                Logger.i(getClass(),"===TEST=== retrieve video list from server.");
                printVideos(knowledgeVideos);
                assertTrue(knowledgeVideos.size()>0);
                getVideoDetail(knowledgeVideos.get(0).getId());
            }

            @Override
            public void done() {
                counter.countDown();
            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
                Logger.i(getClass(),"===TEST=== retrieve video list exception");
                assertTrue(false);
                counter.countDown();
            }

            @Override
            public Handler getCallBackLooperHandler() {
                return null;
            }
        };
        CountDownLatch counter = new CountDownLatch(1);
        KnowledgeVideoListener listener = new KnowledgeVideoListener();
        listener.setCounter(counter);

        KnowledgeVideo video = new KnowledgeVideo();
        video.getVideoList(TOPIC_ID, "", listener, true);
        counter.await();

        if(!video.canLoadMore()) {
            Logger.i(getClass(),"===TEST=== Can't load more video");
            return;
        }
        Logger.i(getClass(),"===TEST=== load more video");
        counter = new CountDownLatch(1);
        listener.setCounter(counter);
        video.nextPage(listener);
        counter.await();
    }
    private void printVideos(List<KnowledgeVideo> list){
        Iterator iter = list.iterator();
        int i=0;
        while(iter.hasNext()){
            KnowledgeVideo video = (KnowledgeVideo) iter.next();
            Logger.i(getClass(), "===TEST=== id=" + video.getId() + " title=" + video.getTitle()+" url="+video.getLocation());
            // 翻页时，id 不重
            if(count>=1){
                if(i++>=Constant.PageSize) {
                    if (!SdkManager.getInstance().getBaseUrl().equalsIgnoreCase(Constant.MockUrl)) {
                        assertTrue(!keys.contains(video.getId()));
                    }
                }
                continue;
            }
            keys.add(video.getId());
        }
        count++;
    }
    public void getVideoDetail(String videoid) {
        final CountDownLatch counter = new CountDownLatch(1);
        IDataRetrieveListener<KnowledgeVideo> listener = new IDataRetrieveListener<KnowledgeVideo>() {
            @Override
            public void onCacheDataRetrieve(KnowledgeVideo knowledgeVideo, boolean b) {
                Logger.i(getClass(),"===TEST=== retrieve video detail from cache.");
                Logger.i(getClass(),"===TEST=== video id="+knowledgeVideo.getId() + "video title="+knowledgeVideo.getTitle()+ "video url="+knowledgeVideo.getLocation());
                assertTrue(knowledgeVideo.getId().length()>0);
            }

            @Override
            public void onServerDataRetrieve(KnowledgeVideo knowledgeVideo) {
                Logger.i(getClass(),"===TEST=== retrieve video detail from server.");
                Logger.i(getClass(),"===TEST=== video id="+knowledgeVideo.getId() + "video title="+knowledgeVideo.getTitle()+ "video url="+knowledgeVideo.getLocation());
                assertTrue(knowledgeVideo.getId().length()>0);
            }

            @Override
            public void done() {
                counter.countDown();
            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
                Logger.i(getClass(),"===TEST=== retrieve video detail exception");
                assertTrue(false);
                counter.countDown();
            }

            @Override
            public Handler getCallBackLooperHandler() {
                return null;
            }
        };
        KnowledgeVideo.getVideoAsync(videoid, listener, false);
        try {
            counter.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
