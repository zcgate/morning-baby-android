package com.morningbaby.sdk.model;

import android.os.Handler;
import android.test.AndroidTestCase;

import com.morningbaby.sdk.Constant;
import com.morningbaby.sdk.SdkManager;
import com.morningbaby.sdk.Tester;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.frame.dao.CacheDefine.IDataRetrieveListener;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/**
 * Created by carl on 15/8/5.
 */
public class HospitalTest extends AndroidTestCase {
    private List<String> keys = new ArrayList<String>();
    private int count=0;
    public void testHospitalList() throws Exception{
        Tester.init();

        class HospitalListener implements IListDataRetrieveListener<Hospital> {
            private CountDownLatch counter;

            public void setCounter(CountDownLatch counter){
                this.counter=counter;
            }
            @Override
            public void onCacheDataRetrieve(List<Hospital> Hospitals, Map<String, Object> stringObjectMap, boolean b) {
                Logger.i(getClass(), "===TEST=== retrieve Hospital list from cache.");
                printHospitals(Hospitals);
                assertTrue(Hospitals.size()>0);
                getHospitalDetail(Hospitals.get(0).getId());
            }

            @Override
            public void onServerDataRetrieve(List<Hospital> Hospitals, Map<String, Object> stringObjectMap) {
                Logger.i(getClass(),"===TEST=== retrieve Hospital list from server.");
                printHospitals(Hospitals);
                assertTrue(Hospitals.size()>0);
                getHospitalDetail(Hospitals.get(0).getId());
            }

            @Override
            public void done() {
                this.counter.countDown();
            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
                Logger.i(getClass(),"===TEST=== retrieve Hospital list exception");
                assertTrue(false);
                this.counter.countDown();
            }

            @Override
            public Handler getCallBackLooperHandler() {
                return null;
            }
        };
        CountDownLatch counter = new CountDownLatch(1);
        HospitalListener listener = new HospitalListener() ;
        listener.setCounter(counter);
        Hospital hospital = new Hospital();
        hospital.getHospitalList(listener, true);
        try {
            counter.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(!hospital.canLoadMore()) {
            Logger.i(getClass(),"===TEST=== Can't load more hospital");
            return;
        }
        Logger.i(getClass(),"===TEST=== load more hospital");
        counter = new CountDownLatch(1);
        listener.setCounter(counter);
        hospital.nextPage(listener);

        try {
            counter.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    private void printHospitals(List<Hospital> list){
        Iterator iter = list.iterator();
        int i=0;
        while(iter.hasNext()){
            Hospital hospital = (Hospital) iter.next();
            Logger.i(getClass(), "===TEST=== id=" + hospital.getId() + " title=" + hospital.getName());
            // 翻页时，id 不重
            if(count>=1){
                if(i++>=Constant.PageSize) {
                    if (!SdkManager.getInstance().getBaseUrl().equalsIgnoreCase(Constant.MockUrl)) {
                        assertTrue(!keys.contains(hospital.getId()));
                    }
                }
                continue;
            }
            keys.add(hospital.getId());
        }
        count++;
    }
    public void getHospitalDetail(String hospitalId) {
        final CountDownLatch counter = new CountDownLatch(1);
        IDataRetrieveListener<Hospital> listener = new IDataRetrieveListener<Hospital>() {
            @Override
            public void onCacheDataRetrieve(Hospital Hospital, boolean b) {
                Logger.i(getClass(),"===TEST=== retrieve Hospital detail from cache.");
                Logger.i(getClass(),"===TEST=== Hospital id="+Hospital.getId() + "Hospital name="+Hospital.getName()+ "Hospital city="+Hospital.getAddressCity());
                assertTrue(Hospital.getId().length()>0);
            }

            @Override
            public void onServerDataRetrieve(Hospital Hospital) {
                Logger.i(getClass(),"===TEST=== retrieve Hospital detail from server.");
                Logger.i(getClass(),"===TEST=== Hospital id="+Hospital.getId() + "Hospital name="+Hospital.getName()+ "Hospital city="+Hospital.getAddressCity());
                assertTrue(Hospital.getId().length()>0);
            }

            @Override
            public void done() {
                counter.countDown();
            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
                Logger.i(getClass(),"===TEST=== retrieve Hospital detail exception");
                assertTrue(false);
                counter.countDown();
            }

            @Override
            public Handler getCallBackLooperHandler() {
                return null;
            }
        };
        Hospital.getHospitalAsync(hospitalId,listener,false);
        try {
            counter.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
