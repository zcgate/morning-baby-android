package com.morningbaby.sdk.model;

import android.os.Handler;
import android.test.AndroidTestCase;

import com.morningbaby.sdk.Constant;
import com.morningbaby.sdk.SdkManager;
import com.morningbaby.sdk.Tester;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.frame.dao.CacheDefine.IDataRetrieveListener;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/**
 * Created by carl on 15/8/5.
 */
public class DoctorTest extends AndroidTestCase {
    private static final String hospitalId="55c38ec0c339c3506147f8a8";
    private static final String departmentId="55c505eb1f0861b643ad857f";
    private List<String> keys = new ArrayList<String>();
    private int count = 0;
    public void testDoctorList() throws Exception{
        Tester.init();
        class DoctorListener implements IListDataRetrieveListener<Doctor> {

            private CountDownLatch counter;
            public void setCounter(CountDownLatch counter){
                this.counter = counter;
            }
            @Override
            public void onCacheDataRetrieve(List<Doctor> Doctors, Map<String, Object> stringObjectMap, boolean b) {
                Logger.i(getClass(), "===TEST=== retrieve Doctor list from cache.");
                printDoctors(Doctors);
                assertTrue(Doctors.size()>0);
                getDoctorDetail(Doctors.get(0).getId());
            }

            @Override
            public void onServerDataRetrieve(List<Doctor> Doctors, Map<String, Object> stringObjectMap) {
                Logger.i(getClass(),"===TEST=== retrieve Doctor list from server.");
                printDoctors(Doctors);
                assertTrue(Doctors.size()>0);
                getDoctorDetail(Doctors.get(0).getId());
            }

            @Override
            public void done() {
                counter.countDown();
            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
                Logger.i(getClass(),"===TEST=== retrieve Doctor list exception");
                assertTrue(false);
                counter.countDown();
            }

            @Override
            public Handler getCallBackLooperHandler() {
                return null;
            }
        };
        CountDownLatch counter = new CountDownLatch(1);
        DoctorListener listener= new DoctorListener() ;
        listener.setCounter(counter);
        Doctor doctor = new Doctor();
        doctor.getDoctorList(hospitalId, departmentId, listener, true);
        counter.await();

        if(!doctor.canLoadMore()) {
            Logger.i(getClass(),"===TEST=== Can't load more doctor");
            return;
        }
        Logger.i(getClass(),"===TEST=== load more doctor");
        counter = new CountDownLatch(1);
        listener.setCounter(counter);
        doctor.nextPage(listener);
        counter.await();
    }
    private void printDoctors(List<Doctor> list){
        Iterator iter = list.iterator();
        int i=0;
        while(iter.hasNext()){
            Doctor doctor = (Doctor) iter.next();
            Logger.i(getClass(), "===TEST=== id=" + doctor.getId() + " title=" + doctor.getName());
            // 翻页时，id 不重
            if(count>=1){
                if(i++>=Constant.PageSize) {
                    if (!SdkManager.getInstance().getBaseUrl().equalsIgnoreCase(Constant.MockUrl)) {
                        assertTrue(!keys.contains(doctor.getId()));
                    }
                }
                continue;
            }
            keys.add(doctor.getId());
        }
        count++;
    }
    public void getDoctorDetail(String DoctorId) {
        final CountDownLatch counter = new CountDownLatch(1);
        IDataRetrieveListener<Doctor> listener = new IDataRetrieveListener<Doctor>() {
            @Override
            public void onCacheDataRetrieve(Doctor doctor, boolean b) {
                Logger.i(getClass(),"===TEST=== retrieve Doctor detail from cache.");
                Logger.i(getClass(),"===TEST=== Doctor id="+doctor.getId() + "Doctor name="+doctor.getName()+ "Doctor city="+doctor.getTitle());
                assertTrue(doctor.getId().length()>0);
            }

            @Override
            public void onServerDataRetrieve(Doctor doctor) {
                Logger.i(getClass(),"===TEST=== retrieve Doctor detail from server.");
                Logger.i(getClass(),"===TEST=== Doctor id="+doctor.getId() + "Doctor name="+doctor.getName()+ "Doctor city="+doctor.getTitle());
                assertTrue(doctor.getId().length()>0);
            }

            @Override
            public void done() {
                counter.countDown();
            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
                Logger.i(getClass(),"===TEST=== retrieve Doctor detail exception");
                assertTrue(false);
                counter.countDown();
            }

            @Override
            public Handler getCallBackLooperHandler() {
                return null;
            }
        };
        Doctor.getDoctorAsync(DoctorId,listener,false);
        try {
            counter.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
