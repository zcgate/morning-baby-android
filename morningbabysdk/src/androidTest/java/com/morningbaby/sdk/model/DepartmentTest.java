package com.morningbaby.sdk.model;

import android.os.Handler;
import android.test.AndroidTestCase;

import com.morningbaby.sdk.Constant;
import com.morningbaby.sdk.SdkManager;
import com.morningbaby.sdk.Tester;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.frame.dao.CacheDefine.IDataRetrieveListener;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/**
 * Created by carl on 15/8/5.
 */
public class DepartmentTest extends AndroidTestCase {
    private static final String hospitalId="55c38ec0c339c3506147f8a8";
    private List<String> keys = new ArrayList<String>();
    int count=0;
    public void testDepartmentList() throws Exception{
        Tester.init();
        class DepartmentListener implements IListDataRetrieveListener<Department> {

            private CountDownLatch counter;
            public void setCounter(CountDownLatch counter){
                this.counter=counter;
            }
            @Override
            public void onCacheDataRetrieve(List<Department> Departments, Map<String, Object> stringObjectMap, boolean b) {
                Logger.i(getClass(), "===TEST=== retrieve Department list from cache.");
                printDepartments(Departments);
                assertTrue(Departments.size()>0);
                getDepartmentDetail(Departments.get(0).getId());
            }

            @Override
            public void onServerDataRetrieve(List<Department> Departments, Map<String, Object> stringObjectMap) {
                Logger.i(getClass(),"===TEST=== retrieve Department list from server.");
                printDepartments(Departments);
                assertTrue(Departments.size()>0);
                getDepartmentDetail(Departments.get(0).getId());
            }

            @Override
            public void done() {
                this.counter.countDown();
            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
                Logger.i(getClass(),"===TEST=== retrieve Department list exception");
                assertTrue(false);
                this.counter.countDown();
            }

            @Override
            public Handler getCallBackLooperHandler() {
                return null;
            }
        };

        CountDownLatch counter = new CountDownLatch(1);
        DepartmentListener listener = new DepartmentListener();
        listener.setCounter(counter);
        Department department = new Department();
        department.getDepartmentList(hospitalId,listener,true);
        counter.await();

        if(!department.canLoadMore()) {
            Logger.i(getClass(),"===TEST=== Can't load more department");
            return;
        }
        Logger.i(getClass(),"===TEST=== load more department");
        counter = new CountDownLatch(1);
        listener.setCounter(counter);
        department.nextPage(listener);
        counter.await();
    }
    private void printDepartments(List<Department> list){
        Iterator iter = list.iterator();
        int i=0;
        while(iter.hasNext()){
            Department Department = (Department) iter.next();
            Logger.i(getClass(), "===TEST=== id=" + Department.getId() + " title=" + Department.getName());
            // 翻页时，id 不重
            if(count>=1){
                if(i++>=Constant.PageSize) {
                    if (!SdkManager.getInstance().getBaseUrl().equalsIgnoreCase(Constant.MockUrl)) {
                        assertTrue(!keys.contains(Department.getId()));
                    }
                }
                continue;
            }
            keys.add(Department.getId());
        }
        count++;
    }
    public void getDepartmentDetail(String DepartmentId) {
        final CountDownLatch counter = new CountDownLatch(1);
        IDataRetrieveListener<Department> listener = new IDataRetrieveListener<Department>() {
            @Override
            public void onCacheDataRetrieve(Department Department, boolean b) {
                Logger.i(getClass(),"===TEST=== retrieve Department detail from cache.");
                Logger.i(getClass(),"===TEST=== Department id="+Department.getId() + "Department name="+Department.getName()+ "Department city="+Department.getHospitalId());
                assertTrue(Department.getId().length()>0);
            }

            @Override
            public void onServerDataRetrieve(Department Department) {
                Logger.i(getClass(),"===TEST=== retrieve Department detail from server.");
                Logger.i(getClass(),"===TEST=== Department id="+Department.getId() + "Department name="+Department.getName()+ "Department city="+Department.getHospitalId());
                assertTrue(Department.getId().length()>0);
            }

            @Override
            public void done() {
                counter.countDown();
            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
                Logger.i(getClass(),"===TEST=== retrieve Department detail exception");
                assertTrue(false);
                counter.countDown();
            }

            @Override
            public Handler getCallBackLooperHandler() {
                return null;
            }
        };
        Department.getDepartmentAsync(DepartmentId,listener,false);
        try {
            counter.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
