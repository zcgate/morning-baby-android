package com.morningbaby.sdk.model;

import android.os.Handler;
import android.test.AndroidTestCase;

import com.morningbaby.sdk.Constant;
import com.morningbaby.sdk.SdkManager;
import com.morningbaby.sdk.Tester;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.frame.dao.CacheDefine.IDataRetrieveListener;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/**
 * Created by carl on 15/8/5.
 */
public class ArticleTest extends AndroidTestCase {
    //private final String TOPIC_ID="23232-54312";
    //private final String TOPIC_ID="20fb40f3-f865-4519-ae3e-ed2541a8f7d4";
    private final String TOPIC_ID="9d691e45-765c-4c58-af23-e996d75ed37c";
    private List<String> keys = new ArrayList<String>();
    int count=0;

    public void testArticleList() throws Exception{
        Tester.init();
        class ArticleListener implements IListDataRetrieveListener<KnowledgeArticle> {

            private CountDownLatch counter;
            public void setCounter(CountDownLatch counter){
                this.counter=counter;
            }
            @Override
            public void onCacheDataRetrieve(List<KnowledgeArticle> knowledgeArticles, Map<String, Object> stringObjectMap, boolean b) {
                Logger.i(getClass(), "===TEST=== retrieve Article list from cache.");
                printArticles(knowledgeArticles);
                assertTrue(knowledgeArticles.size()>0);
                getArticleDetail(knowledgeArticles.get(0).getId());
            }

            @Override
            public void onServerDataRetrieve(List<KnowledgeArticle> knowledgeArticles, Map<String, Object> stringObjectMap) {
                Logger.i(getClass(),"===TEST=== retrieve Article list from server.");
                printArticles(knowledgeArticles);
                assertTrue(knowledgeArticles.size()>0);
                getArticleDetail(knowledgeArticles.get(0).getId());
            }

            @Override
            public void done() {
                this.counter.countDown();
            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
                Logger.i(getClass(),"===TEST=== retrieve Article list exception");
                assertTrue(false);
                this.counter.countDown();
            }

            @Override
            public Handler getCallBackLooperHandler() {
                return null;
            }
        };
        String key = "妈妈";
        CountDownLatch counter = new CountDownLatch(1);
        ArticleListener listener = new ArticleListener() ;
        listener.setCounter(counter);
        KnowledgeArticle article = new KnowledgeArticle();
        article.getArticleList("", key, listener, true);
        counter.await();
        if(!article.canLoadMore()) {
            Logger.i(getClass(),"===TEST=== Can't load more hospital");
            return;
        }
        Logger.i(getClass(),"===TEST=== load more hospital");
        counter = new CountDownLatch(1);
        listener.setCounter(counter);
        article.nextPage(listener);
        counter.await();
    }
    private void printArticles(List<KnowledgeArticle> list){
        Iterator iter = list.iterator();
        int i=0;
        while(iter.hasNext()){
            KnowledgeArticle Article = (KnowledgeArticle) iter.next();
            Logger.i(getClass(), "===TEST=== id=" + Article.getId() + " title=" + Article.getTitle()+" brief="+Article.getBrief());
            // 翻页时，id 不重
            if(count>=1){
                if(i++>=Constant.PageSize) {
                    if (!SdkManager.getInstance().getBaseUrl().equalsIgnoreCase(Constant.MockUrl)) {
                        assertTrue(!keys.contains(Article.getId()));
                    }
                }
                continue;
            }
            keys.add(Article.getId());
        }
        count++;
    }
    public void getArticleDetail(String article_id) {
        final CountDownLatch counter = new CountDownLatch(1);
        IDataRetrieveListener<KnowledgeArticle> listener = new IDataRetrieveListener<KnowledgeArticle>() {
            @Override
            public void onCacheDataRetrieve(KnowledgeArticle knowledgeArticle, boolean b) {
                Logger.i(getClass(),"===TEST=== retrieve Article detail from cache.");
                Logger.i(getClass(),"===TEST=== Article id="+knowledgeArticle.getId() + "Article title="+knowledgeArticle.getTitle()+ "Article content="+knowledgeArticle.getContent());
                assertTrue(knowledgeArticle.getId().length()>0);
            }

            @Override
            public void onServerDataRetrieve(KnowledgeArticle knowledgeArticle) {
                Logger.i(getClass(),"===TEST=== retrieve Article detail from server.");
                Logger.i(getClass(),"===TEST=== Article id="+knowledgeArticle.getId() + "Article title="+knowledgeArticle.getTitle()+ "Article content="+knowledgeArticle.getContent());
                assertTrue(knowledgeArticle.getId().length()>0);
            }

            @Override
            public void done() {
                counter.countDown();
            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
                Logger.i(getClass(),"===TEST=== retrieve Article detail exception");
                assertTrue(false);
                counter.countDown();
            }

            @Override
            public Handler getCallBackLooperHandler() {
                return null;
            }
        };
        KnowledgeArticle.getArticleAsync(article_id,listener,false);
        try {
            counter.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
