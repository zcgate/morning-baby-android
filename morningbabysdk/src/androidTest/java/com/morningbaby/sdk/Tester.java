package com.morningbaby.sdk;

import android.content.Context;
import com.morningbaby.sdk.model.User;

/**
 * Created by carl on 15/8/4.
 */
public class Tester {
    public static String phone = "15900000000";
    public static String email = "fzcarl@163.com";
    public static String pwd = "123456";

    private static boolean isInit=false;
    public static void init(){
        init(true);
    }
    public static void init(boolean login){
        try {
            Thread.sleep(2000);
            if(login){
                User.getCurrentUser().login(email,pwd);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        isInit=true;
    }
    public static void setIsInit(boolean flag){
        isInit=flag;
    }
    public static boolean isInit(){
        return isInit;
    }
}
