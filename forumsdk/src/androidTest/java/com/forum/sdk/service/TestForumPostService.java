package com.forum.sdk.service;

import android.test.suitebuilder.annotation.SmallTest;

import com.forum.sdk.ForumConfigManager;
import com.forum.sdk.IForumConfig;
import com.forum.sdk.bean.post.ForumPostInfo;
import com.forum.sdk.bean.post.ForumPostList;
import com.forum.sdk.bean.section.ForumSectionInfo;
import com.forum.sdk.dao.post.bean.ForumPostParam;
import com.forum.sdk.dao.section.bean.ForumSectionParam;
import com.nd.smartcan.frame.exception.DaoException;

import junit.framework.TestCase;

/**
 * Created by dcj on 2015/8/19.
 */
public class TestForumPostService extends TestCase {
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        IForumConfig forumConfig = new IForumConfig() {
            @Override
            public String getCurrentUid() {
                return "1111";
            }

            @Override
            public String getForumUrl() {
                return "http://192.168.0.191:8080/api/v1/";
            }
        };
        ForumConfigManager.INSTANCE.setForumConfig(forumConfig);
    }
    /*
    创建一个版块
     */
    @SmallTest
    public void testCreatePost() throws DaoException {
        ForumPostParam fpp = new ForumPostParam();
        fpp.setTitle("nan dao cheng gong le");
        fpp.setForumId("bc8c6560-d1fe-48a2-b1c4-a6ef82bbab89");
        fpp.setContent("这是一个测试内容");
        //fpp.setSource("bu yao wen wo cong na li lai");
        ForumPostInfo fpi = ForumServiceFactory.INSTANCE.getForumPostService().createPost(fpp);
        assertEquals(fpp.getForumId(), "bc8c6560-d1fe-48a2-b1c4-a6ef82bbab89");
    }
    @SmallTest
    public void testGetPostInfo() throws DaoException {

        String pid = "758916db-89d5-4295-8ae3-500fcc8f2815";
        ForumPostInfo fpi = ForumServiceFactory.INSTANCE.getForumPostService().getPostDetail(pid);

        assertEquals(fpi.getId(), "758916db-89d5-4295-8ae3-500fcc8f2815");
    }

    @SmallTest
    public void testGetSectionPostList() throws DaoException {

        String forumId = "bc8c6560-d1fe-48a2-b1c4-a6ef82bbab89";
        ForumPostList fpl = ForumServiceFactory.INSTANCE.getForumPostService().getSectionPostList(forumId,Long.MAX_VALUE,5,true);

        assertEquals(fpl.getCount(), 7);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
