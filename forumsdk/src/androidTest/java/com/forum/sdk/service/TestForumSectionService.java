package com.forum.sdk.service;

import android.test.suitebuilder.annotation.SmallTest;

import com.forum.sdk.ForumConfigManager;
import com.forum.sdk.IForumConfig;
import com.forum.sdk.bean.section.ForumCategoryList;
import com.forum.sdk.bean.section.ForumSectionInfo;
import com.forum.sdk.bean.section.ForumSectionList;
import com.forum.sdk.bean.tag.ForumTagList;
import com.forum.sdk.dao.section.bean.ForumSectionParam;
import com.nd.smartcan.frame.exception.DaoException;

import junit.framework.TestCase;

/**
 * Created by dcj on 8/18/2015.
 */
public class TestForumSectionService extends TestCase {
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        IForumConfig forumConfig = new IForumConfig() {
            @Override
            public String getCurrentUid() {
                return "1111";
            }

            @Override
            public String getForumUrl() {
                return "http://192.168.0.191:8080/api/v1/";
            }
        };
        ForumConfigManager.INSTANCE.setForumConfig(forumConfig);
    }
    /*
    创建一个版块
     */
    @SmallTest
    public void testCreateSection() throws DaoException {
        ForumSectionParam fsp = new ForumSectionParam();
        fsp.setName("这是一个来自测试19");
        fsp.setCategory("fwyXFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
        fsp.setCheck(0);
        fsp.setTagList("tebie, hao");
        ForumSectionInfo fsi = ForumServiceFactory.INSTANCE.getForumSectionService().createSection(fsp);
        assertEquals(fsi.getCategory(), "fwyXFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
    }
    /*
     根据ID 获取版块信息
     */
    @SmallTest
    public void testGetSectionInfo() throws DaoException {

        String forumId = "bc8c6560-d1fe-48a2-b1c4-a6ef82bbab89";
        ForumSectionInfo fsi = ForumServiceFactory.INSTANCE.getForumSectionService().getSectionInfo(forumId);
        System.out.print(fsi.toString());
        assertEquals(fsi.getFid(), 9);
    }
    /*
    获取标签列表
     */
    @SmallTest
    public void testGetTagList() throws DaoException {

        ForumTagList ftl = ForumServiceFactory.INSTANCE.getForumSectionService().getTagList(0, 100, true);

        assertTrue(ftl.getCount() > 0);
    }
    /*
    热门板块列表
     */
    @SmallTest
    public void testGetHotSectionList() throws DaoException
    {
        ForumSectionList fst = ForumServiceFactory.INSTANCE.getForumSectionService().getHotSectionList(0, 5, true);
        assertEquals(fst.getItems().size(), 5);
    }
    /*
    板块类别列表
     */
    @SmallTest
    public void testGetCategoryList() throws DaoException
    {
        ForumCategoryList fcl = ForumServiceFactory.INSTANCE.getForumSectionService().getCategoryList(0,5,true);
        assertEquals(fcl.getItems().size(), 5);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
