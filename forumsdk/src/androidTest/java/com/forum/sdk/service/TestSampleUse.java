package com.forum.sdk.service;

import android.test.suitebuilder.annotation.SmallTest;

import com.forum.sdk.ForumConfigManager;
import com.forum.sdk.IForumConfig;
import com.forum.sdk.bean.post.ForumPostInfo;
import com.forum.sdk.bean.post.ForumPostList;
import com.forum.sdk.bean.section.ForumCategoryList;
import com.forum.sdk.bean.section.ForumSectionInfo;
import com.forum.sdk.bean.section.ForumSectionList;
import com.forum.sdk.bean.tag.ForumTagList;
import com.forum.sdk.dao.post.bean.ForumPostParam;
import com.forum.sdk.dao.section.bean.ForumSectionParam;
import com.nd.smartcan.frame.exception.DaoException;

import junit.framework.TestCase;

/**
 * Created by dcj on 2015/8/20.
 */
public class TestSampleUse extends TestCase {
    /*
    初始化 REST API的URL 和 USERID
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        IForumConfig forumConfig = new IForumConfig() {
            @Override
            public String getCurrentUid() {
                return "1111";// 自己指定
            }

            @Override
            public String getForumUrl() {
                return "http://192.168.0.191:8080/api/v1/";//连接手机测试，要和手机在一个网段，不要用localhost或127.0.0.1
            }
        };
        ForumConfigManager.INSTANCE.setForumConfig(forumConfig);
    }
    /*
    创建一个版块
     */
    @SmallTest
    public void testCreateSection() throws DaoException {
        ForumSectionParam fsp = new ForumSectionParam();
        fsp.setName("这是一个测试");
        //暂时sdk没有添加category的API，但是forum提供了，可以手动的添加，这是category的ID
        fsp.setCategory("fwyXFFFFFFFFFFFFFFFFFFFFFFFFFFFF");fsp.setCheck(0);
        fsp.setTagList("tebie, hao");//会根据填写的内容自动生成标签，需要“，”分割
        ForumSectionInfo fsi = ForumServiceFactory.INSTANCE.getForumSectionService().createSection(fsp);
        assertEquals(fsi.getCategory(), "fwyXFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
    }
    /*
     根据ID 获取版块信息
     */
    @SmallTest
    public void testGetSectionInfo() throws DaoException {

        String forumId = "bc8c6560-d1fe-48a2-b1c4-a6ef82bbab89";//上一步生成的 forumID
        ForumSectionInfo fsi = ForumServiceFactory.INSTANCE.getForumSectionService().getSectionInfo(forumId);
        //System.out.print(fsi.toString());
        assertEquals(fsi.getFid(), 9);
    }
    /**
     *获取标签列表
     *
     * 三个参数意义
     *  page 页偏移量，不小于0的值，传错置为0，默认为0
     *  size 每页条数限制，默认20，最大100，传错置为20
     *  isNeedCount 是否需要返回总数，false不需要，true需要
     * return  tag list
     */
    @SmallTest
    public void testGetTagList() throws DaoException {

        ForumTagList ftl = ForumServiceFactory.INSTANCE.getForumSectionService().getTagList(0, 100, true);

        assertTrue(ftl.getCount() > 0);
    }
    /*
    热门板块列表
    参数意义同上
     */
    @SmallTest
    public void testGetHotSectionList() throws DaoException
    {
        ForumSectionList fst = ForumServiceFactory.INSTANCE.getForumSectionService().getHotSectionList(0, 5, true);
        assertEquals(fst.getItems().size(), 5);
    }
    /*
    板块类别列表
    参数意义同上
     */
    @SmallTest
    public void testGetCategoryList() throws DaoException
    {
        ForumCategoryList fcl = ForumServiceFactory.INSTANCE.getForumSectionService().getCategoryList(0, 5, true);
        assertEquals(fcl.getItems().size(), 5);
    }

    /*
    发布一个帖子
     */
    @SmallTest
    public void testCreatePost() throws DaoException {
        ForumPostParam fpp = new ForumPostParam();
        fpp.setTitle("帖子标题");
        fpp.setForumId("bc8c6560-d1fe-48a2-b1c4-a6ef82bbab89");//之前创建的版块
        fpp.setContent("这是一个测试内容");
        //fpp.setSource("bu yao wen wo cong na li lai");
        ForumPostInfo fpi = ForumServiceFactory.INSTANCE.getForumPostService().createPost(fpp);
        assertEquals(fpp.getForumId(), "bc8c6560-d1fe-48a2-b1c4-a6ef82bbab89");
    }
    /*
    帖子详细信息
     */
    @SmallTest
    public void testGetPostInfo() throws DaoException {

        String pid = "758916db-89d5-4295-8ae3-500fcc8f2815";//上一步生成的
        ForumPostInfo fpi = ForumServiceFactory.INSTANCE.getForumPostService().getPostDetail(pid);

        assertEquals(fpi.getId(), "758916db-89d5-4295-8ae3-500fcc8f2815");
    }

    /*
    得到版块下所有的帖子
     */
    @SmallTest
    public void testGetSectionPostList() throws DaoException {

        String forumId = "bc8c6560-d1fe-48a2-b1c4-a6ef82bbab89";
        ForumPostList fpl = ForumServiceFactory.INSTANCE.getForumPostService().getSectionPostList(forumId, Long.MAX_VALUE, 5, true);

        assertEquals(fpl.getCount(), 7);
    }
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}

