package com.forum.sdk.dao.report;

import com.forum.sdk.bean.report.ForumReportInfo;
import com.forum.sdk.common.ForumRequireUrl;
import com.forum.sdk.common.ForumRestDao;
import com.forum.sdk.dao.report.bean.ForumReportParam;
import com.nd.smartcan.frame.dao.RestDao;
import com.nd.smartcan.frame.exception.DaoException;

/**
 * 举报处理
 *
 * @see
 */
public class ForumReportDao extends ForumRestDao<ForumReportParam> {


    @Override
    protected String getResourceUri() {
        return ForumRequireUrl.REPORT_URL;
    }

    /**
     * 举报主帖
     *
     * @param forumReport the com.morningbaby.forum report
     * @return  report info
     * @throws DaoException the dao exception
     */
    public ForumReportInfo reportPost(ForumReportParam forumReport) throws DaoException {

        return post(forumReport,null,ForumReportInfo.class);
    }
}
