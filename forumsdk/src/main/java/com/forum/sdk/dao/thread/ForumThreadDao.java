package com.forum.sdk.dao.thread;



import com.forum.sdk.ForumConfigManager;
import com.forum.sdk.bean.post.ForumThreadInfo;
import com.forum.sdk.bean.post.ForumThreadList;
import com.forum.sdk.common.ForumConstDefine;
import com.forum.sdk.common.ForumRequireUrl;
import com.forum.sdk.common.ForumRestDao;
import com.forum.sdk.dao.thread.bean.ForumTheadIds;
import com.forum.sdk.dao.thread.bean.ForumThreadParam;
import com.nd.smartcan.frame.dao.RestDao;
import com.nd.smartcan.frame.exception.DaoException;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 跟帖相关处理
 *
 * @author  : huangszh
 * @date 2015年04月02日 15:41:59
 * @see
 */
public class ForumThreadDao extends ForumRestDao<ForumThreadParam> {

    @Override
    protected String getResourceUri() {
        return ForumRequireUrl.THREAD_URL;
    }

    /**
     * 发布跟帖
     *
     * @param forumThreadParam 跟帖信息
     * @return com.morningbaby.forum thread info
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:34:55
     */
    public ForumThreadInfo createThread(ForumThreadParam forumThreadParam) throws DaoException{

        return post(forumThreadParam,null,ForumThreadInfo.class);
    }

    /**
     * 删除跟帖
     *
     * @param postId 主帖id
     * @return com.morningbaby.forum thread info
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:34:55
     */
    public ForumThreadInfo deleteThread(String postId) throws DaoException{
        setObjId(postId);
        return delete(null,ForumThreadInfo.class);
    }

    /**
     * 获取主贴的跟帖列表
     *
     * @param postId 主帖id
     * @param minThread 获取大于minThread的跟帖列表，首次获取时传0
     * @param size 获取的数量
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return post thread list
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:34:55
     */
    public ForumThreadList getPostThreadList(String postId, long minThread, int size, boolean isNeedCount) throws DaoException{
        StringBuilder url = new StringBuilder(getResourceUri());
        url.append(File.separator).append(ForumConstDefine.ParamKeyConst.POSTS)
                .append(File.separator).append(postId)
                .append("?").append(ForumConstDefine.ParamKeyConst.MIN_THREAD_ID).append("=").append(minThread)
                .append("&").append(ForumConstDefine.ParamKeyConst.$LIMIT).append("=").append(size)
                .append("&").append(ForumConstDefine.ParamKeyConst.$COUNT).append("=").append(isNeedCount);
        return get(url.toString(),null,ForumThreadList.class);
    }

    /**
     * 获取我收到的跟帖列表
     *
     * @param maxThread 获取小于maxThread的跟帖列表，首次获取时传Long.MAX_VALUE
     * @param size 获取的数量
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return post reply me thread list
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:34:55
     */
    public ForumThreadList getPostReplyMeThreadList(long maxThread, int size, boolean isNeedCount) throws DaoException{
        StringBuilder url = new StringBuilder(getResourceUri());
        url.append(File.separator).append(ForumConstDefine.ParamKeyConst.USER)
                .append(File.separator).append(ForumConfigManager.INSTANCE.getCurrentUid())
                .append("?").append(ForumConstDefine.ParamKeyConst.MAX_THREAD_ID).append("=").append(maxThread)
                .append("&").append(ForumConstDefine.ParamKeyConst.$LIMIT).append("=").append(size)
                .append("&").append(ForumConstDefine.ParamKeyConst.$COUNT).append("=").append(isNeedCount);
        return get(url.toString(),null,ForumThreadList.class);
    }

    /**
     * 批量获取跟帖列表
     * @param idList 跟帖id列表
     * @return ForumThreadList
     * @throws DaoException the dao exception
     */
    public ForumThreadList getThreadList(List<String> idList) throws DaoException {
        if(idList == null || idList.isEmpty()){
            ForumThreadList list = new ForumThreadList();
            list.setItems(new ArrayList<ForumThreadInfo>());
            return list;
        }
        StringBuilder url = new StringBuilder(getResourceUri()).append("/list");
        ForumTheadIds ids = new ForumTheadIds();
        ids.setObjectIds(idList);
        return post(url.toString(), ids, null, ForumThreadList.class);
    }
}
