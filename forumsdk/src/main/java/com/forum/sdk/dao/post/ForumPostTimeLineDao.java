package com.forum.sdk.dao.post;


import com.forum.sdk.ForumConfigManager;
import com.forum.sdk.bean.post.ForumPostInfo;
import com.forum.sdk.bean.post.ForumPostList;
import com.forum.sdk.common.ForumConstDefine;
import com.forum.sdk.common.ForumRequireUrl;
import com.forum.sdk.common.ForumRestDao;
import com.nd.smartcan.frame.dao.RestDao;
import com.nd.smartcan.frame.exception.DaoException;

import java.io.File;

/**
 * 主贴时间线相关处理
 *
 */
public class ForumPostTimeLineDao extends ForumRestDao<ForumPostInfo> {

    @Override
    protected String getResourceUri() {
        return ForumRequireUrl.POST_TIMELINE_URL;
    }

    /**
     * 获取版块的主帖列表
     *
     * @param id 版块id
     * @param maxActive 获取小于maxActive的主帖列表，首次获取时传Long.MAX_VALUE
     * @param size 获取的数量
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return post list
     * @throws DaoException the dao exception
     */
    public ForumPostList getSectionPostList(String id, long maxActive, int size, boolean isNeedCount) throws DaoException {
        StringBuilder url = new StringBuilder(getResourceUri());
        url.append(File.separator).append(ForumConstDefine.ParamKeyConst.FORUM)
                .append(File.separator).append(id)
                .append("?").append(ForumConstDefine.ParamKeyConst.MAX_ACTIVE).append("=").append(maxActive)
                .append("&").append(ForumConstDefine.ParamKeyConst.$LIMIT).append("=").append(size)
                .append("&").append(ForumConstDefine.ParamKeyConst.$COUNT).append("=").append(isNeedCount);
        return get(url.toString(),null,ForumPostList.class);
    }

    /**
     * 获取我订阅的主帖列表
     *
     * @param maxActive 获取小于maxActive的主帖列表，首次获取时传Long.MAX_VALUE
     * @param size 获取的数量
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return ForumPostList
     * @throws DaoException the dao exception
     */
    public ForumPostList getMyRssPostList(long maxActive, int size, boolean isNeedCount) throws DaoException {
        StringBuilder url = new StringBuilder(getResourceUri());
        url.append(File.separator).append(ForumConstDefine.ParamKeyConst.RSS)
                .append("?").append(ForumConstDefine.ParamKeyConst.MAX_ACTIVE).append("=").append(maxActive)
                .append("&").append(ForumConstDefine.ParamKeyConst.$LIMIT).append("=").append(size)
                .append("&").append(ForumConstDefine.ParamKeyConst.$COUNT).append("=").append(isNeedCount);
        return get(url.toString(),null,ForumPostList.class);
    }

    /**
     * 获取我发布的主帖列表
     *
     * @param page 页偏移量，不小于0的值，传错置为0，默认为0
     * @param size 每页条数限制，默认20，最大100，传错置为20
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return ForumPostList
     * @throws DaoException the dao exception
     */
    public ForumPostList getMyPostList(int page, int size, boolean isNeedCount) throws DaoException {
        StringBuilder url = new StringBuilder(getResourceUri());
        url.append(File.separator).append(ForumConstDefine.ParamKeyConst.USER)
                .append(File.separator).append(ForumConfigManager.INSTANCE.getCurrentUid())
                .append("?").append(ForumConstDefine.ParamKeyConst.PAGE).append("=").append(page)
                .append("&").append(ForumConstDefine.ParamKeyConst.SIZE).append("=").append(size)
                .append("&").append(ForumConstDefine.ParamKeyConst.$COUNT).append("=").append(isNeedCount);
        return get(url.toString(),null,ForumPostList.class);
    }

    /**
     * 获取我以回帖方式参与的主帖列表
     *
     * @param page 页偏移量，不小于0的值，传错置为0，默认为0
     * @param size 每页条数限制，默认20，最大100，传错置为20
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return post list
     * @throws DaoException the dao exception
     */
    public ForumPostList getMyThreadJoinPostList(int page, int size, boolean isNeedCount) throws DaoException {
        StringBuilder url = new StringBuilder(getResourceUri());
        url.append(File.separator).append(ForumConstDefine.ParamKeyConst.MARKED)
                .append(File.separator).append(ForumConfigManager.INSTANCE.getCurrentUid())
                .append("?").append(ForumConstDefine.ParamKeyConst.PAGE).append("=").append(page)
                .append("&").append(ForumConstDefine.ParamKeyConst.SIZE).append("=").append(size)
                .append("&").append(ForumConstDefine.ParamKeyConst.$COUNT).append("=").append(isNeedCount);
        return get(url.toString(),null,ForumPostList.class);
    }
}
