package com.forum.sdk.dao.post.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;
import com.forum.sdk.bean.geo.ForumGeographyInfo;
import com.forum.sdk.common.ForumConstDefine;

/**
 * 发布主贴需要的参数 （版块uuid和内容content必填，其他可选）
 *
 * @see
 */
public class ForumPostParam extends ForumBaseType {

    /** 必填，版块uuid */
    @JsonProperty("forum_id")
    private String mForumId;
    /** 标题，必选 */
    @JsonProperty("title")
    private String mTitle;
    /** 帖子内容,必须 */
    @JsonProperty("article")
    private String mContent;

    /** 图片id列表，半角逗号分隔，可选 */
    @JsonProperty("image_list")
    private String mImageList;

    /** 音视频附件id，可选，如果mCategory为AUDIO或者VIDEO，则必选 */
    @JsonProperty("attach_id")
    private String mAttachId;

    /**
     *  主贴类别，EXT, AUDIO, VIDEO，默认为TEXT，可选
     *  @see com.forum.sdk.common.ForumConstDefine.ContentCategory
     */
    @JsonProperty("content_category")
    private String mCategory;
    /** 地理位置，可选 */
    @JsonProperty("geo")
    private ForumGeographyInfo mForumGeo;
    /** 附加信息，可选 */
    @JsonProperty("addition")
    private String mAddition;
    /** 客户端类型，可选 */
    @JsonProperty("source")
    private String mSource = ForumConstDefine.FORUM_CLIENT_TYPE;


    /**
     * 获取版块uuid
     *
     * @return com.morningbaby.forum id
     */
    @JsonProperty("forumId")
    public String getForumId() {
        return mForumId;
    }

    /**
     * 设置版块uuid
     *
     * @param forumId the com.morningbaby.forum id
     */
    @JsonProperty("forumId")
    public void setForumId(String forumId) {
        this.mForumId = forumId;
    }

    /**
     * 获取主题内容
     * @return content content
     */
    @JsonProperty("article")
    public String getContent() {
        return mContent;
    }

    /**
     * 设置主题内容
     *
     * @param content the content
     */
    @JsonProperty("article")
    public void setContent(String content) {
        this.mContent = content;
    }

    /**
     * 获取标题
     *
     * @return title title
     */
    @JsonProperty("title")
    public String getTitle() {
        return mTitle;
    }

    /**
     * 设置标题
     *
     * @param title the title
     */
    @JsonProperty("title")
    public void setTitle(String title) {
        this.mTitle = title;
    }

    /**
     * 获取图片id列表
     *
     * @return image list
     */
    @JsonProperty("image_list")
    public String getImageList() {
        return mImageList;
    }

    /**
     * 设置图片id列表
     *
     * @param imageList the image list
     */
    @JsonProperty("image_list")
    public void setImageList(String imageList) {
        this.mImageList = imageList;
    }

    /**
     * 获取音视频附件id
     *
     * @return mAttachId
     */
    @JsonProperty("attach_id")
    public String getAttachId() {
        return mAttachId;
    }

    /**
     * 设置音视频附件id
     *
     * @param attachId
     */
    @JsonProperty("attach_id")
    public void setAttachId(String attachId) {
        this.mAttachId = attachId;
    }

    /**
     * 获取地理信息
     *
     * @return com.morningbaby.forum geo
     */
    @JsonProperty("geo")
    public ForumGeographyInfo getForumGeo() {
        return mForumGeo;
    }

    /**
     * 设置地理信息
     *
     * @param forumGeo the com.morningbaby.forum geo
     */
    @JsonProperty("geo")
    public void setForumGeo(ForumGeographyInfo forumGeo) {
        this.mForumGeo = forumGeo;
    }

    /**
     * 获取附加信息
     *
     * @return addition addition
     */
    @JsonProperty("addition")
    public String getAddition() {
        return mAddition;
    }

    /**
     * 设置附加信息
     *
     * @param addition the addition
     */
    @JsonProperty("addition")
    public void setAddition(String addition) {
        this.mAddition = addition;
    }

    /**
     * 获取客户端类型
     *
     * @return the source
     * @author  : huangszh
     */
    @JsonProperty("source")
    public String getSource() {
        return mSource;
    }

    /**
     * 设置客户端类型
     *
     * @param source the source
     */
    @JsonProperty("source")
    public void setSource(String source) {
        mSource = source;
    }

    /**
     * 获取主贴类别
     *
     * @return the category
     * @see com.forum.sdk.common.ForumConstDefine.ContentCategory
     */
    @JsonProperty("content_category")
    public String getCategory() {
        return mCategory;
    }

    /**
     * 设置主贴类别
     *
     * @param category the category
     * @see com.forum.sdk.common.ForumConstDefine.ContentCategory
     */
    @JsonProperty("content_category")
    public void setCategory(String category) {
        mCategory = category;
    }

}
