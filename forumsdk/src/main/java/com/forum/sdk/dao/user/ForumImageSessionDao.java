package com.forum.sdk.dao.user;


import com.forum.sdk.bean.user.ForumImageSession;
import com.forum.sdk.common.ForumConstDefine;
import com.forum.sdk.common.ForumRequireUrl;
import com.forum.sdk.common.ForumRestDao;
import com.nd.smartcan.frame.dao.RestDao;
import com.nd.smartcan.frame.exception.DaoException;

import java.io.File;

/**
 * Created by Administrator on 2015/4/30.
 */
public class ForumImageSessionDao extends ForumRestDao<ForumImageSession> {
    @Override
    protected String getResourceUri() {
        return ForumRequireUrl.IMAGE_URL;
    }

    /**
     * 内容服务的上传授权session获取
     * @return ForumImageSession
     * @throws DaoException
     */
    public ForumImageSession getImageSession() throws DaoException{
        StringBuilder url = new StringBuilder(getResourceUri());
        url.append(File.separator).append(ForumConstDefine.ParamKeyConst.SESSION);
        return get(url.toString(),null,ForumImageSession.class);
    }
}
