package com.forum.sdk.dao.thread.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;
import com.forum.sdk.bean.geo.ForumGeographyInfo;
import com.forum.sdk.common.ForumConstDefine;


/**
 * 发布跟帖需要的参数 （主贴uuid和内容content必填，其他可选）
 *
 * @author  : huangszh
 * @date 2015年04月02日 15:40:00
 * @see
 */
public class ForumThreadParam extends ForumBaseType {

    /** 必填，主贴uuid */
    @JsonProperty("post_id")
    private String mPostId;
    /** 帖子内容,必须 */
    @JsonProperty("article")
    private String mContent;
    /**
     * 跟帖类别,可选TEXT, AUDIO, VIDEO,默认TEXT
     * @see com.forum.sdk.common.ForumConstDefine.ContentCategory
     */
    @JsonProperty("content_category")
    private String mCategory;
    /** 图片id列表，半角逗号分隔，可选 */
    @JsonProperty("image_list")
    private String mImageList;
    /** 音视频附件id，可选，如果mCategory为AUDIO或者VIDEO，则必选 */
    @JsonProperty("attach_id")
    private String mAttachId;
    /** 客户端类型，可选 */
    @JsonProperty("source")
    private String mSource = ForumConstDefine.FORUM_CLIENT_TYPE;
    /** 地理位置，可选 */
    @JsonProperty("geo")
    private ForumGeographyInfo mGeo;
    /** 附加信息，可选 */
    @JsonProperty("addition")
    private String mAddition;


    /**
     * 获取主贴id
     *
     * @return  post id
     * @author  : huangszh
     * @date  : 2015年04月02日 14:21:39
     */
    @JsonProperty("post_id")
    public String getPostId() {
        return mPostId;
    }

    /**
     * 设置主贴id
     *
     * @param postId the post id
     * @author  : huangszh
     * @date  : 2015年04月02日 14:21:39
     */
    @JsonProperty("post_id")
    public void setPostId(String postId) {
        this.mPostId = postId;
    }

    /**
     * 获取跟帖内容
     *
     * @return  content
     * @author  : huangszh
     * @date  : 2015年04月02日 14:21:39
     */
    @JsonProperty("article")
    public String getContent() {
        return mContent;
    }

    /**
     * 设置跟帖内容
     *
     * @param content the content
     * @author  : huangszh
     * @date  : 2015年04月02日 14:21:39
     */
    @JsonProperty("article")
    public void setContent(String content) {
        this.mContent = content;
    }

    /**
     * 获取图片id列表
     *
     * @return  image list
     * @author  : huangszh
     * @date  : 2015年04月02日 14:21:39
     */
    @JsonProperty("image_list")
    public String getImageList() {
        return mImageList;
    }

    /**
     * 设置图片id列表
     *
     * @param imageList the image list
     * @author  : huangszh
     * @date  : 2015年04月02日 14:21:39
     */
    @JsonProperty("image_list")
    public void setImageList(String imageList) {
        this.mImageList = imageList;
    }

    /**
     * 获取音视频附件id
     *
     * @return mAttachId
     * @author  : huangszh
     * @date  : 2015年04月02日 14:21:39
     */
    @JsonProperty("attach_id")
    public String getAttachId() {
        return mAttachId;
    }

    /**
     * 设置音视频附件id
     *
     * @param attachId
     * @author  : huangszh
     * @date  : 2015年04月02日 14:21:39
     */
    @JsonProperty("attach_id")
    public void setAttachId(String attachId) {
        this.mAttachId = attachId;
    }

    /**
     * 获取跟帖类别
     *
     * @return the category
     * @author  : huangszh
     * @see com.forum.sdk.common.ForumConstDefine.ContentCategory
     */
    @JsonProperty("content_category")
    public String getCategory() {
        return mCategory;
    }

    /**
     * 设置跟帖类别
     *
     * @param category the category
     * @author  : huangszh
     * @see com.forum.sdk.common.ForumConstDefine.ContentCategory
     */
    @JsonProperty("content_category")
    public void setCategory(String category) {
        mCategory = category;
    }

    /**
     * 获取来源
     *
     * @return the source
     * @author  : huangszh
     */
    @JsonProperty("source")
    public String getSource() {
        return mSource;
    }

    /**
     * 设置来源
     *
     * @param source the source
     * @author  : huangszh
     */
    @JsonProperty("source")
    public void setSource(String source) {
        mSource = source;
    }

    /**
     * 获取地理位置信息
     *
     * @return the geo
     * @author  : huangszh
     */
    @JsonProperty("geo")
    public ForumGeographyInfo getGeo() {
        return mGeo;
    }

    /**
     * 设置地理位置信息
     *
     * @param geo the geo
     * @author  : huangszh
     */
    @JsonProperty("geo")
    public void setGeo(ForumGeographyInfo geo) {
        mGeo = geo;
    }

    /**
     * 获取附加信息
     *
     * @return the addition
     * @author  : huangszh
     */
    @JsonProperty("addition")
    public String getAddition() {
        return mAddition;
    }

    /**
     * 设置附加信息
     *
     * @param addition the addition
     * @author  : huangszh
     */
    @JsonProperty("addition")
    public void setAddition(String addition) {
        mAddition = addition;
    }
}

