package com.forum.sdk.dao.section.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;

/**
 * 版块搜索所需参数
 *
 * @author  : huangszh
 * @date 2015年04月13日 14:48:54
 * @see
 */
public class ForumSectionSearchParam extends ForumBaseType {

    /** 标签名 */
    @JsonProperty("tag_name")
    private String mTagName;
    /** 版块名 */
    @JsonProperty("name")
    private String mSectionName;

    /**
     * 获取版块名
     *
     * @return  section name
     * @author  : huangszh
     * @date  : 2015年04月13日 14:48:54
     */
    @JsonProperty("name")
    public String getSectionName() {
        return mSectionName;
    }

    /**
     * 设置版块名
     *
     * @param sectionName the section name
     * @author  : huangszh
     * @date  : 2015年04月13日 14:48:54
     */
    @JsonProperty("name")
    public void setSectionName(String sectionName) {
        this.mSectionName = sectionName;
    }

    /**
     * 获取标签名
     *
     * @return  tag name
     * @author  : huangszh
     * @date  : 2015年04月13日 14:48:55
     */
    @JsonProperty("tag_name")
    public String getTagName() {
        return mTagName;
    }

    /**
     * 设置标签名
     *
     * @param tagName the tag name
     * @author  : huangszh
     * @date  : 2015年04月13日 14:48:55
     */
    @JsonProperty("tag_name")
    public void setTagName(String tagName) {
        this.mTagName = tagName;
    }
}
