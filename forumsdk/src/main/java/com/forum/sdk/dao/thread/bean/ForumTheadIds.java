package com.forum.sdk.dao.thread.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;


import java.util.List;

/**
 * 跟帖id列表
 *
 * <br>Created 2015年2月28日 下午5:51:29
 * @author  : huangszh
 */
public class ForumTheadIds extends ForumBaseType {

    /**
     * 
     */
    private static final long serialVersionUID = 6751267460132039379L;
    
    /** 跟帖uuid列表，最多不超过100条*/
    @JsonProperty("thread_ids")
    private List<String> objectIds;

    /**
     * 跟帖uuid列表，最多不超过100条
     * @return  object ids
     * @author  : huangszh
     */
    @JsonProperty("thread_ids")
    public List<String> getObjectIds() {
        return objectIds;
    }

    /**
     * 跟帖uuid列表，最多不超过100条
     * @param objectIds List<String>
     * @author  : huangszh
     */
    @JsonProperty("thread_ids")
    public void setObjectIds(List<String> objectIds) {
        this.objectIds = objectIds;
    }
    
}
