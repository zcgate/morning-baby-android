package com.forum.sdk.dao.section;

import com.forum.sdk.ForumConfigManager;
import com.forum.sdk.bean.section.ForumCategoryList;
import com.forum.sdk.bean.section.ForumSearchList;
import com.forum.sdk.bean.section.ForumSectionInfo;
import com.forum.sdk.bean.section.ForumSectionList;
import com.forum.sdk.bean.tag.ForumTagList;
import com.forum.sdk.common.ForumConstDefine;
import com.forum.sdk.common.ForumRequireUrl;
import com.forum.sdk.common.ForumRestDao;
import com.forum.sdk.dao.section.bean.ForumSectionParam;
import com.forum.sdk.dao.section.bean.ForumSectionSearchParam;
import com.nd.smartcan.frame.exception.DaoException;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * 版块请求处理
 *
 * @author  : huangszh
 * @date 2015年04月01日 13:42:26
 * @see
 */
public class ForumSectionDao extends ForumRestDao<ForumSectionParam> {

    @Override
    protected String getResourceUri() {
        return ForumRequireUrl.SECTION_URL;
    }

    /**
     * 创建版块
     *
     * @param sectionPost 创建所需参数
     * @return  不为null时，表示创建成功
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月01日 10:27:54
     */
    public ForumSectionInfo createSection(ForumSectionParam sectionPost) throws DaoException{

        return post(sectionPost,null,ForumSectionInfo.class);
    }

    /**
     * 修改版块信息
     *
     * @param sectionId 版块id
     * @param sectionPost 修改的版块信息
     * @return  section info
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月01日 10:27:51
     */
    public ForumSectionInfo editSection(String sectionId,ForumSectionParam sectionPost) throws DaoException{
        setObjId(sectionId);
        return put(sectionPost,null,ForumSectionInfo.class);
    }

    /**
     * 获取版块信息
     *
     * @param sectionId 版块id
     * @return  section info
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月01日 10:28:39
     */
    public ForumSectionInfo getSectionInfo(String sectionId) throws DaoException{
        StringBuilder url = new StringBuilder(getResourceUri());
        url.append(File.separator).append(sectionId);
        return get(url.toString(),null,ForumSectionInfo.class);
    }

    /**
     *获取标签列表
     *
     * @param page 页偏移量，不小于0的值，传错置为0，默认为0
     * @param size 每页条数限制，默认20，最大100，传错置为20
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return  tag list
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:00:07
     */
    public ForumTagList getTagList(int page,int size, boolean isNeedCount) throws DaoException{

        StringBuilder url = new StringBuilder(getResourceUri());
        url.append(File.separator).append(ForumConstDefine.ParamKeyConst.TAG_LIST)
                .append("?").append(ForumConstDefine.ParamKeyConst.PAGE).append("=").append(page)
                .append("&").append(ForumConstDefine.ParamKeyConst.SIZE).append("=").append(size)
                .append("&").append(ForumConstDefine.ParamKeyConst.$COUNT).append("=").append(isNeedCount);
        return get(url.toString(),null,ForumTagList.class);
    }

    /**
     * 获取我订阅的版块列表
     *
     * @param page 页偏移量，不小于0的值，传错置为0，默认为0
     * @param size 每页条数限制，默认20，最大100，传错置为20
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return  my follow section list
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:00:07
     */
    public ForumSectionList getMyFollowSectionList(int page, int size, boolean isNeedCount) throws DaoException{
        StringBuilder url = new StringBuilder(getResourceUri());
        url.append(File.separator).append(ForumConstDefine.ParamKeyConst.SUBSCRIBED)
                .append(File.separator).append(ForumConfigManager.INSTANCE.getCurrentUid())
                .append("?").append(ForumConstDefine.ParamKeyConst.PAGE).append("=").append(page)
                .append("&").append(ForumConstDefine.ParamKeyConst.SIZE).append("=").append(size)
                .append("&").append(ForumConstDefine.ParamKeyConst.$COUNT).append("=").append(isNeedCount);
        return get(url.toString(),null,ForumSectionList.class);
    }


    /**
     * 获取我管理的版块列表
     *
     * @param page 页偏移量，不小于0的值，传错置为0，默认为0
     * @param size 每页条数限制，默认20，最大100，传错置为20
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return  my manage section list
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:00:08
     */
    public ForumSectionList getMyManageSectionList(int page, int size, boolean isNeedCount) throws DaoException{
        StringBuilder url = new StringBuilder(getResourceUri());
        url.append(File.separator).append(ForumConstDefine.ParamKeyConst.MANAGED)
                .append(File.separator).append(ForumConfigManager.INSTANCE.getCurrentUid())
                .append("?").append(ForumConstDefine.ParamKeyConst.PAGE).append("=").append(page)
                .append("&").append(ForumConstDefine.ParamKeyConst.SIZE).append("=").append(size)
                .append("&").append(ForumConstDefine.ParamKeyConst.$COUNT).append("=").append(isNeedCount);
        return get(url.toString(),null,ForumSectionList.class);
    }

    /**
     * 获取热门版块列表
     *
     * @param page 页偏移量，不小于0的值，传错置为0，默认为0
     * @param size 每页条数限制，默认20，最大100，传错置为20
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return  hot section list
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月01日 14:02:18
     */
    public ForumSectionList getHotSectionList(int page,int size, boolean isNeedCount) throws DaoException{
        StringBuilder url = new StringBuilder(getResourceUri());
        url.append(File.separator).append(ForumConstDefine.ParamKeyConst.HOT)
        .append("?").append(ForumConstDefine.ParamKeyConst.PAGE).append("=").append(page)
        .append("&").append(ForumConstDefine.ParamKeyConst.SIZE).append("=").append(size)
        .append("&").append(ForumConstDefine.ParamKeyConst.$COUNT).append("=").append(isNeedCount);
        return get(url.toString(),null,ForumSectionList.class);
    }

    /**
     * 搜索版块
     *
     * @param tag 标签名称，和name至少要填一个
     * @param name 版块名称，和tag至少要填一个
     * @param page 页偏移量，不小于0的值，传错置为0，默认为0
     * @param size 每页条数限制，默认20，最大100，传错置为20
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return  com.morningbaby.forum section list
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:00:08
     */
    public ForumSectionList searchSectionList(String tag, String name, int page, int size, boolean isNeedCount) throws DaoException{
        StringBuilder url = new StringBuilder(getResourceUri());
        url.append(File.separator).append(ForumConstDefine.ParamKeyConst.SEARCH)
                .append("?").append(ForumConstDefine.ParamKeyConst.PAGE).append("=").append(page)
                .append("&").append(ForumConstDefine.ParamKeyConst.SIZE).append("=").append(size)
                .append("&").append(ForumConstDefine.ParamKeyConst.$COUNT).append("=").append(isNeedCount);

        ForumSectionSearchParam sectionSearchParam = new ForumSectionSearchParam();
        sectionSearchParam.setSectionName(name);
        sectionSearchParam.setTagName(tag);
        return post(url.toString(),sectionSearchParam,null,ForumSectionList.class);
    }

    /**
     * 获取版块的类别列表
     *
     * @param page the page
     * @param size the size
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return the category list
     * @throws DaoException the dao exception
     * @author  : huangszh
     */
    public ForumCategoryList getCategoryList(int page, int size, boolean isNeedCount) throws DaoException{
        StringBuilder url = new StringBuilder(getResourceUri());
        url.append(File.separator).append(ForumConstDefine.ParamKeyConst.CATEGORY_LIST)
        .append("?").append(ForumConstDefine.ParamKeyConst.PAGE).append("=").append(page)
        .append("&").append(ForumConstDefine.ParamKeyConst.SIZE).append("=").append(size)
        .append("&").append(ForumConstDefine.ParamKeyConst.$COUNT).append("=").append(isNeedCount);
        return get(url.toString(),null,ForumCategoryList.class);

    }

    /**
     * 根据分类id获取版块列表
     *
     * @param id the id
     * @param page the page
     * @param size the size
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return the section list by category id
     * @throws DaoException the dao exception
     * @author  : huangszh
     */
    public ForumSectionList getSectionListByCategoryId(String id, int page, int size, boolean isNeedCount) throws DaoException{

        StringBuilder url = new StringBuilder(getResourceUri());
        url.append(File.separator).append(ForumConstDefine.ParamKeyConst.CATEGORY)
                .append(File.separator).append(id)
                .append("?").append(ForumConstDefine.ParamKeyConst.PAGE).append("=").append(page)
                .append("&").append(ForumConstDefine.ParamKeyConst.SIZE).append("=").append(size)
                .append("&").append(ForumConstDefine.ParamKeyConst.$COUNT).append("=").append(isNeedCount);
        return get(url.toString(),null,ForumSectionList.class);
    }

    /**
     * 版块搜索关键字列表
     *
     * @param page the page
     * @param size the size
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return the section list by category id
     * @throws DaoException the dao exception
     * @author  : zhuhp
     */
    public ForumSearchList getSearchInfoList(int page, int size, boolean isNeedCount) throws  DaoException{
        StringBuilder url = new StringBuilder(getResourceUri());
        url.append(File.separator).append(ForumConstDefine.ParamKeyConst.SEARCH_LIST)
                .append("?").append(ForumConstDefine.ParamKeyConst.PAGE).append("=").append(page)
                .append("&").append(ForumConstDefine.ParamKeyConst.SIZE).append("=").append(size)
                .append("&").append(ForumConstDefine.ParamKeyConst.$COUNT).append("=").append(isNeedCount);
        return get(url.toString(),null,ForumSearchList.class);
    }
}
