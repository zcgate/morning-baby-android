package com.forum.sdk.dao.counter;


import com.forum.sdk.bean.counter.ForumUnreadCounter;
import com.forum.sdk.common.ForumConstDefine;
import com.forum.sdk.common.ForumRequireUrl;
import com.forum.sdk.common.ForumRestDao;
import com.nd.smartcan.frame.dao.RestDao;
import com.nd.smartcan.frame.exception.DaoException;

import java.io.File;

/**
 * 计数器相关处理
 *
 * @see
 */
public class ForumCounterDao extends ForumRestDao<ForumUnreadCounter> {

    @Override
    protected String getResourceUri() {

        return ForumRequireUrl.COUNTER_URL;
    }

    /**
     * 获取未读计数信息
     *
     * @return unread counter
     * @throws DaoException the dao exception
     */
    public ForumUnreadCounter getUnreadCounter() throws DaoException{

        StringBuilder url = new StringBuilder(getResourceUri());
        url.append(File.separator).append(ForumConstDefine.ParamKeyConst.UNREAD);

        return get(url.toString(),null,ForumUnreadCounter.class);

    }

}
