package com.forum.sdk.dao.post.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;

import java.util.List;

/**
 * 主帖id列表
 *
 * <br>Created 2015年2月28日 下午5:51:29
 * @author  : huangszh
 */
public class ForumPostIds extends ForumBaseType {

    /**
     * 
     */
    private static final long serialVersionUID = 6751267460132039379L;
    
    /** 主帖uuid列表，最多不超过100条*/
    @JsonProperty("object_ids")
    private List<String> objectIds;

    /**
     * 主帖uuid列表，最多不超过100条
     * @return  object ids
     */
    @JsonProperty("object_ids")
    public List<String> getObjectIds() {
        return objectIds;
    }

    /**
     * 主帖uuid列表，最多不超过100条
     * @param objectIds List<String>
     * @author  : huangszh
     */
    @JsonProperty("object_ids")
    public void setObjectIds(List<String> objectIds) {
        this.objectIds = objectIds;
    }
    
    
    
}
