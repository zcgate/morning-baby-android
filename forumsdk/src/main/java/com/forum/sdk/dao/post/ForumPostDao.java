package com.forum.sdk.dao.post;


import com.forum.sdk.bean.counter.ForumPostCounter;
import com.forum.sdk.bean.counter.ForumPostCounterList;
import com.forum.sdk.bean.post.ForumPostInfo;
import com.forum.sdk.bean.post.ForumPostList;
import com.forum.sdk.common.ForumConstDefine;
import com.forum.sdk.common.ForumRequireUrl;
import com.forum.sdk.common.ForumRestDao;
import com.forum.sdk.dao.post.bean.ForumPostIds;
import com.forum.sdk.dao.post.bean.ForumPostParam;
import com.nd.smartcan.frame.dao.RestDao;
import com.nd.smartcan.frame.exception.DaoException;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 主贴相关处理

 */
public class ForumPostDao extends ForumRestDao<ForumPostParam> {

    @Override
    protected String getResourceUri() {
        return ForumRequireUrl.POST_URL;
    }


    /**
     * 发布主帖
     *
     * @param forumPostParam 主帖信息
     * @return ForumPost post
     * @throws DaoException the dao exception
     */
    public ForumPostInfo createPost(ForumPostParam forumPostParam) throws DaoException {

        return post(forumPostParam,null,ForumPostInfo.class);
    }

    /**
     * 获取主帖详情
     *
     * @param postId 主帖id
     * @return ForumPostInfo post detail
     * @throws DaoException the dao exception
     */
    public ForumPostInfo getPostDetail(String postId) throws DaoException {
        StringBuilder url = new StringBuilder(getResourceUri());
        url.append(File.separator).append(postId);
        return get(url.toString(),null,ForumPostInfo.class);
    }

    /**
     * 删除主帖
     *
     * @param postId 主帖id
     * @return ForumPostInfo com.morningbaby.forum post info
     * @throws DaoException the dao exception
     */
    public ForumPostInfo deletePost(String postId) throws DaoException {

        setObjId(postId);
        return delete(null,ForumPostInfo.class);
    }

    /**
     * 批量获取主贴详情
     *
     * @param idList 主帖id列表
     * @return post list
     * @throws DaoException the dao exception
     */
    public ForumPostList getPostDetailList(List<String> idList) throws DaoException {
        if(idList == null || idList.isEmpty()){
            ForumPostList list = new ForumPostList();
            list.setItems(new ArrayList<ForumPostInfo>());
            return list;
        }
        StringBuilder url = new StringBuilder(getResourceUri())
                .append(File.separator).append(ForumConstDefine.ParamKeyConst.LIST);
        ForumPostIds list = new ForumPostIds();
        list.setObjectIds(idList);
        return post(url.toString(), list, null, ForumPostList.class);
    }

    /**
     * 批量获取主帖计数信息
     *
     * @param idList 主帖id列表
     * @return post counter list
     * @throws DaoException the dao exception
     */
    public ForumPostCounterList getPostCounterList(List<String> idList) throws DaoException {
        if(idList == null || idList.isEmpty()){
            ForumPostCounterList list = new ForumPostCounterList();
            list.setItems(new ArrayList<ForumPostCounter>());
            return list;
        }
        StringBuilder url = new StringBuilder(getResourceUri())
                .append(File.separator).append(ForumConstDefine.ParamKeyConst.COUNTER_LIST);
        ForumPostIds list = new ForumPostIds();
        list.setObjectIds(idList);
        return post(url.toString(), list, null, ForumPostCounterList.class);
    }

}
