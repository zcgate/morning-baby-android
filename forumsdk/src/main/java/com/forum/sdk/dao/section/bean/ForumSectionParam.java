package com.forum.sdk.dao.section.bean;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;


/**
 * 创建 /修改版块需要的参数信息
 *      1.创建时，版块名name,版块简介intro和标签名列表tagList必填，其他可选。
 *      2.修改时，都可选，但是必须有一个有值修改才有意义。
 *
 * @see
 */
public class ForumSectionParam extends ForumBaseType {

    /** 创建时必填，版块名 */
    @JsonProperty("name")
    private String mName;
    /** 创建时选填，版块简介 */
    @JsonProperty("intro")
    private String mIntro;
    /** 创建时选填，标签名列表，多个标签中间使用逗号隔开 （如：闲聊,胡扯）*/
    @JsonProperty("tag_list")
    private String mTagList;
    /** 必填，版块类别 */
    @JsonProperty("category")
    private String mCategory;
    /** 选填，版块图标id */
    @JsonProperty("image_id")
    private String mImageId;
    /** 选填，扩展信息 */
    @JsonProperty("addition")
    private String mAddition;
    /** 订阅该版块是否需要审核(0:不需要,1：需要,默认为0.)*/
    @JsonProperty("check")
    private int mCheck;

    /**
     * 获取版块是否需要审核
     * @return check
     */
    @JsonProperty("check")
    public int getCheck() {
        return mCheck;
    }

    /**
     * 设置是否需要审核
     * @param check check
     */
    @JsonProperty("check")
    public void setCheck(int check) {
        this.mCheck = check;
    }

    /**
     * 获取图片id列表
     *
     * @return  image id
     * @author  : huangszh
     * @date  : 2015年04月02日 11:04:52
     */
    @JsonProperty("image_id")
    public String getImageId() {
        return mImageId;
    }

    /**
     * 设置图片id列表
     *
     * @param imageId the image id
     * @author  : huangszh
     * @date  : 2015年04月02日 11:04:52
     */
    @JsonProperty("image_id")
    public void setImageId(String imageId) {
        this.mImageId = imageId;
    }

    /**
     * 获取版块名
     *
     * @return  name
     * @author  : huangszh
     * @date  : 2015年04月02日 11:04:52
     */
    @JsonProperty("name")
    public String getName() {
        return mName;
    }

    /**
     * 设置版块名
     *
     * @param name the name
     * @author  : huangszh
     * @date  : 2015年04月02日 11:04:52
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.mName = name;
    }

    /**
     * 获取版块简介
     *
     * @return  intro
     * @author  : huangszh
     * @date  : 2015年04月02日 11:04:52
     */
    @JsonProperty("intro")
    public String getIntro() {
        return mIntro;
    }

    /**
     * 设置版块简介
     *
     * @param intro the intro
     * @author  : huangszh
     * @date  : 2015年04月02日 11:04:52
     */
    @JsonProperty("intro")
    public void setIntro(String intro) {
        this.mIntro = intro;
    }

    /**
     * 获取标签列表
     *
     * @return  tag list
     * @author  : huangszh
     * @date  : 2015年04月02日 11:04:52
     */
    @JsonProperty("tag_list")
    public String getTagList() {
        return mTagList;
    }

    /**
     * 设置标签列表
     *
     * @param tagList the tag list
     * @author  : huangszh
     * @date  : 2015年04月02日 11:04:52
     */
    @JsonProperty("tag_list")
    public void setTagList(String tagList) {
        this.mTagList = tagList;
    }

    /**
     * 获取附加信息
     *
     * @return  addition
     * @author  : huangszh
     * @date  : 2015年04月02日 11:04:52
     */
    @JsonProperty("addition")
    public String getAddition() {
        return mAddition;
    }

    /**
     * 设置附加信息
     *
     * @param addition the addition
     * @author  : huangszh
     * @date  : 2015年04月02日 11:04:52
     */
    @JsonProperty("addition")
    public void setAddition(String addition) {
        this.mAddition = addition;
    }


    /**
     * 获取版块分类
     *
     * @return  category
     * @author  : huangszh
     * @date  : 2015年04月02日 14:42:53
     */
    @JsonProperty("category")
    public String getCategory() {
        return mCategory;
    }

    /**
     * 设置版块分类
     *
     * @param category the category
     * @author  : huangszh
     * @date  : 2015年04月02日 14:42:55
     */
    @JsonProperty("category")
    public void setCategory(String category) {
        this.mCategory = category;
    }
}
