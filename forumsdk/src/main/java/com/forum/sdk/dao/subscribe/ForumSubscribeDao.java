package com.forum.sdk.dao.subscribe;


import com.forum.sdk.bean.user.ForumSectionUserInfo;
import com.forum.sdk.bean.user.ForumSectionUserList;
import com.forum.sdk.common.ForumConstDefine;
import com.forum.sdk.common.ForumRequireUrl;
import com.forum.sdk.common.ForumRestDao;
import com.forum.sdk.dao.subscribe.bean.ForumSubscribeParam;
import com.forum.sdk.dao.subscribe.bean.ForumUserParam;
import com.nd.smartcan.frame.dao.RestDao;
import com.nd.smartcan.frame.exception.DaoException;

import java.io.File;

/**
 * 订阅相关处理
 *
 * @author  : huangszh
 * @date 2015年04月02日 14:56:47
 * @see
 */
public class ForumSubscribeDao extends ForumRestDao<ForumSectionUserInfo> {

    @Override
    protected String getResourceUri() {
        return ForumRequireUrl.SUBSCRIBE_URL;
    }

    /**
     * 订阅版块
     *
     * @param sectionId 版块id
     * @return com.morningbaby.forum section user info
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:13:20
     */
    public ForumSectionUserInfo followSection(String sectionId) throws DaoException{
        ForumSubscribeParam subscribeParam = new ForumSubscribeParam();
        subscribeParam.setSectionId(sectionId);
        return post(getResourceUri(),subscribeParam,null,ForumSectionUserInfo.class);
    }

    /**
     * 取消订阅版块
     *
     * @param sectionId 版块id
     * @return com.morningbaby.forum section user info
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:13:20
     */
    public ForumSectionUserInfo unFollowSection(String sectionId) throws DaoException{
        setObjId(sectionId);
        return delete(null, ForumSectionUserInfo.class);
    }

    /**
     * 修改版块成员角色
     *
     * @param sectionId 版块id
     * @param uid 用户id
     * @param role 角色，1:版主,2:管理员,3:普通成员
     * @return com.morningbaby.forum section user info
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:13:20
     */
    public ForumSectionUserInfo editSectionMemberRole(String sectionId, long uid, int role) throws DaoException{

        StringBuilder url = new StringBuilder(getResourceUri());
        url.append(File.separator).append(ForumConstDefine.ParamKeyConst.ROLE);
        ForumUserParam userParam = new ForumUserParam();
        userParam.setSectionId(sectionId);
        userParam.setUid(uid);
        userParam.setRole(role);
        return post(url.toString(), userParam, null, ForumSectionUserInfo.class);
    }

    /**
     * 获取版块成员列表
     *
     * @param sectionId 版块id
     * @param page 页偏移量，不小于0的值，传错置为0，默认为0
     * @param size 每页条数限制，默认20，最大100，传错置为20
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return list section member
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:13:20
     */
    public ForumSectionUserList getSectionMemberList(String sectionId, int page, int size, boolean isNeedCount) throws DaoException{
        StringBuilder url = new StringBuilder(getResourceUri());
        url.append(File.separator).append(sectionId)
                .append("?").append(ForumConstDefine.ParamKeyConst.PAGE).append("=").append(page)
                .append("&").append(ForumConstDefine.ParamKeyConst.SIZE).append("=").append(size)
                .append("&").append(ForumConstDefine.ParamKeyConst.$COUNT).append("=").append(isNeedCount);
        return get(url.toString(),null,ForumSectionUserList.class);
    }
}
