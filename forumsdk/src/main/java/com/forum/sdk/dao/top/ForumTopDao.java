package com.forum.sdk.dao.top;

import com.forum.sdk.bean.post.ForumPostInfo;
import com.forum.sdk.common.ForumConstDefine;
import com.forum.sdk.common.ForumRequireUrl;
import com.forum.sdk.common.ForumRestDao;
import com.nd.smartcan.frame.dao.RestDao;
import com.nd.smartcan.frame.exception.DaoException;

import java.io.File;

/**
 * 置顶相关处理
 *
 * @author  : huangszh
 * @date 2015年04月02日 16:13:02
 * @see
 */
public class ForumTopDao extends ForumRestDao<ForumPostInfo> {

    @Override
    protected String getResourceUri() {
        return ForumRequireUrl.TOP_URL;
    }

    /**
     * 置顶主帖
     *
     * @param postId 主帖id
     * @return com.morningbaby.forum post info
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:25:52
     */
    public ForumPostInfo topPost(String postId) throws DaoException {

        StringBuilder url = new StringBuilder(getResourceUri());
        url.append(File.separator).append(ForumConstDefine.ParamKeyConst.POSTS)
                .append(File.separator).append(postId);

        return put(url.toString(),null,null,ForumPostInfo.class);
    }

    /**
     * 取消置顶主帖
     *
     * @param postId 主帖id
     * @return ForumPost com.morningbaby.forum post info
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:25:52
     */
    public ForumPostInfo cancelTopPost(String postId) throws DaoException {
        StringBuilder url = new StringBuilder(getResourceUri());
        url.append(File.separator).append(ForumConstDefine.ParamKeyConst.POSTS)
                .append(File.separator).append(postId);

        return delete(url.toString(),null,ForumPostInfo.class);
    }

}
