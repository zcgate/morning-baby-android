package com.forum.sdk.dao.subscribe.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;

/**
 * 订阅版块所需参数
 *
 * @author  : huangszh
 * @date 2015年04月13日 14:49:31
 * @see
 */
public class ForumSubscribeParam extends ForumBaseType {

    /** 版块uuid */
    @JsonProperty("forum_id")
    private String mSectionId;

    /**
     * 获取版块id
     *
     * @return  section id
     * @author  : huangszh
     * @date  : 2015年04月13日 14:49:31
     */
    @JsonProperty("forum_id")
    public String getSectionId() {
        return mSectionId;
    }

    /**
     * 设置版块id
     *
     * @param sectionId the section id
     * @author  : huangszh
     * @date  : 2015年04月13日 14:49:31
     */
    @JsonProperty("forum_id")
    public void setSectionId(String sectionId) {
        this.mSectionId = sectionId;
    }

}
