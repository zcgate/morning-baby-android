package com.forum.sdk.dao.section;


import com.forum.sdk.bean.section.ForumSectionInfo;
import com.forum.sdk.common.ForumConstDefine;
import com.nd.smartcan.frame.dao.OrmDao;

/**
 * Created by zhuhp on 2015/7/3.
 */
public class ForumSectionOrmDao extends OrmDao<ForumSectionInfo,String>{

    public static ForumSectionOrmDao dao = new ForumSectionOrmDao();

    public static void clearCurrentUserCacheData(long uid){
        dao.executeRaw("delete from "+ ForumConstDefine.TableName.TABLE_SECTION +" where "+ForumConstDefine.TableColumnName.COLUMN_CURRENT_UID+" ='"+uid+"'");
    }
}
