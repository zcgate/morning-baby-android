package com.forum.sdk.dao.subscribe.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;

/**
 * 订阅所需参数
 *
 * @author  : huangszh
 * @date 2015年04月13日 14:49:53
 * @see
 */
public class ForumUserParam extends ForumBaseType {

    /** 创建时必填，版块名 */
    @JsonProperty("uid")
    private long mUid;
    /** 创建时必填，版块名 */
    @JsonProperty("forum_id")
    private String mSectionId;
    /** 创建时必填，版块名 */
    @JsonProperty("role")
    private int mRole;

    /**
     *获取用户uid
     *
     * @return  uid
     * @author  : huangszh
     * @date  : 2015年04月13日 14:49:53
     */
    @JsonProperty("uid")
    public long getUid() {
        return mUid;
    }

    /**
     * 设置用户uid
     *
     * @param uid the uid
     * @author  : huangszh
     * @date  : 2015年04月13日 14:49:53
     */
    @JsonProperty("uid")
    public void setUid(long uid) {
        this.mUid = uid;
    }

    /**
     * 获取版块id
     *
     * @return  section id
     * @author  : huangszh
     * @date  : 2015年04月13日 14:49:54
     */
    @JsonProperty("forum_id")
    public String getSectionId() {
        return mSectionId;
    }

    /**
     * 设置版块id
     *
     * @param sectionId the section id
     * @author  : huangszh
     * @date  : 2015年04月13日 14:49:54
     */
    @JsonProperty("forum_id")
    public void setSectionId(String sectionId) {
        this.mSectionId = sectionId;
    }

    /**
     * 获取角色类型
     *
     * @return  role
     * @author  : huangszh
     * @date  : 2015年04月13日 14:49:54
     * @see com.forum.sdk.common.ForumConstDefine.RoleType
     */
    @JsonProperty("role")
    public int getRole() {
        return mRole;
    }

    /**
     * 设置角色类型
     *
     * @param role the role
     * @author  : huangszh
     * @date  : 2015年04月13日 14:49:54
     * @see com.forum.sdk.common.ForumConstDefine.RoleType
     */
    @JsonProperty("role")
    public void setRole(int role) {
        this.mRole = role;
    }

}
