package com.forum.sdk.dao.report.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;

/**
 * 举报需要的参数

 */
public class ForumReportParam extends ForumBaseType {

    /** 主贴uuid */
    @JsonProperty("post_id")
    private String mPostId;
    /** 跟帖uuid */
    @JsonProperty("thread_id")
    private String mThreadId;
    /**
     * 举报类型
     * @see com.forum.sdk.common.ForumConstDefine.ReportType
     */
    @JsonProperty("type")
    private int mType;
    /** 举报描述 */
    @JsonProperty("desc")
    private String mDesc;


    /**
     * 获取主贴uuid
     *
     * @return  post id
     * @author  : huangszh
     * @date  : 2015年04月01日 16:55:04
     */
    @JsonProperty("post_id")
    public String getPostId() {
        return mPostId;
    }

    /**
     * 设置主贴uuid
     *
     * @param postId the post id
     * @author  : huangszh
     * @date  : 2015年04月01日 16:55:04
     */
    @JsonProperty("post_id")
    public void setPostId(String postId) {
        this.mPostId = postId;
    }

    /**
     * 获取跟帖uuid
     *
     * @return  thread id
     * @author  : huangszh
     * @date  : 2015年04月01日 16:55:04
     */
    @JsonProperty("thread_id")
    public String getThreadId() {
        return mThreadId;
    }

    /**
     * 设置跟帖uuid
     *
     * @param threadId the thread id
     * @author  : huangszh
     * @date  : 2015年04月01日 16:55:04
     */
    @JsonProperty("thread_id")
    public void setThreadId(String threadId) {
        this.mThreadId = threadId;
    }

    /**
     * 获取举报类型
     *
     * @return  type
     * @author  : huangszh
     * @date  : 2015年04月01日 16:55:04
     * @see com.forum.sdk.common.ForumConstDefine.ReportType
     */
    @JsonProperty("type")
    public int getType() {
        return mType;
    }

    /**
     * 设置举报类型
     *
     * @param type the type
     * @author  : huangszh
     * @date  : 2015年04月01日 16:55:04
     * @see com.forum.sdk.common.ForumConstDefine.ReportType
     */
    @JsonProperty("type")
    public void setType(int type) {
        this.mType = type;
    }

    /**
     * 获取举报描述
     *
     * @return  desc
     * @author  : huangszh
     * @date  : 2015年04月01日 16:55:04
     */
    @JsonProperty("desc")
    public String getDesc() {
        return mDesc;
    }

    /**
     * 设置举报描述
     *
     * @param desc the desc
     * @author  : huangszh
     * @date  : 2015年04月01日 16:55:04
     */
    @JsonProperty("desc")
    public void setDesc(String desc) {
        this.mDesc = desc;
    }
}
