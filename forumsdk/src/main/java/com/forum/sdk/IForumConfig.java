package com.forum.sdk;

/**
 * 论坛sdk使用配置接口
 *
 * @author  : huangszh
 * @date 2015年04月01日 09:42:21
 * @see
 */
public interface IForumConfig {

    /**
     * 获取当前用户uid
     * @return  current uid
     * @author  : huangszh
     */
    String getCurrentUid();

    /**
     * 获取论坛服务域名
     * @return  com.morningbaby.forum url
     * @author  : huangszh
     */
    String getForumUrl();
}
