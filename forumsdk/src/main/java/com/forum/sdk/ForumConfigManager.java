package com.forum.sdk;

import android.text.TextUtils;

import com.forum.sdk.common.ForumConstDefine;
import com.forum.sdk.common.ForumRequireUser;
import com.nd.smartcan.core.restful.ClientResource;

/**
 * 论坛配置管理类
 *
 * @author  : huangszh
 * @date 2015年04月01日 09:41:00
 * @see
 */
public enum ForumConfigManager {

    /**
     * The INSTANCE.
     */
    INSTANCE;

    private IForumConfig mConfigInterface;

    /**
     * 外部设置配置监听，用于从外部获取配置，调用方一定要调用
     * @param configInterface the config interface
     * @author  : huangszh
     */
    public void setForumConfig(IForumConfig configInterface){
        mConfigInterface = configInterface;
        ClientResource.bindGlobalArgument(ForumConstDefine.UrlResourceConst.URL_RESOURCE_KEY, getForumUrl());
        //ClientResource.bindGlobalArgument(ForumConstDefine.UserResourceConst.USER_RESOURCE_KEY, getCurrentUid());
        ForumRequireUser.USER_ID = getCurrentUid();
    }

    /**
     * 获取当前用户id
     * @return 当前用户id
     * @author  : huangszh
     */
    public String getCurrentUid(){
        if(mConfigInterface == null){
            return "";
        }
        return mConfigInterface.getCurrentUid();
    }

    /**
     * 获取论坛服务域名
     * @return 论坛服务域名
     * @author  : huangszh
     */
    public String getForumUrl(){
        if(mConfigInterface == null || TextUtils.isEmpty(mConfigInterface.getForumUrl())){
            return "http://localhost:8080/api/v1/";
        }
        return mConfigInterface.getForumUrl();
    }
}
