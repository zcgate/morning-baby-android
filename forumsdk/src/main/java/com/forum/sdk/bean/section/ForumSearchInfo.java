package com.forum.sdk.bean.section;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;


/**
 * 版块搜索关键字
 * Created by zhuhp on 2015/4/30.
 */
public class ForumSearchInfo extends ForumBaseType {

    /** 存储用的id*/
    @JsonProperty("uuid")
    private String mUuid;

    /** 搜索的名称*/
    @JsonProperty("name")
    private String mName;

    /** 搜索类型*/
    @JsonProperty("type")
    private int mType;

    /** 搜索热度*/
    @JsonProperty("heat")
    private int mHeat;

    /**
     * 存储用的id
     * @return uuid
     */
    public String getUuid() {
        return mUuid;
    }

    /**
     * 存储用的id
     * @param uuid uuid
     */
    public void setUuid(String uuid) {
        this.mUuid = uuid;
    }

    /**
     * 搜索的名称
     * @return name
     */
    public String getName() {
        return mName;
    }

    /**
     * 搜索的名称
     * @param name name
     */
    public void setName(String name) {
        this.mName = name;
    }

    /**
     * 搜索类型
     * @return type
     */
    public int getType() {
        return mType;
    }

    /**
     * 搜索类型
     * @param type type
     */
    public void setType(int type) {
        this.mType = type;
    }

    /**
     * 搜索热度
     * @return heat
     */
    public int getHeat() {
        return mHeat;
    }

    /**
     * 搜索热度
     * @param heat heat
     */
    public void setHeat(int heat) {
        this.mHeat = heat;
    }
}
