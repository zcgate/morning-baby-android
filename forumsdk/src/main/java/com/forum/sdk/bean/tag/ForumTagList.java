package com.forum.sdk.bean.tag;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;

import java.util.List;

/**
 * 标签列表
 *
 * @author  : huangszh
 * @date 2015年04月01日 10:37:56
 * @see
 */
public class ForumTagList extends ForumBaseType {

    /** 标签总数*/
    @JsonProperty("count")
    private int mCount;

    /** 标签列表*/
    @JsonProperty("items")
    private List<ForumTagInfo> mItems;

    /**
     * 获取标签列表
     *
     * @return list items
     * @author  : huangszh
     * @date  : 2015年04月01日 16:51:11
     */
    public List<ForumTagInfo> getItems() {
        return mItems;
    }

    /**
     * 设置标签列表
     *
     * @param items the items
     * @author  : huangszh
     * @date  : 2015年04月01日 16:51:11
     */
    public void setItems(List<ForumTagInfo> items) {
        this.mItems = items;
    }

    /**
     * 获取标签总数
     *
     * @return  count
     * @author  : huangszh
     * @date  : 2015年04月01日 16:51:11
     */
    public int getCount() {
        return mCount;
    }

    /**
     * 设置标签总数
     *
     * @param count the count
     * @author  : huangszh
     * @date  : 2015年04月01日 16:51:11
     */
    public void setCount(int count) {
        this.mCount = count;
    }
}
