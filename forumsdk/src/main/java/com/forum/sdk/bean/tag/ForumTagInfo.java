package com.forum.sdk.bean.tag;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;

import java.util.Date;

/**
 * 标签信息
 *
 * @author  : huangszh
 * @date 2015年04月01日 09:54:57
 * @see
 */
public class ForumTagInfo extends ForumBaseType {


    /** uuid */
    @JsonProperty("id")
    private String mId;
    /** 标签自增id */
    @JsonProperty("tid")
    private long mTid;
    /** 标签热度 */
    @JsonProperty("heat")
    private int mHeat;
    /** 标签名 */
    @JsonProperty("name")
    private String mName;
    /** 添加时间 */
    @JsonProperty("add_time")
    private Date mAddTime;
    /** 附加信息 */
    @JsonProperty("addition")
    private String mAddition;

    /**
     * 获取标签uuid
     *
     * @return  id
     */
    public String getId() {
        return mId;
    }

    /**
     * 设置标签uuid
     *
     * @param id the id
     */
    public void setId(String id) {
        this.mId = id;
    }

    /**
     * 获取标签自增id, (一般列表取更多使用)
     *
     * @return  tid
     */
    public long getTid() {
        return mTid;
    }

    /**
     * 设置标签自增id
     *
     * @param tid the tid
     */
    public void setTid(long tid) {
        this.mTid = tid;
    }

    /**
     * 获取标签热度
     *
     * @return  heat
     */
    public int getHeat() {
        return mHeat;
    }

    /**
     * 设置标签热度
     *
     * @param heat the heat
     * @author  : huangszh
     * @date  : 2015年04月01日 16:46:36
     */
    public void setHeat(int heat) {
        this.mHeat = heat;
    }

    /**
     * 获取标签名
     *
     * @return  name
     */
    public String getName() {
        return mName;
    }

    /**
     * 设置标签名
     *
     * @param name the name
     */
    public void setName(String name) {
        this.mName = name;
    }

    /**
     * 获取添加时间
     *
     * @return  add time
     */
    public Date getAddTime() {
        return mAddTime;
    }

    /**
     * 设置添加时间
     *
     * @param addTime the add time
     */
    public void setAddTime(Date addTime) {
        this.mAddTime = addTime;
    }

    /**
     * 获取附加信息
     *
     * @return  addition
     */
    public String getAddition() {
        return mAddition;
    }

    /**
     * 设置附加信息
     *
     * @param addition the addition
     */
    public void setAddition(String addition) {
        this.mAddition = addition;
    }
}
