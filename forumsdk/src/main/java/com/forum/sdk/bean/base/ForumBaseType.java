package com.forum.sdk.bean.base;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * 论坛bean基类
 *
 * <br>Created 2015年2月6日 上午10:42:55
 * @author  : huangszh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ForumBaseType implements Serializable{

	/**
     * 
     */
    private static final long serialVersionUID = 5607117804420986093L;
    /** 扩展信息*/
	private Object mExtObject;

	/**
	 * 获取扩展信息
	 *
	 * <br>Created 2015年2月6日 上午10:44:52
	 * @return
	 * @author       zhuhp
	 */
	public Object getExtObject() {
		return mExtObject;
	}

	/**
	 * 设置扩展信息
	 * 
	 * <br>Created 2015年2月6日 上午10:44:17
	 * @param extObject 扩展对象
	 * @author       zhuhp
	 */
	public void setExtObject(Object extObject) {
		this.mExtObject = extObject;
	}

}