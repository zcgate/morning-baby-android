package com.forum.sdk.bean.post;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;


import java.util.List;

/**
 *  跟帖列表
 *
 * @author  : huangszh
 * @date 2015年04月01日 10:37:56
 * @see
 */
public class ForumThreadList extends ForumBaseType {

    /** 跟帖总数*/
    @JsonProperty("count")
    private int mCount;

    /** 跟帖列表*/
    @JsonProperty("items")
    private List<ForumThreadInfo> mItems;

    /**
     * 获取跟帖列表
     *
     * @return list items
     * @author  : huangszh
     * @date  : 2015年04月01日 16:51:11
     */
    public List<ForumThreadInfo> getItems() {
        return mItems;
    }

    /**
     * 设置跟帖列表
     *
     * @param items the items
     * @author  : huangszh
     * @date  : 2015年04月01日 16:51:11
     */
    public void setItems(List<ForumThreadInfo> items) {
        this.mItems = items;
    }

    /**
     * 获取跟帖总数
     *
     * @return  count
     * @author  : huangszh
     * @date  : 2015年04月01日 16:51:11
     */
    public int getCount() {
        return mCount;
    }

    /**
     * 设置跟帖总数s
     *
     * @param count the count
     * @author  : huangszh
     * @date  : 2015年04月01日 16:51:11
     */
    public void setCount(int count) {
        this.mCount = count;
    }
}
