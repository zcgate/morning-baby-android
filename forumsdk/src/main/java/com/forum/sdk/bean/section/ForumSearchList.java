package com.forum.sdk.bean.section;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;

import java.util.List;

/**
 * Created by zhuhp on 2015/4/30.
 */
public class ForumSearchList extends ForumBaseType {

    /** 总数*/
    @JsonProperty("count")
    private int mCount;

    /** 搜索关键字列表*/
    @JsonProperty("items")
    private List<ForumSearchInfo> mItems;

    /**
     * 总数
     * @return count
     */
    public int getCount() {
        return mCount;
    }

    /**
     * 总数
     * @param count count
     */
    public void setCount(int count) {
        this.mCount = count;
    }

    /**
     * 搜索关键字列表
     * @return items
     */
    public List<ForumSearchInfo> getItems() {
        return mItems;
    }

    /**
     * 搜索关键字列表
     * @param items items
     */
    public void setItems(List<ForumSearchInfo> items) {
        this.mItems = items;
    }
}
