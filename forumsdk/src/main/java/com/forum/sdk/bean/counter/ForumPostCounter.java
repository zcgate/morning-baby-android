package com.forum.sdk.bean.counter;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;


/**
 * 主贴计数器
 *
 * @author  : huangszh
 * @date 2015年04月01日 15:11:09
 */
public class ForumPostCounter extends ForumBaseType {


    /** uuid，主帖资源id */
    @JsonProperty("id")
    private String mId;
    /** 跟帖数 */
    @JsonProperty("thread_num")
    private int mThreadNum;
    /** 浏览数 */
    @JsonProperty("glance_num")
    private int mGlanceNum;

    /**
     * 获取主贴uuid
     *
     * @return string
     * @author  : huangszh
     * @date  : 2015年04月01日 17:05:00
     */
    public String getId() {
        return mId;
    }

    /**
     * 设置主贴uuid
     *
     * @param id 主贴uuid
     * @author  : huangszh
     * @date  : 2015年04月01日 17:05:00
     */
    public void setId(String id) {
        this.mId = id;
    }

    /**
     * 获取跟帖数
     *
     * @return thread num
     * @author  : huangszh
     * @date  : 2015年04月01日 17:05:00
     */
    public int getThreadNum() {
        return mThreadNum;
    }

    /**
     * 设置跟帖数
     *
     * @param threadNum 跟帖数
     * @author  : huangszh
     * @date  : 2015年04月01日 17:05:00
     */
    public void setThreadNum(int threadNum) {
        this.mThreadNum = threadNum;
    }

    /**
     * 获取浏览数
     *
     * @return  glance num
     * @author  : huangszh
     * @date  : 2015年04月01日 17:05:00
     */
    public int getGlanceNum() {
        return mGlanceNum;
    }

    /**
     * 设置浏览数
     *
     * @param glanceNum 浏览数
     * @author  : huangszh
     * @date  : 2015年04月01日 17:05:00
     */
    public void setGlanceNum(int glanceNum) {
        this.mGlanceNum = glanceNum;
    }

}
