package com.forum.sdk.bean.post;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;
import com.forum.sdk.bean.geo.ForumGeographyInfo;
import com.forum.sdk.bean.section.ForumSectionSummary;

import java.util.Date;

/**
 * 主贴信息
 *
 * @author  : huangszh
 * @date 2015年04月01日 09:54:18
 * @see
 */
public class ForumPostInfo extends ForumBaseType {
    /** uuid，帖子资源id */
    @JsonProperty("id")
    private String mId;
    /** 帖子自增ID */
    @JsonProperty("pid")
    private long mPid;
    /** 版块简介 */
    @JsonProperty("forum_summary")
    private ForumSectionSummary mForumSummary;
    /** 帖子标题 */
    @JsonProperty("title")
    private String mTitle;
    /** 作者uid */
    @JsonProperty("uid")
    private long mUid;
    /** 发布日期时间 */
    @JsonProperty("created_at")
    private Date mCreatedAt;
    /** 最后活跃日期时间 */
    @JsonProperty("active_at")
    private Date mActiveAt;
    /** 帖子摘要，最大140 */
    @JsonProperty("summary")
    private String mSummary;
    /** 是否截断：true：是，false：否 */
    @JsonProperty("truncated")
    private boolean mTruncated;
    /** 帖子完整内容 */
    @JsonProperty("article")
    private String mArticle;
    /**
     * 类型：图文(text),音频(audio),视频(video)
     *
     */
    @JsonProperty("content_category")
    private String mCategory;
    /** 图片id列表，半角逗号分隔 */
    @JsonProperty("image_list")
    private String mImageList;
    /** 跟帖数 */
    @JsonProperty("thread_num")
    private int mThreadNum;
    /** 浏览数 */
    @JsonProperty("glance_num")
    private int mGlanceNum;
    /** 来源 */
    @JsonProperty("source")
    private String mSource;
    /** 地址信息 */
    @JsonProperty("geo")
    private ForumGeographyInfo mGeo;
    /** 附加信息 */
    @JsonProperty("addition")
    private String mAddition;
    /**
     * 帖子标志，二进制表示，从右到左，第一位表示是否置顶，第二位表示是否加精，
     * 比如1表示置顶但没有加精，2表示加精没有置顶，3表示置顶+加精。
     * @see com.forum.sdk.common.ForumConstDefine.FlagState
     */
    @JsonProperty("flag")
    private int mFlag;
    /** 帖子状态,0正常状态，1 管理员删除，2系统屏蔽
     * @see com.forum.sdk.common.ForumConstDefine.StatusType
     */
    @JsonProperty("status")
    private int mStatus;

    /**
     * 获取附加信息
     *
     * @return addition
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:22
     */
    public String getAddition() {
        return mAddition;
    }

    /**
     * 设置附加信息
     *
     * @param addition the addition
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:22
     */
    public void setAddition(String addition) {
        this.mAddition = addition;
    }

    /**
     * 获取主贴uuid
     *
     * @return  id
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:22
     */
    public String getId() {
        return mId;
    }

    /**
     * 设置主贴uuid
     *
     * @param id the id
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:22
     */
    public void setId(String id) {
        this.mId = id;
    }

    /**
     * 获取主贴自增id
     *
     * @return  pid
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:22
     */
    public long getPid() {
        return mPid;
    }

    /**
     * 设置主贴自增id
     *
     * @param pid the pid
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:22
     */
    public void setPid(long pid) {
        this.mPid = pid;
    }

    /**
     * 获取版块简介
     *
     * @return com.morningbaby.forum section mSummary
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:22
     */
    public ForumSectionSummary getForumSummary() {
        return mForumSummary;
    }

    /**
     * 设置版块简介
     *
     * @param forumSummary the com.morningbaby.forum summary
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:22
     */
    public void setForumSummary(ForumSectionSummary forumSummary) {
        this.mForumSummary = forumSummary;
    }

    /**
     * 获取主贴标题
     *
     * @return  title
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:22
     */
    public String getTitle() {
        return mTitle;
    }

    /**
     * 设置主贴标题
     *
     * @param title the title
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:22
     */
    public void setTitle(String title) {
        this.mTitle = title;
    }

    /**
     * 获取作者uid
     *
     * @return  uid
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:22
     */
    public long getUid() {
        return mUid;
    }

    /**
     * 设置作者uid
     *
     * @param uid the uid
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:22
     */
    public void setUid(long uid) {
        this.mUid = uid;
    }

    /**
     * 获取发布时间
     *
     * @return  created at
     * @author  : huangszh
     * @函数名称  :getCreatedAt
     * @date  : 2015年04月01日 16:57:23
     */
    public Date getCreatedAt() {
        return mCreatedAt;
    }

    /**
     * 设置发布时间
     *
     * @param createdAt the created at
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:23
     */
    public void setCreatedAt(Date createdAt) {
        this.mCreatedAt = createdAt;
    }

    /**
     * 获取最后活跃时间
     *
     * @return  active time
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:23
     */
    public Date getActiveAt() {
        return mActiveAt;
    }

    /**
     * 设置最后活跃时间
     *
     * @param activeAt the active at
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:23
     */
    public void setActiveAt(Date activeAt) {
        this.mActiveAt = activeAt;
    }

    /**
     * 获取主贴摘要 ，最大140字
     *
     * @return  summary
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:23
     */
    public String getSummary() {
        return mSummary;
    }

    /**
     * 设置主贴摘要 ，最大140字
     *
     * @param summary the summary
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:23
     */
    public void setSummary(String summary) {
        this.mSummary = summary;
    }

    /**
     * 是否被截断
     *
     * @return boolean
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:23
     */
    public boolean isTruncated() {
        return mTruncated;
    }

    /**
     * 设置是否被截断
     *
     * @param truncated the truncated
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:23
     */
    public void setTruncated(boolean truncated) {
        this.mTruncated = truncated;
    }

    /**
     * 获取主贴完整内容
     *
     * @return  article
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:23
     */
    public String getArticle() {
        return mArticle;
    }

    /**
     * 设置主贴完整内容
     *
     * @param article the article
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:23
     */
    public void setArticle(String article) {
        this.mArticle = article;
    }

    /**
     * 获取主贴类型
     *
     * @return  category
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:23
     */
    public String getCategory() {
        return mCategory;
    }

    /**
     * 设置主贴类型
     *
     * @param category the category
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:23
     */
    public void setCategory(String category) {
        this.mCategory = category;
    }

    /**
     * 获取图片id列表
     *
     * @return  image list
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:23
     */
    public String getImageList() {
        return mImageList;
    }

    /**
     * 设置图片id列表
     *
     * @param imageList the image list
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:23
     */
    public void setImageList(String imageList) {
        this.mImageList = imageList;
    }

    /**
     * 获取来源
     *
     * @return  source
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:23
     */
    public String getSource() {
        return mSource;
    }

    /**
     * 设置来源
     *
     * @param source the source
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:23
     */
    public void setSource(String source) {
        this.mSource = source;
    }

    /**
     * 获取跟帖数
     *
     * @return  thread num
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:23
     */
    public int getThreadNum() {
        return mThreadNum;
    }

    /**
     * 设置跟帖数
     *
     * @param threadNum the thread num
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:23
     */
    public void setThreadNum(int threadNum) {
        this.mThreadNum = threadNum;
    }

    /**
     * 获取浏览数
     *
     * @return  glance num
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:23
     */
    public int getGlanceNum() {
        return mGlanceNum;
    }

    /**
     * 设置浏览数
     *
     * @param glanceNum the glance num
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:23
     */
    public void setGlanceNum(int glanceNum) {
        this.mGlanceNum = glanceNum;
    }

    /**
     * 获取地址信息
     *
     * @return com.morningbaby.forum geographyInfo
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:23
     */
    public ForumGeographyInfo getGeo() {
        return mGeo;
    }

    /**
     * 设置地址信息
     *
     * @param geo the geo
     * @author  : huangszh
     * @date  : 2015年04月01日 16:57:23
     */
    public void setGeo(ForumGeographyInfo geo) {
        this.mGeo = geo;
    }

    /**
     * 获取标记，二进制表示，第一位表示是否置顶，第二位表示是否加精，比如1表示置顶但没有加精，2表示加精没有置顶，3表示置顶+加精。
     *
     * @return the mFlag
     * @author  : huangszh
     * @see com.forum.sdk.common.ForumConstDefine.FlagState
     */
    public int getFlag() {
        return mFlag;
    }

    /**
     * 设置帖子标记
     *
     * @param flag the flag
     * @author  : huangszh
     * @see com.forum.sdk.common.ForumConstDefine.FlagState
     */
    public void setFlag(int flag) {
        mFlag = flag;
    }

    /**
     * 获取状态
     *
     * @return the mStatus
     * @author  : huangszh
     * @see com.forum.sdk.common.ForumConstDefine.StatusType
     */
    public int getStatus() {
        return mStatus;
    }

    /**
     * 设置帖子状态
     *
     * @param status the status
     * @author  : huangszh
     * @see com.forum.sdk.common.ForumConstDefine.StatusType
     */
    public void setStatus(int status) {
        this.mStatus = status;
    }
}
