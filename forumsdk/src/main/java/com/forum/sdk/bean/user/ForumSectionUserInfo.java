package com.forum.sdk.bean.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;

import java.util.Date;

/**
 * 版块用户信息
 *
 * @author  : huangszh
 * @date 2015年04月01日 09:55:24
 * @see
 */
public class ForumSectionUserInfo extends ForumBaseType {

    /** 版块id */
    @JsonProperty("forum_id")
    private String mForumId;
    /** 用户id */
    @JsonProperty("uid")
    private long mUid;
    /** 等级 */
    @JsonProperty("grade")
    private int mGrade;
    /** 加入时间 */
    @JsonProperty("join_time")
    private Date mJoinTime;
    /**
     * 成员角色 1:版主,2:管理员,3:普通成员
     * @see com.forum.sdk.common.ForumConstDefine.RoleType
     */
    @JsonProperty("role")
    private int mRole;
    /**
     * 审批状态.  0：未审批;1：审批通过;-1:审批拒绝
     * @see com.forum.sdk.common.ForumConstDefine.ApprovalStatus
     */
    @JsonProperty("status")
    private int mStatus;
    /** 附加信息 */
    @JsonProperty("addition")
    private String mAddition;

    /**
     * 获取版块id
     *
     * @return  com.morningbaby.forum id
     * @author  : huangszh
     * @date  : 2015年04月01日 16:33:50
     */
    public String getForumId() {
        return mForumId;
    }

    /**
     * 设置版块id
     *
     * @param forumId the com.morningbaby.forum id
     * @author  : huangszh
     * @date  : 2015年04月01日 16:33:50
     */
    public void setForumId(String forumId) {
        this.mForumId = forumId;
    }

    /**
     * 获取用户id
     *
     * @return  uid
     * @author  : huangszh
     * @date  : 2015年04月01日 16:33:50
     */
    public long getUid() {
        return mUid;
    }

    /**
     * 设置用户id
     *
     * @param uid the uid
     * @author  : huangszh
     * @date  : 2015年04月01日 16:33:50
     */
    public void setUid(long uid) {
        this.mUid = uid;
    }

    /**
     * 获取用户等级
     *
     * @return  grade
     * @author  : huangszh
     * @date  : 2015年04月01日 16:33:50
     */
    public int getGrade() {
        return mGrade;
    }

    /**
     * 设置用户等级
     *
     * @param grade the grade
     * @author  : huangszh
     * @date  : 2015年04月01日 16:33:50
     */
    public void setGrade(int grade) {
        this.mGrade = grade;
    }

    /**
     * 获取加入时间
     *
     * @return  join time
     * @author  : huangszh
     * @date  : 2015年04月01日 16:33:50
     */
    public Date getJoinTime() {
        return mJoinTime;
    }

    /**
     * 设置加入时间
     *
     * @param joinTime the join time
     * @author  : huangszh
     * @date  : 2015年04月01日 16:33:50
     */
    public void setJoinTime(Date joinTime) {
        this.mJoinTime = joinTime;
    }

    /**
     * 获取成员角色类型  (1:版主,2:管理员,3:普通成员)
     *
     * @return  role
     * @author  : huangszh
     * @date  : 2015年04月01日 16:33:50
     * @see com.forum.sdk.common.ForumConstDefine.RoleType
     */
    public int getRole() {
        return mRole;
    }

    /**
     * 设置成员角色类型
     *
     * @param role the role
     * @author  : huangszh
     * @date  : 2015年04月01日 16:33:50
     * @see com.forum.sdk.common.ForumConstDefine.RoleType
     */
    public void setRole(int role) {
        this.mRole = role;
    }

    /**
     * 获取审批状态.  (0：未审批;1：审批通过;-1:审批拒绝)
     *
     * @return  status
     * @author  : huangszh
     * @date  : 2015年04月01日 16:33:50
     * @see com.forum.sdk.common.ForumConstDefine.ApprovalStatus
     */
    public int getStatus() {
        return mStatus;
    }

    /**
     * 设置审批状态
     *
     * @param status the status
     * @author  : huangszh
     * @date  : 2015年04月01日 16:33:50
     * @see com.forum.sdk.common.ForumConstDefine.ApprovalStatus
     */
    public void setStatus(int status) {
        this.mStatus = status;
    }

    /**
     * 获取附加信息
     *
     * @return  addition
     * @author  : huangszh
     * @date  : 2015年04月01日 16:33:50
     */
    public String getAddition() {
        return mAddition;
    }

    /**
     * 设置附加信息
     *
     * @param addition the addition
     * @author  : huangszh
     * @date  : 2015年04月01日 16:33:50
     */
    public void setAddition(String addition) {
        this.mAddition = addition;
    }
}
