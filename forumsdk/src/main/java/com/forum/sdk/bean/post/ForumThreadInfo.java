package com.forum.sdk.bean.post;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;
import com.forum.sdk.bean.geo.ForumGeographyInfo;


import java.util.Date;

/**
 * 跟帖信息
 *
 * @author  : huangszh
 * @date 2015年04月01日 09:54:40
 * @see
 */
public class ForumThreadInfo extends ForumBaseType {

    /** uuid，跟帖资源id */
    @JsonProperty("id")
    private String mId;
    /** 帖子自增ID */
    @JsonProperty("thread_id")
    private long mThreadId;
    /** 作者uid */
    @JsonProperty("uid")
    private long mUid;
    /** 主贴id */
    @JsonProperty("post_id")
    private String mPostId;
    /** 主贴作者uid */
    @JsonProperty("post_uid")
    private long mPostUid;
    /** 楼层号 */
    @JsonProperty("floor")
    private int mFloor;
    /** 发布日期时间 */
    @JsonProperty("created_at")
    private Date mCreatedAt;
    /** 帖子完整内容 */
    @JsonProperty("article")
    private String mContent;
    /**
     * 类型：图文(text),音频(audio),视频(video)
     * @see com.forum.sdk.common.ForumConstDefine.ContentCategory
     */
    @JsonProperty("content_category")
    private String mCategory;
    /** 图片id列表，半角逗号分隔 */
    @JsonProperty("image_list")
    private String mImageList;
    /** 来源 */
    @JsonProperty("source")
    private String mSource;
    /** 地址信息 */
    @JsonProperty("geo")
    private ForumGeographyInfo mGeo;
    /** 附加信息 */
    @JsonProperty("addition")
    private String mAddition;
    /** 帖子状态,0正常状态，1 管理员删除，2系统屏蔽
     * @see com.forum.sdk.common.ForumConstDefine.StatusType
     */
    @JsonProperty("status")
    private int mStatus;

    /**
     * 获取跟帖uuid
     *
     * @return  id
     * @author  : huangszh
     * @date  : 2015年04月01日 17:02:49
     */
    public String getId() {
        return mId;
    }

    /**
     * 设置跟帖uuid
     *
     * @param id the id
     * @author  : huangszh
     * @date  : 2015年04月01日 17:02:49
     */
    public void setId(String id) {
        this.mId = id;
    }

    /**
     * 获取跟帖自增id
     *
     * @return thread id
     * @author  : huangszh
     * @date  : 2015年04月01日 17:02:50
     */
    public long getThreadId() {
        return mThreadId;
    }

    /**
     * 设置跟帖自增id
     *
     * @param threadId the thread id
     * @author  : huangszh
     * @date  : 2015年04月01日 17:02:50
     */
    public void setThreadId(long threadId) {
        this.mThreadId = threadId;
    }

    /**
     * 获取用户uid
     *
     * @return  uid
     * @author  : huangszh
     * @date  : 2015年04月01日 17:02:50
     */
    public long getUid() {
        return mUid;
    }

    /**
     * 设置用户uid
     *
     * @param uid the uid
     * @author  : huangszh
     * @date  : 2015年04月01日 17:02:50
     */
    public void setUid(long uid) {
        this.mUid = uid;
    }

    /**
     * 获取主贴uuid
     *
     * @return  post id
     * @author  : huangszh
     * @date  : 2015年04月01日 17:02:50
     */
    public String getPostId() {
        return mPostId;
    }

    /**
     * 设置主贴uuid
     *
     * @param postId the post id
     * @author  : huangszh
     * @date  : 2015年04月01日 17:02:50
     */
    public void setPostId(String postId) {
        this.mPostId = postId;
    }

    /**
     * 获取主贴作者uid
     *
     * @return the post uid
     * @author  : huangszh
     */
    public long getPostUid() {
        return mPostUid;
    }

    /**
     * 设置主贴作者uid
     *
     * @param postUid the post uid
     * @author  : huangszh
     */
    public void setPostUid(long postUid) {
        mPostUid = postUid;
    }
    /**
     * 获取楼层号
     *
     * @return  floor
     * @author  : huangszh
     * @date  : 2015年04月01日 17:02:50
     */
    public int getFloor() {
        return mFloor;
    }

    /**
     * 设置楼层号
     *
     * @param floor the floor
     * @author  : huangszh
     * @date  : 2015年04月01日 17:02:50
     */
    public void setFloor(int floor) {
        this.mFloor = floor;
    }

    /**
     * 获取发布时间
     *
     * @return  created at
     * @author  : huangszh
     * @date  : 2015年04月01日 17:02:50
     */
    public Date getCreatedAt() {
        return mCreatedAt;
    }

    /**
     * 设置发布时间
     *
     * @param createdAt the created at
     * @author  : huangszh
     * @date  : 2015年04月01日 17:02:50
     */
    public void setCreatedAt(Date createdAt) {
        this.mCreatedAt = createdAt;
    }

    /**
     * 获取跟帖内容
     *
     * @return  content
     * @author  : huangszh
     * @date  : 2015年04月01日 17:02:50
     */
    public String getContent() {
        return mContent;
    }

    /**
     * 设置跟帖内容
     *
     * @param content the content
     * @author  : huangszh
     * @date  : 2015年04月01日 17:02:50
     */
    public void setContent(String content) {
        this.mContent = content;
    }

    /**
     * 获取跟帖类型
     *
     * @return  category
     * @author  : huangszh
     * @date  : 2015年04月01日 17:02:50
     */
    public String getCategory() {
        return mCategory;
    }

    /**
     * 设置跟帖类型
     *
     * @param category the category
     * @author  : huangszh
     * @date  : 2015年04月01日 17:02:50
     */
    public void setCategory(String category) {
        this.mCategory = category;
    }

    /**
     * 获取图片id列表
     *
     * @return  image list
     * @author  : huangszh
     * @date  : 2015年04月01日 17:02:50
     */
    public String getImageList() {
        return mImageList;
    }

    /**
     * 设置图片id列表
     *
     * @param imageList the image list
     * @author  : huangszh
     * @date  : 2015年04月01日 17:02:50
     */
    public void setImageList(String imageList) {
        this.mImageList = imageList;
    }

    /**
     * 获取来源
     *
     * @return  source
     * @author  : huangszh
     * @date  : 2015年04月01日 17:02:50
     */
    public String getSource() {
        return mSource;
    }

    /**
     * 设置来源
     *
     * @param source the source
     * @author  : huangszh
     * @date  : 2015年04月01日 17:02:50
     */
    public void setSource(String source) {
        this.mSource = source;
    }

    /**
     * 获取地理信息
     *
     * @return com.morningbaby.forum mGeo
     * @author  : huangszh
     * @date  : 2015年04月01日 17:02:50
     */
    public ForumGeographyInfo getGeo() {
        return mGeo;
    }

    /**
     * 设置地理信息
     *
     * @param geo the geo
     * @author  : huangszh
     * @date  : 2015年04月01日 17:02:50
     */
    public void setGeo(ForumGeographyInfo geo) {
        this.mGeo = geo;
    }

    /**
     * 获取附加信息
     *
     * @return  addition
     * @author  : huangszh
     * @date  : 2015年04月01日 17:02:50
     */
    public String getAddition() {
        return mAddition;
    }

    /**
     * 设置附加信息
     *
     * @param addition the addition
     * @author  : huangszh
     * @date  : 2015年04月01日 17:02:50
     */
    public void setAddition(String addition) {
        this.mAddition = addition;
    }

    /**
     * 获取状态
     *
     * @return the mStatus
     * @author  : huangszh
     * @see com.forum.sdk.common.ForumConstDefine.StatusType
     */
    public int getStatus() {
        return mStatus;
    }

    /**
     * 设置帖子状态
     *
     * @param status the status
     * @author  : huangszh
     * @see com.forum.sdk.common.ForumConstDefine.StatusType
     */
    public void setStatus(int status) {
        this.mStatus = status;
    }
}
