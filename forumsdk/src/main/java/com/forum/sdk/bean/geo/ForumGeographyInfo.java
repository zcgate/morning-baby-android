package com.forum.sdk.bean.geo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;


/**
 * 地理位置信息
 *
 * @author  : huangszh
 * @date 2015年04月01日 09:56:05
 */
public class ForumGeographyInfo extends ForumBaseType {

    /** 经度 */
    @JsonProperty("longtitude")
    private String mLongtitude;
    /** 纬度 */
    @JsonProperty("latitude")
    private String mLatitude;
    /** 城市代码 */
    @JsonProperty("city")
    private String mCityCode;
    /** 实际地址 */
    @JsonProperty("address")
    private String mAddress;

    /**
     * 获取经度
     *
     * @return  longtitude
     * @author  : huangszh
     * @date  : 2015年04月01日 17:04:55
     */
    public String getLongtitude() {
        return mLongtitude;
    }

    /**
     * 设置经度
     *
     * @param longtitude 经度
     * @author  : huangszh
     * @date  : 2015年04月01日 17:04:55
     */
    public void setLongtitude(String longtitude) {
        this.mLongtitude = longtitude;
    }

    /**
     * 获取纬度
     *
     * @return  latitude
     * @author  : huangszh
     * @date  : 2015年04月01日 17:04:55
     */
    public String getLatitude() {
        return mLatitude;
    }

    /**
     * 设置纬度
     *
     * @param latitude 纬度
     * @author  : huangszh
     * @date  : 2015年04月01日 17:04:55
     */
    public void setLatitude(String latitude) {
        this.mLatitude = latitude;
    }

    /**
     * 获取城市代码
     *
     * @return city
     * @author  : huangszh
     * @date  : 2015年04月01日 17:04:56
     */
    public String getCity() {
        return mCityCode;
    }

    /**
     * 设置城市代码
     *
     * @param city the city
     * @author  : huangszh
     * @date  : 2015年04月01日 17:04:56
     */
    public void setCity(String city) {
        this.mCityCode = city;
    }

    /**
     * 获取实际地址
     *
     * @return  return the address
     * @author  : huangszh
     * @date  : 2015年04月01日 17:04:56
     */
    public String getAddress() {
        return mAddress;
    }

    /**
     * 设置实际地址
     *
     * @param address the address
     * @author  : huangszh
     * @date  : 2015年04月01日 17:04:56
     */
    public void setAddress(String address) {
        this.mAddress = address;
    }

}
