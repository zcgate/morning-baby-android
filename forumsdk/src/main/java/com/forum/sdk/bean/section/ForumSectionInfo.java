package com.forum.sdk.bean.section;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;
import com.forum.sdk.bean.tag.ForumTagInfo;
import com.forum.sdk.common.ForumConstDefine;
import com.forum.sdk.dao.section.ForumSectionDao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 版块信息
 *
 * @author  : huangszh
 * @date 2015年04月01日 09:53:54
 * @see
 */
@DatabaseTable(tableName = ForumConstDefine.TableName.TABLE_SECTION)
public class ForumSectionInfo extends ForumBaseType {

    /** uuid，版块id */
    @JsonProperty("id")
    @DatabaseField(columnName = "id")
    private String mId;
    /** 版块自增id */
    @JsonProperty("fid")
    @DatabaseField(columnName = "fid")
    private long mFid;
    /** 创建的用户id */
    @JsonProperty("uid")
    @DatabaseField(columnName = "uid")
    private long mUid;
    /** 图标id */
    @JsonProperty("image_id")
    @DatabaseField(columnName = "image_id")
    private String mImageId;
    /** 版块名字 */
    @JsonProperty("name")
    @DatabaseField(columnName = "name")
    private String mName;
    /** 添加时间 */
    @JsonProperty("add_time")
    @DatabaseField(columnName = "add_time")
    private Date mAddTime;
    /** 简介 */
    @JsonProperty("intro")
    @DatabaseField(columnName = "intro")
    private String mIntro;
    /** 成员数 */
    @JsonProperty("member_num")
    @DatabaseField(columnName = "member_num")
    private int mMemberNum;
    /** 贴子数 */
    @JsonProperty("post_num")
    @DatabaseField(columnName = "post_num")
    private int mPostNum;
    /** 等级 */
    @JsonProperty("grade")
    @DatabaseField(columnName = "grade")
    private int mGrade;
    /** 热度 */
    @JsonProperty("heat")
    @DatabaseField(columnName = "heat")
    private int mHeat;
    /** 认证标志 */
    @JsonProperty("cer_mark")
    @DatabaseField(columnName = "cer_mark")
    private String mCerMark;
    /** 标签列表 */
    @JsonProperty("tag_list")
    @DatabaseField(columnName = "tag_list")
    private String mTagList;
    /** 标签列表 标签详情(获取版块列表的接口不返回标签详情，获取版块信息的接口返回详情)*/
    @JsonProperty("tag_info")
    private List<ForumTagInfo> mTagInfo;
    /** 版块分类信息 */
    @JsonProperty("category_info")
    private ForumCategoryInfo mCategoryInfo;
    /** 版块分类uuid */
    @JsonProperty("category")
    @DatabaseField(columnName = "category")
    private String mCategory;
    /**
     * 审批状态.0：未审批;1：审批通过;-1:审批拒绝
     * @see com.forum.sdk.common.ForumConstDefine.ApprovalStatus
     */
    @JsonProperty("status")
    @DatabaseField(columnName = "status")
    private int mStatus;
    /** 附加信息 */
    @JsonProperty("addition")
    @DatabaseField(columnName = "addition")
    private String mAddition;
    /**
     * 用户在当前版块的角色：0=未订阅 1=版主 2=管理员 3=普通成员
     * @see com.forum.sdk.common.ForumConstDefine.RoleType
     */
    @JsonProperty("role")
    @DatabaseField(columnName = "role")
    private int mRole;

    /**
     * 订阅该版块是否需要审核 0:不需要,1：需要
     * @see com.forum.sdk.common.ForumConstDefine.CheckType
     */
    @JsonProperty("check")
    @DatabaseField(columnName = "check")
    private int mCheck;

    /**
     * 获取数据库数据时使用，保证不同组织的用户，得到的数据时不一样的
     */
    @DatabaseField(columnName = ForumConstDefine.TableColumnName.COLUMN_CURRENT_UID)
    private long currentUid;


    /**
     * 获取版块uuid
     *
     * @return  id
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:11
     */
    public String getId() {
        return mId;
    }

    /**
     * 设置版块uuid
     *
     * @param id the id
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:11
     */
    public void setId(String id) {
        this.mId = id;
    }

    /**
     * 获取版块自增id
     *
     * @return  fid
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:11
     */
    public long getFid() {
        return mFid;
    }

    /**
     * 设置版块自增id
     *
     * @param fid the fid
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:11
     */
    public void setFid(long fid) {
        this.mFid = fid;
    }

    /**
     * 获取创建用户的uid
     *
     * @return  uid
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:11
     */
    public long getUid() {
        return mUid;
    }

    /**
     * 设置创建用户的uid
     *
     * @param uid the uid
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:11
     */
    public void setUid(long uid) {
        this.mUid = uid;
    }

    /**
     * 获取版块图标id
     *
     * @return  image id
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:11
     */
    public String getImageId() {
        return mImageId;
    }

    /**
     * 设置版块图标id
     * @param imageId the image id
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:11
     */
    public void setImageId(String imageId) {
        this.mImageId = imageId;
    }

    /**
     * 获取版块名字
     *
     * @return  name
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:11
     */
    public String getName() {
        return mName;
    }

    /**
     * 设置版块名字
     *
     * @param name the name
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:11
     */
    public void setName(String name) {
        this.mName = name;
    }

    /**
     * 获取版块添加时间
     *
     * @return  add time
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:11
     */
    public Date getAddTime() {
        return mAddTime;
    }

    /**
     * 设置版块添加时间
     *
     * @param addTime the add time
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:11
     */
    public void setAddTime(Date addTime) {
        this.mAddTime = addTime;
    }

    /**
     * 获取版块简介
     *
     * @return  intro
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:11
     */
    public String getIntro() {
        return mIntro;
    }

    /**
     * 设置版块简介
     *
     * @param intro the intro
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:12
     */
    public void setIntro(String intro) {
        this.mIntro = intro;
    }

    /**
     * 获取成员数
     *
     * @return  member num
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:12
     */
    public int getMemberNum() {
        return mMemberNum;
    }

    /**
     * 设置成员数
     *
     * @param memberNum the member num
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:12
     */
    public void setMemberNum(int memberNum) {
        this.mMemberNum = memberNum;
    }

    /**
     * 获取帖子数
     *
     * @return  post num
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:12
     */
    public int getPostNum() {
        return mPostNum;
    }

    /**
     * 设置帖子数
     *
     * @param postNum the post num
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:12
     */
    public void setPostNum(int postNum) {
        this.mPostNum = postNum;
    }

    /**
     * 获取等级
     *
     * @return  grade
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:12
     */
    public int getGrade() {
        return mGrade;
    }

    /**
     * 设置等级
     *
     * @param grade the grade
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:12
     */
    public void setGrade(int grade) {
        this.mGrade = grade;
    }

    /**
     * 获取热度
     *
     * @return  heat
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:12
     */
    public int getHeat() {
        return mHeat;
    }

    /**
     * 设置热度
     *
     * @param heat the heat
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:12
     */
    public void setHeat(int heat) {
        this.mHeat = heat;
    }

    /**
     * 获取认证标志
     *
     * @return  cer mark
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:12
     */
    public String getCerMark() {
        return mCerMark;
    }

    /**
     * 设置认证标志
     * @param cerMark the cer mark
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:12
     */
    public void setCerMark(String cerMark) {
        this.mCerMark = cerMark;
    }

    /**
     * 获取标签列表
     *
     * @return  tag list
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:12
     */
    public String getTagList() {
        return mTagList;
    }

    /**
     * 设置标签列表
     *
     * @param tagList the tag list
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:12
     */
    public void setTagList(String tagList) {
        this.mTagList = tagList;
    }

    /**
     * 获取审批状态
     *
     * @return  status
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:12
     */
    public int getStatus() {
        return mStatus;
    }

    /**
     * 设置审批状态
     *
     * @param status the status
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:12
     * @see com.forum.sdk.common.ForumConstDefine.ApprovalStatus
     */
    public void setStatus(int status) {
        this.mStatus = status;
    }

    /**
     * 获取附加信息
     *
     * @return  addition
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:12
     * @see com.forum.sdk.common.ForumConstDefine.ApprovalStatus
     */
    public String getAddition() {
        return mAddition;
    }

    /**
     * 设置附加信息
     *
     * @param addition the addition
     * @author  : huangszh
     * @date  : 2015年04月01日 16:52:12
     */
    public void setAddition(String addition) {
        this.mAddition = addition;
    }

    /**
     * 获取标签详情
     *
     * @return list tag info
     * @author  : huangszh
     * @date  : 2015年04月02日 15:18:26
     */
    public List<ForumTagInfo> getTagInfo() {
        return mTagInfo;
    }

    /**
     * 设置标签详情
     *
     * @param tagInfo the tag info
     * @author  : huangszh
     * @date  : 2015年04月02日 15:18:26
     */
    public void setTagInfo(List<ForumTagInfo> tagInfo) {
        this.mTagInfo = tagInfo;
    }

    /**
     * 获取版块分类
     *
     * @return  category
     * @author  : huangszh
     * @date  : 2015年04月02日 15:18:26
     */
    public String getCategory() {
        return mCategory;
    }

    /**
     * 设置版块分类
     *
     * @param category the category
     * @author  : huangszh
     * @date  : 2015年04月02日 15:18:26
     */
    public void setCategory(String category) {
        this.mCategory = category;
    }


    /**
     * 获取用户在当前版块的角色 ：0=未订阅 1=版主 2=管理员 3=普通成员
     *
     * @return  role
     * @author  : huangszh
     * @date  : 2015年04月03日 16:06:38
     * @see com.forum.sdk.common.ForumConstDefine.RoleType
     */
    public int getRole() {
        return mRole;
    }

    /**
     * 设置用户在当前版块的角色
     *
     * @param role the role
     * @author  : huangszh
     * @date  : 2015年04月03日 16:06:41
     * @see com.forum.sdk.common.ForumConstDefine.RoleType
     */
    public void setRole(int role) {
        this.mRole = role;
    }

    /**
     * 获取版块分类
     *
     * @return  mCategoryInfo
     * @author  : huangszh
     * @date  : 2015年04月02日 15:18:26
     */
    public ForumCategoryInfo getCategoryInfo() {
        return mCategoryInfo;
    }

    /**
     * 设置版块分类
     *
     * @param categoryInfo the mCategoryInfo
     * @author  : huangszh
     * @date  : 2015年04月02日 15:18:26
     */
    public void setCategoryInfo(ForumCategoryInfo categoryInfo) {
        this.mCategoryInfo = categoryInfo;
    }

    /**
     * 订阅该版块是否需要审核 0:不需要,1：需要
     *
     * @return  mCheck
     * @author  : zhuhp
     * @date  : 2015年04月03日 16:06:38
     * @see com.forum.sdk.common.ForumConstDefine.CheckType
     */
    public int getCheck() {
        return mCheck;
    }

    /**
     * 订阅该版块是否需要审核 0:不需要,1：需要
     *
     * @param check the check
     * @author  : zhuhp
     * @date  : 2015年04月03日 16:06:41
     * @see com.forum.sdk.common.ForumConstDefine.CheckType
     */
    public void setCheck(int check) {
        this.mCheck = check;
    }

    /**
     * 数据库存储时使用
     * @return
     */
    public long getCurrentUid() {
        return currentUid;
    }

    /**
     * 将数据保存到数据库前，需要设置当前用户id
     * @param currentUid
     */
    public void setCurrentUid(long currentUid) {
        this.currentUid = currentUid;
    }


    private ForumSectionDao dao;
    private synchronized ForumSectionDao getDao(){
        if(dao==null){
            dao = new ForumSectionDao();
        }
        return dao;
    }
}
