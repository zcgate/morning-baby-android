package com.forum.sdk.bean.section;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;


/**
 * 版块简介
 *
 * @see
 */
public class ForumSectionSummary extends ForumBaseType {


    /** uuid，版块id */
    @JsonProperty("id")
    private String mId;

    /** 图标id */
    @JsonProperty("image_id")
    private String mImageId;

    /** 版块名字 */
    @JsonProperty("name")
    private String mName;

    /** 认证标志 */
    @JsonProperty("cer_mark")
    private String mCerMark;

    /** 版块类别id*/
    @JsonProperty("category")
    private String mCategoryId;

    /**
     * 版块类别id
     *
     * @return  id
     */
    public String getCategoryId() {
        return mCategoryId;
    }

    /**
     * 设置版块id
     *
     * @param categoryId the id
     */
    public void setCategoryId(String categoryId) {
        this.mCategoryId = categoryId;
    }

    /**
     * 获取版块uuid
     *
     * @return  id
     */
    public String getId() {
        return mId;
    }

    /**
     * 设置版块uuid
     *
     * @param id the id
     */
    public void setId(String id) {
        this.mId = id;
    }

    /**
     *  获取版块图标id
     *
     * @return  image id
     */
    public String getImageId() {
        return mImageId;
    }

    /**
     * 设置版块图标id
     *
     * @param imageId the image id
     */
    public void setImageId(String imageId) {
        this.mImageId = imageId;
    }

    /**
     * 获取版块名字
     *
     * @return  name
     */
    public String getName() {
        return mName;
    }

    /**
     * 设置版块名字
     *
     * @param name the name
     */
    public void setName(String name) {
        this.mName = name;
    }

    /**
     * 获取认证标志
     *
     * @return  cer mark
     */
    public String getCerMark() {
        return mCerMark;
    }

    /**
     * 设置认证标志
     *
     * @param cerMark the cer mark
     */
    public void setCerMark(String cerMark) {
        this.mCerMark = cerMark;
    }
}
