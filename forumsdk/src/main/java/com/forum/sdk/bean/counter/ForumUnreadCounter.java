package com.forum.sdk.bean.counter;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;


/**
 * 未读计数信息
 *
 * @author  : huangszh
 * @date 2015年04月02日 10:39:58
 */
public class ForumUnreadCounter extends ForumBaseType {
    /** 用户uid */
    @JsonProperty("uid")
    private long mUid;
    /** 未读的跟帖数 */
    @JsonProperty("thread_num")
    private int mThreadNum;

    /**
     * 获取用户uid
     *
     * @return the uid
     * @author  : huangszh
     * @date  : 2015年04月02日 10:39:58
     */
    public long getUid() {
        return mUid;
    }

    /**
     * 设置用户uid
     *
     * @param uid the uid
     * @author  : huangszh
     * @date  : 2015年04月02日 10:39:58
     */
    public void setUid(long uid) {
        this.mUid = uid;
    }

    /**
     * 获取跟帖数
     *
     * @return thread num
     * @author  : huangszh
     * @date  : 2015年04月02日 10:39:58
     */
    public int getThreadNum() {
        return mThreadNum;
    }

    /**
     * 设置跟帖数
     *
     * @param threadNum 跟帖数
     * @author  : huangszh
     * @date  : 2015年04月02日 10:39:58
     */
    public void setThreadNum(int threadNum) {
        this.mThreadNum = threadNum;
    }
}
