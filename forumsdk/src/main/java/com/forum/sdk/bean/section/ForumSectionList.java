package com.forum.sdk.bean.section;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;

import java.util.List;

/**
 * 版块列表
 *
 * @see
 */
public class ForumSectionList extends ForumBaseType {

    /** 版块总数*/
    @JsonProperty("count")
    private int mCount;

    /** 版块列表*/
    @JsonProperty("items")
    private List<ForumSectionInfo> mItems;

    /**
     * 获取版块列表
     *
     * @return list items
     */
    public List<ForumSectionInfo> getItems() {
        return mItems;
    }

    /**
     * 设置版块列表
     *
     * @param items the items
     */
    public void setItems(List<ForumSectionInfo> items) {
        this.mItems = items;
    }

    /**
     * 获取版块总数
     *
     * @return  count
     */
    public int getCount() {
        return mCount;
    }

    /**
     * 设置版块总数
     *
     * @param count the count
     */
    public void setCount(int count) {
        this.mCount = count;
    }
}
