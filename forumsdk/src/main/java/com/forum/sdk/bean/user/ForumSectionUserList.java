package com.forum.sdk.bean.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;

import java.util.List;

/**
 * 用户列表
 *
 * @author  : huangszh
 * @date 2015年04月01日 10:37:56
 * @see
 */
public class ForumSectionUserList extends ForumBaseType {

    /** 用户总数*/
    @JsonProperty("count")
    private int mCount;

    /** 用户列表*/
    @JsonProperty("items")
    private List<ForumSectionUserInfo> mItems;

    /**
     * 获取用户列表
     *
     * @return list items
     * @author  : huangszh
     * @date  : 2015年04月01日 16:51:11
     */
    public List<ForumSectionUserInfo> getItems() {
        return mItems;
    }

    /**
     * 设置用户列表
     *
     * @param items the items
     * @author  : huangszh
     * @date  : 2015年04月01日 16:51:11
     */
    public void setItems(List<ForumSectionUserInfo> items) {
        this.mItems = items;
    }

    /**
     * 获取用户总数
     *
     * @return  count
     * @author  : huangszh
     * @date  : 2015年04月01日 16:51:11
     */
    public int getCount() {
        return mCount;
    }

    /**
     * 设置用户总数
     *
     * @param count the count
     * @author  : huangszh
     * @date  : 2015年04月01日 16:51:11
     */
    public void setCount(int count) {
        this.mCount = count;
    }
}
