package com.forum.sdk.bean.post;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;

import java.util.List;

/**
 * 主贴列表
 *
 * @author  : huangszh
 * @date 2015年04月01日 10:37:56
 * @see
 */
public class ForumPostList extends ForumBaseType {

    /** 主贴总数*/
    @JsonProperty("count")
    private int mCount;

    /** 主贴列表*/
    @JsonProperty("items")
    private List<ForumPostInfo> mItems;

    /**
     * 获取主贴列表
     *
     * @return list items
     * @author  : huangszh
     * @date  : 2015年04月01日 16:51:11
     */
    public List<ForumPostInfo> getItems() {
        return mItems;
    }

    /**
     * 设置主贴列表
     *
     * @param items the items
     * @author  : huangszh
     * @date  : 2015年04月01日 16:51:11
     */
    public void setItems(List<ForumPostInfo> items) {
        this.mItems = items;
    }

    /**
     * 获取主贴总数
     *
     * @return  count
     * @author  : huangszh
     * @date  : 2015年04月01日 16:51:11
     */
    public int getCount() {
        return mCount;
    }

    /**
     * 设置主贴总数
     *
     * @param count the count
     * @author  : huangszh
     * @date  : 2015年04月01日 16:51:11
     */
    public void setCount(int count) {
        this.mCount = count;
    }
}
