package com.forum.sdk.bean.section;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;

import java.util.ArrayList;

/**
 * 论坛类别信息
 *
 * @author  : huangszh
 */
public class ForumCategoryInfo extends ForumBaseType {
    /** category唯一uuid */
    @JsonProperty("uuid")
    private String mId;
    /** category名称 */
    @JsonProperty("name")
    private String mName;
    /** 子分类的列表。如果是叶子节点（没有子分类）这个字段为空数组 */
    @JsonProperty("children")
    private ArrayList<ForumCategoryInfo> mChildren;

    /**
     * 获取类别uuid
     *
     * @return the id
     * @author  : huangszh
     */
    public String getId() {
        return mId;
    }

    /**
     * 设置类别uuid
     *
     * @param id the id
     * @author  : huangszh
     */
    public void setId(String id) {
        mId = id;
    }

    /**
     * 获取类别名称
     *
     * @return the name
     * @author  : huangszh
     */
    public String getName() {
        return mName;
    }

    /**
     * 设置类别名称
     *
     * @param name the name
     * @author  : huangszh
     */
    public void setName(String name) {
        mName = name;
    }

    /**
     * 获取子分类的列表
     *
     * @return the children
     * @author  : huangszh
     */
    public ArrayList<ForumCategoryInfo> getChildren() {
        return mChildren;
    }

    /**
     * 设置子分类的列表
     *
     * @param children the children
     * @author  : huangszh
     */
    public void setChildren(ArrayList<ForumCategoryInfo> children) {
        mChildren = children;
    }
}
