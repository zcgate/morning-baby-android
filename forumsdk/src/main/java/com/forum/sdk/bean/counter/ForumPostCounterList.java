package com.forum.sdk.bean.counter;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;


import java.util.List;

/**
 * 计数器列表
 *
 * @author  : huangszh
 * @date 2015年04月01日 10:37:56
 */
public class ForumPostCounterList extends ForumBaseType {

    /** 计数器列表*/
    @JsonProperty("items")
    private List<ForumPostCounter> mItems;

    /**
     * 获取计数器列表
     *
     * @return items
     * @author  : huangszh
     * @date  : 2015年04月01日 16:51:11
     */
    public List<ForumPostCounter> getItems() {
        return mItems;
    }

    /**
     * 设置计数器列表
     *
     * @param items 计数器列表
     * @author  : huangszh
     * @date  : 2015年04月01日 16:51:11
     */
    public void setItems(List<ForumPostCounter> items) {
        this.mItems = items;
    }
}
