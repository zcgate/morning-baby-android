package com.forum.sdk.bean.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;


/**
 * 内容服务的上传授权session信息
 * Created by zhuhp on 2015/4/30.
 */
public class ForumImageSession extends ForumBaseType {

    /** session使用者uid*/
    @JsonProperty("uid")
    private long mUid;

    /** 申请到的session*/
    @JsonProperty("session")
    private String mSession;

    /** 上传目录*/
    @JsonProperty("path")
    private String mPath;

    /**
     * session使用者uid
     * @return uid
     */
    public long getUid() {
        return mUid;
    }

    /**
     * session使用者uid
     * @param uid
     */
    public void setUid(long uid) {
        this.mUid = uid;
    }

    /**
     * 申请到的session
     * @return session
     */
    public String getSession() {
        return mSession;
    }

    /**
     * 申请到的session
     * @param session
     */
    public void setSession(String session) {
        this.mSession = session;
    }

    /**
     * 上传目录
     * @return path
     */
    public String getPath() {
        return mPath;
    }

    /**
     * 上传目录
     * @param path
     */
    public void setPath(String path) {
        this.mPath = path;
    }
}
