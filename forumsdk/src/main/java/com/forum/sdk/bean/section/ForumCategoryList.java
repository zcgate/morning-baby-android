package com.forum.sdk.bean.section;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;

import java.util.List;

/**
 * 论坛类别list
 *
 * @author  : huangszh
 */
public class ForumCategoryList extends ForumBaseType {

    /** 论坛类别总数*/
    @JsonProperty("count")
    private int mCount;

    /** 论坛类别列表*/
    @JsonProperty("items")
    private List<ForumCategoryInfo> mItems;

    /**
     * 获取论坛类别列表
     *
     * @return list items
     * @author  : huangszh
     * @date  : 2015年04月01日 16:51:11
     */
    public List<ForumCategoryInfo> getItems() {
        return mItems;
    }

    /**
     * 设置论坛类别列表
     *
     * @param items the items
     * @author  : huangszh
     * @date  : 2015年04月01日 16:51:11
     */
    public void setItems(List<ForumCategoryInfo> items) {
        this.mItems = items;
    }

    /**
     * 获取论坛类别总数
     *
     * @return count count
     * @author  : huangszh
     * @date  : 2015年04月01日 16:51:11
     */
    public int getCount() {
        return mCount;
    }

    /**
     * 设置论坛类别总数
     *
     * @param count the count
     * @author  : huangszh
     * @date  : 2015年04月01日 16:51:11
     */
    public void setCount(int count) {
        this.mCount = count;
    }
}
