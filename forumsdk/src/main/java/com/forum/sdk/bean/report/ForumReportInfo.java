package com.forum.sdk.bean.report;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.forum.sdk.bean.base.ForumBaseType;
import com.forum.sdk.bean.post.ForumPostInfo;
import com.forum.sdk.bean.post.ForumThreadInfo;


import java.util.Date;

/**
 * 举报信息
 *
 * @author  : huangszh
 * @date 2015年04月01日 15:13:47
 * @see
 */
public class ForumReportInfo extends ForumBaseType {
    /** 自增id */
    @JsonProperty("id")
    private long mId;
    /**
     * 状态（0 未处理，1 驳回，2 删除）
     * @see com.forum.sdk.common.ForumConstDefine.ReportStatus
     */
    @JsonProperty("status")
    private int mStatus;
    /** 主贴uuid */
    @JsonProperty("post_info")
    private ForumPostInfo mPostInfo;
    /** 跟帖uuid */
    @JsonProperty("thread_info")
    private ForumThreadInfo mThreadInfo;
    /** 被举报次数*/
    @JsonProperty("count")
    private int mCount;
    /** 举报人uid */
    @JsonProperty("uid")
    private long mUid;
    /** 举报时间 */
    @JsonProperty("report_time")
    private Date mReportTime;
    /** 处理时间 */
    @JsonProperty("deal_time")
    private Date mDealTime;


    /**
     * 获取举报自增id
     *
     * @return id 
     * @author  : huangszh
     * @date  : 2015年04月01日 16:55:04
     */
    public long getId() {
        return mId;
    }

    /**
     * 设置举报自增id
     *
     * @param id the id
     * @author  : huangszh
     * @date  : 2015年04月01日 16:55:04
     */
    public void setId(long id) {
        this.mId = id;
    }

    /**
     * 获取主贴信息
     *
     * @return post info
     * @author  : huangszh
     * @date  : 2015年04月01日 16:55:04
     */
    public ForumPostInfo getPostInfo() {
        return mPostInfo;
    }

    /**
     * 设置主贴信息
     *
     * @param postInfo the post info
     * @author  : huangszh
     * @date  : 2015年04月01日 16:55:04
     */
    public void setPostInfo(ForumPostInfo postInfo) {
        this.mPostInfo = postInfo;
    }

    /**
     * 获取跟帖信息
     *
     * @return thread info
     * @author  : huangszh
     * @date  : 2015年04月01日 16:55:04
     */
    public ForumThreadInfo getThreadInfo() {
        return mThreadInfo;
    }

    /**
     * 设置跟帖信息
     *
     * @param threadInfo the thread info
     * @author  : huangszh
     * @date  : 2015年04月01日 16:55:04
     */
    public void setThreadInfo(ForumThreadInfo threadInfo) {
        this.mThreadInfo = threadInfo;
    }

    /**
     * 获取被举报次数
     *
     * @return mCount
     * @author  : huangszh
     * @date  : 2015年04月01日 16:55:04
     */
    public int getCount() {
        return mCount;
    }

    /**
     * 设置被举报次数
     *
     * @param count the count
     * @author  : huangszh
     * @date  : 2015年04月01日 16:55:04
     */
    public void setCount(int count) {
        this.mCount = count;
    }

    /**
     * 获取举报人uid
     *
     * @return uid
     * @author  : huangszh
     * @date  : 2015年04月01日 16:55:04
     */
    public long getUid() {
        return mUid;
    }

    /**
     * 设置举报人uid
     *
     * @param uid the uid
     * @author  : huangszh
     * @date  : 2015年04月01日 16:55:04
     */
    public void setUid(long uid) {
        this.mUid = uid;
    }

    /**
     * 获取举报时间
     *
     * @return report time
     * @author  : huangszh
     * @date  : 2015年04月01日 16:55:04
     */
    public Date getReportTime() {
        return mReportTime;
    }

    /**
     * 设置举报时间
     *
     * @param reportTime the report time
     * @author  : huangszh
     * @date  : 2015年04月01日 16:55:04
     */
    public void setReportTime(Date reportTime) {
        this.mReportTime = reportTime;
    }


    /**
     * 获取举报时间
     *
     * @return the deal time
     * @author  : huangszh
     */
    public Date getDealTime() {
        return mDealTime;
    }

    /**
     * 设置举报时间
     *
     * @param dealTime the deal time
     * @author  : huangszh
     */
    public void setDealTime(Date dealTime) {
        mDealTime = dealTime;
    }

    /**
     * 获取状态
     *
     * @return the status
     * @author  : huangszh
     * @see com.forum.sdk.common.ForumConstDefine.ReportStatus
     */
    public int getStatus() {
        return mStatus;
    }

    /**
     * 设置状态
     *
     * @param status the status
     * @author  : huangszh
     * @see com.forum.sdk.common.ForumConstDefine.ReportStatus
     */
    public void setStatus(int status) {
        mStatus = status;
    }

}
