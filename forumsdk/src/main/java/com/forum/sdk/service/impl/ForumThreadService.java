package com.forum.sdk.service.impl;

import com.forum.sdk.bean.post.ForumThreadInfo;
import com.forum.sdk.bean.post.ForumThreadList;
import com.forum.sdk.dao.thread.ForumThreadDao;
import com.forum.sdk.dao.thread.bean.ForumThreadParam;
import com.forum.sdk.service.IForumThreadService;
import com.nd.smartcan.frame.exception.DaoException;

import java.util.List;

/**
 *  跟帖服务具体实现类
 *
 * @author  : huangszh
 * @date 2015年04月02日 09:51:47
 * @see
 */
public class ForumThreadService implements IForumThreadService {

    @Override
    public ForumThreadInfo createThread(ForumThreadParam forumThreadParam) throws DaoException {

        return new ForumThreadDao().createThread(forumThreadParam);
    }

    @Override
    public ForumThreadInfo deleteThread(String threadId) throws DaoException {

        return new ForumThreadDao().deleteThread(threadId);
    }

    @Override
    public ForumThreadList getPostThreadList(String postId, long minThread, int size, boolean isNeedCount) throws DaoException {

        return new ForumThreadDao().getPostThreadList(postId, minThread, size, isNeedCount);
    }

    @Override
    public ForumThreadList getPostReplyMeThreadList(long maxThread, int size, boolean isNeedCount) throws DaoException {

        return new ForumThreadDao().getPostReplyMeThreadList(maxThread, size, isNeedCount);
    }

    @Override
    public ForumThreadList getThreadList(List<String> idList) throws DaoException {

        return new ForumThreadDao().getThreadList(idList);
    }
}
