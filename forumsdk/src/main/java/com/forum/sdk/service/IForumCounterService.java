package com.forum.sdk.service;


import com.forum.sdk.bean.counter.ForumUnreadCounter;
import com.nd.smartcan.frame.exception.DaoException;

/**
 * 计数器服务
 *
 * @author  : huangszh
 * @date 2015年04月02日 09:52:20
 * @see
 */
public interface IForumCounterService {

    /**
     * 获取未读计数信息
     *
     * @return  unread counter
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:40:56
     */
    ForumUnreadCounter getUnreadCounter() throws DaoException;

}
