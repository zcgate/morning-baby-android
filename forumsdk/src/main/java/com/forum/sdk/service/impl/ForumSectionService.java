package com.forum.sdk.service.impl;


import com.forum.sdk.bean.section.ForumCategoryList;
import com.forum.sdk.bean.section.ForumSearchList;
import com.forum.sdk.bean.section.ForumSectionInfo;
import com.forum.sdk.bean.section.ForumSectionList;
import com.forum.sdk.bean.tag.ForumTagList;
import com.forum.sdk.common.ForumConstDefine;
import com.forum.sdk.dao.section.ForumSectionDao;
import com.forum.sdk.dao.section.ForumSectionOrmDao;
import com.forum.sdk.dao.section.bean.ForumSectionParam;
import com.forum.sdk.service.IForumSectionService;
import com.nd.smartcan.frame.exception.DaoException;

import java.util.List;

/**
 * 版块服务具体实现类
 *
 * @author  : huangszh
 * @date 2015年04月01日 13:56:33
 * @see
 */
public class ForumSectionService implements IForumSectionService {

    @Override
    public ForumSectionInfo createSection(ForumSectionParam sectionPost) throws DaoException {

        return new ForumSectionDao().createSection(sectionPost);
    }

    @Override
    public ForumSectionInfo editSection(String sectionId,ForumSectionParam sectionPost) throws DaoException {

         return new ForumSectionDao().editSection(sectionId,sectionPost);
    }

    @Override
    public ForumSectionInfo getSectionInfo(String sectionId) throws DaoException {

        return new ForumSectionDao().getSectionInfo(sectionId);
    }

    @Override
    public ForumTagList getTagList(int page, int size, boolean isNeedCount) throws DaoException {

        return new ForumSectionDao().getTagList(page, size, isNeedCount);
    }

    @Override
    public ForumSectionList getMyFollowSectionList(int page, int size, boolean isNeedCount) throws DaoException {

        return new ForumSectionDao().getMyFollowSectionList(page, size, isNeedCount);
    }

    @Override
    public ForumSectionList getMyManageSectionList(int page, int size, boolean isNeedCount) throws DaoException {

        return new ForumSectionDao().getMyManageSectionList(page, size, isNeedCount);
    }

    @Override
    public ForumSectionList getHotSectionList(int page, int size, boolean isNeedCount) throws DaoException {

        return new ForumSectionDao().getHotSectionList(page, size, isNeedCount);
    }

    @Override
    public ForumSectionList searchSectionList(String tag, String name, int page, int size, boolean isNeedCount) throws DaoException {

        return new ForumSectionDao().searchSectionList(tag, name, page, size, isNeedCount);
    }

    @Override
    public ForumCategoryList getCategoryList(int page, int size, boolean isNeedCount) throws DaoException {
        return new ForumSectionDao().getCategoryList(page, size, isNeedCount);
    }

    @Override
    public ForumSectionList getSectionListByCategoryId(String id, int page, int size, boolean isNeedCount) throws DaoException {
        return new ForumSectionDao().getSectionListByCategoryId(id, page, size, isNeedCount);
    }

    @Override
    public ForumSearchList getSearchInfoList(int page, int size, boolean isNeedCount) throws DaoException {
        return new ForumSectionDao().getSearchInfoList(page, size, isNeedCount);
    }

    @Override
    public ForumSectionList getCacheHotSectionList(long uid) {
        String sql = "select * from "+ ForumConstDefine.TableName.TABLE_SECTION+" where "
                + ForumConstDefine.TableColumnName.COLUMN_CURRENT_UID+" ='"+uid+"'";
        List<ForumSectionInfo> data = ForumSectionOrmDao.dao.query(sql);
        ForumSectionList list = new ForumSectionList();
        list.setItems(data);
        list.setCount(20);
        return list;
    }

    @Override
    public void clearCacheHotSectionList(long uid) {
        ForumSectionOrmDao.clearCurrentUserCacheData(uid);
    }

    @Override
    public void saveHotSectionList(ForumSectionList list,long uid) {
        if(list == null){
            return;
        }
        List<ForumSectionInfo> data = list.getItems();
        if((data == null)||(data.isEmpty())){
            return;
        }
        for(ForumSectionInfo info:data){
            try {
                info.setCurrentUid(uid);
                ForumSectionOrmDao.dao.insert(info);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

}
