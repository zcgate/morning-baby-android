package com.forum.sdk.service;

import com.forum.sdk.bean.counter.ForumPostCounterList;
import com.forum.sdk.bean.post.ForumPostInfo;
import com.forum.sdk.bean.post.ForumPostList;
import com.forum.sdk.bean.report.ForumReportInfo;
import com.forum.sdk.dao.post.bean.ForumPostParam;
import com.forum.sdk.dao.report.bean.ForumReportParam;
import com.nd.smartcan.frame.exception.DaoException;

import java.util.List;

/**
 * 主帖服务
 *
 * @author  : huangszh
 * @date 2015年04月01日 17:41:23
 * @see
 */
public interface IForumPostService {

    /**
     * 发布主帖
     *
     * @param forumPostParam 主帖信息
     * @return com.morningbaby.forum post info
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:25:52
     */
    ForumPostInfo createPost(ForumPostParam forumPostParam) throws DaoException;

    /**
     * 获取主帖详情
     *
     * @param postId 主帖id
     * @return post detail
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:25:52
     */
    ForumPostInfo getPostDetail(String postId) throws DaoException;

    /**
     * 删除主帖
     *
     * @param postId 主帖id
     * @return com.morningbaby.forum post info
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:25:52
     */
    ForumPostInfo deletePost(String postId) throws DaoException;

    /**
     * 批量获取主贴详情
     *
     * @param idList 主帖id列表
     * @return post detail list
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:25:52
     */
    ForumPostList getPostDetailList(List<String> idList) throws DaoException;

    /**
     * 批量获取主帖计数信息
     *
     * @param idList 主帖id列表
     * @return post counter list
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:25:52
     */
    ForumPostCounterList getPostCounterList(List<String> idList) throws DaoException;

    /**
     * 举报主帖
     *
     * @param forumReportParam 举报信息
     * @return com.morningbaby.forum report info
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:25:52
     */
    ForumReportInfo reportPost(ForumReportParam forumReportParam) throws DaoException;

    /**
     * 置顶主帖
     *
     * @param postId 主帖id
     * @return com.morningbaby.forum post info
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:25:52
     */
    ForumPostInfo topPost(String postId) throws DaoException;

    /**
     * 取消置顶主帖
     *
     * @param postId 主帖id
     * @return com.morningbaby.forum post info
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:25:52
     */
    ForumPostInfo cancelTopPost(String postId) throws DaoException;

    /**
     * 获取版块的主帖列表
     *
     * @param postId 版块id
     * @param maxActive 获取小于maxActive的主帖列表，首次获取时传Long.MAX_VALUE
     * @param size 获取的数量
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return section post list
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:25:52
     */
    ForumPostList getSectionPostList(String postId, long maxActive, int size, boolean isNeedCount) throws DaoException;

    /**
     * 获取我订阅的主帖列表
     *
     * @param maxActive 获取小于maxActive的主帖列表，首次获取时传Long.MAX_VALUE
     * @param size 获取的数量
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return my rss post list
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:25:52
     */
    ForumPostList getMyRssPostList(long maxActive, int size, boolean isNeedCount) throws DaoException;

    /**
     * 获取我发布的主帖列表
     *
     * @param page 页偏移量，不小于0的值，传错置为0，默认为0
     * @param size 每页条数限制，默认20，最大100，传错置为20
     * @return my post list
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:25:52
     */
    ForumPostList getMyPostList(int page, int size, boolean isNeedCount) throws DaoException;

    /**
     * 获取我以回帖方式参与的主帖列表
     *
     * @param page 页偏移量，不小于0的值，传错置为0，默认为0
     * @param size 每页条数限制，默认20，最大100，传错置为20
     * @return my thread join post list
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:25:53
     */
    ForumPostList getMyThreadJoinPostList(int page, int size, boolean isNeedCount) throws DaoException;

}
