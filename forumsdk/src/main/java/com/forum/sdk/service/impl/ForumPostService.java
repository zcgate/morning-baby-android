package com.forum.sdk.service.impl;


import com.forum.sdk.bean.counter.ForumPostCounterList;
import com.forum.sdk.bean.post.ForumPostInfo;
import com.forum.sdk.bean.post.ForumPostList;
import com.forum.sdk.bean.report.ForumReportInfo;
import com.forum.sdk.dao.post.ForumPostDao;
import com.forum.sdk.dao.post.ForumPostTimeLineDao;
import com.forum.sdk.dao.post.bean.ForumPostParam;
import com.forum.sdk.dao.report.ForumReportDao;
import com.forum.sdk.dao.report.bean.ForumReportParam;
import com.forum.sdk.dao.top.ForumTopDao;
import com.forum.sdk.service.IForumPostService;
import com.nd.smartcan.frame.exception.DaoException;

import java.util.List;

/**
 * 主帖服务具体实现类
 *
 * @author  : huangszh
 * @date 2015年04月02日 10:45:32
 * @see
 */
public class ForumPostService implements IForumPostService {

    @Override
    public ForumPostInfo createPost(ForumPostParam forumPostParam) throws DaoException {

        return new ForumPostDao().createPost(forumPostParam);
    }

    @Override
    public ForumPostInfo getPostDetail(String postId) throws DaoException {

        return new ForumPostDao().getPostDetail(postId);
    }

    @Override
    public ForumPostInfo deletePost(String postId) throws DaoException {

        return new ForumPostDao().deletePost(postId);
    }

    @Override
    public ForumPostList getPostDetailList(List<String> idList) throws DaoException {

        return new ForumPostDao().getPostDetailList(idList);
    }


    @Override
    public ForumPostCounterList getPostCounterList(List<String> idList) throws DaoException {

        return new ForumPostDao().getPostCounterList(idList);
    }

    @Override
    public ForumReportInfo reportPost(ForumReportParam forumReportParam) throws DaoException {

        return new ForumReportDao().reportPost(forumReportParam);
    }

    @Override
    public ForumPostInfo topPost(String postId) throws DaoException {

        return new ForumTopDao().topPost(postId);
    }

    @Override
    public ForumPostInfo cancelTopPost(String postId) throws DaoException {

        return new ForumTopDao().cancelTopPost(postId);
    }

    @Override
    public ForumPostList getSectionPostList(String sectionId, long maxActive, int size, boolean isNeedCount) throws DaoException {

        return new ForumPostTimeLineDao().getSectionPostList(sectionId, maxActive, size, isNeedCount);
    }

    @Override
    public ForumPostList getMyRssPostList(long maxActive, int size, boolean isNeedCount) throws DaoException {

        return new ForumPostTimeLineDao().getMyRssPostList(maxActive, size, isNeedCount);
    }

    @Override
    public ForumPostList getMyPostList(int page, int size, boolean isNeedCount) throws DaoException {

        return new ForumPostTimeLineDao().getMyPostList(page, size, isNeedCount);
    }

    @Override
    public ForumPostList getMyThreadJoinPostList(int page, int size, boolean isNeedCount) throws DaoException {

        return new ForumPostTimeLineDao().getMyThreadJoinPostList(page, size, isNeedCount);
    }
}
