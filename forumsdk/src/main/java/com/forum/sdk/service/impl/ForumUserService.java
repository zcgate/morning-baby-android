package com.forum.sdk.service.impl;

import com.forum.sdk.bean.user.ForumImageSession;
import com.forum.sdk.dao.user.ForumImageSessionDao;
import com.forum.sdk.service.IForumUserService;
import com.nd.smartcan.frame.exception.DaoException;

/**
 * 用户服务
 * Created by zhuhp on 2015/4/30.
 */
public class ForumUserService implements IForumUserService {
    @Override
    public ForumImageSession getImageSession() throws DaoException {
        return new ForumImageSessionDao().getImageSession();
    }
}
