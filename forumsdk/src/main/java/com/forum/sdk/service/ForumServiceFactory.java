package com.forum.sdk.service;


import com.forum.sdk.service.impl.ForumCounterService;
import com.forum.sdk.service.impl.ForumPostService;
import com.forum.sdk.service.impl.ForumSectionService;
import com.forum.sdk.service.impl.ForumSubscribeService;
import com.forum.sdk.service.impl.ForumThreadService;
import com.forum.sdk.service.impl.ForumUserService;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 论坛服务工厂
 *
 * @author  : huangszh
 * @date 2015年04月01日 09:58:51
 * @see
 */
public enum ForumServiceFactory {

    /**
     * The INSTANCE.
     */
    INSTANCE;

    /** 版块服务 */
    private IForumSectionService iForumSectionService;

    /** 主帖服务 */
    private IForumPostService iForumPostService;

    /** 跟帖服务 */
    private IForumThreadService iForumThreadService;

    /** 计数器服务 */
    private IForumCounterService iForumCounterService;

    /** 订阅服务 */
    private IForumSubscribeService iForumSubscribeService;

    /** 用户服务*/
    private IForumUserService iForumUserService;

    private final AtomicBoolean mSectionFlag = new AtomicBoolean();
    private final AtomicBoolean mPostFlag = new AtomicBoolean();
    private final AtomicBoolean mThreadFlag = new AtomicBoolean();
    private final AtomicBoolean mCounterFlag = new AtomicBoolean();
    private final AtomicBoolean mSubscribeFlag = new AtomicBoolean();
    private final AtomicBoolean mUserFlag = new AtomicBoolean();

    /**
     * 获取版块服务实现
     *
     * @return  com.morningbaby.forum section service
     * @author  : huangszh
     * @date  : 2015年04月01日 13:58:36
     */
    public IForumSectionService getForumSectionService() {
        if(iForumSectionService == null){
            synchronized (mSectionFlag){
                if(iForumSectionService == null) {
                    iForumSectionService = new ForumSectionService();
                }
            }
        }
        return iForumSectionService;
    }


    /**
     * 获取主帖服务实现
     *
     * @return  com.morningbaby.forum post service
     * @author  : huangszh
     * @date  : 2015年04月02日 10:48:04
     */
    public IForumPostService getForumPostService() {
        if(iForumPostService == null){
            synchronized (mPostFlag){
                if(iForumPostService == null) {
                    iForumPostService = new ForumPostService();
                }
            }
        }
        return iForumPostService;
    }

    /**
     * 获取跟帖服务实现
     *
     * @return  com.morningbaby.forum thread service
     * @author  : huangszh
     * @date  : 2015年04月02日 10:48:04
     */
    public IForumThreadService getForumThreadService() {
        if(iForumThreadService == null){
            synchronized (mThreadFlag){
                if(iForumThreadService == null) {
                    iForumThreadService = new ForumThreadService();
                }
            }
        }
        return iForumThreadService;
    }

    /**
     * 获取计数器服务实现
     *
     * @return  com.morningbaby.forum counter service
     * @author  : huangszh
     * @date  : 2015年04月02日 10:48:04
     */
    public IForumCounterService getForumCounterService() {
        if(iForumCounterService == null){
            synchronized (mCounterFlag){
                if(iForumCounterService == null) {
                    iForumCounterService = new ForumCounterService();
                }
            }
        }
        return iForumCounterService;
    }

    /**
     * 获取订阅服务实现
     *
     * @return  com.morningbaby.forum subscribe service
     * @author  : huangszh
     * @date  : 2015年04月02日 10:48:04
     */
    public IForumSubscribeService getForumSubscribeService() {
        if(iForumSubscribeService == null){
            synchronized (mSubscribeFlag){
                if(iForumSubscribeService == null) {
                    iForumSubscribeService = new ForumSubscribeService();
                }
            }
        }
        return iForumSubscribeService;
    }

    /**
     * 获取用户服务实现
     *
     * @return  com.morningbaby.forum user service
     * @author  : huangszh
     * @date  : 2015年04月02日 10:48:04
     */
    public IForumUserService getForumUserService() {
        if(iForumUserService == null){
            synchronized (mUserFlag){
                if(iForumUserService == null) {
                    iForumUserService = new ForumUserService();
                }
            }
        }
        return iForumUserService;
    }

}
