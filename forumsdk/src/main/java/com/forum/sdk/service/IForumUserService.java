package com.forum.sdk.service;


import com.forum.sdk.bean.user.ForumImageSession;
import com.nd.smartcan.frame.exception.DaoException;

/**
 * 用户服务
 * Created by zhuhp on 2015/4/30.
 */
public interface IForumUserService {

    /**
     * 内容服务的上传授权session获取
     * @return ForumImageSession
     * @throws DaoException
     */
    ForumImageSession getImageSession() throws DaoException;
}
