package com.forum.sdk.service;


import com.forum.sdk.bean.user.ForumSectionUserInfo;
import com.forum.sdk.bean.user.ForumSectionUserList;
import com.nd.smartcan.frame.exception.DaoException;

/**
 * 订阅服务
 *
 * @author  : huangszh
 * @date 2015年04月01日 17:39:39
 * @see
 */
public interface IForumSubscribeService {

    /**
     * 订阅版块
     *
     * @param sectionId 版块id
     * @return com.morningbaby.forum section user info
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:13:20
     */
    ForumSectionUserInfo followSection(String sectionId) throws DaoException;

    /**
     * 取消订阅版块
     *
     * @param sectionId 版块id
     * @return com.morningbaby.forum section user info
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:13:20
     */
    ForumSectionUserInfo unFollowSection(String sectionId) throws DaoException;

    /**
     *修改版块成员角色
     *
     * @param sectionId 版块id
     * @param uid 用户id
     * @param role 角色类型，值参见 @RoleType 1:版主,2:管理员,3:普通成员
     * @return com.morningbaby.forum section user info
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:13:20
     */
    ForumSectionUserInfo editSectionMemberRole(String sectionId, long uid, int role) throws DaoException;

    /**
     * 获取版块成员列表
     *
     * @param sectionId 版块id
     * @param page 页偏移量，不小于0的值，传错置为0，默认为0
     * @param size 每页条数限制，默认20，最大100，传错置为20
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return  section member list
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:13:20
     */
    ForumSectionUserList getSectionMemberList(String sectionId, int page, int size, boolean isNeedCount) throws DaoException;

}
