package com.forum.sdk.service;


import com.forum.sdk.bean.section.ForumCategoryList;
import com.forum.sdk.bean.section.ForumSearchList;
import com.forum.sdk.bean.section.ForumSectionInfo;
import com.forum.sdk.bean.section.ForumSectionList;
import com.forum.sdk.bean.tag.ForumTagList;
import com.forum.sdk.dao.section.bean.ForumSectionParam;
import com.nd.smartcan.frame.exception.DaoException;

/**
 * 版块服务端接口
 *
 * @author  : huangszh
 * @date 2015年04月01日 09:59:37
 * @see
 */
public interface IForumSectionService {

    /**
     * 创建版块
     *
     * @param sectionPost 创建所需参数
     * @return 不为null时 ，表示创建成功
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月01日 10:27:54
     */
    ForumSectionInfo createSection(ForumSectionParam sectionPost) throws DaoException;


    /**
     * 修改版块信息
     *
     * @param sectionId 版块uuid
     * @param sectionPost 修改的版块信息
     * @return section com.morningbaby.forum section info
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月01日 10:27:51
     */
    ForumSectionInfo editSection(String sectionId, ForumSectionParam sectionPost) throws DaoException;

    /**
     * 获取版块信息
     *
     * @param sectionId 版块id
     * @return section section info
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月01日 10:28:39
     */
    ForumSectionInfo getSectionInfo(String sectionId) throws DaoException;


    /**
     * 获取标签列表
     *
     * @param page 页偏移量，不小于0的值，传错置为0，默认为0
     * @param size 每页条数限制，默认20，最大100，传错置为20
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return tag list
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:00:07
     */
    ForumTagList getTagList(int page, int size, boolean isNeedCount) throws DaoException;

    /**
     * 获取我订阅的版块列
     *
     * @param page 页偏移量，不小于0的值，传错置为0，默认为0
     * @param size 每页条数限制，默认20，最大100，传错置为20
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return my follow section list
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:00:07
     */
    ForumSectionList getMyFollowSectionList(int page, int size, boolean isNeedCount) throws DaoException;


    /**
     * 获取我管理的版块列表
     *
     * @param page 页偏移量，不小于0的值，传错置为0，默认为0
     * @param size 每页条数限制，默认20，最大100，传错置为20
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return my manage section list
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:00:08
     */
    ForumSectionList getMyManageSectionList(int page, int size, boolean isNeedCount) throws DaoException;

    /**
     * 获取热门版块列表
     *
     * @param page 页偏移量，不小于0的值，传错置为0，默认为0
     * @param size 每页条数限制，默认20，最大100，传错置为20
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return hot section list
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月01日 14:02:18
     */
    ForumSectionList getHotSectionList(int page, int size, boolean isNeedCount) throws DaoException;

    /**
     * 搜索版块
     *
     * @param tag 标签名称，和name至少要填一个
     * @param name 版块名称，和tag至少要填一个
     * @param page 页偏移量，不小于0的值，传错置为0，默认为0
     * @param size 每页条数限制，默认20，最大100，传错置为20
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return com.morningbaby.forum section list
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:00:08
     */
    ForumSectionList searchSectionList(String tag, String name, int page, int size, boolean isNeedCount) throws DaoException;


    /**
     * 获取版块的类别列表
     *
     * @param page the page
     * @param size the size
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return the category list
     * @throws DaoException the dao exception
     * @author  : huangszh
     */
    ForumCategoryList getCategoryList(int page, int size, boolean isNeedCount) throws DaoException;


    /**
     * 根据分类id获取版块列表
     *
     * @param id the id
     * @param page the page
     * @param size the size
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return the section list by category id
     * @throws DaoException the dao exception
     * @author  : huangszh
     */
    ForumSectionList getSectionListByCategoryId(String id, int page, int size, boolean isNeedCount) throws DaoException;

    /**
     * 版块搜索关键字列表
     *
     * @param page the page
     * @param size the size
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return the section list by category id
     * @throws DaoException the dao exception
     * @author  : zhuhp
     */
    ForumSearchList getSearchInfoList(int page, int size, boolean isNeedCount) throws  DaoException;

    /**
     * 获取缓存的热门版块列表
     *
     * @param uid the uid
     * @return the cache section list by uid
     * @author  : zhuhp
     */
    ForumSectionList getCacheHotSectionList(long uid);

    /**
     * 清除缓存的热门版块列表
     *
     * @param uid the uid
     * @return the cache section list by uid
     * @author  : zhuhp
     */
    void clearCacheHotSectionList(long uid);

    /**
     * 保存缓存的热门版块列表
     *
     * @param uid the uid
     * @return the cache section list by uid
     * @author  : zhuhp
     */
    void saveHotSectionList(ForumSectionList list, long uid);
}
