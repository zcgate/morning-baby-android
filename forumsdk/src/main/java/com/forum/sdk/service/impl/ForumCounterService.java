package com.forum.sdk.service.impl;


import com.forum.sdk.bean.counter.ForumUnreadCounter;
import com.forum.sdk.dao.counter.ForumCounterDao;
import com.forum.sdk.service.IForumCounterService;
import com.nd.smartcan.frame.exception.DaoException;

/**
 * 计数器服务具体实现类
 *
 * @author  : huangszh
 * @date 2015年04月02日 10:45:41
 * @see
 */
public class ForumCounterService implements IForumCounterService {

    @Override
    public ForumUnreadCounter getUnreadCounter() throws DaoException {
        return new ForumCounterDao().getUnreadCounter();
    }
}
