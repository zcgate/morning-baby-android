package com.forum.sdk.service.impl;

import com.forum.sdk.bean.user.ForumSectionUserInfo;
import com.forum.sdk.bean.user.ForumSectionUserList;
import com.forum.sdk.dao.subscribe.ForumSubscribeDao;
import com.forum.sdk.service.IForumSubscribeService;
import com.nd.smartcan.frame.exception.DaoException;

/**
 * 订阅服务具体实现类
 *
 * @author  : huangszh
 * @date 2015年04月02日 10:45:03
 * @see
 */
public class ForumSubscribeService implements IForumSubscribeService {

    @Override
    public ForumSectionUserInfo followSection(String sectionId) throws DaoException {

        return new ForumSubscribeDao().followSection(sectionId);
    }

    @Override
    public ForumSectionUserInfo unFollowSection(String sectionId) throws DaoException {

        return new ForumSubscribeDao().unFollowSection(sectionId);
    }

    @Override
    public ForumSectionUserInfo editSectionMemberRole(String sectionId, long uid, int role) throws DaoException {

        return new ForumSubscribeDao().editSectionMemberRole(sectionId, uid, role);
    }

    @Override
    public ForumSectionUserList getSectionMemberList(String sectionId, int page, int size, boolean isNeedCount) throws DaoException {

        return new ForumSubscribeDao().getSectionMemberList(sectionId, page, size, isNeedCount);
    }
}
