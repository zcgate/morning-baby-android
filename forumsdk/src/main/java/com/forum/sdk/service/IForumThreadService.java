package com.forum.sdk.service;


import com.forum.sdk.bean.post.ForumThreadInfo;
import com.forum.sdk.bean.post.ForumThreadList;
import com.forum.sdk.dao.thread.bean.ForumThreadParam;
import com.nd.smartcan.frame.exception.DaoException;

import java.util.List;

/**
 * 跟帖服务
 *
 * @author  : huangszh
 * @date 2015年04月02日 10:34:55
 * @see
 */
public interface IForumThreadService {

    /**
     * 发布跟帖
     *
     * @param forumThreadParam 跟帖信息
     * @return com.morningbaby.forum thread
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:34:55
     */
    ForumThreadInfo createThread(ForumThreadParam forumThreadParam) throws DaoException;

    /**
     * 删除跟帖
     *
     * @param threadId 跟帖id
     * @return com.morningbaby.forum thread
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:34:55
     */
    ForumThreadInfo deleteThread(String threadId) throws DaoException;

    /**
     * 获取主贴的跟帖列表
     *
     * @param postId 主帖id
     * @param minThread 获取大于minThread的跟帖列表，首次获取时传0
     * @param size 获取的数量
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return post thread list
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:34:55
     */
    ForumThreadList getPostThreadList(String postId, long minThread, int size, boolean isNeedCount) throws DaoException;

    /**
     * 获取我收到的跟帖列表
     *
     * @param maxThread 获取小于maxThread的跟帖列表，首次获取时传Long.MAX_VALUE
     * @param size 获取的数量
     * @param isNeedCount 是否需要返回总数，false不需要，true需要
     * @return post reply me thread list
     * @throws DaoException the dao exception
     * @author  : huangszh
     * @date  : 2015年04月02日 10:34:55
     */
    ForumThreadList getPostReplyMeThreadList(long maxThread, int size, boolean isNeedCount) throws DaoException;

    /**
     * 批量获取跟帖详情
     * @param idList 跟帖id列表
     * @return ForumPostList
     * @throws DaoException
     */
    ForumThreadList getThreadList(List<String> idList) throws DaoException;
}

