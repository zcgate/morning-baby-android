package com.forum.sdk.common;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.forum.sdk.dao.section.bean.ForumSectionParam;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.core.restful.ClientResource;
import com.nd.smartcan.core.restful.ResourceException;
import com.nd.smartcan.datalayer.interfaces.IDataResult;
import com.nd.smartcan.frame.dao.CacheDao;
import com.nd.smartcan.frame.dao.RestDao;
import com.nd.smartcan.frame.exception.DaoException;

import java.io.IOException;
import java.util.Map;

/**
 * Created by dcj on 2015/8/18.
 */
public abstract class ForumRestDao<T> extends CacheDao<T> {

    protected static final String DATASOURCE_NAME = "base";
    
    protected String getUserId() {
        return ForumRequireUser.USER_ID;
    }

    @Override
    protected <R> R get(String uri, Map<String, Object> param, Class<R> returnClass) throws DaoException {
        ClientResource cr = new ClientResource(uri);
        cr.bindArgument(param);
        cr.addHeader("USER-ID", getUserId());
        try {
            return cr.get(returnClass);
        } catch (ResourceException var6) {
            throw new DaoException(var6);
        }
    }

    @Override
    protected <R> R post(String uri, Object data, Map<String, Object> param, Class<R> returnClass) throws DaoException {
        ClientResource cr = new ClientResource(uri);
        cr.bindArgument(param);
        cr.addField(data);
        cr.addHeader("USER-ID", getUserId());
        try {
            return cr.post(returnClass);
        } catch (ResourceException var7) {
            throw new DaoException(var7);
        }
    }


    @Override
    protected <R> R put(String uri, Object data, Map<String, Object> param, Class<R> returnClass) throws DaoException {
        ClientResource cr = new ClientResource(uri);
        cr.bindArgument(param);
        cr.addField(data);
        cr.addHeader("USER-ID", getUserId());
        try {
            return cr.put(returnClass);
        } catch (ResourceException var7) {
            throw new DaoException(var7);
        }
    }

    @Override
    protected <R> R delete(String uri, Map<String, Object> param, Class<R> returnClass) throws DaoException {
        ClientResource cr = new ClientResource(uri);
        cr.bindArgument(param);
        cr.addHeader("USER-ID", getUserId());
        try {
            return cr.delete(returnClass);
        } catch (ResourceException var6) {
            throw new DaoException(var6);
        }
    }

    @Override
    protected <R> R patch(String uri, Map<String, Object> data, Map<String, Object> param, Class<R> returnClass) throws DaoException {
        ClientResource cr = new ClientResource(uri);
        cr.bindArgument(param);
        cr.addField(data);
        cr.addHeader("USER-ID", getUserId());
        try {
            return cr.patch(returnClass);
        } catch (ResourceException var7) {
            throw new DaoException(var7);
        }
    }

    protected <R> R get(String uri, Map<String, Object> param, TypeReference<R> typeReference) throws DaoException {
        ClientResource cr = new ClientResource(uri);
        cr.bindArgument(param);
        cr.addHeader("USER-ID", getUserId());
        try {
            String e = cr.get();
            Logger.d(this.getClass(), "toObject start");
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            Logger.d(this.getClass(), "ObjectMapper created");
            Object r = objectMapper.readValue(e, typeReference);
            Logger.d(this.getClass(), "toObject end");
            return (R)r;
        } catch (ResourceException var8) {
            throw new DaoException(var8);
        } catch (JsonMappingException var9) {
            Logger.w("RestDao", var9.getMessage());
            throw new DaoException(0, var9.getMessage());
        } catch (JsonParseException var10) {
            Logger.w("RestDao", var10.getMessage());
            throw new DaoException(0, var10.getMessage());
        } catch (IOException var11) {
            Logger.w("RestDao", var11.getMessage());
            throw new DaoException(0, var11.getMessage());
        }
    }


    public boolean canLoadMore(){
        IDataResult result = getDefaultListDataLayer().getDataLayer().dataSourceForName(DATASOURCE_NAME).allResult();
        if(result==null){
            return false;
        }
        return result.canLoadMore();
    }
}
