package com.forum.sdk.common;

/**
 * 论坛常量定义
 *
 * @author  : huangszh
 * @date 2015年04月01日 09:45:56
 * @see
 */
public class ForumConstDefine {

    /** ClientType*/
    public static final String FORUM_CLIENT_TYPE = "Android";

    /**
     * URL域名使用key
     * @author  : huangszh
     */
    public static class UrlResourceConst{

        /** URL 在clientresource里的key*/
        public static final String URL_RESOURCE_KEY = "ForumUrl";

        /** URL在DAO里的要被替换的字符串*/
        public static final String URL_RESOURCE_CONTENT = "${" + URL_RESOURCE_KEY + "}";
    }

    public static class UserResourceConst{

        /** user 在clientresource里的key*/
        public static final String USER_RESOURCE_KEY = "userId";

        /** user 在DAO里的要被替换的字符串*/
        public static final String USER_RESOURCE_CONTENT = "${" + USER_RESOURCE_KEY + "}";
    }

    /**
     * 成员角色类别
     *
     * @author  : huangszh
     * @date 2015年04月02日 15:21:39
     */
    public static class RoleType{
        /** 版主 */
        public static final int SECTION_OWNER = 1;
        /** 管理员 */
        public static final int SECTION_MANAGER = 2;
        /** 普通成员 */
        public static final int SECTION_MEMBER = 3;
    }

    /**
     * 版块是否需要审核类别
     *
     * @author  : zhuhp
     * @date 2015年04月02日 15:21:39
     */
    public static class CheckType{
        /** 需要审核*/
        public static final int NEED_CHECK = 1;
        /** 不需要审核*/
        public static final int NO_NEED_CHECK = 0;
    }

    /**
     * 帖子标志
     * 二进制表示，第一位表示是否置顶，第二位表示是否加精，比如1表示置顶但没有加精，2表示加精没有置顶，3表示置顶+加精
     * @author  : huangszh
     */
    public static class FlagState {
        /** 未置顶 */
        public static final int NORMAL = 0;
        /** 置顶  */
        public static final int TOP = 1;
        /** 加精  */
        public static final int BOUTIQUE = 2;
    }

    /**
     * 审批状态
     *
     * @author  : huangszh
     * @date 2015年04月02日 15:23:46
     */
    public static class ApprovalStatus{
        /** 未审批 */
        public static final int APPROVAL_WAITING = 0;
        /** 审批通过 */
        public static final int APPROVAL_SUCCESS = 1;
        /** 审批拒绝 */
        public static final int APPROVAL_REFUSE = 2;
    }

    /**
     * 举报处理状态
     *
     * @author  : huangszh
     */
    public static class ReportStatus{
        /** 未处理审批 */
        public static final int REPORT_WAITING = 0;
        /** 驳回 */
        public static final int REPORT_REFUSE = 1;
        /** 删除 */
        public static final int REPORT_DELETE = 2;
    }


    /**
     * 贴子类别
     *
     * @author  : huangszh
     */
    public static class ContentCategory {

        public static final String TEXT = "TEXT";
        public static final String AUDIO = "AUDIO";
        public static final String VIDEO = "VIDEO";
    }


    /**
     * 举报类型
     *
     * @author  : huangszh
     */
    public static class ReportType{
        /** 其他 */
        public static final int OTHER = 0;
        /** 垃圾广告 */
        public static final int SPAM = 1;
        /** 反动 */
        public static final int REACTION = 2;
        /** 色情 */
        public static final int EROTICISM = 3;
        /** 暴力 */
        public static final int VIOLENCE = 4;
    }

    /**
     * 主贴/帖子状态类型
     *
     * @author  : huangszh
     */
    public static class StatusType{
        /** 正常显示 */
        public static final int NORMAL = 0;
        /** 管理员删除 */
        public static final int MANAGER_DELETE = 1;
        /** 系统屏蔽 */
        public static final int SYSTEM_SHIELD = 2;
    }


    /**
     * 请求参数key常量
     *
     * @author  : huangszh
     */
    public static class ParamKeyConst{

        /* 热门 */
        public static final String HOT = "hot";

        public static final String TAG_LIST = "tag_list";

        public static final String SUBSCRIBED = "subscribed";

        public static final String MANAGED = "managed";

        public static final String TAG_NAME = "tag_name";

        public static final String NAME = "name";

        public static final String DETAIL = "detail";

        public static final String LIST = "list";

        public static final String COUNTER = "counter";

        public static final String COUNTER_LIST = "counter_list";

        public static final String FORUM = "forum";

        public static final String RSS = "rss";

        public static final String USER = "user";

        public static final String POSTS = "posts";

        public static final String UNREAD = "unread";

        public static final String MARKED = "marked";

        public static final String ROLE = "role";

        public static final String SEARCH = "search";

        public static final String CATEGORY = "category";

        public static final String CATEGORY_LIST = "category_list";

        public static final String PAGE = "page";

        public static final String SIZE = "size";

        public static final String $COUNT = "$count";

        public static final String $LIMIT = "$limit";

        public static final String MIN_ACTIVE = "min_active";

        public static final String MAX_ACTIVE = "max_active";

        public static final String MIN_THREAD_ID = "min_thread_id";

        public static final String MAX_THREAD_ID = "max_thread_id";

        public static final String SESSION = "sessions";

        public static final String SEARCH_LIST = "search_list";
    }

    /** 默认页大小为20 */
    public static final int PAGE_SIZE = 20;
    /** 最大限制为100 */
    public static final int MAX_PAGE_SIZE = 100;

    public static class TableName{
        public static final String TABLE_SECTION = "section_info";
        public static final String TABLE_POST = "post_info";
    }

    public static class TableColumnName{
        public static final String COLUMN_CURRENT_UID = "current_uid";
        public static final String COLUMN_POST_TYPE = "post_type";
    }

    public static class PostListType{
        public static final String LIST_HOT = "hot";
        public static final String LIST_SUBSCRIBE = "subscribe";
    }

}
