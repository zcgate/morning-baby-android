package com.forum.sdk.common;

public class ForumRequireUrl {

    public static final String BASE_URL = ForumConstDefine.UrlResourceConst.URL_RESOURCE_CONTENT;
    //public static final String BASE_URL = "http://192.168.0.191:8080/api/v1/";
    /**
     * 版块服务url
     */
    public static final String SECTION_URL = BASE_URL + "forums";

    /**
     * 主帖服务url
     */
    public static final String POST_URL = BASE_URL + "posts";

    /**
     * 主帖时间线服务url
     */
    public static final String POST_TIMELINE_URL = BASE_URL + "timelines";

    /**
     * 跟帖服务url
     */
    public static final String THREAD_URL = BASE_URL + "threads";

    /**
     * 计数器服务url
     */
    public static final String COUNTER_URL = BASE_URL + "counters";

    /**
     * 订阅服务url
     */
    public static final String SUBSCRIBE_URL = BASE_URL + "subscribers";

    /**
     * 举报服务url
     */
    public static final String REPORT_URL = BASE_URL + "reports";

    /**
     * 置顶服务url
     */
    public static final String TOP_URL = BASE_URL + "top";

    /**
     * 内容服务url
     */
    public static final String IMAGE_URL = BASE_URL + "images";

}
