package com.morningbaby.app.gui.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.morningbaby.app.MorningBabyAction;
import com.morningbaby.app.R;
import com.morningbaby.app.gui.activity.KnowledgeSearchActivity;
import com.morningbaby.app.logic.BaseFragment;
import com.morningbaby.sdk.model.KnowledgeTopic;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;
import com.viewpagerindicator.TabPageIndicator;

import java.util.List;
import java.util.Map;

/**
 * Created by apple on 15/8/4.
 */
public class KnowledgeLibFragment extends BaseFragment {
    private List<KnowledgeTopic> mKnowledgeTopics;
    private ViewGroup m_main;
    private Handler mHandler=new Handler();
    private ViewPager pager;
    @Override
    protected String getTitle() {
        return getString(R.string.knowledgelib);
    }

    @Override
    protected boolean showTitle() {
        return false;
    }

    @Override
    protected View createContentView() {
        m_main= (ViewGroup) LayoutInflater.from(getActivity()).inflate(R.layout.knowledge_loading, null);
        return m_main;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        IListDataRetrieveListener<KnowledgeTopic> listener = new IListDataRetrieveListener<KnowledgeTopic>() {
            @Override
            public void onCacheDataRetrieve(List<KnowledgeTopic> knowledgeTopics, Map<String, Object> stringObjectMap, boolean b) {
                mKnowledgeTopics=knowledgeTopics;
            }

            @Override
            public void onServerDataRetrieve(List<KnowledgeTopic> knowledgeTopics, Map<String, Object> stringObjectMap) {
                mKnowledgeTopics=knowledgeTopics;
            }

            @Override
            public void done() {
                displayFragments();
            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
                Toast.makeText(getActivity(),R.string.network_error,Toast.LENGTH_SHORT).show();
                displayFragments();
            }

            @Override
            public Handler getCallBackLooperHandler() {
                return mHandler;
            }
        };
        new KnowledgeTopic().getTopicList(listener, false);
        getView().findViewById(R.id.iv_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoActivity(KnowledgeSearchActivity.class);
            }
        });
    }

    private void displayFragments(){
        if(isDetached())
            return;
        KnowledgeAdapter adapter=new KnowledgeAdapter(getFragmentManager());
        View tab = LayoutInflater.from(getActivity()).inflate(R.layout.knowledge,
                null);
        m_main.addView(tab, 1);

        pager = (ViewPager) tab.findViewById(R.id.tab_viewpager);
        pager.setOffscreenPageLimit(3);
        pager.setAdapter(adapter);

        TabPageIndicator indicator = (TabPageIndicator) tab.findViewById(R.id.tab_indicator);
        indicator.setViewPager(pager);
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int arg0) {

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });

    }

    class KnowledgeAdapter extends FragmentStatePagerAdapter {
        public KnowledgeAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if(mKnowledgeTopics !=null)
                return mKnowledgeTopics.get(position).getName();
            return "";
        }

        @Override
        public Fragment getItem(int position) {
            KnowledgeTopic topic= mKnowledgeTopics.get(position);
            Bundle bundle=new Bundle();
            bundle.putString("topicId",topic.getId());
            KnowledgeFragment fragment=new KnowledgeFragment();
            fragment.setArguments(bundle);
            return fragment;
        }

        @Override
        public int getCount() {
            if(mKnowledgeTopics !=null)
                return mKnowledgeTopics.size();
            return 0;
        }
    }

    @Override
    protected void onReceiveAction(Context context, Intent intent) {
        String action=intent.getAction();
        if(MorningBabyAction.ACTION_SCREENING_MORE.equals(action)){
            if(pager!=null)
                pager.setCurrentItem(0);
        }
    }

    @Override
    protected void addActions(IntentFilter intentFilter) {
        addAction(MorningBabyAction.ACTION_SCREENING_MORE, intentFilter);
    }
}
