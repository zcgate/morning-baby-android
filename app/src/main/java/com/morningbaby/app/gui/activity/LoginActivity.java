package com.morningbaby.app.gui.activity;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.morningbaby.app.R;
import com.morningbaby.app.logic.ActivityCommand;
import com.morningbaby.app.logic.BaseActivity;
import com.morningbaby.app.logic.RequestControl;
import com.morningbaby.app.tool.ImageLoaderControl;
import com.morningbaby.app.tool.PublicUtil;
import com.morningbaby.app.view.RoundImageView;
import com.morningbaby.sdk.model.ReturnObject;
import com.morningbaby.sdk.model.User;

/**
 * Created by apple on 15/8/3.
 */
public class LoginActivity extends BaseActivity implements View.OnClickListener{
    private EditText mAccountView, mPwdView;
    @Override
    protected int getContentViewID() {
        return R.layout.login;
    }

    @Override
    protected void onAfterCreate(Bundle savedInstanceState) {
        Bitmap bg=new ImageLoaderControl(Bitmap.Config.ARGB_8888).loadImageSync(R.drawable.mem_login_bg);
        findViewById(R.id.content).setBackgroundDrawable(new BitmapDrawable(bg));

        RoundImageView logo= (RoundImageView) findViewById(R.id.iv_logo);
        int radius = PublicUtil.dip2px(this, 100);
        logo.setCornerRadius(radius, radius, radius, radius);

        mAccountView= (EditText) findViewById(R.id.et_account);
        mPwdView = (EditText) findViewById(R.id.et_pwd);
        findViewById(R.id.btn_login).setOnClickListener(this);
        findViewById(R.id.btn_register).setOnClickListener(this);
    }

    private void login(){
        final String account=mAccountView.getText().toString();
        final String pwd=mPwdView.getText().toString();
        if(account.length()==0|| pwd.length()==0)
            return;
        RequestControl control=new RequestControl(new Handler());
        control.doRequest(this,new RequestControl.RequestCallback() {
            private boolean isNeedCompleteInfo=false;
            @Override
            public Object request() {
                User user=User.getCurrentUser();
                boolean flag= user.login(account, pwd);
                user.loadUserData();
                isNeedCompleteInfo=user.needCompleteInfo();
                return flag;
            }

            @Override
            public void afterRequest(Object result) {
                boolean data= (boolean) result;
                if(data){
                    Toast.makeText(LoginActivity.this, R.string.mem_login_succeed, Toast.LENGTH_SHORT).show();
                    if(isNeedCompleteInfo)
                        gotoActivity(BabyInfoActivity.class);
                    else
                        gotoActivity(MainActivity.class);
                    finish();
                }
                else{
                    Toast.makeText(LoginActivity.this,R.string.mem_login_failed,Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_login:
                login();
                break;
            case R.id.btn_register:
                gotoActivity(RegisterActivity.class);
                break;
        }
    }
}
