package com.morningbaby.app.gui.fragment;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.morningbaby.app.R;
import com.morningbaby.app.adapter.HospitalAdapter;
import com.morningbaby.app.gui.activity.HospitalDetailActivity;
import com.morningbaby.app.logic.ActivityCommand;
import com.morningbaby.app.logic.BaseFragment;
import com.morningbaby.app.logic.CustomLocationManager;
import com.morningbaby.app.view.CustomRefreshLayout;
import com.morningbaby.sdk.model.Hospital;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by apple on 15/8/5.
 */
public class HospitalFragment extends BaseFragment {

    private ListView m_lv_hospitals;
    private HospitalAdapter mAdapter;
    private List<Hospital> mHospitals;
    private Location mLocation;
    private Hospital mHospitalObject;

    private Handler mHandler = new Handler();
    private CustomRefreshLayout mRefreshLayout;

    private boolean locationEnable=true;

    private ListHeaderCallback mHeaderCallback;
    @Override
    protected int getContentViewID() {
        return R.layout.list;
    }

    @Override
    protected boolean showTitle() {
        return false;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mHospitalObject = new Hospital();
        m_lv_hospitals = (ListView) getView().findViewById(R.id.listview);
        if(mHeaderCallback!=null){
            m_lv_hospitals.addHeaderView(mHeaderCallback.getHeaderView());
        }
        mRefreshLayout = (CustomRefreshLayout) getView().findViewById(R.id.swiperefresh);
        mRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.orange));
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData(true);
            }
        });
        mRefreshLayout.setOnLoadListener(new CustomRefreshLayout.OnLoadListener() {
            @Override
            public void onLoad() {
                nextPage();
            }
        });
        new CustomLocationManager(new CustomLocationManager.CustomLocationListener() {
            @Override
            public void onLocationChange(Location location) {
                mLocation = location;
                if (mAdapter != null) {
                    mAdapter.setLocation(mLocation);
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void unsupport() {
                locationEnable=false;
                Toast.makeText(getActivity(),R.string.hospital_open_location,Toast.LENGTH_LONG).show();
            }

            @Override
            public void timeout() {

            }
        }).startLocationListener();
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mRefreshLayout.setRefreshing(true);
                loadData(false);
            }
        });
        m_lv_hospitals.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                position=position-m_lv_hospitals.getHeaderViewsCount();
                Bundle bundle=new Bundle();
                bundle.putString("hospitalId", mHospitals.get(position).getId());
                ActivityCommand com=new ActivityCommand();
                com.setBundle(bundle);
                gotoActivity(com, HospitalDetailActivity.class);
            }
        });
    }

    public void setListHeaderCallback(ListHeaderCallback callback){
        mHeaderCallback=callback;
    }

    public void loadData(boolean force) {
        mHospitalObject.getHospitalList(listener, force);
        if(mHeaderCallback!=null)
            mHeaderCallback.loadData(force);
    }

    public void nextPage() {
        mHospitalObject.nextPage(listener);
    }

    private Object sync = new Object();

    private void showHospitalList(List<Hospital> Hospitals) {
        synchronized (sync) {
            if(isDetached())
                return;
            if (mHospitals == null) {
                mHospitals = new ArrayList<>();
            } else {
                mHospitals.clear();
            }
            if (Hospitals != null) {
                mHospitals.addAll(Hospitals);
            }
            if (mAdapter == null) {
                mAdapter = new HospitalAdapter(getActivity(), mHospitals);
                mAdapter.setLocation(mLocation);
                m_lv_hospitals.setAdapter(mAdapter);
            } else {
                mAdapter.notifyDataSetChanged();
            }
            mAdapter.setLocationEnable(locationEnable);
        }
    }

    IListDataRetrieveListener<Hospital> listener = new IListDataRetrieveListener<Hospital>() {

        @Override
        public void onCacheDataRetrieve(List<Hospital> Hospitals, Map<String, Object> stringObjectMap, boolean b) {
            Logger.i(getClass(), "retrieve Hospital list from cache.");
            showHospitalList(Hospitals);
        }

        @Override
        public void onServerDataRetrieve(List<Hospital> Hospitals, Map<String, Object> stringObjectMap) {
            Logger.i(getClass(), "retrieve Hospital list from server.");
            showHospitalList(Hospitals);
        }

        @Override
        public void done() {
            mRefreshLayout.setRefreshing(false);
            mRefreshLayout.setLoading(false);
            mRefreshLayout.setCanLoadMore(mHospitalObject.canLoadMore());
        }

        @Override
        public void onException(Exception e) {
            e.printStackTrace();
            fail();
        }

        @Override
        public Handler getCallBackLooperHandler() {
            return mHandler;
        }
    };

    private void fail() {
        Toast.makeText(getActivity(), R.string.network_error, Toast.LENGTH_SHORT).show();
    }

    public interface ListHeaderCallback{
        public View getHeaderView();
        public void loadData(boolean force);
    }
}
