package com.morningbaby.app.gui.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.morningbaby.app.MorningBabyAction;
import com.morningbaby.app.R;
import com.morningbaby.app.adapter.HotActivityAdapter;
import com.morningbaby.app.gui.activity.CreateCommunityActivity;
import com.morningbaby.app.logic.BaseFragment;
import com.morningbaby.app.view.DotView;
import com.morningbaby.app.view.forum.ForumHotView;
import com.morningbaby.app.view.forum.ForumSubscribeView;
import com.morningbaby.sdk.model.HotActivity;
import com.viewpagerindicator.UnderlinePageIndicator;

import java.util.List;

/**
 * Created by apple on 15/8/4.
 */
public class ForumFragment extends BaseFragment implements View.OnClickListener, ViewPager.OnPageChangeListener{
    private Dialog mLetterDialog;
    private Handler mHandler=new Handler();
    private HotActivityAdapter mHotActAdapter;
    private DotView mDotView;
    private List<HotActivity> mHotActivityData;

    private HospitalFragment mFragment;


    private ViewPager mMainViewPager;
    private UnderlinePageIndicator mIndicator;
    private TextView mHotTextView;
    private TextView mSubscribeTextView;
    private MyPagerAdapter mAdapter;

    @Override
    protected int getContentViewID() {
        return R.layout.forum;
    }

    @Override
    protected boolean showTitle() {
        return false;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initContentView();
        initEvent();
    }

    @Override
    protected void addActions(IntentFilter intentFilter) {
        addAction(MorningBabyAction.ACTION_SCREENING_DONE, intentFilter);
    }

    @Override
    protected void onReceiveAction(Context context, Intent intent) {
        String action=intent.getAction();
        if(MorningBabyAction.ACTION_SCREENING_DONE.equals(action)){
            if(mLetterDialog!=null&&mLetterDialog.isShowing())
            {
                mLetterDialog.dismiss();
                mLetterDialog=null;
            }
        }
    }

    protected void initContentView() {


        final View view= getView();

        mMainViewPager = (ViewPager) view.findViewById(R.id.vp_forum_square);
        mMainViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position) {
                mDotView.setCurPoint(position);
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        mIndicator = (UnderlinePageIndicator) view.findViewById(R.id.indicator_square_forum);

        mHotTextView = (TextView) view.findViewById(R.id.tv_forum_square_hot);
        mSubscribeTextView = (TextView) view.findViewById(R.id.tv_form_square_subscribe);

        mAdapter = new MyPagerAdapter(getFragmentManager());
        mMainViewPager.setAdapter(mAdapter);

        mIndicator.setFades(false);
        mIndicator.setSelectedColor(getResources().getColor(R.color.orange));
        mIndicator.setViewPager(mMainViewPager);

        getView().findViewById(R.id.tv_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), CreateCommunityActivity.class);
                startActivity(intent);

            }
        });

    }

    protected void initEvent() {
        mHotTextView.setOnClickListener(this);
        mSubscribeTextView.setOnClickListener(this);
        mIndicator.setOnPageChangeListener(this);
    }

    @Override
    public void onClick(View arg0) {
        int id = arg0.getId();
        if (id == R.id.tv_forum_square_hot) {
            // 点击“热门”tab
            mMainViewPager.setCurrentItem(0);
        } else if (id == R.id.tv_form_square_subscribe) {
            // 点击“订阅”tab
            mMainViewPager.setCurrentItem(1);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        updateTagView(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void updateTagView(int type) {
        Resources res = getResources();
        switch (type) {
            case 0:
                // 帖子列表
                mHotTextView.setTextColor(res.getColor(R.color.orange));
                mSubscribeTextView.setTextColor(res
                        .getColor(R.color.normal));
//            mHotPostTextView.setTextColor(res.getColor(R.color.forum_cor_community_square_header_text_normal));
                break;
            case 1:
                mHotTextView.setTextColor(res.getColor(R.color.normal));
                mSubscribeTextView.setTextColor(res
                        .getColor(R.color.orange));
                break;
            default:
                break;
        }

    }

    public static class MyPagerAdapter extends FragmentPagerAdapter {

        /**
         * “热门”tab所对应的fragment
         */
        private ForumHotView mHotView;

        /**
         * “订阅”tab所对应的fragment
         */
        private ForumSubscribeView mMyGroup;

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return 2;
        }


        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                // 热门团体tab
                if (mHotView == null) {
                    mHotView = ForumHotView.newInstance();
                }
                return mHotView;
            } else if (position == 1) {
                // 推荐tab
                if (mMyGroup == null) {
                    mMyGroup = ForumSubscribeView.newInstance();
                }
                return mMyGroup;
            } else{
                return null;
            }
        }


    }
}
