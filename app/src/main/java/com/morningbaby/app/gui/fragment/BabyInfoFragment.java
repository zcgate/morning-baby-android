package com.morningbaby.app.gui.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.morningbaby.app.R;
import com.morningbaby.app.logic.BaseFragment;
import com.morningbaby.app.logic.RequestControl;
import com.morningbaby.sdk.model.User;

import java.util.Calendar;

/**
 * Created by apple on 15/8/7.
 */
public class BabyInfoFragment extends BaseFragment implements View.OnClickListener{

    private  TextView m_tv_name,m_tv_due_date,m_tv_birthday,
            m_tv_weeks, m_tv_gender,m_tv_reason,m_tv_weight,
            m_tv_height,m_tv_len,m_tv_mark1,m_tv_mark2,m_tv_mark3;

    private User mUser;
    private Handler mHandler=new Handler();
    @Override
    protected boolean showTitle() {
        return false;
    }

    @Override
    protected int getContentViewID() {
        return R.layout.babyinfo_fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getView().findViewById(R.id.ll_name).setOnClickListener(this);
        getView().findViewById(R.id.ll_due_date).setOnClickListener(this);
        getView().findViewById(R.id.ll_birthday).setOnClickListener(this);
        getView().findViewById(R.id.ll_weeks).setOnClickListener(this);
        getView().findViewById(R.id.ll_gender).setOnClickListener(this);
        getView().findViewById(R.id.ll_reason).setOnClickListener(this);
        getView().findViewById(R.id.ll_weight).setOnClickListener(this);
        getView().findViewById(R.id.ll_height).setOnClickListener(this);
        getView().findViewById(R.id.ll_len).setOnClickListener(this);
        getView().findViewById(R.id.ll_mark1).setOnClickListener(this);
        getView().findViewById(R.id.ll_mark2).setOnClickListener(this);
        getView().findViewById(R.id.ll_mark3).setOnClickListener(this);

        m_tv_name=(TextView)getView().findViewById(R.id.tv_name);
        m_tv_due_date=(TextView)getView().findViewById(R.id.tv_due_date);
        m_tv_birthday=(TextView)getView().findViewById(R.id.tv_birthday);
        m_tv_weeks=(TextView)getView().findViewById(R.id.tv_weeks);
        m_tv_gender=(TextView)getView().findViewById(R.id.tv_gender);
        m_tv_reason=(TextView)getView().findViewById(R.id.tv_reason);
        m_tv_weight=(TextView)getView().findViewById(R.id.tv_weight);
        m_tv_height=(TextView)getView().findViewById(R.id.tv_height);
        m_tv_len=(TextView)getView().findViewById(R.id.tv_len);
        m_tv_mark1=(TextView)getView().findViewById(R.id.tv_mark1);
        m_tv_mark2=(TextView)getView().findViewById(R.id.tv_mark2);
        m_tv_mark3=(TextView)getView().findViewById(R.id.tv_mark3);

        initValue();
    }

    public void setUser(User user){
        mUser=user;
    }

    private void initValue(){
        if(mUser==null){
            mUser = User.getCurrentUser();
            RequestControl control=new RequestControl(mHandler);
            control.doRequest(getActivity(), new RequestControl.RequestCallback() {
                @Override
                public Object request() {
                    mUser.loadUserData();
                    return null;
                }

                @Override
                public void afterRequest(Object result) {
                    displayValue();
                }
            });
        }
        else{
            displayValue();
        }
    }

    public void displayValue(){
        m_tv_name.setText(mUser.getBabyName());
        m_tv_due_date.setText(mUser.getBabyBornPdd());
        m_tv_birthday.setText(mUser.getBabyBornDate());
        m_tv_weeks.setText(mUser.getBabyBornWeek() + getString(R.string.week));
        m_tv_gender.setText("male".equals(mUser.getBabyGender())? R.string.gender_male : R.string.gender_female);
        m_tv_reason.setText(mUser.getBabyPrematureReason());
        m_tv_weight.setText(mUser.getBabyBornWeight() + "kg");
        m_tv_height.setText(mUser.getBabyBornLength() + "cm");
        m_tv_len.setText(mUser.getBabyBornHead() + "cm");
        m_tv_mark1.setText(mUser.getBabyApgarScoreOne() + getString(R.string.score));
        m_tv_mark2.setText(mUser.getBabyApgarScoreFive() + getString(R.string.score));
        m_tv_mark3.setText(mUser.getBabyApgarScoreTen() + getString(R.string.score));
    }

    private void popDatePickerDialog(final TextView textView,final ConfirmCallback confirmCallback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LinearLayout main = new LinearLayout(getActivity());
        main.setPadding(0, 10, 0, 0);
        main.setOrientation(LinearLayout.VERTICAL);
        main.setGravity(Gravity.CENTER);
        builder.setView(main);
        final DatePicker datePicker = new DatePicker(getActivity());
        main.addView(datePicker);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        datePicker.init(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH), null);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        StringBuffer sb = new StringBuffer();
                        sb.append(String.format("%d%02d%02d",
                                datePicker.getYear(),
                                datePicker.getMonth() + 1,
                                datePicker.getDayOfMonth()));
                        textView.setText(sb.toString());
                        if(confirmCallback!=null){
                            confirmCallback.contirm(sb.toString());
                        }
                        dialog.cancel();
                    }
                });
        builder.setNegativeButton(android.R.string.cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        Dialog dialog = builder.create();
        dialog.show();
    }

    private void popDialog(final TextView resultView,String defValue,final String unit,boolean isNum,final ConfirmCallback confirmCallback) {
        final Dialog updateDialog = new Dialog(getActivity(), R.style.CustomDialog);
        updateDialog.setContentView(R.layout.babyinfo_dialog);
        final EditText editText= (EditText) updateDialog.findViewById(R.id.et_value);
        editText.setText(defValue);
        if(isNum){
            editText.setInputType(InputType.TYPE_CLASS_NUMBER |InputType.TYPE_NUMBER_FLAG_DECIMAL);
        }
        TextView textView= (TextView) updateDialog.findViewById(R.id.tv_unit);
        if(TextUtils.isEmpty(unit)){
            editText.getLayoutParams().width= ViewGroup.LayoutParams.MATCH_PARENT;
            textView.setVisibility(View.GONE);
        }else{
            textView.setText(unit);
        }
        updateDialog.findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = editText.getText().toString();
                resultView.setText(editText.getText().toString() + unit);
                if (confirmCallback != null) {
                    confirmCallback.contirm(value);
                }
                updateDialog.dismiss();
            }
        });
        updateDialog.show();
    }

    private void changeGender() {
        final String[] mItems = { getString(R.string.gender_male),
                getString(R.string.gender_female)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(mItems, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                m_tv_gender.setText(mItems[which]);
                mUser.setBabyGender(which == 0 ? "male" : "female");
            }
        });
        AlertDialog dialog=builder.create();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }

    private void popScoreDialog(final TextView view,final ConfirmCallback callback){
        final String[] mItems = { getString(R.string.babyinfo_score0),
                getString(R.string.babyinfo_score1),
                getString(R.string.babyinfo_score2),
                getString(R.string.babyinfo_score3),
                getString(R.string.babyinfo_score4),
                getString(R.string.babyinfo_score5),
                getString(R.string.babyinfo_score6),
                getString(R.string.babyinfo_score7),
                getString(R.string.babyinfo_score8),
                getString(R.string.babyinfo_score9),
                getString(R.string.babyinfo_score10)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(mItems, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                view.setText(mItems[which]);
                if(callback!=null){
                    callback.contirm(String.valueOf(which));
                }
            }
        });
        AlertDialog dialog=builder.create();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }

    public void done(){
        RequestControl control=new RequestControl(mHandler);
        control.doRequest(getActivity(), new RequestControl.RequestCallback() {
            @Override
            public Object request() {
                return mUser.saveUpdate();
            }
            @Override
            public void afterRequest(Object result) {
                    getActivity().onBackPressed();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_name:
                popDialog(m_tv_name, mUser.getBabyName(), "", false, new ConfirmCallback() {
                    @Override
                    public void contirm(String value) {
                        mUser.setBabyName(value);
                    }
                });
                break;
            case R.id.ll_due_date:
                popDatePickerDialog(m_tv_due_date, new ConfirmCallback() {
                    @Override
                    public void contirm(String value) {
                        mUser.setBabyBornPdd(value);
                    }
                });
                break;
            case R.id.ll_birthday:
                popDatePickerDialog(m_tv_birthday, new ConfirmCallback() {
                    @Override
                    public void contirm(String value) {
                        mUser.setBabyBornDate(value);
                    }
                });
                break;
            case R.id.ll_weeks:
                popDialog(m_tv_weeks, String.valueOf(mUser.getBabyBornWeek()), getString(R.string.week), true, new ConfirmCallback() {
                    @Override
                    public void contirm(String value) {
                        int week ;
                        try {
                            week = Integer.parseInt(value);
                        }
                        catch (NumberFormatException e){
                            week = 0;
                        }
                        mUser.setBabyBornWeek(week);
                    }
                });
                break;
            case R.id.ll_gender:
                changeGender();
                break;
            case R.id.ll_reason:
                popDialog(m_tv_reason, mUser.getBabyPrematureReason(), "", false, new ConfirmCallback() {
                    @Override
                    public void contirm(String value) {
                        mUser.setBabyPrematureReason(value);
                    }
                });
                break;
            case R.id.ll_weight:
                popDialog(m_tv_weight, String.valueOf(mUser.getBabyBornWeight()), "kg", true, new ConfirmCallback() {
                    @Override
                    public void contirm(String value) {
                        float weight;
                        try{
                            weight= Float.parseFloat(value);
                        }
                        catch (NumberFormatException e){
                            weight=0;
                        }
                        mUser.setBabyBornWeight(weight);
                    }
                });
                break;
            case R.id.ll_height:
                popDialog(m_tv_height, String.valueOf(mUser.getBabyBornLength()), "cm", true, new ConfirmCallback() {
                    @Override
                    public void contirm(String value) {
                        float length ;
                        try {
                            length = Float.parseFloat(value);
                        }
                        catch (NumberFormatException e){
                            length=0;
                        }
                        mUser.setBabyBornLength(length);
                    }
                });
                break;
            case R.id.ll_len:
                popDialog(m_tv_len, String.valueOf(mUser.getBabyBornHead()), "cm", true, new ConfirmCallback() {
                    @Override
                    public void contirm(String value) {
                        float head;
                        try{
                            head = Float.parseFloat(value);
                        }
                        catch (NumberFormatException e){
                            head = 0 ;
                        }
                        mUser.setBabyBornHead(head);
                    }
                });
                break;
            case R.id.ll_mark1:
                popScoreDialog(m_tv_mark1, new ConfirmCallback() {
                    @Override
                    public void contirm(String value) {
                        mUser.setBabyApgarScoreOne(Integer.valueOf(value));
                    }
                });
                break;
            case R.id.ll_mark2:
                popScoreDialog(m_tv_mark2, new ConfirmCallback() {
                    @Override
                    public void contirm(String value) {
                        mUser.setBabyApgarScoreFive(Integer.valueOf(value));
                    }
                });
                break;
            case R.id.ll_mark3:
                popScoreDialog(m_tv_mark3,new ConfirmCallback() {
                    @Override
                    public void contirm(String value) {
                        mUser.setBabyApgarScoreTen(Integer.valueOf(value));
                    }
                });
                break;
        }
    }

    public static interface ConfirmCallback{
        public void contirm(String value);
    }
}
