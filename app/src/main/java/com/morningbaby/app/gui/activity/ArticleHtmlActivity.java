package com.morningbaby.app.gui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import com.morningbaby.app.R;
import com.morningbaby.app.log.Logger;
import com.morningbaby.app.logic.BaseActivity;
import com.morningbaby.sdk.model.KnowledgeArticle;
import com.nd.smartcan.frame.dao.CacheDefine.IDataRetrieveListener;

/**
 * Created by carl on 15/8/10.
 */
public class ArticleHtmlActivity extends BaseActivity{
    private WebView mWebView;
    private SwipeRefreshLayout mRefreshLayout;
    private String mArticleId;
    private Handler handler=new Handler();
    @Override
    protected String getActionBarTitle() {
        return getString(R.string.klg_article);
    }

    @Override
    protected int getContentViewID() {
        return R.layout.klg_article_html;
    }

    private void displayContent(KnowledgeArticle knowledgeArticle){
        if(isFinishing())
            return;
        if(knowledgeArticle==null) {
            Logger.i(getClass(),"webcontent article is null");
            return;
        }
        setActionBarTitle(knowledgeArticle.getTitle());
        Logger.i(getClass(), "webcontent=" + knowledgeArticle.getContent());
        //mWebView.loadData(knowledgeArticle.getContent(),"text/HTML","utf-8");
        mWebView.loadData(knowledgeArticle.getContent(),"text/HTML; charset=UTF-8",null);
        //mContentView.setText("      "+knowledgeArticle.getContent());
    }

    @Override
    protected void onAfterCreate(Bundle savedInstanceState) {
        mArticleId=getIntent().getStringExtra("articleId");
        mWebView= (WebView) findViewById(R.id.wv_content);
        mWebView.getSettings().setDefaultTextEncodingName("UTF -8");//设置默认为utf-8

        mRefreshLayout=(SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        mRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.orange));
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mRefreshLayout.setRefreshing(true);
                getData(false);
            }
        });
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData(true);
            }
        });
    }

    IDataRetrieveListener<KnowledgeArticle> listener = new IDataRetrieveListener<KnowledgeArticle>() {
        @Override
        public void onCacheDataRetrieve(KnowledgeArticle knowledgeArticle, boolean b) {
            Logger.i(getClass(),"webcontent= get from cache article");
            displayContent(knowledgeArticle);
        }

        @Override
        public void onServerDataRetrieve(KnowledgeArticle knowledgeArticle) {
            Logger.i(getClass(),"webcontent= get from server article");
            displayContent(knowledgeArticle);
        }

        @Override
        public void done() {
            mRefreshLayout.setRefreshing(false);
        }

        @Override
        public void onException(Exception e) {
            e.printStackTrace();
            fail();
        }

        @Override
        public Handler getCallBackLooperHandler() {
            return handler;
        }
    };

    private void getData(boolean force){
        KnowledgeArticle.getArticleAsync(mArticleId, listener, force);
    }

    private void fail(){
        Toast.makeText(this, R.string.network_error, Toast.LENGTH_SHORT).show();
    }

}
