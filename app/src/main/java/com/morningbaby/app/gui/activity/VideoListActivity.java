package com.morningbaby.app.gui.activity;

import android.os.Bundle;

import com.morningbaby.app.R;
import com.morningbaby.app.gui.fragment.VideoListFragment;
import com.morningbaby.app.logic.BaseActivity;

/**
 * Created by apple on 15/8/6.
 */
public class VideoListActivity extends BaseActivity{

    @Override
    protected int getContentViewID() {
        return R.layout.fragment;
    }

    @Override
    protected String getActionBarTitle() {
        return getString(R.string.klg_video);
    }

    @Override
    protected void onAfterCreate(Bundle savedInstanceState) {
        if(savedInstanceState==null){
            VideoListFragment fragment = new VideoListFragment();
            fragment.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.ll_fragment, fragment, "video").commit();
        }
    }


}
