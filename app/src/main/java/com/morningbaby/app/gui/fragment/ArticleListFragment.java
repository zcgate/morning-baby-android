package com.morningbaby.app.gui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.morningbaby.app.R;
import com.morningbaby.app.adapter.ArticleListAdapter;
import com.morningbaby.app.gui.activity.ArticleHtmlActivity;
import com.morningbaby.app.logic.ActivityCommand;
import com.morningbaby.app.logic.BaseFragment;
import com.morningbaby.app.view.CustomRefreshLayout;
import com.morningbaby.sdk.model.KnowledgeArticle;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by apple on 15/8/11.
 */
public class ArticleListFragment extends BaseFragment{
    private CustomRefreshLayout refreshLayout;
    private ListView mListView;
    private ArticleListAdapter mAdapter;
    private List<KnowledgeArticle> mKnowledgeArticles;

    private String mTopicId;
    private String mFilter;
    private Handler mHandler=new Handler();
    private KnowledgeArticle mArticleObject;
    @Override
    protected boolean showTitle() {
        return false;
    }

    @Override
    protected int getContentViewID() {
        return R.layout.list;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mTopicId=getArguments().getString("topicId");
        mFilter=getArguments().getString("filter");
        mArticleObject =new KnowledgeArticle();
        mListView= (ListView) getView().findViewById(R.id.listview);
        refreshLayout= (CustomRefreshLayout) getView().findViewById(R.id.swiperefresh);
        refreshLayout.setColorSchemeColors(getResources().getColor(R.color.orange));
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData(true);
            }
        });
        refreshLayout.setOnLoadListener(new CustomRefreshLayout.OnLoadListener() {
            @Override
            public void onLoad() {
                next();
            }
        });
        refreshLayout.post(new Runnable() {
            @Override
            public void run() {
                refreshLayout.setRefreshing(true);
                loadData(false);
            }
        });
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putString("articleId", mKnowledgeArticles.get(position).getId());
                ActivityCommand com = new ActivityCommand();
                com.setBundle(bundle);
                //gotoActivity(com,ArticleDetailActivity.class);
                gotoActivity(com,ArticleHtmlActivity.class);
            }
        });
    }

    public void refresh(Bundle bundle){
        mTopicId=bundle.getString("topicId");
        mFilter=bundle.getString("filter");
        refreshLayout.post(new Runnable() {
            @Override
            public void run() {
                refreshLayout.setRefreshing(true);
                loadData(false);
            }
        });
    }

    private void loadData(boolean force){
        if(TextUtils.isEmpty(mTopicId)&&TextUtils.isEmpty(mFilter))
            return;
        mArticleObject.getArticleList(mTopicId, mFilter, listener, force);
    }

    private void next(){
        if(TextUtils.isEmpty(mTopicId)&&TextUtils.isEmpty(mFilter))
            return;
        mArticleObject.nextPage(listener);
    }

    IListDataRetrieveListener<KnowledgeArticle> listener = new IListDataRetrieveListener<KnowledgeArticle>() {
        @Override
        public void onCacheDataRetrieve(List<KnowledgeArticle> KnowledgeArticles, Map<String, Object> stringObjectMap, boolean b) {
            Logger.i(getClass(), "retrieve Article list from cache.");
            showList(KnowledgeArticles);
        }

        @Override
        public void onServerDataRetrieve(List<KnowledgeArticle> KnowledgeArticles, Map<String, Object> stringObjectMap) {
            Logger.i(getClass(), "retrieve Article list from server.");
            showList(KnowledgeArticles);
        }

        @Override
        public void done() {
            refreshLayout.setRefreshing(false);
            refreshLayout.setLoading(false);
            refreshLayout.setCanLoadMore(mArticleObject.canLoadMore());
        }

        @Override
        public void onException(Exception e) {
            e.printStackTrace();
            fail();
        }

        @Override
        public Handler getCallBackLooperHandler() {
            return mHandler;
        }
    };


    private Object sync=new Object();
    private void showList(List<KnowledgeArticle> KnowledgeArticles){
        synchronized (sync){
            if(isDetached())
                return;
            if(mKnowledgeArticles==null){
                mKnowledgeArticles=new ArrayList<>();
            }else{
                mKnowledgeArticles.clear();
            }
            if(KnowledgeArticles!=null){
                mKnowledgeArticles.addAll(KnowledgeArticles);
            }
            if(mAdapter==null){
                mAdapter=new ArticleListAdapter(mKnowledgeArticles);
                mListView.setAdapter(mAdapter);
            }else{
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    private void fail(){
        Toast.makeText(getActivity(), R.string.network_error, Toast.LENGTH_SHORT).show();
    }
}
