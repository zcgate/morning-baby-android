package com.morningbaby.app.gui.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.morningbaby.app.MorningBabyAction;
import com.morningbaby.app.MorningBabyApp;
import com.morningbaby.app.R;
import com.morningbaby.app.logic.BaseActivity;
import com.morningbaby.app.logic.RequestControl;
import com.morningbaby.app.tool.FileUtil;
import com.morningbaby.app.tool.PublicUtil;
import com.morningbaby.sdk.model.Screening;
import com.morningbaby.sdk.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by apple on 15/8/4.
 */
public class ScreeningActivity extends BaseActivity implements View.OnClickListener{
    private JSONArray mData;
    private int mCurrentPos=-1;
    private int mCheckPos=-1;
    private TextView mTotalView,mQuestionView,mOptionView;
    private RadioGroup mOptionsView;
    private LinearLayout m_ll_nums;
    private Button m_btn_next;
    private HorizontalScrollView m_hs_nums;

    private Screening mScreening;
    private boolean mHasScreen;
    @Override
    protected String getActionBarTitle() {
        return getString(R.string.app_name);
    }

    @Override
    protected int getContentViewID() {
        return R.layout.screening;
    }

    @Override
    protected void onAfterCreate(Bundle savedInstanceState) {
        mScreening= User.getCurrentUser().getScreening();
        String content= FileUtil.getAssetsFileContent("questionnaire");
        try {
            mData=new JSONArray(content);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(mData==null)
            return;
        m_hs_nums=(HorizontalScrollView)findViewById(R.id.hs_nums);
        mTotalView= (TextView) findViewById(R.id.tv_total);
        mQuestionView= (TextView) findViewById(R.id.tv_question);
        mOptionView=(TextView)findViewById(R.id.tv_option);
        m_btn_next=(Button)findViewById(R.id.btn_next);
        findViewById(R.id.btn_prior).setOnClickListener(this);
        m_btn_next.setOnClickListener(this);
        mOptionsView= (RadioGroup) findViewById(R.id.rg_options);
        mOptionsView.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    checked(checkedId);
                }
        });
        initNums();
        select(0);

        new Thread(new Runnable() {
            @Override
            public void run() {
                mHasScreen = User.getCurrentUser().getScreening().hasScreening();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initQuestion();
                    }
                });
            }
        }).start();
    }

    private void checked(int checkedId){
        mCheckPos=checkedId;
        String option="A";
        switch (checkedId){
            case 0:
                option="A";
                break;
            case 1:
                option="B";
                break;
            case 2:
                option="C";
                break;
            case 3:
                option="D";
                break;
            case 4:
                option="E";
                break;
        }
        mOptionView.setText(option);
    }

    private void initQuestion(){
        try {
            int defIndex=mScreening.getScreening(mCurrentPos + 1);
            if(defIndex!=0)
                defIndex--;
            JSONObject json=mData.getJSONObject(mCurrentPos);
            String ques=json.getString("question");
            mQuestionView.setText(ques);
            mOptionsView.removeAllViews();
            JSONArray options=json.getJSONArray("options");
            int len=options.length();
            for(int i=0;i<len;i++){
                RadioButton item=new RadioButton(this);
                item.setId(i);
                item.setTextColor(getResources().getColor(R.color.normal));
                item.setTextSize(PublicUtil.dip2sp(this, 16));
                item.setButtonDrawable(getResources().getDrawable(R.drawable.radiostyle));
                item.setText(options.getString(i));
                item.setPadding(PublicUtil.dip2px(this,10),0,0,0);
                if(i==defIndex)
                    item.setChecked(true);
                mOptionsView.addView(item,ViewGroup.LayoutParams.MATCH_PARENT,PublicUtil.dip2px(this,45));
                View view=new View(this);
                view.setBackgroundResource(R.color.divider);
                mOptionsView.addView(view, ViewGroup.LayoutParams.MATCH_PARENT,PublicUtil.dip2px(this,1));
                if(mHasScreen){
                    item.setClickable(false);
                }
            }
            checked(defIndex);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initNums(){
        int length=mData.length();
        m_ll_nums= (LinearLayout) findViewById(R.id.ll_nums);
        LinearLayout.LayoutParams lp=new LinearLayout.LayoutParams(PublicUtil.dip2px(this,53)
                ,PublicUtil.dip2px(this,30));
        for(int i=0;i<length;i++){
            View item= LayoutInflater.from(this)
                    .inflate(R.layout.screening_num_item,null);
            item.setTag(i);
            TextView nums= (TextView) item.findViewById(R.id.tv_num);
            nums.setText("" + (i+1));
            m_ll_nums.addView(item,lp);
        }
    }

    private void select(int index){
        if(mCurrentPos!=-1){
            ((ViewGroup)m_ll_nums.findViewWithTag(mCurrentPos)).getChildAt(0).setSelected(false);
        }
        mCurrentPos=index;
        mTotalView.setText(getString(R.string.screening_questions, mCurrentPos + 1, mData.length()));
        ((ViewGroup)m_ll_nums.findViewWithTag(mCurrentPos)).getChildAt(0).setSelected(true);

        if(mCurrentPos==mData.length()-1){
            m_btn_next.setBackgroundResource(R.drawable.screening_btn_finish);
            m_btn_next.setTextColor(Color.WHITE);
            if(!mHasScreen){
                m_btn_next.setText(R.string.screening_finish);
            }else {
                m_btn_next.setText("查看结果");
            }
        }else{
            m_btn_next.setBackgroundResource(R.drawable.screening_btn);
            m_btn_next.setTextColor(getResources().getColor(R.color.normal));
            m_btn_next.setText(R.string.screening_next);
        }
        m_hs_nums.smoothScrollTo(index*PublicUtil.dip2px(this,53),0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_prior:
                if(mCurrentPos==0)
                    return;
                if(!mHasScreen){
                    mScreening.setScreening(mCurrentPos + 1, mCheckPos + 1);
                }
                select(mCurrentPos - 1);
                initQuestion();
                break;
            case R.id.btn_next:
                if(mCurrentPos==mData.length()-1){
                    commit();
                }else{
                    if(!mHasScreen){
                        mScreening.setScreening(mCurrentPos + 1, mCheckPos + 1);
                    }
                    select(mCurrentPos+1);
                    initQuestion();
                }
                break;
        }
    }

    private void commit(){
        RequestControl control=new RequestControl(new Handler());
        control.doRequest(this,new RequestControl.RequestCallback() {
            @Override
            public Object request() {
                if(!mHasScreen){
                    return mScreening.post();
                }
                else return true;
            }

            @Override
            public void afterRequest(Object result) {
                if(isFinishing())
                    return;
                boolean data= (boolean) result;
                if(data){
                    gotoActivity(ScreeningResultActivity.class);
                    finish();
                    MorningBabyApp.getInstance().doBroadcast(MorningBabyAction.ACTION_SCREENING_DONE);
                }
                else{
                    Toast.makeText(ScreeningActivity.this,R.string.network_error,Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
