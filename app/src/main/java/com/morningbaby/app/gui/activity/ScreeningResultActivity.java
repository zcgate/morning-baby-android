package com.morningbaby.app.gui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.morningbaby.app.MorningBabyAction;
import com.morningbaby.app.MorningBabyApp;
import com.morningbaby.app.R;
import com.morningbaby.app.gui.fragment.HospitalFragment;
import com.morningbaby.app.logic.BaseActivity;
import com.morningbaby.app.logic.RequestControl;
import com.morningbaby.sdk.model.User;

/**
 * Created by apple on 15/8/4.
 */
public class ScreeningResultActivity extends BaseActivity{

    @Override
    protected String getActionBarTitle() {
        return getString(R.string.screening_result_title);
    }

    @Override
    protected int getContentViewID() {
        return R.layout.screening_result;
    }

    @Override
    protected void onAfterCreate(Bundle savedInstanceState) {
        final View view= LayoutInflater.from(this).inflate(R.layout.screening_result_header,null);
        HospitalFragment.ListHeaderCallback callback=new HospitalFragment.ListHeaderCallback() {
            @Override
            public View getHeaderView() {
                return view;
            }

            @Override
            public void loadData(boolean force) {

            }
        };
        if(savedInstanceState==null){
            HospitalFragment fragment = new HospitalFragment();
            fragment.setListHeaderCallback(callback);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.ll_fragment, fragment, "hospital").commit();
        }
        RequestControl requestControl=new RequestControl(new Handler());
        requestControl.doRequest(this, new RequestControl.RequestCallback() {
            @Override
            public Object request() {
                return User.getCurrentUser().getScreening().isInRisk();
            }

            @Override
            public void afterRequest(Object result) {
                if((boolean)result){
                    view.findViewById(R.id.btn_more).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                            MorningBabyApp.getInstance().doBroadcast(MorningBabyAction.ACTION_SCREENING_MORE);
                        }
                    });
                }
                else{
                    TextView text=(TextView)view.findViewById(R.id.tv_result);
                    text.setText(R.string.screening_result_safe);
                    view.findViewById(R.id.ll_more).setVisibility(View.GONE);
                }
            }
        });
    }
}
