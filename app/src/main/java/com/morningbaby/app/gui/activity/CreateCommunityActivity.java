package com.morningbaby.app.gui.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.forum.sdk.bean.section.ForumCategoryInfo;
import com.forum.sdk.bean.section.ForumCategoryList;
import com.forum.sdk.service.ForumServiceFactory;
import com.morningbaby.app.R;
import com.morningbaby.app.adapter.CommunityCategoryExpandAdapter;
import com.morningbaby.app.logic.BaseActivity;
import com.morningbaby.app.tool.BlurUtil;
import com.morningbaby.app.tool.PublicUtil;
import com.morningbaby.app.view.ChageImgType;
import com.morningbaby.app.view.CircleImageView;
import com.nd.smartcan.frame.exception.DaoException;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by think on 2015/8/18.
 */
public class CreateCommunityActivity extends BaseActivity implements OnGestureListener{

    private ProgressBar progressBar;

    private CommunityCategoryExpandAdapter expandAdapter;

    private ExpandableListView llTypeGroup;

    private Set<Integer> expandSet = new HashSet<Integer>();

    // 软键盘控制
    private InputMethodManager mInputMethodManager;

    CreateCommunityOnClickListener createCommunityOnClickListener = null;

    private long beginTime = 0L;

    private static final int START_CHAGE_USR_IMG_POPMENU_REQUESTCODE = 1;

    private GestureDetector mGestureDetector;

    private Intent dataIntent = null;

    private ImageView imageView;

    private static final int PHOTO_REQUEST_CAMERA = 1;// 拍照
    private static final int PHOTO_REQUEST_GALLERY = 2;// 从相册中选择
    private static final int PHOTO_REQUEST_CUT = 3;// 结果

    private Button uploadPic;

    protected int getContentViewID() {
        return R.layout.main;
    }


    @Override
    protected void onAfterCreate(Bundle savedInstanceState) throws DaoException {
        initContentView();
        initListView();
        initEvent();
    }

    protected void initContentView() {

        setContentView(R.layout.forum_activity_create_community);

//        findViewById(R.id.btn_ask_expert).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });

        BitmapDrawable bmp = ((BitmapDrawable) getResources().getDrawable(
                R.drawable.forum_ic_community_header));
        updateUserImage(bmp.getBitmap());

        llTypeGroup = (ExpandableListView) findViewById(R.id.elv_community_type_group);

        beginTime = System.currentTimeMillis();

        createCommunityOnClickListener = new CreateCommunityOnClickListener();

        mGestureDetector = new GestureDetector(this);

        mInputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

    }

    protected void initEvent() {

        ((Button) findViewById(R.id.btn_create_community_next_community_signs))
                .setOnClickListener(createCommunityOnClickListener);

        ((Button) findViewById(R.id.btn_create_community_commit))
                .setOnClickListener(createCommunityOnClickListener);

        ((Button) findViewById(R.id.btn_create_community_community_img))
                .setOnClickListener(createCommunityOnClickListener);


        findViewById(R.id.btn_ask_expert).setOnClickListener(createCommunityOnClickListener);

        ((ScrollView) findViewById(R.id.sv_create_community))
                .setOnTouchListener(new View.OnTouchListener() {

                    @Override
                    public boolean onTouch(View arg0, MotionEvent event) {
                        // TODO Auto-generated method stub
                        return mGestureDetector.onTouchEvent(event);
                    }

                });

    }

    protected void initListView() throws DaoException {
//        ForumCategoryList typeResult = ForumServiceFactory.INSTANCE.getForumSectionService().getCategoryList(0, 5, true);
        ForumCategoryList typeResult = new ForumCategoryList();
        List<ForumCategoryInfo> item = new ArrayList<ForumCategoryInfo>();
        ForumCategoryInfo forumCategoryInfo = new ForumCategoryInfo();
        forumCategoryInfo.setId("11111");
        forumCategoryInfo.setName("教育");
        ForumCategoryInfo forumCategoryInfo1 = new ForumCategoryInfo();
        forumCategoryInfo1.setId("11111");
        forumCategoryInfo1.setName("社会");
        item.add(forumCategoryInfo1);
        item.add(forumCategoryInfo);
        typeResult.setItems(item);
        ProgressBar pbLoadingType = (ProgressBar) findViewById(R.id.pb_create_community_load_community_type);
        pbLoadingType.setVisibility(View.GONE);

        ProgressBar pbLoadingSigns = (ProgressBar)findViewById(R.id.pb_create_community_load_community_signs);
        pbLoadingSigns.setVisibility(View.GONE);

        expandAdapter = new CommunityCategoryExpandAdapter(CreateCommunityActivity.this, typeResult);
        llTypeGroup.setAdapter(expandAdapter);
        llTypeGroup.setVisibility(View.VISIBLE);

        expandAdapter.setOnClickExpandListener(new CommunityCategoryExpandAdapter.OnClickExpandListener() {
            @Override
            public void clickExpandButton(int groupPosition) {
                if (expandSet.contains(groupPosition)) {
                    llTypeGroup.collapseGroup(groupPosition);
                    expandSet.remove(groupPosition);
                } else {
                    llTypeGroup.expandGroup(groupPosition);
                    expandSet.add(groupPosition);
                }
            }
        });

    }

    private void updateUserImage(Bitmap bmp) {

        ImageView ivAvatarImg = (ImageView) findViewById(R.id.iv_create_community_community_img);
        CircleImageView civAvatar = (CircleImageView) findViewById(R.id.civ_community_avatar);

        try {
            Bitmap bmpBlur = BlurUtil.blurFullBitmap(bmp, 10);// Blur.apply(this, bmp, 10);
            ivAvatarImg.setImageBitmap(bmpBlur);
            civAvatar.setImageBitmap(bmp);
        }catch (Exception e){
            e.printStackTrace();
            System.gc();
        }

    }


    private void popChageImgMenu() {

        // 启动修改头像的弹出菜单
        int[] viewIds = { R.id.btn_chage_img_from_camera, R.id.btn_chage_img_from_photos,
                R.id.btn_chage_img_cancel, R.id.rl_chage_usr_img_popmenu };
        int[] results = { ChageImgType.CHAGE_IMG_FROM_CAMERA, ChageImgType.CHAGE_IMG_FROM_PHOTO,
                ChageImgType.CHAGE_IMG_CANCEL, ChageImgType.CHAGE_IMG_CANCEL };

        Intent intent = PopMenuActivity.getPopMenuIntent(this, R.layout.forum_popmenu_change_usrimg,
                viewIds, results, viewIds.length);
        this.startActivityForResult(intent, START_CHAGE_USR_IMG_POPMENU_REQUESTCODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            // ChageUsrImgPopMenu Activity关闭后的 requestCode

        switch (resultCode) {
            case ChageImgType.CHAGE_IMG_CANCEL:
                // 选中取消按钮
                break;

            case ChageImgType.CHAGE_IMG_FROM_CAMERA:
                File temp = new File(Environment.getExternalStorageDirectory()
                        + "/xiaoma.jpg");
                startPhotoZoom(Uri.fromFile(temp));
                break;

            case ChageImgType.CHAGE_IMG_FROM_PHOTO:
                startPhotoZoom(data.getData());
                break;
            case ChageImgType.UPLOAD_IMG_FROM_PHOTO:
                if(data == null){
                    return;
                }else{
                    dataIntent = data;
                    setPicToView(data);
                }
            default:
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void startPhotoZoom(Uri uri) {
		/*
		 * 至于下面这个Intent的ACTION是怎么知道的，大家可以看下自己路径下的如下网页
		 * yourself_sdk_path/docs/reference/android/content/Intent.html
		 * 直接在里面Ctrl+F搜：CROP ，之前小马没仔细看过，其实安卓系统早已经有自带图片裁剪功能,
		 * 是直接调本地库的，小马不懂C C++  这个不做详细了解去了，有轮子就用轮子，不再研究轮子是怎么
		 * 制做的了...吼吼
		 */
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        //下面这个crop=true是设置在开启的Intent中设置显示的VIEW可裁剪
        intent.putExtra("crop", "true");
        // aspectX aspectY 是宽高的比例
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // outputX outputY 是裁剪图片宽高
        intent.putExtra("outputX", 150);
        intent.putExtra("outputY", 150);
        intent.putExtra("return-data", true);
        startActivityForResult(intent, 4);
    }

    /**
     * 保存裁剪之后的图片数据
     * @param picdata
     */
    private void setPicToView(Intent picdata) {
        Bundle extras = picdata.getExtras();
        if (extras != null) {
            Bitmap photo = extras.getParcelable("data");
            Drawable drawable = new BitmapDrawable(photo);
            imageView.setBackgroundDrawable(drawable);
        }
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }

    private class CreateCommunityOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View arg0) {

            int id = arg0.getId();
            if (id == R.id.btn_ask_expert) {
                // 回退按钮
                if (mInputMethodManager.isActive()) {
                    // 隐藏键盘
                    mInputMethodManager.hideSoftInputFromWindow(
                            ((ScrollView) findViewById(R.id.sv_create_community)).getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }

                CreateCommunityActivity.this.finish();
            } else if (id == R.id.btn_create_community_commit) {
                // 提交按钮
                beginTime = System.currentTimeMillis();
//                commit();
            } else if (id == R.id.btn_create_community_community_img) {
                // 修改头像

                popChageImgMenu();
            } else if (id == R.id.btn_create_community_next_community_signs) {
                // 更新标签
                beginTime = System.currentTimeMillis();
//                nextCommunitySigns();
            }

        }

    }
}
