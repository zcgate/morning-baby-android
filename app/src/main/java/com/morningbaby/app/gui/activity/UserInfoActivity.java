package com.morningbaby.app.gui.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.morningbaby.app.R;
import com.morningbaby.app.gui.fragment.BabyInfoFragment;
import com.morningbaby.app.log.Logger;
import com.morningbaby.app.logic.BaseActivity;
import com.morningbaby.app.logic.RequestControl;
import com.morningbaby.app.tool.ImageLoaderControl;
import com.morningbaby.app.tool.SysIntent;
import com.morningbaby.sdk.model.User;

/**
 * Created by apple on 15/8/8.
 */
public class UserInfoActivity extends BaseActivity implements View.OnClickListener{
    private BabyInfoFragment mFragment;
    private User mUser;
    private ImageLoaderControl mLoaderControl=new ImageLoaderControl();
    private Handler mHandler=new Handler();

    private ImageView m_iv_photo;
    private TextView m_tv_nickname,m_tv_gender,m_tv_phone,m_tv_email,m_tv_city,m_tv_address;

    @Override
    protected int getContentViewID() {
        return R.layout.userinfo;
    }

    @Override
    protected void onAfterCreate(Bundle savedInstanceState) {
        mUser = User.getCurrentUser();
        if(savedInstanceState!=null){
            m_photoUri=savedInstanceState.getParcelable("photoUri");
        }
        if(savedInstanceState==null){
            mFragment=new BabyInfoFragment();
            mFragment.setUser(mUser);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.ll_fragment, mFragment, "babyinfo").commit();
        }
        else{
            mFragment= (BabyInfoFragment) getSupportFragmentManager().findFragmentByTag("babyinfo");
        }

        findViewById(R.id.title_iv_back).setOnClickListener(this);
        findViewById(R.id.btn_finish).setOnClickListener(this);

        m_iv_photo=(ImageView)findViewById(R.id.iv_photo);
        m_tv_nickname=(TextView)findViewById(R.id.tv_nickname);
        m_tv_gender=(TextView)findViewById(R.id.tv_gender);
        m_tv_phone=(TextView)findViewById(R.id.tv_phone);
        m_tv_email=(TextView)findViewById(R.id.tv_email);
        m_tv_city=(TextView)findViewById(R.id.tv_city);
        m_tv_address=(TextView)findViewById(R.id.tv_address);
        findViewById(R.id.ll_photo).setOnClickListener(this);
        findViewById(R.id.ll_nickname).setOnClickListener(this);
        findViewById(R.id.ll_gender).setOnClickListener(this);
        findViewById(R.id.ll_phone).setOnClickListener(this);
        findViewById(R.id.ll_email).setOnClickListener(this);
        findViewById(R.id.ll_city).setOnClickListener(this);
        findViewById(R.id.ll_address).setOnClickListener(this);

        initUser();
    }

    private void initUser(){
        RequestControl control=new RequestControl(mHandler);
        control.doRequest(this, new RequestControl.RequestCallback() {
            @Override
            public Object request() {
                mUser.loadUserData();
                return null;
            }

            @Override
            public void afterRequest(Object result) {
                displayValue();
            }
        });
    }

    private void displayUserInfo(){
        m_tv_nickname.setText(mUser.getNick());
        m_tv_gender.setText("male".equals(mUser.getGender())? R.string.gender_male : R.string.gender_female);
        m_tv_phone.setText(mUser.getPhone());
        m_tv_email.setText(mUser.getEmail());
        String address="";
        String province=mUser.getAddressProvince();
        if(!TextUtils.isEmpty(province))
            address+=province;
        String city=mUser.getAddressCity();
        if(!TextUtils.isEmpty(city))
            address+=city;
        String disinct=mUser.getAddressDistinct();
        if(!TextUtils.isEmpty(disinct))
            address+=disinct;
        m_tv_city.setText(address);
        m_tv_address.setText(mUser.getAddressDetail());
        if(!TextUtils.isEmpty(mUser.getPicture())){
            mLoaderControl.displayImage(mUser.getPicture(),m_iv_photo);
        }
    }

    private void popAddressDialog(){
        final Dialog updateDialog = new Dialog(this, R.style.CustomDialog);
        updateDialog.setContentView(R.layout.userinfo_address_dialog);
        final EditText province= (EditText) updateDialog.findViewById(R.id.et_province);
        province.setText(mUser.getAddressProvince());

        final EditText city= (EditText) updateDialog.findViewById(R.id.et_city);
        city.setText(mUser.getAddressCity());

        final EditText distinct= (EditText) updateDialog.findViewById(R.id.et_distinct);
        distinct.setText(mUser.getAddressDistinct());

        updateDialog.findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String v1 = province.getText().toString();
                String v2 = city.getText().toString();
                String v3 = distinct.getText().toString();
                String result = "";
                if (v1.length() > 0)
                    result += v1 + getString(R.string.province);
                if (v2.length() > 0)
                    result += v2 + getString(R.string.city);
                if (v3.length() > 0)
                    result += v3 + getString(R.string.distinct);
                m_tv_city.setText(result);
                mUser.setAddressProvince(v1);
                mUser.setAddressCity(v2);
                mUser.setAddressDistinct(v3);
                updateDialog.dismiss();
            }
        });
        updateDialog.show();
    }

    private void popDialog(final TextView resultView,String defValue,int inputType,final BabyInfoFragment.ConfirmCallback confirmCallback) {
        final Dialog updateDialog = new Dialog(this, R.style.CustomDialog);
        updateDialog.setContentView(R.layout.babyinfo_dialog);
        final EditText editText= (EditText) updateDialog.findViewById(R.id.et_value);
        editText.setText(defValue);
        if(inputType!=-1)
            editText.setInputType(inputType);
        updateDialog.findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value=editText.getText().toString();
                resultView.setText(editText.getText().toString());
                if(confirmCallback!=null){
                    confirmCallback.contirm(value);
                }
                updateDialog.dismiss();
            }
        });
        updateDialog.show();
    }

    private void changeGender() {
        final String[] mItems = { getString(R.string.gender_male),
                getString(R.string.gender_female)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(mItems, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                m_tv_gender.setText(mItems[which]);
                mUser.setGender(which == 0 ? "male" : "female");
            }
        });
        AlertDialog dialog=builder.create();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }

    private void displayValue(){
        displayUserInfo();
        if(mFragment!=null)
            mFragment.displayValue();
    }

    private void done(){
        RequestControl control=new RequestControl(mHandler);
        control.doRequest(this, new RequestControl.RequestCallback() {
            @Override
            public Object request() {
                return mUser.saveUpdate();
            }

            @Override
            public void afterRequest(Object result) {
                finish();
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("photoUri",m_photoUri);
        super.onSaveInstanceState(outState);
        Logger.d(getClass(), "onSaveInstanceState");
    }

    private Uri m_photoUri;
    private void changePhoto() {
        final String[] mItems = { getString(R.string.user_album),
                getString(R.string.user_takephoto)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(mItems, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    SysIntent.choosePictureFromGallery(UserInfoActivity.this);
                } else if (which == 1) {
                    m_photoUri = SysIntent
                            .takePhoto(UserInfoActivity.this);
                }
            }
        });
        AlertDialog dialog=builder.create();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SysIntent.CHOOSE_PICTURE) {
                SysIntent.cutPhoto(this, data.getData(), 1, 1, 200, 200);
            } else if (requestCode == SysIntent.TAKE_PHOTO) {
                SysIntent.cutPhoto(this, m_photoUri, 1, 1, 200, 200);
            } else if (requestCode == SysIntent.CUT_PHOTO) {
                Bundle extras = data.getExtras();
                if (extras != null) {
                    try {
                        m_photoUri=null;
                        final Bitmap photo = extras.getParcelable("data");
                        RequestControl control=new RequestControl(mHandler);
                        control.doRequest(this,new RequestControl.RequestCallback() {
                            @Override
                            public Object request() {
                                return User.getCurrentUser().setAvatar(photo);
                            }

                            @Override
                            public void afterRequest(Object result) {
                                if((boolean) result){
                                    if(!TextUtils.isEmpty(mUser.getPicture())){
                                        mLoaderControl.displayImage(mUser.getPicture(),m_iv_photo);
                                    }
                                    //m_iv_photo.setImageBitmap(photo);
                                }
                            }
                        });
                        //m_iv_photo.setImageBitmap(photo);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    @Override
    public void onClick(final View v) {
        switch (v.getId()){
            case R.id.title_iv_back:
                finish();
                break;
            case R.id.btn_finish:
                done();
                break;
            case R.id.ll_photo:
                changePhoto();
                break;
            case R.id.ll_nickname:
                popDialog(m_tv_nickname, mUser.getNick(), -1, new BabyInfoFragment.ConfirmCallback() {
                    @Override
                    public void contirm(String value) {
                        mUser.setNick(value);
                    }
                });
                break;
            case R.id.ll_gender:
                changeGender();
                break;
            case R.id.ll_phone:
                popDialog(m_tv_phone, mUser.getPhone(), InputType.TYPE_CLASS_PHONE, new BabyInfoFragment.ConfirmCallback() {
                    @Override
                    public void contirm(String value) {
                        mUser.setPhone(value);
                    }
                });
                break;
            case R.id.ll_email:
                popDialog(m_tv_email, mUser.getEmail(), InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS, new BabyInfoFragment.ConfirmCallback() {
                    @Override
                    public void contirm(String value) {
                        mUser.setEmail(value);
                    }
                });
                break;
            case R.id.ll_city:
                popAddressDialog();
                break;
            case R.id.ll_address:
                popDialog(m_tv_address, mUser.getAddressDetail(), -1, new BabyInfoFragment.ConfirmCallback() {
                    @Override
                    public void contirm(String value) {
                        mUser.setAddressDetail(value);
                    }
                });
                break;
        }
    }

}
