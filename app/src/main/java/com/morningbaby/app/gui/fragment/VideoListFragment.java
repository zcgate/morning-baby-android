package com.morningbaby.app.gui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.morningbaby.app.R;
import com.morningbaby.app.adapter.VideoListAdapter;
import com.morningbaby.app.gui.activity.VideoDetailActivity;
import com.morningbaby.app.logic.ActivityCommand;
import com.morningbaby.app.logic.BaseFragment;
import com.morningbaby.app.view.CustomRefreshLayout;
import com.morningbaby.sdk.model.KnowledgeVideo;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by apple on 15/8/11.
 */
public class VideoListFragment extends BaseFragment {
    private CustomRefreshLayout refreshLayout;
    private ListView mListView;
    private VideoListAdapter mAdapter;
    private List<KnowledgeVideo> mKnowledgeVideos;

    private String mTopicId;
    private String mFilter;
    private Handler mHandler=new Handler();
    private KnowledgeVideo mVideoObject;
    @Override
    protected boolean showTitle() {
        return false;
    }

    @Override
    protected int getContentViewID() {
        return R.layout.list;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mTopicId=getArguments().getString("topicId");
        mFilter=getArguments().getString("filter");
        mVideoObject=new KnowledgeVideo();
        mListView= (ListView) getView().findViewById(R.id.listview);
        refreshLayout= (CustomRefreshLayout) getView().findViewById(R.id.swiperefresh);
        refreshLayout.setColorSchemeColors(getResources().getColor(R.color.orange));
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData(true);
            }
        });
        refreshLayout.setOnLoadListener(new CustomRefreshLayout.OnLoadListener() {
            @Override
            public void onLoad() {
                next();
            }
        });
        refreshLayout.post(new Runnable() {
            @Override
            public void run() {
                refreshLayout.setRefreshing(true);
                loadData(false);
            }
        });
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putString("videoId", mKnowledgeVideos.get(position).getId());
                ActivityCommand com = new ActivityCommand();
                com.setBundle(bundle);
                gotoActivity(com, VideoDetailActivity.class);
            }
        });
    }

    public void refresh(Bundle bundle){
        mTopicId=bundle.getString("topicId");
        mFilter=bundle.getString("filter");
        refreshLayout.post(new Runnable() {
            @Override
            public void run() {
                refreshLayout.setRefreshing(true);
                loadData(false);
            }
        });
    }

    private void loadData(boolean force){
        if(TextUtils.isEmpty(mTopicId)&&TextUtils.isEmpty(mFilter))
            return;
        mVideoObject.getVideoList(mTopicId, mFilter, listener, force);
    }

    private void next(){
        if(TextUtils.isEmpty(mTopicId)&&TextUtils.isEmpty(mFilter))
            return;
        mVideoObject.nextPage(listener);
    }

    IListDataRetrieveListener<KnowledgeVideo> listener = new IListDataRetrieveListener<KnowledgeVideo>() {
        @Override
        public void onCacheDataRetrieve(List<KnowledgeVideo> knowledgeVideos, Map<String, Object> stringObjectMap, boolean b) {
            Logger.i(getClass(), "retrieve video list from cache.");
            showList(knowledgeVideos);
        }

        @Override
        public void onServerDataRetrieve(List<KnowledgeVideo> knowledgeVideos, Map<String, Object> stringObjectMap) {
            Logger.i(getClass(), "retrieve video list from server.");
            showList(knowledgeVideos);
        }

        @Override
        public void done() {
            refreshLayout.setRefreshing(false);
            refreshLayout.setLoading(false);
            refreshLayout.setCanLoadMore(mVideoObject.canLoadMore());
        }

        @Override
        public void onException(Exception e) {
            e.printStackTrace();
            fail();
        }

        @Override
        public Handler getCallBackLooperHandler() {
            return mHandler;
        }
    };


    private Object sync=new Object();
    private void showList(List<KnowledgeVideo> knowledgeVideos){
        synchronized (sync){
            if(isDetached())
                return;
            if(mKnowledgeVideos==null){
                mKnowledgeVideos=new ArrayList<>();
            }else{
                mKnowledgeVideos.clear();
            }
            if(knowledgeVideos!=null){
                mKnowledgeVideos.addAll(knowledgeVideos);
            }
            if(mAdapter==null){
                mAdapter=new VideoListAdapter(mKnowledgeVideos);
                mListView.setAdapter(mAdapter);
            }else{
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    private void fail(){
        Toast.makeText(getActivity(), R.string.network_error, Toast.LENGTH_SHORT).show();
    }

}
