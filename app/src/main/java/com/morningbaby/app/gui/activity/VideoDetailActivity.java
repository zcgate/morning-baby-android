package com.morningbaby.app.gui.activity;

import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.morningbaby.app.R;
import com.morningbaby.app.logic.BaseActivity;
import com.morningbaby.app.tool.ImageLoaderControl;
import com.morningbaby.sdk.model.KnowledgeVideo;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.frame.dao.CacheDefine.IDataRetrieveListener;

/**
 * Created by apple on 15/8/6.
 */
public class VideoDetailActivity extends BaseActivity implements SurfaceHolder.Callback,
        MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnVideoSizeChangedListener, MediaPlayer.OnPreparedListener {
    private MediaPlayer m_mediaPlayer;
    private SurfaceView m_surfaceView;
    private SeekBar m_seekBar;

    private ImageView m_iv_thumb, m_iv_play, m_iv_play2;
    private TextView m_tv_current, m_tv_total;
    private ProgressBar m_pb_loading;

    private LinearLayout m_ll_progress;
    private KnowledgeVideo mKnowledgeVideo;
    private long m_lastTime;
    private boolean m_check = true;
    private Thread m_thread;
    private boolean m_isPlay = false;

    private SwipeRefreshLayout mRefreshLayout;
    private String mVideoid;
    private Handler handler = new Handler();
    @Override
    protected int getContentViewID() {
        return R.layout.klg_video_detail;
    }

    @Override
    protected String getActionBarTitle() {
        return getString(R.string.klg_video);
    }

    @Override
    protected void onAfterCreate(Bundle savedInstanceState) {
        m_surfaceView = (SurfaceView) findViewById(R.id.surfaceview);
        m_surfaceView.getHolder().addCallback(this);
        m_seekBar = (SeekBar) findViewById(R.id.seekbar);
        m_iv_play = (ImageView) findViewById(R.id.iv_play);
        m_iv_play2 = (ImageView) findViewById(R.id.iv_play2);
        m_tv_current = (TextView) findViewById(R.id.tv_current);
        m_tv_total = (TextView) findViewById(R.id.tv_total);
        m_ll_progress = (LinearLayout) findViewById(R.id.ll_progress);
        m_pb_loading = (ProgressBar) findViewById(R.id.pb_loading);

        m_iv_thumb = (ImageView) findViewById(R.id.iv_thumb);
        mRefreshLayout=(SwipeRefreshLayout)findViewById(R.id.swiperefresh);
        mRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.orange));
        mVideoid = getIntent().getStringExtra("videoId");
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mRefreshLayout.setRefreshing(true);
                getVideoData(false);
            }
        });
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(m_isPlay){
                    mRefreshLayout.setRefreshing(false);
                    return;
                }
                getVideoData(true);
            }
        });
    }

    private void getVideoData(boolean force) {
        KnowledgeVideo.getVideoAsync(mVideoid, listener, force);
    }

    IDataRetrieveListener<KnowledgeVideo> listener = new IDataRetrieveListener<KnowledgeVideo>() {
        @Override
        public void onCacheDataRetrieve(KnowledgeVideo knowledgeVideo, boolean b) {
            Logger.i(getClass(), "retrieve video detail from cache.");
            mKnowledgeVideo = knowledgeVideo;
        }

        @Override
        public void onServerDataRetrieve(KnowledgeVideo knowledgeVideo) {
            Logger.i(getClass(), "retrieve video detail from server.");
            mKnowledgeVideo = knowledgeVideo;
        }

        @Override
        public void done() {
            mRefreshLayout.setRefreshing(false);
            showContent();
        }

        @Override
        public void onException(Exception e) {
            e.printStackTrace();
            fail();
        }

        @Override
        public Handler getCallBackLooperHandler() {
            return handler;
        }
    };

    public int getCurrentPosition() {
        if (m_mediaPlayer == null)
            return 0;
        return m_mediaPlayer.getCurrentPosition();
    }

    public int getDuration() {
        if (m_mediaPlayer == null)
            return 0;
        return m_mediaPlayer.getDuration();
    }

    public String convertTimeToMs(int time) {
        time /= 1000;
        int minute = time / 60;
        int second = time % 60;
        return String.format("%02d:%02d", minute, second);
    }

    Handler m_handler = new Handler();
    Runnable updateThread = new Runnable() {
        public void run() {
            // 获得歌曲现在播放位置并设置成播放进度条的值
            if (m_seekBar == null)
                return;
            int old = m_seekBar.getProgress();
            int playTo = getCurrentPosition();
            m_seekBar.setProgress(playTo);
            m_tv_current.setText(convertTimeToMs(playTo));
            // 每次延迟100毫秒再启动线程
            m_handler.postDelayed(updateThread, 100);
            if (m_pb_loading.getVisibility() == View.VISIBLE && old != m_seekBar.getProgress()) {
                m_pb_loading.setVisibility(View.GONE);
            }
        }
    };

    protected void switchControl() {
        if (m_ll_progress.getVisibility() == View.GONE) {
            Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_bottom_in);
            m_ll_progress.setAnimation(animation);
            m_ll_progress.setVisibility(View.VISIBLE);
        }
        m_lastTime = System.currentTimeMillis();
    }

    private void setThread() {
        if (m_thread != null)
            return;
        m_thread = new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                while (m_check) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    if (m_ll_progress == null)
                        return;
                    long now = System.currentTimeMillis();
                    if (m_ll_progress.getVisibility() == View.VISIBLE
                            && now - m_lastTime > 3000) {
                        if (isFinishing())
                            return;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                // TODO Auto-generated method stub
                                Animation animation = AnimationUtils.loadAnimation(VideoDetailActivity.this, R.anim.slide_bottom_out);
                                m_ll_progress.setAnimation(animation);
                                m_ll_progress.setVisibility(View.GONE);
                            }
                        });

                    }
                }
            }

        });
        m_thread.start();
    }

    private void showContent() {
        if(isFinishing())
            return;
        if(mKnowledgeVideo==null)
            return;
        new ImageLoaderControl().displayImage(mKnowledgeVideo.getThumbnail(), m_iv_thumb);
        TextView title = (TextView) findViewById(R.id.tv_title);
        title.setText(mKnowledgeVideo.getTitle());
        TextView desc = (TextView) findViewById(R.id.tv_desc);
        desc.setText(mKnowledgeVideo.getDescription());
        TextView tag = (TextView) findViewById(R.id.tv_tag);
        tag.setText(mKnowledgeVideo.getTags());

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeState();
            }
        };
        m_iv_play.setOnClickListener(listener);
        m_iv_play2.setOnClickListener(listener);

        m_seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    if (m_mediaPlayer == null) {
                        initMediaPlayer();
                        startPlay();
                    } else {
                        m_mediaPlayer.seekTo(progress);
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        m_surfaceView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                switchControl();
            }
        });
        if(m_mediaPlayer==null)
            initMediaPlayer();
    }

    public void startPlay() {
        if (m_mediaPlayer != null && !m_mediaPlayer.isPlaying())
            m_mediaPlayer.start();
        m_handler.post(updateThread);
    }

    public void pausePlay() {
        if (m_mediaPlayer != null && m_mediaPlayer.isPlaying())
            m_mediaPlayer.pause();
        m_handler.removeCallbacks(updateThread);
    }

    private boolean isInitDone=false;
    private void initMediaPlayer() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Logger.d(getClass(), "start init");
                try {
                    m_mediaPlayer = new MediaPlayer();
                    m_mediaPlayer.reset();
                    m_mediaPlayer.setDataSource(mKnowledgeVideo.getLocation());
                    m_mediaPlayer.prepare();
                    // 设置音频流类型
                    m_mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    m_mediaPlayer.setOnVideoSizeChangedListener(VideoDetailActivity.this);
                    m_mediaPlayer.setOnPreparedListener(VideoDetailActivity.this);
                    m_mediaPlayer.setOnCompletionListener(VideoDetailActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Logger.d(getClass(), "end init");
                isInitDone=true;
                if (isFinishing())
                    return;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        m_seekBar.setMax(getDuration());
                        m_tv_total.setText("/" + convertTimeToMs(getDuration()));
                        setThread();
                    }
                });
            }
        }).start();
    }

    private void fail() {
        Toast.makeText(this, R.string.network_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        m_mediaPlayer.seekTo(0);
        m_seekBar.setProgress(0);
        changeState();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        try {
            SurfaceHolder holder = m_surfaceView.getHolder();
            holder.setFormat(PixelFormat.RGB_565);
            m_mediaPlayer.setDisplay(holder);
            m_mediaPlayer.setScreenOnWhilePlaying(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
        m_surfaceView.getHolder().setFixedSize(width, height);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        releaseMediaPlayer();
    }

    // 释放MediaPlayer
    public void releaseMediaPlayer() {
        if (m_mediaPlayer != null) {
            if (!m_mediaPlayer.isPlaying())
                m_mediaPlayer.start();
            m_mediaPlayer.release();
            m_mediaPlayer = null;
            m_handler.removeCallbacks(updateThread);
        }
    }

    private void changeState() {
        if (!m_isPlay) {
            m_isPlay = true;
            m_iv_play.setImageResource(R.drawable.video_pause);
            m_iv_play2.setVisibility(View.GONE);
            if (m_iv_thumb.getParent() != null)
                ((ViewGroup) m_iv_thumb.getParent()).removeView(m_iv_thumb);
            m_pb_loading.setVisibility(View.VISIBLE);
            if(!isInitDone){
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (true){
                            if(isFinishing())
                                return;
                            if(isInitDone){
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        startPlay();
                                    }
                                });
                                break;
                            }
                        }
                    }
                }).start();
            }else{
                startPlay();
            }
        } else {
            if(!isInitDone)
                return;
            m_isPlay = false;
            m_iv_play.setImageResource(R.drawable.video_play);
            m_iv_play2.setVisibility(View.VISIBLE);
            m_pb_loading.setVisibility(View.GONE);
            pausePlay();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        m_check = false;
    }
}
