package com.morningbaby.app.gui.fragment;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.morningbaby.app.MorningBabyApp;
import com.morningbaby.app.R;
import com.morningbaby.app.gui.activity.LoginActivity;
import com.morningbaby.app.gui.activity.ScreeningActivity;
import com.morningbaby.app.gui.activity.UserInfoActivity;
import com.morningbaby.app.log.Logger;
import com.morningbaby.app.logic.BaseFragment;
import com.morningbaby.app.logic.RequestControl;
import com.morningbaby.app.tool.ImageLoaderControl;
import com.morningbaby.app.tool.PublicUtil;
import com.morningbaby.app.tool.Setting;
import com.morningbaby.app.view.RoundImageView;
import com.morningbaby.sdk.model.User;

/**
 * Created by apple on 15/8/4.
 */
public class UserFragment extends BaseFragment implements View.OnClickListener{
    private ImageLoaderControl mLoaderControl;
    private boolean isWifiOn=true;
    RoundImageView photo;
    @Override
    protected boolean showTitle() {
        return false;
    }

    @Override
    protected int getContentViewID() {
        return R.layout.user;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mLoaderControl=new ImageLoaderControl(Bitmap.Config.ARGB_8888);
        Bitmap bg=mLoaderControl.loadImageSync(R.drawable.user_title_bg);
        getView().findViewById(R.id.ll_user_title).setBackgroundDrawable(new BitmapDrawable(bg));

        photo= (RoundImageView)getView().findViewById(R.id.iv_photo);
        int radius = PublicUtil.dip2px(getActivity(), 100);
        photo.setCornerRadius(radius, radius, radius, radius);
        photo.setOnClickListener(this);

        TextView version= (TextView) getView().findViewById(R.id.tv_version);
        version.setText(MorningBabyApp.getInstance().getVersionName());

        getView().findViewById(R.id.ll_notification).setOnClickListener(this);
        getView().findViewById(R.id.iv_switch).setOnClickListener(this);
        getView().findViewById(R.id.ll_screening).setOnClickListener(this);
        getView().findViewById(R.id.ll_version).setOnClickListener(this);
        getView().findViewById(R.id.ll_feedback).setOnClickListener(this);
        getView().findViewById(R.id.ll_aboutus).setOnClickListener(this);
        getView().findViewById(R.id.btn_logout).setOnClickListener(this);

        ImageView iv_switch=(ImageView)getView().findViewById(R.id.iv_switch);
        changeSwitchState(iv_switch);
    }
    @Override
    public void onResume(){
        RequestControl control=new RequestControl(new Handler());
        control.doRequest(getActivity(), new RequestControl.RequestCallback() {

            @Override
            public Object request() {
                User.getCurrentUser().loadUserData();
                return null;
            }
            @Override
            public void afterRequest(Object result) {
                if(!TextUtils.isEmpty(User.getCurrentUser().getPicture())){
                    Logger.i(getClass(),"picture=..."+User.getCurrentUser().getPicture());
                    mLoaderControl.displayImage(User.getCurrentUser().getPicture(),photo);
                }
            }
        });
        super.onResume();
    }

    private void changeSwitchState(ImageView iv_switch){
        isWifiOn= Setting.dlUnderWifi();
        if(isWifiOn){
            iv_switch.setImageResource(R.drawable.switch_on);
        }else{
            iv_switch.setImageResource(R.drawable.switch_off);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_photo:
                gotoActivity(UserInfoActivity.class);
                break;
            case R.id.ll_notification:
                break;
            case R.id.iv_switch:
                Setting.setDlUnderWifi(!isWifiOn);
                changeSwitchState((ImageView) v);
                break;
            case R.id.ll_version:
                break;
            case R.id.ll_feedback:
                break;
            case R.id.ll_aboutus:
                break;
            case R.id.btn_logout:
                logout();
            case R.id.ll_screening:
                gotoActivity(ScreeningActivity.class);
                break;
        }
    }

    private void logout(){
        RequestControl control=new RequestControl(new Handler());
        control.doRequest(getActivity(), new RequestControl.RequestCallback() {
            @Override
            public Object request() {
                return User.getCurrentUser().logout();
            }

            @Override
            public void afterRequest(Object result) {
                gotoActivity(LoginActivity.class);
                getActivity().finish();;
            }
        });
    }
}
