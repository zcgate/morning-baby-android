package com.morningbaby.app.gui.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.morningbaby.app.R;
import com.morningbaby.app.adapter.DoctorListAdapter;
import com.morningbaby.app.logic.BaseActivity;
import com.morningbaby.app.tool.PublicUtil;
import com.morningbaby.app.view.CustomRefreshLayout;
import com.morningbaby.sdk.model.Department;
import com.morningbaby.sdk.model.Doctor;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.frame.dao.CacheDefine.IDataRetrieveListener;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by apple on 15/8/8.
 */
public class DepartmentDetailActivity extends BaseActivity{
    private String mHospitalId;
    private String mDepartmentId;
    private Handler handler=new Handler();
    private Doctor mDoctorObject;

    private ListView m_lv_doctor;
    private DoctorListAdapter mAdapter;
    private List<Doctor> mDoctors;
    private CustomRefreshLayout mRefreshLayout;

    private TextView m_tv_name,m_tv_desc,m_tv_more;
    @Override
    protected int getContentViewID() {
        return R.layout.list;
    }

    @Override
    protected String getActionBarTitle() {
        return getString(R.string.hospital_message);
    }

    @Override
    protected void onAfterCreate(Bundle savedInstanceState) {
        String name=getIntent().getStringExtra("name");
        setActionBarTitle(name);
        mHospitalId=getIntent().getStringExtra("hospitalId");
        mDepartmentId=getIntent().getStringExtra("departmentId");

        Logger.d(getClass(),name+","+mHospitalId+","+mDepartmentId);

        m_lv_doctor = (ListView)findViewById(R.id.listview);
        View header= LayoutInflater.from(this).inflate(R.layout.department_detail,null);
        m_lv_doctor.addHeaderView(header);

        m_tv_name=(TextView)findViewById(R.id.tv_name);
        m_tv_desc=(TextView)findViewById(R.id.tv_desc);
        m_tv_more=(TextView)findViewById(R.id.tv_more);
        measureDesc();
        mDoctorObject=new Doctor();
        mRefreshLayout = (CustomRefreshLayout)findViewById(R.id.swiperefresh);
        mRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.orange));
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mRefreshLayout.setRefreshing(true);
                getDepartmentData(false);
                getDoctorData(false);
            }
        });
        mRefreshLayout.setOnLoadListener(new CustomRefreshLayout.OnLoadListener() {
            @Override
            public void onLoad() {
                nextPage();
            }
        });
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDepartmentData(true);
                getDoctorData(true);
            }
        });
    }

    private boolean isDescInit=false;
    private void measureDesc(){
        ViewTreeObserver vto = m_tv_desc.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                if(isDescInit)
                    return true;
                isDescInit=true;
                if (m_tv_desc.getHeight()>PublicUtil.dip2px(DepartmentDetailActivity.this, 90)) {
                    moreLine();
                } else {
                    m_tv_more.setVisibility(View.GONE);
                    m_tv_more.setOnClickListener(null);
                    m_tv_desc.setEllipsize(null); // 展开
                    m_tv_desc.setSingleLine(false);
                    m_tv_desc.setOnClickListener(null);
                }
                return true;
            }
        });
    }

    private void showDepartment(final Department department){
        if(isFinishing())
            return;
        m_tv_name.setText(department.getName());
        String desc=department.getIntroduction();
        Object tag=m_tv_desc.getText();
        if(tag==null||!tag.toString().equals(desc)){
            isDescInit=false;
            m_tv_desc.setText(desc);
        }else{
            if(m_tv_desc.getLineCount()>5)
                moreLine();
        }
    }

    private boolean moreFlag=true;

    private SpannableString getMoreString(String text){
        SpannableString msp = new SpannableString(text);
        msp.setSpan(new UnderlineSpan(), 0, text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        msp.setSpan(new ForegroundColorSpan(Color.BLUE), 0, text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        msp.setSpan(new AbsoluteSizeSpan(13, true), 0, text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return msp;
    }

    private void moreLine(){
        m_tv_more.setVisibility(View.VISIBLE);
        String text=getString(R.string.more);
        m_tv_more.setText(getMoreString(text));
        m_tv_desc.setLines(5);
        m_tv_desc.setEllipsize(TextUtils.TruncateAt.END);
        m_tv_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(moreFlag){
                    moreFlag=false;
                    m_tv_more.setText(getMoreString(getString(R.string.up)));
                    m_tv_desc.setEllipsize(null); // 展开
                    m_tv_desc.setSingleLine(false);
                    m_tv_desc.setOnClickListener(null);
                }else{
                    moreFlag=true;
                    m_tv_more.setText(getMoreString(getString(R.string.more)));
                    m_tv_desc.setLines(5);
                    m_tv_desc.setEllipsize(TextUtils.TruncateAt.END);
                }
            }
        });
    }

    private Object sync=new Object();

    private void showDoctors(List<Doctor> doctors){
        synchronized (sync){
            if(isFinishing())
                return;
            if(mDoctors==null){
                mDoctors=new ArrayList<>();
            }else{
                mDoctors.clear();
            }
            if(doctors!=null)
                mDoctors.addAll(doctors);
            if(mAdapter==null){
                mAdapter=new DoctorListAdapter(mDoctors);
                m_lv_doctor.setAdapter(mAdapter);
            }else{
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    private void getDepartmentData(boolean force){
        Department.getDepartmentAsync(mDepartmentId, departmentListener, force);
    }


    private boolean departmentDone=false;
    private boolean doctorDone=false;
    private Object donesync=new Object();
    private void dataDone(){
        synchronized (donesync){
            if(doctorDone&&departmentDone) {
                mRefreshLayout.setRefreshing(false);
                mRefreshLayout.setLoading(false);
                mRefreshLayout.setCanLoadMore(mDoctorObject.canLoadMore());
            }
        }
    }

    IDataRetrieveListener<Department> departmentListener = new IDataRetrieveListener<Department>() {
        @Override
        public void onCacheDataRetrieve(Department Department, boolean b) {
            Logger.i(getClass(),"===TEST=== retrieve Department detail from cache.");
            Logger.i(getClass(),"===TEST=== Department id="+Department.getId() + "Department name="+Department.getName()+ "Department city="+Department.getHospitalId());
            showDepartment(Department);
        }

        @Override
        public void onServerDataRetrieve(Department Department) {
            Logger.i(getClass(),"===TEST=== retrieve Department detail from server.");
            Logger.i(getClass(),"===TEST=== Department id="+Department.getId() + "Department name="+Department.getName()+ "Department city="+Department.getHospitalId());
            showDepartment(Department);
        }

        @Override
        public void done() {
            departmentDone=true;
            dataDone();
        }

        @Override
        public void onException(Exception e) {
            e.printStackTrace();
            Logger.i(getClass(), "===TEST=== retrieve Department detail exception");
            fail();
        }

        @Override
        public Handler getCallBackLooperHandler() {
            return handler;
        }
    };

    IListDataRetrieveListener<Doctor> doctorListener = new IListDataRetrieveListener<Doctor>() {

        @Override
        public void onCacheDataRetrieve(List<Doctor> Doctors, Map<String, Object> stringObjectMap, boolean b) {
            Logger.i(getClass(), "retrieve Department list from cache.");
            showDoctors(Doctors);
        }

        @Override
        public void onServerDataRetrieve(List<Doctor> Doctors, Map<String, Object> stringObjectMap) {
            Logger.i(getClass(), "retrieve Department list from server.");
            showDoctors(Doctors);
        }

        @Override
        public void done() {
            doctorDone=true;
            dataDone();
        }

        @Override
        public void onException(Exception e) {
            e.printStackTrace();
            Logger.i(getClass(),"retrieve Department list exception");
            fail();
        }

        @Override
        public Handler getCallBackLooperHandler() {
            return handler;
        }
    };

    private void getDoctorData(boolean force){
        mDoctorObject.getDoctorList(mHospitalId, mDepartmentId, doctorListener, force);
    }

    private void nextPage(){
        mDoctorObject.nextPage(doctorListener);
    }

    private void fail(){
        Toast.makeText(this, R.string.network_error, Toast.LENGTH_SHORT).show();
    }
}

