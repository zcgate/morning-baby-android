package com.morningbaby.app.gui.activity;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseIntArray;
import android.view.View;
import android.widget.Toast;

import com.morningbaby.app.MorningBabyAction;
import com.morningbaby.app.R;
import com.morningbaby.app.gui.fragment.ForumFragment;
import com.morningbaby.app.gui.fragment.HomeFragment;
import com.morningbaby.app.gui.fragment.KnowledgeLibFragment;
import com.morningbaby.app.gui.fragment.UserFragment;
import com.morningbaby.app.logic.BaseActivity;

/**
 * Created by apple on 15/8/4.
 */
public class MainActivity extends BaseActivity implements View.OnClickListener{

    private ViewPager mViewPager;
    private int mCurrentPos=0;
    @Override
    protected int getContentViewID() {
        return R.layout.main;
    }

    @Override
    protected void onAfterCreate(Bundle savedInstanceState) {
        changeTab(true);
        findViewById(R.id.ll_home).setOnClickListener(this);
        findViewById(R.id.ll_knowledge).setOnClickListener(this);
        findViewById(R.id.ll_user).setOnClickListener(this);
        findViewById(R.id.ll_forum).setOnClickListener(this);

        mViewPager= (ViewPager) findViewById(R.id.viewpager);
        mViewPager.setOffscreenPageLimit(3);
        MainFragmentPageAdapter adapter=new MainFragmentPageAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(adapter);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                changeTab(false);
                mCurrentPos=position;
                changeTab(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void changeTab(boolean isSelect){
        switch (mCurrentPos){
            case 0:
                findViewById(R.id.iv_home).setSelected(isSelect);
                findViewById(R.id.tv_home).setSelected(isSelect);
                break;
            case 1:
                findViewById(R.id.iv_knowledge).setSelected(isSelect);
                findViewById(R.id.tv_knowledge).setSelected(isSelect);
                break;
            case 2:
                findViewById(R.id.iv_user).setSelected(isSelect);
                findViewById(R.id.tv_user).setSelected(isSelect);
                break;
            case 3:
                findViewById(R.id.iv_forum).setSelected(isSelect);
                findViewById(R.id.tv_forum).setSelected(isSelect);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_home:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.ll_knowledge:
                mViewPager.setCurrentItem(1);
                break;
            case R.id.ll_user:
                mViewPager.setCurrentItem(2);
                break;
            case R.id.ll_forum:
                mViewPager.setCurrentItem(3);
                break;
        }
    }

    @Override
    protected void onReceiveAction(Context context, Intent intent) {
        String action=intent.getAction();
        if(MorningBabyAction.ACTION_SCREENING_MORE.equals(action)){
            if(mViewPager!=null)
                mViewPager.setCurrentItem(1);
        }
    }

    @Override
    protected void addActions(IntentFilter intentFilter) {
        addAction(MorningBabyAction.ACTION_SCREENING_MORE,intentFilter);
    }

    class MainFragmentPageAdapter extends FragmentPagerAdapter{

        public MainFragmentPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    return new HomeFragment();
                case 1:
                    return new KnowledgeLibFragment();
                case 2:
                    return new UserFragment();
                case 3:
                    return new ForumFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 4;
        }
    }

    private long exitTime = 0;
    @Override
    public void onBackPressed() {
        if(mViewPager.getCurrentItem()!=0){
            mViewPager.setCurrentItem(0);
        }else{
            if ((System.currentTimeMillis() - exitTime) > 2000) {
                Toast.makeText(this, R.string.exit, Toast.LENGTH_SHORT)
                        .show();
                exitTime = System.currentTimeMillis();
            } else {
                finish();
                System.exit(0);
            }
        }
    }
}
