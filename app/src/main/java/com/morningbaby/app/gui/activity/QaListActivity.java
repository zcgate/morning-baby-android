package com.morningbaby.app.gui.activity;

import android.os.Bundle;

import com.morningbaby.app.R;
import com.morningbaby.app.gui.fragment.QaListFragment;
import com.morningbaby.app.logic.BaseActivity;

/**
 * Created by apple on 15/8/6.
 */
public class QaListActivity extends BaseActivity{

    @Override
    protected int getContentViewID() {
        return R.layout.fragment;
    }

    @Override
    protected String getActionBarTitle() {
        return getString(R.string.klg_qa);
    }

    @Override
    protected void onAfterCreate(Bundle savedInstanceState) {
        if(savedInstanceState==null){
            QaListFragment fragment = new QaListFragment();
            fragment.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.ll_fragment, fragment, "qa").commit();
        }
    }

}