package com.morningbaby.app.gui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.morningbaby.app.R;
import com.morningbaby.app.adapter.ArticleListAdapter;
import com.morningbaby.app.adapter.QaListAdapter;
import com.morningbaby.app.adapter.VideoListAdapter;
import com.morningbaby.app.gui.activity.ArticleDetailActivity;
import com.morningbaby.app.gui.activity.ArticleHtmlActivity;
import com.morningbaby.app.gui.activity.ArticleListActivity;
import com.morningbaby.app.gui.activity.QaDetailActivity;
import com.morningbaby.app.gui.activity.QaListActivity;
import com.morningbaby.app.gui.activity.VideoDetailActivity;
import com.morningbaby.app.gui.activity.VideoListActivity;
import com.morningbaby.app.logic.ActivityCommand;
import com.morningbaby.app.logic.BaseFragment;
import com.morningbaby.app.tool.ImageLoaderControl;
import com.morningbaby.app.tool.PublicUtil;
import com.morningbaby.sdk.model.KnowledgeArticle;
import com.morningbaby.sdk.model.KnowledgeQa;
import com.morningbaby.sdk.model.KnowledgeVideo;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by apple on 15/8/6.
 */
public class KnowledgeFragment extends BaseFragment implements View.OnClickListener{
    private String mTopicId;
    private Handler mHandler=new Handler();
    @Override
    protected boolean showTitle() {
        return false;
    }

    @Override
    protected int getContentViewID() {
        return R.layout.knowledge_fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mTopicId=getArguments().getString("topicId");
        initVideo();
        initArticle();
        initQa();
    }

    private void fail(){
        Toast.makeText(getActivity(),R.string.network_error,Toast.LENGTH_SHORT).show();
    }

    private void initVideo(){
        getView().findViewById(R.id.video_more).setOnClickListener(this);
        IListDataRetrieveListener<KnowledgeVideo> listener = new IListDataRetrieveListener<KnowledgeVideo>() {

            @Override
            public void onCacheDataRetrieve(List<KnowledgeVideo> knowledgeVideos, Map<String, Object> stringObjectMap, boolean b) {
                Logger.i(getClass(), "retrieve video list from cache.");
                showVideo(knowledgeVideos);
            }

            @Override
            public void onServerDataRetrieve(List<KnowledgeVideo> knowledgeVideos, Map<String, Object> stringObjectMap) {
                Logger.i(getClass(), "retrieve video list from server.");
                showVideo(knowledgeVideos);
            }

            @Override
            public void done() {

            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
                fail();
            }

            @Override
            public Handler getCallBackLooperHandler() {
                return mHandler;
            }
        };
        new KnowledgeVideo().getVideoList(mTopicId, "", listener, false);
    }


    private void showVideo(List<KnowledgeVideo> knowledgeVideos){
        if(isDetached() || getView()==null)
            return;
        if(knowledgeVideos==null||knowledgeVideos.size()==0){
            return;
        }
        getView().findViewById(R.id.ll_video).setVisibility(View.VISIBLE);

        final List<KnowledgeVideo> data=new ArrayList<>();
        data.add(knowledgeVideos.get(0));
        if(knowledgeVideos.size()>1)
            data.add(knowledgeVideos.get(1));
        ListView listView= (ListView) getView().findViewById(R.id.lv_videos);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle bundle=new Bundle();
                bundle.putString("videoId", data.get(i).getId());
                ActivityCommand com=new ActivityCommand();
                com.setBundle(bundle);
                gotoActivity(com, VideoDetailActivity.class);
            }
        });
        VideoListAdapter adapter=new VideoListAdapter(data);
        listView.setAdapter(adapter);
        PublicUtil.setListViewHeightBasedOnChildren(listView);
    }

    private void initArticle(){
        getView().findViewById(R.id.article_more).setOnClickListener(this);
        IListDataRetrieveListener<KnowledgeArticle> listener = new IListDataRetrieveListener<KnowledgeArticle>() {

            @Override
            public void onCacheDataRetrieve(List<KnowledgeArticle> knowledgeArticles, Map<String, Object> stringObjectMap, boolean b) {
                Logger.i(getClass(), "retrieve Article list from cache.");
                showArticle(knowledgeArticles);
            }

            @Override
            public void onServerDataRetrieve(List<KnowledgeArticle> knowledgeArticles, Map<String, Object> stringObjectMap) {
                Logger.i(getClass(), "retrieve Article list from server.");
                showArticle(knowledgeArticles);
            }

            @Override
            public void done() {

            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
                fail();
            }

            @Override
            public Handler getCallBackLooperHandler() {
                return mHandler;
            }
        };
        new KnowledgeArticle().getArticleList(mTopicId, "", listener, false);
    }

    private void showArticle(List<KnowledgeArticle> knowledgeArticles){
        if(isDetached() || getView()==null)
            return;
        if(knowledgeArticles==null||knowledgeArticles.size()==0){
            return;
        }
        getView().findViewById(R.id.ll_article).setVisibility(View.VISIBLE);

        final List<KnowledgeArticle> data=new ArrayList<>();
        data.add(knowledgeArticles.get(0));
        if(knowledgeArticles.size()>1)
            data.add(knowledgeArticles.get(1));
        final ListView listView= (ListView) getView().findViewById(R.id.lv_articles);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle bundle = new Bundle();
                bundle.putString("articleId", data.get(i).getId());
                ActivityCommand com = new ActivityCommand();
                com.setBundle(bundle);
                //gotoActivity(com,ArticleDetailActivity.class);
                gotoActivity(com,ArticleHtmlActivity.class);
            }
        });
        ArticleListAdapter adapter=new ArticleListAdapter(data);
        listView.setAdapter(adapter);
        PublicUtil.setListViewHeightBasedOnChildren(listView);
    }

    private void initQa(){
        getView().findViewById(R.id.qa_more).setOnClickListener(this);
        IListDataRetrieveListener<KnowledgeQa> listener = new IListDataRetrieveListener<KnowledgeQa>() {

            @Override
            public void onCacheDataRetrieve(List<KnowledgeQa> knowledgeQas, Map<String, Object> stringObjectMap, boolean b) {
                Logger.i(getClass(), "retrieve Qa list from cache.");
                showQa(knowledgeQas);
            }

            @Override
            public void onServerDataRetrieve(List<KnowledgeQa> knowledgeQas, Map<String, Object> stringObjectMap) {
                Logger.i(getClass(), "retrieve Qa list from server.");
                showQa(knowledgeQas);
            }

            @Override
            public void done() {

            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
                fail();
            }

            @Override
            public Handler getCallBackLooperHandler() {
                return mHandler;
            }
        };

        new KnowledgeQa().getQaList(mTopicId, "", listener, false);
    }

    private void showQa(List<KnowledgeQa> knowledgeQas){
        if(isDetached() || getView()==null)
            return;
        if(knowledgeQas==null||knowledgeQas.size()==0){
            return;
        }
        getView().findViewById(R.id.ll_qa).setVisibility(View.VISIBLE);

        final List<KnowledgeQa> data=new ArrayList<>();
        data.add(knowledgeQas.get(0));
        if(knowledgeQas.size()>1)
            data.add(knowledgeQas.get(1));
        ListView listView= (ListView) getView().findViewById(R.id.lv_qas);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle bundle = new Bundle();
                bundle.putString("qaId", data.get(i).getId());
                ActivityCommand com = new ActivityCommand();
                com.setBundle(bundle);
                gotoActivity(com, QaDetailActivity.class);
            }
        });
        QaListAdapter adapter=new QaListAdapter(data);
        listView.setAdapter(adapter);
        PublicUtil.setListViewHeightBasedOnChildren(listView);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.video_more:
                ActivityCommand com=new ActivityCommand();
                com.setBundle(getArguments());
                gotoActivity(com, VideoListActivity.class);
                break;
            case R.id.article_more:
                ActivityCommand com2=new ActivityCommand();
                com2.setBundle(getArguments());
                gotoActivity(com2,ArticleListActivity.class);
                break;
            case R.id.qa_more:
                ActivityCommand com3=new ActivityCommand();
                com3.setBundle(getArguments());
                gotoActivity(com3,QaListActivity.class);
                break;
        }
    }
}
