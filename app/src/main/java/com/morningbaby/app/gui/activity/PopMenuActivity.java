package com.morningbaby.app.gui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;


import com.morningbaby.app.logic.BaseActivity;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * 通用底部弹出菜单
 * 
 * <br>Created 2014-9-24 下午8:05:10
 * @version  
 * @author   阮锋冰		
 *
 * @see 	 
 * 
 *
 */
public class PopMenuActivity extends BaseActivity implements OnClickListener{

	/**
	 * 传递 layout资源id的键值
	 */
	public static final String POPMENU_LAYOUT_RES = "layoutresid";
	
	/**
	 * layout传递需要监听的view的数目
	 */
	public static final String POPMENU_ONCLICK_COUNT = "onclickcount";
	
	/**
	 * layout中需要监听的viewid base的键值
	 */
	public static final String POPMENU_ONCLICK_VIEW_ID_BASE = "viewid";
	
	/**
	 * layout 按钮按下的返回值
	 */
	public static final String POPMENU_ONCLICK_VIEW_RESULT_BASE = "result";
	
	/**
	 * 取消键值
	 */
	public static final int POPMENU_RESULT_CODE_CANCEL = -1;
	
	/**
	 * 保存着 viewID 对应的 resultCode
	 */
	private HashMap<Integer, Integer>onViewClickResultCodes = null;

	@Override
	protected void onCreateContent(Bundle savedInstanceState) {
		
		initData();
		initView();
		initEvent();
	}
	
	private void initData(){
		
		onViewClickResultCodes = new HashMap<Integer, Integer>();
		
		//通过intent获取传入，需要注册监听的view的id，和返回的result
		//其中需要监听的view的count的key值为 onclickcount
		//id的key值格式为 viewid0，viewid1。。。
		//result格式key值格式为result0,result1,result2...
		Intent intent = getIntent();
		int viewCount = intent.getIntExtra(POPMENU_ONCLICK_COUNT, 0);
		for(int i = 0; i < viewCount; i++){
			
			int id = intent.getIntExtra(POPMENU_ONCLICK_VIEW_ID_BASE+i, 0);
			int result = intent.getIntExtra(POPMENU_ONCLICK_VIEW_RESULT_BASE+i, 0);
			
			onViewClickResultCodes.put(id, result);
		}
	}

	private void initView(){
		
		Intent intent = getIntent();
		
		//获取自定义的menu layout的资源id
		int layoutResId = intent.getIntExtra(POPMENU_LAYOUT_RES, 0);
		if(layoutResId != 0){
			//闯入layout id.
			this.setContentView(layoutResId);
			this.getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			
		}else{
			//没有layout ID 使用linearLayout
			LinearLayout ll = new LinearLayout(this);
			this.setContentView(ll);

			//直接返回不处理其他事件
			return;
		}
		
	}
	
	private void initEvent(){
		
		//注册根view的onclick事件
		View rootView = this.getWindow().getDecorView().findViewById(android.R.id.content);
		rootView.setFocusable(true);
		rootView.setClickable(true);
		rootView.setOnClickListener(this);

		//通过id查找view并进行注册监听
		//如果传入错误的id可能导致失败，必须使用try catch
		try{
			Iterator iter = onViewClickResultCodes.entrySet().iterator();
			while(iter.hasNext()){
				Map.Entry entry = (Map.Entry)iter.next();
				int id = (Integer)entry.getKey();
				
				View v = findViewById(id);
				v.setOnClickListener(this);
			}		
		}catch(Exception e){
			e.printStackTrace();
		}

	}
	
	private void finishSelf(int resultCode){
		//设置返回值，并结束activity

		this.setResult(resultCode);
		this.finish();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent  intent = getIntent();
		View rootView = this.getWindow().getDecorView().findViewById(android.R.id.content);
		if(v.equals(rootView)){
			//如果是根目录，则直接返回cancel.
			
			finishSelf(POPMENU_RESULT_CODE_CANCEL);
		}else{
			//其他的view通过注册hash表进行查找
			
			//能响应click监听的控件id一定能够在 onViewClickResultCodes 中找到
			int resultCode = 0;
			resultCode = onViewClickResultCodes.get(v.getId());
			switch (resultCode){
				case 0:
					break;
				case 1:
					intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					//下面这句指定调用相机拍照后的照片存储的路径
					intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri
							.fromFile(new File(Environment
									.getExternalStorageDirectory(),
									"xiaoma.jpg")));
					startActivityForResult(intent, resultCode);

					break;
				case 2:
					intent = new Intent(Intent.ACTION_PICK, null);
					intent.setDataAndType(
							MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
							"image/*");
					startActivityForResult(intent, resultCode);
					break;
			}
			finishSelf(resultCode);
		}
	}
	
	/**
	 * 通过此函数获取启动制定layoutid的activity
	 * 
	 * <br>Created 2014-9-24 下午8:09:33
	 * @param c
	 * @param layoutId
	 * @param viewIds 需要注册onclick监听的view di数组
	 * @param results 对应注册onclick view id返回的 result数组
	 * @param viewCount 需要组成的view数目
	 * @return
	 * @author       Administrator
	 */
	public static Intent getPopMenuIntent(Context c, int layoutId, int[] viewIds, int[] results, int viewCount){
		
		Intent intent = new Intent(c, PopMenuActivity.class);
		
		intent.putExtra(PopMenuActivity.POPMENU_LAYOUT_RES, layoutId);
		
		intent.putExtra(PopMenuActivity.POPMENU_ONCLICK_COUNT, viewCount);
		for(int i = 0; i < viewCount; i++){
			
			intent.putExtra(PopMenuActivity.POPMENU_ONCLICK_VIEW_ID_BASE+i, viewIds[i]);
			intent.putExtra(PopMenuActivity.POPMENU_ONCLICK_VIEW_RESULT_BASE+i, results[i]);
		}
		
		return intent;
	}
}
