package com.morningbaby.app.gui.activity;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.morningbaby.app.R;
import com.morningbaby.app.gui.fragment.BabyInfoFragment;
import com.morningbaby.app.logic.ActivityCommand;
import com.morningbaby.app.logic.BaseActivity;
import com.morningbaby.app.logic.RequestControl;
import com.morningbaby.app.tool.ImageLoaderControl;
import com.morningbaby.sdk.model.ReturnObject;
import com.morningbaby.sdk.model.User;

import org.apache.log4j.chainsaw.Main;

/**
 * Created by apple on 15/8/3.
 */
public class RegisterActivity extends BaseActivity implements View.OnClickListener{
    private EditText mTelephoneView,mEmailView,mPwdView,mPwdConfirmView;
    private Handler handler=new Handler();
    @Override
    protected int getContentViewID() {
        return R.layout.register;
    }

    @Override
    protected void onAfterCreate(Bundle savedInstanceState) {
        Bitmap bg=new ImageLoaderControl(Bitmap.Config.ARGB_8888).loadImageSync(R.drawable.mem_register_bg);
        findViewById(R.id.content).setBackgroundDrawable(new BitmapDrawable(bg));

        mTelephoneView= (EditText) findViewById(R.id.et_telephone);
        mEmailView= (EditText) findViewById(R.id.et_email);
        mPwdView= (EditText) findViewById(R.id.et_pwd);
        mPwdConfirmView= (EditText) findViewById(R.id.et_pwd_confirm);
        findViewById(R.id.ib_back).setOnClickListener(this);
        findViewById(R.id.btn_register).setOnClickListener(this);
    }

    private void register(){
        final String tel=mTelephoneView.getText().toString();
        final String email=mEmailView.getText().toString();
        final String pwd=mPwdView.getText().toString();
        String pwdConfirm=mPwdConfirmView.getText().toString();
        if(tel.length()==0||email.length()==0||
                pwd.length()==0||
                pwdConfirm.length()==0)
            return;
        if(!pwd.equals(pwdConfirm)){
            Toast.makeText(getApplicationContext(),R.string.mem_register_pwd_not_same,Toast.LENGTH_SHORT).show();
            return;
        }else if(pwd.length()<6||pwd.length()>12){
            Toast.makeText(getApplicationContext(),R.string.mem_register_pwd_length_error,Toast.LENGTH_SHORT).show();
            return;
        }

        RequestControl control=new RequestControl(handler);
        control.doRequest(this,new RequestControl.RequestCallback() {
            boolean loginSuccess;
            @Override
            public Object request() {
                User user= User.getCurrentUser();
                ReturnObject regResult=user.register(tel,email,pwd);
                if(regResult.success()){
                    loginSuccess=user.login(tel,pwd);
                }
                return true;
            }

            @Override
            public void afterRequest(Object result) {
                if(loginSuccess){
                    Toast.makeText(RegisterActivity.this,R.string.mem_register_succeed,Toast.LENGTH_SHORT).show();
                    ActivityCommand com=new ActivityCommand();
                    com.setCloseAll(true);
                    gotoActivity(com, BabyInfoActivity.class);
                }else{
                    Toast.makeText(RegisterActivity.this,R.string.mem_register_failed,Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ib_back:
                finish();
                break;
            case R.id.btn_register:
                register();
                break;
        }
    }
}
