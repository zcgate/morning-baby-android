package com.morningbaby.app.gui.activity;

import android.os.Bundle;

import com.morningbaby.app.R;
import com.morningbaby.app.gui.fragment.ArticleListFragment;
import com.morningbaby.app.logic.BaseActivity;


/**
 * Created by apple on 15/8/6.
 */
public class ArticleListActivity extends BaseActivity{

    @Override
    protected int getContentViewID() {
        return R.layout.fragment;
    }

    @Override
    protected String getActionBarTitle() {
        return getString(R.string.klg_article);
    }

    @Override
    protected void onAfterCreate(Bundle savedInstanceState) {
        if(savedInstanceState==null){
            ArticleListFragment fragment = new ArticleListFragment();
            fragment.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.ll_fragment, fragment, "article").commit();
        }
    }

}