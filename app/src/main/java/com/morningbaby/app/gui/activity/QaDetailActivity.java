package com.morningbaby.app.gui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.morningbaby.app.R;
import com.morningbaby.app.logic.BaseActivity;
import com.morningbaby.sdk.model.Expert;
import com.morningbaby.sdk.model.KnowledgeQa;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.frame.dao.CacheDefine.IDataRetrieveListener;

/**
 * Created by apple on 15/8/6.
 */
public class QaDetailActivity extends BaseActivity{
    private SwipeRefreshLayout mRefreshLayout;
    private TextView m_tv_title,m_tv_question,m_tv_tag,m_tv_time,m_tv_answer,m_tv_author;
    private Handler handler=new Handler();
    private String mQaId;
    @Override
    protected String getActionBarTitle() {
        return getString(R.string.klg_qa);
    }

    @Override
    protected int getContentViewID() {
        return R.layout.klg_qa_detail;
    }

    @Override
    protected void onAfterCreate(Bundle savedInstanceState) {
        m_tv_title=(TextView)findViewById(R.id.tv_title);
        m_tv_question=(TextView)findViewById(R.id.tv_question);
        m_tv_tag=(TextView)findViewById(R.id.tv_tag);
        m_tv_time=(TextView)findViewById(R.id.tv_time);
        m_tv_answer=(TextView)findViewById(R.id.tv_answer);
        m_tv_author=(TextView)findViewById(R.id.tv_author);
        mRefreshLayout=(SwipeRefreshLayout)findViewById(R.id.swiperefresh);
        mRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.orange));

        mQaId=getIntent().getStringExtra("qaId");
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mRefreshLayout.setRefreshing(true);
                getData(false);
            }
        });
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData(true);
            }
        });
    }

    IDataRetrieveListener<KnowledgeQa> qaListener = new IDataRetrieveListener<KnowledgeQa>() {
        @Override
        public void onCacheDataRetrieve(KnowledgeQa knowledgeQa, boolean b) {
            Logger.i(getClass(), "retrieve Qa detail from cache.");
            showQa(knowledgeQa);
        }

        @Override
        public void onServerDataRetrieve(KnowledgeQa knowledgeQa) {
            Logger.i(getClass(), "retrieve Qa detail from server.");
            showQa(knowledgeQa);
        }

        @Override
        public void done() {

        }

        @Override
        public void onException(Exception e) {
            e.printStackTrace();
            fail();
        }

        @Override
        public Handler getCallBackLooperHandler() {
            return handler;
        }
    };

    private boolean mForce=false;

    private void getData(boolean force){
        mForce=force;
        KnowledgeQa.getQaAsync(mQaId, qaListener,force);
    }

    private void showQa(KnowledgeQa knowledgeQa){
        if(isFinishing())
            return;
        if(knowledgeQa==null)
            return;
        m_tv_title.setText(knowledgeQa.getTitle());
        m_tv_question.setText("      "+knowledgeQa.getQuestion());
        m_tv_tag.setText(knowledgeQa.getTag());
        m_tv_answer.setText("      "+knowledgeQa.getAnswer());
        //   m_tv_time.setText(knowledgeQa.gett);
        getExpertData(knowledgeQa.getExpertId());
    }

    private void getExpertData(String expertId){
        Expert.getExpertAsync(expertId, expertListener, mForce);
    }

    private void displayExpert(Expert expert){
        if(isFinishing())
            return;
        if(expert!=null)
            m_tv_author.setText(expert.getName());
    }


    IDataRetrieveListener<Expert> expertListener = new IDataRetrieveListener<Expert>() {
        @Override
        public void onCacheDataRetrieve(Expert expert, boolean b) {
            Logger.i(getClass(), "retrieve Expert detail from cache.");
            displayExpert(expert);
        }

        @Override
        public void onServerDataRetrieve(Expert expert) {
            Logger.i(getClass(), "retrieve Expert detail from server.");
            displayExpert(expert);
        }

        @Override
        public void done() {
            mRefreshLayout.setRefreshing(false);
        }

        @Override
        public void onException(Exception e) {
            e.printStackTrace();
            fail();
        }

        @Override
        public Handler getCallBackLooperHandler() {
            return handler;
        }
    };

    private void fail(){
        Toast.makeText(this, R.string.network_error, Toast.LENGTH_SHORT).show();
    }
}
