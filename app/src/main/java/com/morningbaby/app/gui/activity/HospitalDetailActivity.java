package com.morningbaby.app.gui.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.morningbaby.app.R;
import com.morningbaby.app.adapter.DepartmentListAdapter;
import com.morningbaby.app.logic.ActivityCommand;
import com.morningbaby.app.logic.BaseActivity;
import com.morningbaby.app.tool.PublicUtil;
import com.morningbaby.app.tool.SysIntent;
import com.morningbaby.app.view.CustomRefreshLayout;
import com.morningbaby.sdk.model.Department;
import com.morningbaby.sdk.model.Hospital;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.frame.dao.CacheDefine.IDataRetrieveListener;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by apple on 15/8/7.
 */
public class HospitalDetailActivity extends BaseActivity{
    private String mHospitalId;
    private Handler handler=new Handler();
    private Department mDepartmentObject;

    private ListView m_lv_department;
    private DepartmentListAdapter mAdapter;
    private List<Department> mDepartments;
    private CustomRefreshLayout mRefreshLayout;
    private TextView m_tv_name,m_tv_desc,m_tv_address,m_tv_telephone,m_tv_more;
    private LinearLayout m_ll_phone,m_ll_address;
    @Override
    protected int getContentViewID() {
        return R.layout.list;
    }

    @Override
    protected String getActionBarTitle() {
        return getString(R.string.hospital_message);
    }

    @Override
    protected void onAfterCreate(Bundle savedInstanceState) {
        mHospitalId=getIntent().getStringExtra("hospitalId");
        m_lv_department = (ListView)findViewById(R.id.listview);
        View header= LayoutInflater.from(this).inflate(R.layout.hospital_detail,null);
        m_lv_department.addHeaderView(header);

        m_tv_name=(TextView)findViewById(R.id.tv_name);
        m_tv_desc=(TextView)findViewById(R.id.tv_desc);
        m_tv_address=(TextView)findViewById(R.id.tv_address);
        m_tv_telephone=(TextView)findViewById(R.id.tv_telephone);
        m_tv_more=(TextView)findViewById(R.id.tv_more);
        m_ll_phone=(LinearLayout)findViewById(R.id.ll_telephone);
        m_ll_address=(LinearLayout)findViewById(R.id.ll_address);

        measureDesc();

        mDepartmentObject=new Department();
        m_lv_department.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                position=position-m_lv_department.getHeaderViewsCount();
                Bundle bundle = new Bundle();
                bundle.putString("name", m_tv_name.getText().toString());
                bundle.putString("hospitalId", mHospitalId);
                bundle.putString("departmentId", mDepartments.get(position).getId());
                ActivityCommand com = new ActivityCommand();
                com.setBundle(bundle);
                gotoActivity(com, DepartmentDetailActivity.class);
            }
        });
        mRefreshLayout = (CustomRefreshLayout)findViewById(R.id.swiperefresh);
        mRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.orange));
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mRefreshLayout.setRefreshing(true);
                getHospitalData(false);
                getDepartmentData(false);
            }
        });
        mRefreshLayout.setOnLoadListener(new CustomRefreshLayout.OnLoadListener() {
            @Override
            public void onLoad() {
                nextPage();
            }
        });
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getHospitalData(true);
                getDepartmentData(true);
            }
        });
    }

    private boolean isDescInit=false;
    private void measureDesc(){
        ViewTreeObserver vto = m_tv_desc.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                if(isDescInit)
                    return true;
                isDescInit=true;
                if (m_tv_desc.getHeight()>PublicUtil.dip2px(HospitalDetailActivity.this, 90)) {
                    moreLine();
                } else {
                    m_tv_more.setVisibility(View.GONE);
                    m_tv_more.setOnClickListener(null);
                    m_tv_desc.setEllipsize(null); // 展开
                    m_tv_desc.setSingleLine(false);
                    m_tv_desc.setOnClickListener(null);
                }
                return true;
            }
        });
    }

    private void showHospital(final Hospital Hospital){
        if(isFinishing())
            return;
        String address="";
        String province=Hospital.getAddressProvince();
        if(!TextUtils.isEmpty(province))
            address+=province;
        String city=Hospital.getAddressCity();
        if(!TextUtils.isEmpty(city))
            address+=city;
        String disinct=Hospital.getAddressDistinct();
        if(!TextUtils.isEmpty(disinct))
            address+=disinct;
        address+=Hospital.getAddressDetail();
        m_tv_name.setText(Hospital.getName());
        String desc=Hospital.getIntroduction();
        Object tag=m_tv_desc.getText();
        if(tag==null||!tag.toString().equals(desc)){
            isDescInit=false;
            m_tv_desc.setText(desc);
        }else{
            if(m_tv_desc.getLineCount()>5)
                moreLine();
        }
        m_tv_address.setText(address);
        m_tv_telephone.setText(Hospital.getPhone());
        m_ll_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SysIntent.dial(HospitalDetailActivity.this, Hospital.getPhone());
            }
        });

        final String adtemp=address;
        m_ll_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SysIntent.map(HospitalDetailActivity.this,
                        Hospital.getGeoLatitude(), Hospital.getGeoLongitude(),
                        adtemp);
            }
        });

    }

    private boolean moreFlag=true;

    private SpannableString getMoreString(String text){
        SpannableString msp = new SpannableString(text);
        msp.setSpan(new UnderlineSpan(), 0, text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        msp.setSpan(new ForegroundColorSpan(Color.BLUE), 0, text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        msp.setSpan(new AbsoluteSizeSpan(13,true), 0, text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return msp;
    }

    private void moreLine(){
        m_tv_more.setVisibility(View.VISIBLE);
        String text=getString(R.string.more);
        m_tv_more.setText(getMoreString(text));
        m_tv_desc.setLines(5);
        m_tv_desc.setEllipsize(TextUtils.TruncateAt.END);
        m_tv_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(moreFlag){
                    moreFlag=false;
                    m_tv_more.setText(getMoreString(getString(R.string.up)));
                    m_tv_desc.setEllipsize(null); // 展开
                    m_tv_desc.setSingleLine(false);
                    m_tv_desc.setOnClickListener(null);
                }else{
                    moreFlag=true;
                    m_tv_more.setText(getMoreString(getString(R.string.more)));
                    m_tv_desc.setLines(5);
                    m_tv_desc.setEllipsize(TextUtils.TruncateAt.END);
                }
            }
        });
    }

    private Object sync=new Object();

    private void showDepartment(List<Department> Departments){
        synchronized (sync){
            if(isFinishing())
                return;
            if(mDepartments==null){
                mDepartments=new ArrayList<>();
            }else{
                mDepartments.clear();
            }
            if(Departments!=null)
                mDepartments.addAll(Departments);
            if(mAdapter==null){
                mAdapter=new DepartmentListAdapter(mDepartments);
                m_lv_department.setAdapter(mAdapter);
            }else{
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    private void getHospitalData(boolean force){
        Hospital.getHospitalAsync(mHospitalId, hospitalListener, force);
    }

    private boolean hospitalDone=false;
    private boolean departmentDone=false;
    private Object donesync=new Object();
    private void dataDone(){
        synchronized (donesync){
            if(hospitalDone&&departmentDone) {
                mRefreshLayout.setRefreshing(false);
                mRefreshLayout.setLoading(false);
                mRefreshLayout.setCanLoadMore(mDepartmentObject.canLoadMore());
            }
        }
    }

    IDataRetrieveListener<Hospital> hospitalListener = new IDataRetrieveListener<Hospital>() {
        @Override
        public void onCacheDataRetrieve(Hospital Hospital, boolean b) {
            Logger.i(getClass(), "retrieve Hospital detail from cache.");
            showHospital(Hospital);
        }

        @Override
        public void onServerDataRetrieve(Hospital Hospital) {
            Logger.i(getClass(),"retrieve Hospital detail from server.");
            showHospital(Hospital);
        }

        @Override
        public void done() {
            hospitalDone=true;
            dataDone();
        }

        @Override
        public void onException(Exception e) {
            e.printStackTrace();
            Logger.i(getClass(),"retrieve Hospital detail exception");
            fail();
        }

        @Override
        public Handler getCallBackLooperHandler() {
            return handler;
        }
    };

    IListDataRetrieveListener<Department> departmentListener = new IListDataRetrieveListener<Department>() {

        @Override
        public void onCacheDataRetrieve(List<Department> Departments, Map<String, Object> stringObjectMap, boolean b) {
            Logger.i(getClass(), "retrieve Department list from cache.");
            showDepartment(Departments);
        }

        @Override
        public void onServerDataRetrieve(List<Department> Departments, Map<String, Object> stringObjectMap) {
            Logger.i(getClass(), "retrieve Department list from server.");
            showDepartment(Departments);
        }

        @Override
        public void done() {
            departmentDone=true;
            dataDone();
        }

        @Override
        public void onException(Exception e) {
            e.printStackTrace();
            Logger.i(getClass(),"retrieve Department list exception");
            fail();
        }

        @Override
        public Handler getCallBackLooperHandler() {
            return handler;
        }
    };

    private void getDepartmentData(boolean force){
        mDepartmentObject.getDepartmentList(mHospitalId, departmentListener, force);
    }

    private void nextPage(){
        mDepartmentObject.nextPage(departmentListener);
    }

    private void fail(){
        Toast.makeText(this,R.string.network_error,Toast.LENGTH_SHORT).show();
    }

}
