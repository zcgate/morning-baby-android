package com.morningbaby.app.gui.activity;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.morningbaby.app.R;
import com.morningbaby.app.logic.BaseActivity;
import com.morningbaby.app.tool.ImageLoaderControl;
import com.morningbaby.sdk.SdkManager;
import com.morningbaby.sdk.model.User;

/**
 * Created by apple on 15/8/5.
 */
public class LoadingActivity extends BaseActivity{

    @Override
    protected View createContentView() {
        View  image=new View(this);
        Bitmap bg=new ImageLoaderControl(Bitmap.Config.ARGB_8888).loadImageSync(R.drawable.loading);
        image.setBackgroundDrawable(new BitmapDrawable(bg));
        return image;
    }

    @Override
    protected void onAfterCreate(Bundle savedInstanceState) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
            //    gotoActivity(MainActivity.class);
                if(User.getCurrentUser().isLogin()){
                    gotoActivity(MainActivity.class);
                }
                else{
                    gotoActivity(LoginActivity.class);
                }
                finish();
            }
        },1500);
    }
}
