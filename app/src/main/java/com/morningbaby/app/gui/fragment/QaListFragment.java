package com.morningbaby.app.gui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.morningbaby.app.R;
import com.morningbaby.app.adapter.QaListAdapter;
import com.morningbaby.app.gui.activity.QaDetailActivity;
import com.morningbaby.app.logic.ActivityCommand;
import com.morningbaby.app.logic.BaseFragment;
import com.morningbaby.app.view.CustomRefreshLayout;
import com.morningbaby.sdk.model.KnowledgeQa;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by apple on 15/8/11.
 */
public class QaListFragment extends BaseFragment {
    private CustomRefreshLayout refreshLayout;
    private ListView mListView;
    private QaListAdapter mAdapter;
    private List<KnowledgeQa> mKnowledgeQas;

    private String mTopicId;
    private String mFilter;
    private Handler mHandler=new Handler();
    private KnowledgeQa mQaObject;
    @Override
    protected boolean showTitle() {
        return false;
    }

    @Override
    protected int getContentViewID() {
        return R.layout.list;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mTopicId=getArguments().getString("topicId");
        mFilter=getArguments().getString("filter");
        mQaObject =new KnowledgeQa();
        mListView= (ListView) getView().findViewById(R.id.listview);
        refreshLayout= (CustomRefreshLayout)getView(). findViewById(R.id.swiperefresh);
        refreshLayout.setColorSchemeColors(getResources().getColor(R.color.orange));
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData(true);
            }
        });
        refreshLayout.setOnLoadListener(new CustomRefreshLayout.OnLoadListener() {
            @Override
            public void onLoad() {
                next();
            }
        });
        refreshLayout.post(new Runnable() {
            @Override
            public void run() {
                refreshLayout.setRefreshing(true);
                loadData(false);
            }
        });
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putString("qaId", mKnowledgeQas.get(position).getId());
                ActivityCommand com = new ActivityCommand();
                com.setBundle(bundle);
                gotoActivity(com, QaDetailActivity.class);
            }
        });
    }

    public void refresh(Bundle bundle){
        mTopicId=bundle.getString("topicId");
        mFilter=bundle.getString("filter");
        refreshLayout.post(new Runnable() {
            @Override
            public void run() {
                refreshLayout.setRefreshing(true);
                loadData(false);
            }
        });
    }

    private void loadData(boolean force){
        if(TextUtils.isEmpty(mTopicId)&&TextUtils.isEmpty(mFilter))
            return;
        mQaObject.getQaList(mTopicId, mFilter, listener, force);
    }

    private void next(){
        if(TextUtils.isEmpty(mTopicId)&&TextUtils.isEmpty(mFilter))
            return;
        mQaObject.nextPage(listener);
    }

    IListDataRetrieveListener<KnowledgeQa> listener = new IListDataRetrieveListener<KnowledgeQa>() {
        @Override
        public void onCacheDataRetrieve(List<KnowledgeQa> KnowledgeQas, Map<String, Object> stringObjectMap, boolean b) {
            Logger.i(getClass(), "retrieve qa list from cache.");
            showList(KnowledgeQas);
        }

        @Override
        public void onServerDataRetrieve(List<KnowledgeQa> KnowledgeQas, Map<String, Object> stringObjectMap) {
            Logger.i(getClass(), "retrieve qa list from server.");
            showList(KnowledgeQas);
        }

        @Override
        public void done() {
            refreshLayout.setRefreshing(false);
            refreshLayout.setLoading(false);
            refreshLayout.setCanLoadMore(mQaObject.canLoadMore());
        }

        @Override
        public void onException(Exception e) {
            e.printStackTrace();
            fail();
        }

        @Override
        public Handler getCallBackLooperHandler() {
            return mHandler;
        }
    };


    private Object sync=new Object();
    private void showList(List<KnowledgeQa> KnowledgeQas){
        synchronized (sync){
            if(isDetached()) {
                return;
            }
            if(mKnowledgeQas==null){
                mKnowledgeQas=new ArrayList<>();
            }else{
                mKnowledgeQas.clear();
            }
            if(KnowledgeQas!=null){
                mKnowledgeQas.addAll(KnowledgeQas);
            }
            if(mAdapter==null){
                mAdapter=new QaListAdapter(mKnowledgeQas);
                mListView.setAdapter(mAdapter);
            }else{
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    private void fail(){
        Toast.makeText(getActivity(), R.string.network_error, Toast.LENGTH_SHORT).show();
    }
}
