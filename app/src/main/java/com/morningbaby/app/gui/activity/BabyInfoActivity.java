package com.morningbaby.app.gui.activity;

import android.os.Bundle;
import android.view.View;

import com.morningbaby.app.R;
import com.morningbaby.app.gui.fragment.BabyInfoFragment;
import com.morningbaby.app.logic.BaseActivity;

/**
 * Created by apple on 15/8/7.
 */
public class BabyInfoActivity extends BaseActivity implements View.OnClickListener{
    private BabyInfoFragment mFragment;
    @Override
    protected int getContentViewID() {
        return R.layout.babyinfo;
    }

    @Override
    protected void onAfterCreate(Bundle savedInstanceState) {
        if(savedInstanceState==null){
            mFragment=new BabyInfoFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.ll_fragment, mFragment, "babyinfo").commit();
        }
        else{
            mFragment= (BabyInfoFragment) getSupportFragmentManager().findFragmentByTag("babyinfo");
        }

        findViewById(R.id.title_iv_back).setOnClickListener(this);
        findViewById(R.id.btn_finish).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.title_iv_back:
                onBackPressed();
                break;
            case R.id.btn_finish:
                mFragment.done();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        gotoActivity(MainActivity.class);
        finish();
    }
}
