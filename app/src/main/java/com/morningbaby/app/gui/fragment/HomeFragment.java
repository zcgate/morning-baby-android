package com.morningbaby.app.gui.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.morningbaby.app.MorningBabyAction;
import com.morningbaby.app.R;
import com.morningbaby.app.adapter.HotActivityAdapter;
import com.morningbaby.app.gui.activity.BabyInfoActivity;
import com.morningbaby.app.gui.activity.ScreeningActivity;
import com.morningbaby.app.logic.BaseFragment;
import com.morningbaby.app.logic.RequestControl;
import com.morningbaby.app.tool.FileUtil;
import com.morningbaby.app.tool.PublicUtil;
import com.morningbaby.app.tool.SysIntent;
import com.morningbaby.app.tool.environment;
import com.morningbaby.app.view.CustomRefreshLayout;
import com.morningbaby.app.view.DotView;
import com.morningbaby.sdk.model.HotActivity;
import com.morningbaby.sdk.model.Screening;
import com.morningbaby.sdk.model.User;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;
import com.tencent.bugly.crashreport.CrashReport;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by apple on 15/8/4.
 */
public class HomeFragment extends BaseFragment {
    private Dialog mLetterDialog;
    private Handler mHandler=new Handler();
    private ViewPager mViewPager;
    private HotActivityAdapter mHotActAdapter;
    private DotView mDotView;
    private List<HotActivity> mHotActivityData;

    private HospitalFragment mFragment;

    @Override
    protected int getContentViewID() {
        return R.layout.home;
    }

    @Override
    protected boolean showTitle() {
        return false;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final View view= LayoutInflater.from(getActivity()).inflate(R.layout.home_header,null);
        mViewPager= (ViewPager) view.findViewById(R.id.vp_activity);
        mViewPager.setOffscreenPageLimit(3);
        mDotView= (DotView) view.findViewById(R.id.dotview);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position) {
                mDotView.setCurPoint(position);
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        HospitalFragment.ListHeaderCallback callback=new HospitalFragment.ListHeaderCallback() {
            @Override
            public View getHeaderView() {
                return view;
            }

            @Override
            public void loadData(boolean force) {
                loadActData(force);
            }
        };
        if(savedInstanceState==null){
            mFragment=new HospitalFragment();
            mFragment.setListHeaderCallback(callback);
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.ll_fragment, mFragment, "hospitals").commit();
        }
        else{
            mFragment.setListHeaderCallback(callback);
            mFragment= (HospitalFragment) getChildFragmentManager().findFragmentByTag("hospitals");
        }
        getView().findViewById(R.id.btn_ask_expert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SysIntent.dial(getActivity(), MorningBabyAction.ALLIANCE_400_PHONE);
            }
        });
        checkScreening();
    }

    private void checkScreening(){
        final Screening screening = User.getCurrentUser().getScreening();
        RequestControl control=new RequestControl(new Handler());
        control.setShowProgress(false);
        control.setCancelable(false);
        control.doRequest(getActivity(), new RequestControl.RequestCallback() {
            @Override
            public Object request() {
                return screening.hasScreening();
            }

            @Override
            public void afterRequest(Object result) {
                boolean data = (boolean) result;
                if (!data) {
                    popLetterDialog();
                }
            }
        });
    }

    private void popLetterDialog() {
        mLetterDialog = new Dialog(getActivity(), R.style.CustomDialog);
        mLetterDialog.setContentView(R.layout.letter);
        LinearLayout content = (LinearLayout) mLetterDialog.findViewById(R.id.ll_content);
        content.getLayoutParams().width = environment.getScreenWidth(getActivity()) - PublicUtil.dip2px(getActivity(), 50);
        content.getLayoutParams().height = environment.getScreenHeight(getActivity()) * 2 / 3;
        TextView text = (TextView) mLetterDialog.findViewById(R.id.tv_message);
        String letter = FileUtil.getAssetsFileContent("letter");
        text.setText(letter);
        mLetterDialog.findViewById(R.id.btn_start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoActivity(ScreeningActivity.class);
            }
        });
        mLetterDialog.setCancelable(false);
        mLetterDialog.show();
    }

    @Override
    protected void addActions(IntentFilter intentFilter) {
        addAction(MorningBabyAction.ACTION_SCREENING_DONE, intentFilter);
    }

    @Override
    protected void onReceiveAction(Context context, Intent intent) {
        String action=intent.getAction();
        if(MorningBabyAction.ACTION_SCREENING_DONE.equals(action)){
            if(mLetterDialog!=null&&mLetterDialog.isShowing())
            {
                mLetterDialog.dismiss();
                mLetterDialog=null;
            }
        }
    }

    private void loadActData(boolean force){
        new HotActivity().getHotActivityList(listener, 3, force);
    }

    IListDataRetrieveListener<HotActivity> listener = new IListDataRetrieveListener<HotActivity>() {

        @Override
        public void onCacheDataRetrieve(List<HotActivity> HotActivitys, Map<String, Object> stringObjectMap, boolean b) {
            Logger.i(getClass(), "retrieve HotActivity list from cache.");
            showHotActivity(HotActivitys);
        }

        @Override
        public void onServerDataRetrieve(List<HotActivity> HotActivitys, Map<String, Object> stringObjectMap) {
            Logger.i(getClass(), "retrieve HotActivity list from server.");
            showHotActivity(HotActivitys);
        }

        @Override
        public void done() {

        }

        @Override
        public void onException(Exception e) {
            e.printStackTrace();
            Logger.i(getClass(), "retrieve HotActivity list exception");
        }

        @Override
        public Handler getCallBackLooperHandler() {
            return mHandler;
        }
    };

    private Object sync=new Object();
    private void showHotActivity(List<HotActivity> data){
        synchronized (sync){
            if(isDetached())
                return;
            if(mHotActivityData==null){
                mHotActivityData=new ArrayList<>();
            }else{
                mHotActivityData.clear();
            }
            if(data!=null)
                mHotActivityData.addAll(data);
            if(mHotActAdapter==null){
                mHotActAdapter=new HotActivityAdapter(getActivity(),mHotActivityData);
                mViewPager.setAdapter(mHotActAdapter);
            }else{
                mHotActAdapter.notifyDataSetChanged();
            }
            mDotView.removeAllViews();
            mDotView.createView(mHotActivityData.size());
            mViewPager.setCurrentItem(0);
        }
    }

}
