package com.morningbaby.app.gui.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.morningbaby.app.R;
import com.morningbaby.app.adapter.SearchHistoryAdapter;
import com.morningbaby.app.gui.fragment.ArticleListFragment;
import com.morningbaby.app.gui.fragment.QaListFragment;
import com.morningbaby.app.gui.fragment.VideoListFragment;
import com.morningbaby.app.log.Logger;
import com.morningbaby.app.logic.BaseActivity;
import com.morningbaby.app.tool.PublicUtil;
import com.morningbaby.app.tool.environment;
import com.viewpagerindicator.TabPageIndicator;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by apple on 15/8/11.
 */
public class KnowledgeSearchActivity extends BaseActivity implements View.OnClickListener{
    private LinearLayout m_ll_history,m_ll_result;
    private EditText m_et_search;
    private Button m_btn_search;
    private ListView m_lv_history;
    private String mFilter;

    private SearchHistoryAdapter mHistoryAdapter;
    private List<String> mFilters;
    private boolean canSearch =false;
    @Override
    protected String getActionBarTitle() {
        return getString(R.string.search);
    }

    @Override
    protected int getContentViewID() {
        return R.layout.klg_search;
    }

    @Override
    protected void onAfterCreate(Bundle savedInstanceState) {
        m_ll_history= (LinearLayout) findViewById(R.id.ll_history);
        m_ll_result= (LinearLayout) findViewById(R.id.ll_result);
        m_et_search= (EditText) findViewById(R.id.et_search);
        m_btn_search= (Button) findViewById(R.id.btn_search);
        m_lv_history=(ListView)findViewById(R.id.lv_history);
        m_lv_history.setEmptyView(findViewById(R.id.tv_nodata));
        m_lv_history.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mFilter=mFilters.get(i);
                m_et_search.setText(mFilter);
                showResult();
            }
        });
        findViewById(R.id.tv_clear).setOnClickListener(this);
        initSearchView();
        showHistory();
        initTab();
    }

    private void initSearchView(){
        m_et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                canSearch=true;
                if (TextUtils.isEmpty(s.toString())) {
                    m_btn_search.setText(R.string.cancel);
                } else {
                    m_btn_search.setText(R.string.search);
                }
            }
        });
        m_btn_search.setOnClickListener(this);
    }

    private void saveFilter(){
        try{
            SharedPreferences sp=getSharedPreferences("search",0);
            String keys=sp.getString("keys", "");
            JSONObject json=null;
            if(!TextUtils.isEmpty(keys)) {
                json = new JSONObject(keys);
            }
            else{
                json= new JSONObject();
            }
            json.remove(mFilter);
            json.put(mFilter, "");
            sp.edit().putString("keys",json.toString()).commit();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void showHistory(){
        if(mFilters==null)
            mFilters=new ArrayList<>();
        else
            mFilters.clear();
        SharedPreferences sp=getSharedPreferences("search",0);
        String keys=sp.getString("keys", "");
        if(!TextUtils.isEmpty(keys))
        {
            try{
                JSONObject json=new JSONObject(keys);
                List<String> data=new ArrayList<>();
                Iterator<String> ite=json.keys();
                while(ite.hasNext()){
                    data.add(ite.next());
                }
                Collections.reverse(data);
                mFilters.addAll(data);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        if(mHistoryAdapter==null){
            mHistoryAdapter=new SearchHistoryAdapter(mFilters);
            m_lv_history.setAdapter(mHistoryAdapter);
        }else{
            mHistoryAdapter.notifyDataSetChanged();
        }
        Logger.d(getClass(),"mFilters size:"+mFilters.size());
        if(mFilters.size()==0){
            m_lv_history.getLayoutParams().width= ViewGroup.LayoutParams.MATCH_PARENT;
            m_lv_history.getLayoutParams().height= ViewGroup.LayoutParams.MATCH_PARENT;
            findViewById(R.id.ll_clear).setVisibility(View.GONE);
        }else{
            findViewById(R.id.ll_clear).setVisibility(View.VISIBLE);
            PublicUtil.setListViewHeightBasedOnChildren(m_lv_history);
        }
    }

    private void initTab(){
        KnowledgeAdapter adapter=new KnowledgeAdapter(getSupportFragmentManager());
        ViewPager pager = (ViewPager) findViewById(R.id.tab_viewpager);
        pager.setOffscreenPageLimit(3);
        pager.setAdapter(adapter);

        TabPageIndicator indicator = (TabPageIndicator)findViewById(R.id.tab_indicator);
        indicator.setViewPager(pager);
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int arg0) {

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });
    }

    private Bundle getBundle(){
        Bundle bundle=new Bundle();
        bundle.putString("filter",mFilter);
        bundle.putString("topicId","");
        return bundle;
    }

    private SparseArray<Fragment> mFragments=new SparseArray<>();

    class KnowledgeAdapter extends FragmentStatePagerAdapter {
        public KnowledgeAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                case 0:
                    return getString(R.string.klg_qa);
                case 1:
                    return getString(R.string.klg_article);
                case 2:
                    return getString(R.string.klg_video);
            }
            return "";
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment=mFragments.get(position);
            if(fragment==null){
                switch (position){
                    case 0:
                        fragment=new QaListFragment();
                        break;
                    case 1:
                        fragment=new ArticleListFragment();
                        break;
                    case 2:
                        fragment=new VideoListFragment();
                        break;
                }
                fragment.setArguments(getBundle());
                mFragments.put(position,fragment);
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_clear:
                SharedPreferences sp=getSharedPreferences("search",0);
                sp.edit().putString("keys","").commit();
                showHistory();
                break;
            case R.id.btn_search:
                mFilter = m_et_search.getText().toString();
                if (TextUtils.isEmpty(mFilter)|| !canSearch) {
                    finish();
                } else {
                    environment.changeSoftKeyboard();
                    showResult();
                }
                break;
        }
    }

    private void showResult(){
        canSearch=false;
        m_btn_search.setText(R.string.cancel);
        m_ll_history.setVisibility(View.GONE);
        m_ll_result.setVisibility(View.VISIBLE);
        saveFilter();
        QaListFragment qa = (QaListFragment) mFragments.get(0);
        if (qa != null) {
            qa.refresh(getBundle());
        }
        ArticleListFragment article = (ArticleListFragment) mFragments.get(1);
        if (article != null) {
            article.refresh(getBundle());
        }
        VideoListFragment video = (VideoListFragment) mFragments.get(2);
        if (video != null) {
            video.refresh(getBundle());
        }
    }
}
