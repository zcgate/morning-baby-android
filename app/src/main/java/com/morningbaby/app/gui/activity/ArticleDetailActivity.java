package com.morningbaby.app.gui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.morningbaby.app.R;
import com.morningbaby.app.logic.BaseActivity;
import com.morningbaby.sdk.model.KnowledgeArticle;
import com.nd.smartcan.frame.dao.CacheDefine.IDataRetrieveListener;

/**
 * Created by apple on 15/8/6.
 */
public class ArticleDetailActivity extends BaseActivity{
    private TextView mContentView;
    @Override
    protected String getActionBarTitle() {
        return getString(R.string.klg_article);
    }

    @Override
    protected int getContentViewID() {
        return R.layout.klg_article_detail;
    }

    private void displayContent(KnowledgeArticle knowledgeArticle){
        if(knowledgeArticle==null)
            return;
        setActionBarTitle(knowledgeArticle.getTitle());
        mContentView.setText("      "+knowledgeArticle.getContent());
    }

    @Override
    protected void onAfterCreate(Bundle savedInstanceState) {
        mContentView= (TextView) findViewById(R.id.tv_content);
        final Handler handler=new Handler();
        String article_id=getIntent().getStringExtra("articleId");
        IDataRetrieveListener<KnowledgeArticle> listener = new IDataRetrieveListener<KnowledgeArticle>() {
            @Override
            public void onCacheDataRetrieve(KnowledgeArticle knowledgeArticle, boolean b) {
                displayContent(knowledgeArticle);
            }

            @Override
            public void onServerDataRetrieve(KnowledgeArticle knowledgeArticle) {
                displayContent(knowledgeArticle);
            }

            @Override
            public void done() {
                findViewById(R.id.pb_loading).setVisibility(View.GONE);
            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
                fail();
            }

            @Override
            public Handler getCallBackLooperHandler() {
                return handler;
            }
        };
        KnowledgeArticle.getArticleAsync(article_id,listener,false);
    }

    private void fail(){
        Toast.makeText(this, R.string.network_error, Toast.LENGTH_SHORT).show();
    }
}
