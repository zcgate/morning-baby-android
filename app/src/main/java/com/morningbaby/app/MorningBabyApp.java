package com.morningbaby.app;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;

import com.morningbaby.app.log.Logger;
import com.morningbaby.app.tool.FileUtil;
import com.morningbaby.app.view.CustomProgressDialog;
import com.morningbaby.sdk.SdkManager;
import com.morningbaby.sdk.model.User;
import com.nd.smartcan.datalayer.interfaces.IMember;
import com.nd.smartcan.datalayer.tools.MemberWrapper;
import com.nd.smartcan.frame.SmartCanApp;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.tencent.bugly.crashreport.CrashReport;

import java.io.File;

/**
 * Created by apple on 15/8/3.
 */
public class MorningBabyApp extends SmartCanApp {
    private static MorningBabyApp m_self;
    private static String APPID="900006329";

    @Override
    protected void beforeCreate() {
        CrashReport.initCrashReport(this, APPID, true);

    }

    @Override
    protected void afterCreate() {
        m_self = this;
        initImageLoader(getApplicationContext());
        SdkManager.getInstance().setApplication(this);
        //sSdkManager.getInstance().setBaseUrl("http://101.200.89.52:4567/api/v1");
        SdkManager.getInstance().setBaseUrl("http://101.200.89.52:8080/morning-baby-service-0.1.0/api/v1");
        MemberWrapper.instance().setImplement(new IMember() {
            @Override
            public boolean isLogin() {
                return User.getCurrentUser().isLogin();
            }

            @Override
            public String memberSeq() {
                return User.getCurrentUser().getUid();
            }

            @Override
            public String getId() {
                return User.getCurrentUser().getUid();
            }
        });
    }

    public boolean isDebug(){
        return true;
    }

    public static MorningBabyApp getInstance() {
        if (m_self == null)
            m_self = new MorningBabyApp();
        return m_self;
    }

    private Location mCurrentLocation;
    public Location getCurrentLocation(){
        return mCurrentLocation;
    }

    public void setCurrentLocation(Location location){
        mCurrentLocation=location;
    }

    public  void initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        //  ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.diskCache(new UnlimitedDiskCache(new File(FileUtil.getAppExternalCachePath())));
        config.writeDebugLogs(); // Remove for release app
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());
    }

    private CustomProgressDialog m_ProgressDialog = null;
    public void showProgressDialog(Context context){
        showProgressDialog(context,true);
    }

    public void showProgressDialog(Context context,boolean cancelable){
        m_ProgressDialog = new CustomProgressDialog(context);
        m_ProgressDialog.setCancelable(cancelable);
     //   m_ProgressDialog.setMessage(message);
        m_ProgressDialog.show();
    }

    public void dismissProgressDialog(){
        if (m_ProgressDialog != null&&m_ProgressDialog.isShowing()){
            try {
                m_ProgressDialog.dismiss();
                m_ProgressDialog = null;
            } catch (Exception e) {
                Logger.i(getClass(), "Exception");
            }
        }
    }

    public void doBroadcast(String action) {
        doBroadcast(action, null);
    }

    /**
     * 发送广播
     *
     * @param action
     */
    public void doBroadcast(String action, Bundle bundle) {
        Intent intent = new Intent();
        intent.setPackage(getPackageName());
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.setAction(action);
        sendBroadcast(intent);
    }

    public String getVersionName() {
        PackageManager packageManager = getPackageManager();
        PackageInfo packInfo = null;
        String version = "";
        try {
            packInfo = packageManager.getPackageInfo(getPackageName(), 0);
            version = packInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return version;
    }
}
