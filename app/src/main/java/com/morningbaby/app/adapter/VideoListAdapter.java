package com.morningbaby.app.adapter;

import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.morningbaby.app.MorningBabyApp;
import com.morningbaby.app.R;
import com.morningbaby.app.tool.ImageLoaderControl;
import com.morningbaby.sdk.model.KnowledgeVideo;

import java.util.List;

/**
 * Created by apple on 15/8/6.
 */
public class VideoListAdapter extends BaseAdapter{

    private List<KnowledgeVideo> mData;
    private ImageLoaderControl mLoader;
    public VideoListAdapter(List<KnowledgeVideo> data){
        mData=data;
        ColorDrawable drawable=new ColorDrawable(MorningBabyApp.getInstance()
                .getResources().getColor(R.color.bg));
        mLoader=new ImageLoaderControl(drawable);
    }

    @Override
    public int getCount() {
        if(mData!=null)
            return mData.size();
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if(mData!=null)
            return mData.get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder=null;
        if(convertView==null){
            convertView= LayoutInflater.from(MorningBabyApp.getInstance()).inflate(R.layout.klg_video_item,null);
            holder=new ViewHolder();
            holder.image= (ImageView) convertView.findViewById(R.id.iv_image);
            holder.name= (TextView) convertView.findViewById(R.id.tv_name);
            holder.desc= (TextView) convertView.findViewById(R.id.tv_desc);
            holder.tag= (TextView) convertView.findViewById(R.id.tv_tag);
            convertView.setTag(holder);
        }else{
            holder= (ViewHolder) convertView.getTag();
        }
        KnowledgeVideo video=mData.get(position);
        mLoader.displayImage(video.getThumbnail(),holder.image);
        holder.name.setText(video.getTitle());
        holder.desc.setText(video.getDescription());
        holder.tag.setText(video.getTags());
        return convertView;
    }

    class ViewHolder{
        ImageView image;
        TextView name;
        TextView desc;
        TextView tag;
    }
}
