package com.morningbaby.app.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.morningbaby.app.MorningBabyApp;
import com.morningbaby.app.R;
import com.morningbaby.app.log.Logger;
import com.morningbaby.sdk.model.KnowledgeArticle;

import java.util.List;

/**
 * Created by apple on 15/8/6.
 */
public class ArticleListAdapter extends BaseAdapter {

    private List<KnowledgeArticle> mData;
    public ArticleListAdapter(List<KnowledgeArticle> data){
        mData=data;
    }

    @Override
    public int getCount() {
        if(mData!=null)
            return mData.size();
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if(mData!=null)
            return mData.get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder=null;
        if(convertView==null){
            convertView= LayoutInflater.from(MorningBabyApp.getInstance()).inflate(R.layout.klg_article_item,null);
            holder=new ViewHolder();
            holder.name= (TextView) convertView.findViewById(R.id.tv_name);
            holder.desc= (TextView) convertView.findViewById(R.id.tv_desc);
            holder.tag= (TextView) convertView.findViewById(R.id.tv_tag);
            holder.author= (TextView) convertView.findViewById(R.id.tv_author);
            holder.time= (TextView) convertView.findViewById(R.id.tv_time);
            convertView.setTag(holder);
        }else{
            holder= (ViewHolder) convertView.getTag();
        }
        KnowledgeArticle article=mData.get(position);
        holder.name.setText(article.getTitle());
        holder.desc.setText(article.getBrief());
        holder.tag.setText(article.getTags());
        holder.author.setText(article.getAuthor());
        holder.time.setText(article.getTime());
        return convertView;
    }

    class ViewHolder{
        TextView name,desc,author,tag,time;
    }
}
