package com.morningbaby.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.morningbaby.app.MorningBabyApp;
import com.morningbaby.app.R;
import com.morningbaby.app.log.Logger;
import com.morningbaby.app.tool.ImageLoaderControl;
import com.morningbaby.app.tool.PublicUtil;
import com.morningbaby.app.tool.SysIntent;
import com.morningbaby.sdk.model.Hospital;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by apple on 15/8/5.
 */
public class HospitalAdapter extends BaseAdapter{
    private List<Hospital> mHospitals;
    private Location mLocation;
    private Map<String,String> mDistanceMap=new HashMap<>();
    private ImageLoaderControl mLoaderControl;
    private Context context;
    public HospitalAdapter(Context context,List<Hospital> data){
        mHospitals=data;
        Drawable drawable=context.getResources().getDrawable(R.drawable.hospital_default);
        mLoaderControl=new ImageLoaderControl(drawable);
        this.context=context;
    }

    private boolean locationEnable=true;
    public void setLocationEnable(boolean flag){
        locationEnable=flag;
    }

    public void setLocation(Location location){
        mLocation=location;
    }

    private String getDistane(Hospital hospital){
        if(!locationEnable){
            return MorningBabyApp.getInstance().getString(R.string.hospital_locate_error);
        }
        String id=hospital.getId();
        if(mDistanceMap.containsKey(id))
            return mDistanceMap.get(id);
        if(mLocation!=null){
            float distance= PublicUtil.
                    getDistanceInMiles(mLocation.getLatitude(),mLocation.getLongitude(),
                            hospital.getGeoLatitude(),hospital.getGeoLongitude());
            String dis=PublicUtil.getDistanceDisplay(distance);
            mDistanceMap.put(id,dis);
            return dis;
        }
        return MorningBabyApp.getInstance().getString(R.string.hospital_distance_unknown);
    }

    @Override
    public int getCount() {
        if(mHospitals!=null)
            return mHospitals.size();
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if(mHospitals!=null)
            return mHospitals.get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder=null;
        if(convertView==null){
            convertView= LayoutInflater.from(MorningBabyApp.getInstance()).inflate(R.layout.hospital_item,null);
            holder=new ViewHolder();
            holder.logo=(ImageView)convertView.findViewById(R.id.iv_logo);
            holder.name=(TextView)convertView.findViewById(R.id.tv_name);
            holder.address=(TextView)convertView.findViewById(R.id.tv_address);
            holder.desc=(TextView)convertView.findViewById(R.id.tv_desc);
            holder.telephone=(ImageView)convertView.findViewById(R.id.iv_tel);
            holder.distance=(TextView)convertView.findViewById(R.id.tv_distance);
            convertView.setTag(holder);
        }else{
            holder= (ViewHolder) convertView.getTag();
        }
        final Hospital hospital=mHospitals.get(position);
        if(!TextUtils.isEmpty(hospital.getPicture())){
            mLoaderControl.displayImage(hospital.getPicture(), holder.logo);
        }
        holder.name.setText(hospital.getName());
        holder.desc.setText(hospital.getIntroduction());
        String address="";
        String province=hospital.getAddressProvince();
        if(!TextUtils.isEmpty(province))
            address+=province;
        String city=hospital.getAddressCity();
        if(!TextUtils.isEmpty(city))
            address+=city;
        String disinct=hospital.getAddressDistinct();
        if(!TextUtils.isEmpty(disinct))
            address+=disinct;
        address+=hospital.getAddressDetail();
        holder.address.setText(address);
        holder.distance.setText(getDistane(hospital));
        holder.telephone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SysIntent.dial((Activity) context,hospital.getPhone());
            }
        });
        return convertView;
    }

    class ViewHolder{
        ImageView logo;
        TextView name;
        TextView desc;
        TextView address;
        ImageView telephone;
        TextView distance;
    }
}
