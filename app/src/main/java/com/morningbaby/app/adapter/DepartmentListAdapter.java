package com.morningbaby.app.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.morningbaby.app.MorningBabyApp;
import com.morningbaby.app.R;
import com.morningbaby.sdk.model.Department;

import java.util.List;

/**
 * Created by apple on 15/8/7.
 */
public class DepartmentListAdapter extends BaseAdapter{
    private List<Department> mData;
    public DepartmentListAdapter(List<Department> data){
        mData=data;
    }

    @Override
    public int getCount() {
        if(mData!=null)
            return mData.size();
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if(mData!=null)
            mData.get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder=null;
        if(convertView==null){
            convertView= LayoutInflater.from(MorningBabyApp.getInstance())
                    .inflate(R.layout.hospital_department_item,null);
            holder=new ViewHolder();
            holder.name=(TextView)convertView.findViewById(R.id.tv_name);
            holder.desc=(TextView)convertView.findViewById(R.id.tv_desc);
            convertView.setTag(holder);
        }else{
            holder= (ViewHolder) convertView.getTag();
        }
        Department department=mData.get(position);
        holder.name.setText(department.getName());
        holder.desc.setText(department.getIntroduction());
        return convertView;
    }

    class ViewHolder{
        TextView name;
        TextView desc;
    }
}
