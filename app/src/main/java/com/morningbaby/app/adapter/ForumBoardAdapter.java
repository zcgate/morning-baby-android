package com.morningbaby.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.forum.sdk.bean.section.ForumSearchInfo;
import com.forum.sdk.bean.section.ForumSectionInfo;
import com.forum.sdk.bean.section.ForumSectionList;
import com.morningbaby.app.MorningBabyApp;
import com.morningbaby.app.R;
import com.morningbaby.app.tool.ImageLoaderControl;
import com.morningbaby.app.tool.PublicUtil;
import com.morningbaby.app.tool.SysIntent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zchao on 8/20/15.
 */
public class ForumBoardAdapter  extends BaseAdapter {
//    private List<Hospital> mHospitals;
    private ForumSectionList mForumSectionList;
    private ImageLoaderControl mLoaderControl;
    private Context context;
    public ForumBoardAdapter(Context context, ForumSectionList data){
        mForumSectionList=data;
        Drawable drawable=context.getResources().getDrawable(R.drawable.forum_board_default);
        mLoaderControl=new ImageLoaderControl(drawable);
        this.context=context;
    }

    @Override
    public int getCount() {
        if(mForumSectionList!=null)
            return mForumSectionList.getCount();
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if(mForumSectionList!=null)
            return mForumSectionList.getItems().get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder=null;
        if(convertView==null){
            convertView= LayoutInflater.from(MorningBabyApp.getInstance()).inflate(R.layout.forum_board_item,null);
            holder=new ViewHolder();
            holder.logo=(ImageView)convertView.findViewById(R.id.iv_forum_board_logo);
            holder.name=(TextView)convertView.findViewById(R.id.tv_forum_board_name);
            holder.desc=(TextView)convertView.findViewById(R.id.tv_forum_board_desc);
            holder.joinNumber=(TextView)convertView.findViewById(R.id.tv_forum_board_join_number);
            holder.postNumber=(TextView)convertView.findViewById(R.id.tv_forum_board_post_num);
            convertView.setTag(holder);
        }else{
            holder= (ViewHolder) convertView.getTag();
        }
        final ForumSectionInfo forumSectionInfo=mForumSectionList.getItems().get(position);
        if(!TextUtils.isEmpty(forumSectionInfo.getImageId())){
            mLoaderControl.displayImage(forumSectionInfo.getImageId(), holder.logo);
        }
        holder.name.setText(forumSectionInfo.getName());
        holder.desc.setText(forumSectionInfo.getIntro());
        holder.joinNumber.setText(forumSectionInfo.getMemberNum());
        holder.postNumber.setText(forumSectionInfo.getPostNum());
        return convertView;
    }

    class ViewHolder{
        ImageView logo;
        TextView name;
        TextView desc;
        TextView joinNumber;
        TextView postNumber;
    }
}
