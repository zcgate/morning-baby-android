package com.morningbaby.app.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;

import com.forum.sdk.bean.section.ForumCategoryInfo;
import com.forum.sdk.bean.section.ForumCategoryList;
import com.morningbaby.app.R;
import com.morningbaby.app.view.ExRadioButton;
import com.morningbaby.app.view.ExRadioGroup;


/**
 * Created by Administrator on 2015/4/21.
 */
public class CommunityCategoryExpandAdapter extends BaseExpandableListAdapter implements View.OnClickListener {

    private ForumCategoryList mList;

    private Context mContext;
    private LayoutInflater mInflater;

    public boolean initSelect = true;

    private OnClickExpandListener mListener;

    /**
     * 团体类型的 ExRadioButton的组
     */
    public ExRadioGroup communityTypeRadioGroup = new ExRadioGroup();

    public CommunityCategoryExpandAdapter(Context context, ForumCategoryList list){
        mContext = context;
        mList = list;
        mInflater = LayoutInflater.from(mContext);
    }

    public void setOnClickExpandListener(OnClickExpandListener listener){
        mListener = listener;
    }

    @Override
    public int getGroupCount() {
        if(mList != null && mList.getItems() != null){
            Log.e("getGroupCount", "size:" + mList.getItems().size());
            return mList.getItems().size();
        }
        return 0;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if(mList != null && mList.getItems() != null && mList.getItems().get(groupPosition) != null &&
                mList.getItems().get(groupPosition).getChildren() != null ){
            Log.e("getChildrenCount", "size:" + mList.getItems().get(groupPosition).getChildren().size());
            return mList.getItems().get(groupPosition).getChildren().size();
        }
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public void onClick(View v) {
        if(mListener != null){
            mListener.clickExpandButton((int) v.getTag());
        }
    }

    class ViewHolder {
        public ImageView ivExp;
        public ExRadioButton rbSelect;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        Log.e("getGroupView", "groupPosition:" + groupPosition);
        ViewHolder holder = new ViewHolder();
        if(convertView == null){
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.forum_category_item, null);
            holder.ivExp = (ImageView) convertView.findViewById(R.id.ivExp);
            holder.ivExp.setOnClickListener(this);
            holder.rbSelect = (ExRadioButton) convertView.findViewById(R.id.rbSelect);
            holder.rbSelect.init();
            holder.rbSelect.setCheckImgResId(R.drawable.forum_btn_create_team_team_type_checked);
            communityTypeRadioGroup.addExRadioButton(holder.rbSelect);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(isExpanded){
            holder.ivExp.setImageResource(R.drawable.forum_expand);
            holder.ivExp.setVisibility(View.GONE);
        } else {
            holder.ivExp.setImageResource(R.drawable.forum_next_to);
            holder.ivExp.setVisibility(View.GONE);
        }
        ForumCategoryInfo info = mList.getItems().get(groupPosition);
        holder.rbSelect.setTag(info.getId());
        holder.rbSelect.setTitle(info.getName());
        holder.ivExp.setTag(groupPosition);
        //默认选中第一个
        if(initSelect && groupPosition == 0){
            holder.rbSelect.setCheck(true);
            initSelect = false;
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null){
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.forum_category_child_item, null);
            holder.rbSelect = (ExRadioButton) convertView.findViewById(R.id.rbSelect);
            holder.rbSelect.init();
            holder.rbSelect.setCheckImgResId(R.drawable.forum_btn_create_team_team_type_checked);
            communityTypeRadioGroup.addExRadioButton(holder.rbSelect);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        ForumCategoryInfo info = mList.getItems().get(groupPosition).getChildren().get(childPosition);
        holder.rbSelect.setTitle(info.getName());
        holder.rbSelect.setTag(info.getId());
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public interface OnClickExpandListener{
        void clickExpandButton(int groupPosition);
    }
}
