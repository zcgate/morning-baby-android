package com.morningbaby.app.adapter;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.morningbaby.app.MorningBabyApp;
import com.morningbaby.app.R;
import com.morningbaby.app.tool.ImageLoaderControl;
import com.morningbaby.sdk.model.Doctor;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by apple on 15/8/8.
 */
public class DoctorListAdapter extends BaseAdapter{
    private List<Doctor> mData;
    private ImageLoaderControl mLoaderControl;
    public DoctorListAdapter(List<Doctor> data){
        mData=data;
        mLoaderControl=new ImageLoaderControl();
    }

    @Override
    public int getCount() {
        if(mData!=null)
            return mData.size();
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if(mData!=null)
            mData.get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder=null;
        if(convertView==null){
            convertView= LayoutInflater.from(MorningBabyApp.getInstance())
                    .inflate(R.layout.hospital_doctor_item,null);
            holder=new ViewHolder();
            holder.photo=(ImageView)convertView.findViewById(R.id.iv_photo);
            holder.name=(TextView)convertView.findViewById(R.id.tv_name);
            holder.title=(TextView)convertView.findViewById(R.id.tv_title);
            holder.desc=(TextView)convertView.findViewById(R.id.tv_desc);
            holder.visit_time=(TextView)convertView.findViewById(R.id.tv_visit_time);
            convertView.setTag(holder);
        }else{
            holder= (ViewHolder) convertView.getTag();
        }
        Doctor doctor=mData.get(position);
        if(!TextUtils.isEmpty(doctor.getPhoto())) {
            mLoaderControl.displayImage(doctor.getPhoto(), holder.photo);
        }
        holder.name.setText(doctor.getName());
        holder.title.setText(doctor.getTitle());
        holder.desc.setText(doctor.getIntroduction());
        List<Map<String, String>> visits=doctor.getTimeVisits();
        if(visits!=null){
            int size=visits.size();
            String visitResult="";
            for(int i=0;i<size;i++){
                Map<String,String> item=visits.get(i);
                Set<String> keys=item.keySet();
                Iterator<String> ite=keys.iterator();
                int count=0;
                while (ite.hasNext()){
                    String key=ite.next();
                    if(count==1)
                        visitResult+="~";
                    String time=item.get(key);
                    try{
                        visitResult+=time.substring(0,2)+":"+time.substring(2,4);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    count++;
                }
                if(i/2==0){
                    visitResult+="      ";
                }else{
                    visitResult+="\r\n";
                }
            }
            holder.visit_time.setText(visitResult);
        }else{
            holder.visit_time.setText("");
        }
        return convertView;
    }

    class ViewHolder{
        ImageView photo;
        TextView name;
        TextView title;
        TextView desc;
        TextView visit_time;
    }
}
