package com.morningbaby.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.morningbaby.app.log.Logger;
import com.morningbaby.app.tool.ImageLoaderControl;
import com.morningbaby.app.tool.PublicUtil;
import com.morningbaby.app.tool.environment;
import com.morningbaby.sdk.model.HotActivity;

import java.util.List;

/**
 * Created by apple on 15/8/6.
 */
public class HotActivityAdapter extends PagerAdapter {
    private ImageLoaderControl mLoaderControl=new ImageLoaderControl();
    private List<HotActivity> mData;
    private Context context;
    public HotActivityAdapter(Context context,List<HotActivity> data){
        mData=data;
        this.context=context;
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        if (mData != null)
            return mData.size();
        return 0;
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        // TODO Auto-generated method stub
        return arg0 == arg1;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        container.removeView(((View) object));
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        // TODO Auto-generated method stub
        ImageView image = new ImageView(context);
        image.setScaleType(ImageView.ScaleType.CENTER_CROP);
        container.addView(image,
                environment.getScreenWidth((Activity) context),
                PublicUtil.dip2px(context, 150));
        mLoaderControl.displayImage(mData.get(position).getActivityPic(), image);
        Logger.d(getClass(),mData.get(position).getActivityPic());
        return image;
    }

    @Override
    public int getItemPosition(Object object) {
        // TODO Auto-generated method stub
        return POSITION_NONE;
    }

}

