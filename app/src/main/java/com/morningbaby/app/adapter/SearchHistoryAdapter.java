package com.morningbaby.app.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.morningbaby.app.MorningBabyApp;
import com.morningbaby.app.R;

import java.util.List;

/**
 * Created by apple on 15/8/12.
 */
public class SearchHistoryAdapter extends BaseAdapter{

    private List<String> mKeys;

    public SearchHistoryAdapter(List<String> keys){
        mKeys=keys;
    }

    @Override
    public int getCount() {
        if(mKeys!=null)
            return mKeys.size();
        return 0;
    }

    @Override
    public Object getItem(int i) {
        if(mKeys!=null)
            return mKeys.get(i);
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        TextView textView=null;
        if(view==null){
            view= LayoutInflater.from(MorningBabyApp.getInstance()).inflate(R.layout.search_key_item,null);
            textView= (TextView) view.findViewById(R.id.tv_key);
            view.setTag(textView);
        }else{
            textView= (TextView) view.getTag();
        }
        textView.setText(mKeys.get(i));
        return view;
    }
}
