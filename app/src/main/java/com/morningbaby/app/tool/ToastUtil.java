package com.morningbaby.app.tool;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.morningbaby.app.R;

import java.lang.ref.WeakReference;



/***
 * 提供Toast相关的显示
 * 
 * <br>
 * Created 2014-8-20 上午11:24:31
 * 
 * @version
 * @author ChenYong
 * 
 * @see
 */
public final class ToastUtil {
    
    public static WeakReference<View> mToastCustomeView;
    
    /**
     * 
     * Creates a new instance of ToastUtil. <br>
     * Created 2014年9月9日 上午9:41:34
     */
    private ToastUtil() {
        super();
    }

    /**
     * 通过Toast显示信息
     * 
     * <br>
     * Created 2014年8月29日 下午3:36:27
     * 
     * @param context
     *            上下文
     * @param msg
     *            信息
     * @author 何贤挺
     */
    public static void showLongToast(Context context, String msg) {
        if (context == null) {
            return;
        }
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    /**
     * 通过Toast显示信息
     * 
     * <br>
     * Created 2014年8月29日 下午3:36:27
     * 
     * @param context
     *            上下文
     * @param resId
     *            信息ID
     * @author 何贤挺
     */
    public static void showLongToast(Context context, int resId) {
        if (context == null) {
            return;
        }
        Toast.makeText(context, resId, Toast.LENGTH_LONG).show();
    }

    /**
     * 通过Toast显示信息
     * 
     * <br>
     * Created 2014年8月29日 下午3:36:27
     * 
     * @param context
     *            上下文
     * @param msg
     *            信息
     * @author 何贤挺
     */
    public static void showShortToast(Context context, String msg) {
        if (context == null) {
            return;
        }
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * 通过Toast显示信息
     * 
     * <br>
     * Created 2014年8月29日 下午3:36:27
     * 
     * @param context
     *            上下文
     * @param resId
     *            信息ID
     * @author 何贤挺
     */
    public static void showShortToast(Context context, int resId) {
        if (context == null) {
            return;
        }
        Toast.makeText(context, resId, Toast.LENGTH_SHORT).show();
    }
    
    
    /***
     * 调试用的toast
     * 
     * <br>Created 2015-1-12 上午11:08:46
     * @param context
     * @param msg
     * @author       ChenYong
     */
    public static void showLongToast4Debug(Context context, String msg) {
       
        if(false){
            if (context == null) {
                return;
            }
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
        }
    }
    
    /**
     * 显示自定义的toast。short
     * 
     * <br>Created 2015-1-14 下午5:58:13
     * @param context context
     * @param msg 显示的msg
     * @author       zhuhp
     */
    public static void showShortCustomToast(Context context,String msg){
        showCustomeToast(context, msg, Toast.LENGTH_SHORT, Gravity.CENTER);
    }
    
    /**
     * 显示自定义的toast。short
     * 
     * <br>Created 2015-1-14 下午5:58:13
     * @param context context
     * @param msg 显示的msg
     * @author       zhuhp
     */
    public static void showLongCustomToast(Context context,String msg){
        showCustomeToast(context, msg, Toast.LENGTH_LONG, Gravity.CENTER);
    }
    
    /**
     * 
     * 
     * <br>Created 2015-1-14 下午7:12:35
     * @param context context
     * @param msg 信息
     * @param gravity toast位置
     * @author       zhuhp
     */
    private static void showCustomeToast(Context context,String msg,int duration,int gravity){
        if(context==null){
            return;
        }
        Toast toast = initToastLayout(context, msg);
        toast.setGravity(gravity, 0, 0);
        toast.setDuration(duration);
        toast.show();
    }
    
    /**
     * 初始化taost layout
     * 
     * <br>Created 2015-1-14 下午5:58:59
     * @param context context
     * @param msg 信息
     * @return
     * @author       zhuhp
     */
    private static Toast initToastLayout(Context context,String msg){
        View view = null;
        if(mToastCustomeView != null){
            view = mToastCustomeView.get();
        } 
        if(view==null){
            LayoutInflater inflater=LayoutInflater.from(context);
            view = inflater.inflate(R.layout.forum_toast_custome_layout, null);
            mToastCustomeView = new WeakReference<View>(view);
        }
        TextView tv=(TextView)view.findViewById(R.id.tv_toast);
        tv.setText(msg);
        Toast toast = new Toast(context.getApplicationContext());
        toast.setView(view);
        return toast;
    }

}
