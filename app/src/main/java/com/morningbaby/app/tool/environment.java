package com.morningbaby.app.tool;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.KeyguardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.morningbaby.app.MorningBabyApp;
import com.morningbaby.app.log.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

public class environment {
	/**
	 * 檢查是否有網絡連接
	 * 
	 * @return
	 */
	public static boolean isNetworkAvailable() {
		// Context context = mActivity.getApplicationContext();
		Context context = MorningBabyApp.getInstance().getApplicationContext();
		ConnectivityManager connectivity = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity == null) {
			return false;
		} else {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public static boolean isWifiConnect() {
		ConnectivityManager connectivityManager = (ConnectivityManager) MorningBabyApp
				.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
		if (networkInfo != null) {
			if (networkInfo.isConnected()) {
				int type = networkInfo.getType();
				if (type == ConnectivityManager.TYPE_WIFI) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 設備屏幕是否鎖定
	 * 
	 * @return
	 */
	public static boolean isScreenLock() {
		Context context = MorningBabyApp.getInstance().getApplicationContext();
		KeyguardManager mKeyguardManager = (KeyguardManager) context
				.getSystemService(Context.KEYGUARD_SERVICE);
		if (mKeyguardManager.inKeyguardRestrictedInputMode()) {
			return true;
		} else
			return false;
	}

	/**
	 * 檢測app是否在前臺
	 * 
	 * @return
	 */
	public static boolean isAppOnForeground() {
		// Returns a list of application processes that are running on the
		// device
		if (environment.isScreenLock()) {
			return false;
		}
		ActivityManager activityManager = (ActivityManager) MorningBabyApp.getInstance()
				.getApplicationContext()
				.getSystemService(Context.ACTIVITY_SERVICE);
		ComponentName cn = activityManager.getRunningTasks(1).get(0).topActivity;

		Logger.i(environment.class, cn.getPackageName() + " pack=" + MorningBabyApp.getInstance().getPackageName());
		if (cn.getPackageName().equals(MorningBabyApp.getInstance().getPackageName())) {
			return true;
		}
		return false;
	}

	public static String getTopActivityName() {
		ActivityManager activityManager = (ActivityManager) MorningBabyApp.getInstance()
				.getApplicationContext()
				.getSystemService(Context.ACTIVITY_SERVICE);
		ComponentName cn = activityManager.getRunningTasks(1).get(0).topActivity;
		return cn.getClassName();
	}

	public static String formatDateTime(Date date) {
		return environment.formatDateTime(date, "yyyy-MM-dd HH:mm:ss");
	}

	public static String formatDateTime(Date date, String formatstring) {
		SimpleDateFormat format = new SimpleDateFormat(formatstring);
		return format.format(date);
	}

	public static long parseTime(String time) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Date date = format.parse(time);
			return date.getTime();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * 取得文件的後綴名
	 * 
	 * @param p_file
	 * @return
	 */
	public static String getFileExt(String p_file) {
		int index = p_file.lastIndexOf(".");
		if (index == -1)
			return "";
		return p_file.substring(index + 1);
	}

	/**
	 * 是否存在sdcard
	 * 
	 * @return
	 */
	public static boolean isSdcardExist() {
		if (android.os.Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 取得設備號
	 * 
	 * @return
	 */
	public static String getUUID() {
		String uniqueId ="";
		try {
			TelephonyManager tm = (TelephonyManager) MorningBabyApp.getInstance()
					.getSystemService(Context.TELEPHONY_SERVICE);
			String tmDevice, tmSerial, androidId;
			tmDevice = "" + tm.getDeviceId();
			tmSerial = "" + tm.getSimSerialNumber();
			androidId = ""
					+ android.provider.Settings.Secure.getString(MorningBabyApp
							.getInstance().getContentResolver(),
					android.provider.Settings.Secure.ANDROID_ID);
			UUID deviceUuid = new UUID(androidId.hashCode(),
					((long) tmDevice.hashCode() << 32)
							| tmSerial.hashCode());
			uniqueId = deviceUuid.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Logger.d(environment.class, "uuid=" + uniqueId);
		return uniqueId;
	}

	/**
	 * 取得設備的全屏高度
	 *
	 * @return
	 */
	public static int getRawScreenHeight(Activity activity) {
		Display display = activity.getWindowManager().getDefaultDisplay();
		try {
			Class c = Class.forName("android.view.Display");
			Method method = c.getMethod("getRawHeight");
			return (Integer) method.invoke(display);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			DisplayMetrics dm = new DisplayMetrics();
			display.getMetrics(dm);
			return dm.heightPixels;
		}
	}

	/**
	 * 取得屏幕高度，在有虛擬按鍵的設備上得到的是扣除狀態欄后的高度
	 * 
	 * @return
	 */
	public static int getScreenHeight(Activity activity) {
		DisplayMetrics dm = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		return dm.heightPixels;
	}

	/**
	 * 全屏時屏幕的高度
	 *
	 * @return
	 */
	public static int getValidScreenHeight(Activity activity) {
		if (getScreenHeight(activity) == getRawScreenHeight(activity)
				|| !isTabletDevice())
			return getScreenHeight(activity) - getStatusHeight();
		else
			return getScreenHeight(activity);
	}

	public static int getScreenWidth(Activity activity) {
		DisplayMetrics dm = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		return dm.widthPixels;
	}

	public static int getScreenDpi(Activity activity) {
		DisplayMetrics dm = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		return dm.densityDpi;
	}

	public static boolean isHighDpi(Activity activity) {
		int dpi = environment.getScreenDpi(activity);
		if (dpi > DisplayMetrics.DENSITY_DEFAULT) {// hdpi or xdpi
			return true;
		}
		return false;
	}

	/**
	 * 取得狀態欄的高度
	 * 
	 * @return
	 */
	public static int getStatusHeight() {
		try {
			Class c = Class.forName("com.android.internal.R$dimen");
			Object obj = c.newInstance();
			Field field = c.getField("status_bar_height");
			int x = Integer.parseInt(field.get(obj).toString());
			return MorningBabyApp.getInstance().getResources().getDimensionPixelSize(x);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	public static boolean isTabletDevice() {
		if (Build.VERSION.SDK_INT >= 11) { // honeycomb
			// test screen size, use reflection because isLayoutSizeAtLeast is
			// only available since 11
			Configuration con = MorningBabyApp.getInstance().getResources()
					.getConfiguration();
			try {
				Method mIsLayoutSizeAtLeast = con.getClass().getMethod(
						"isLayoutSizeAtLeast", int.class);
				Boolean r = (Boolean) mIsLayoutSizeAtLeast.invoke(con,
						0x00000004); // Configuration.SCREENLAYOUT_SIZE_XLARGE
				return r;
			} catch (Exception x) {
				x.printStackTrace();
				return false;
			}
		}
		return false;
	}

	/**
	 * 取得系統當前語言別
	 * 
	 * @return
	 */
	public static String getLanguage() {
		return Locale.getDefault().getCountry();
	}

	public static boolean isServiceWorked(String serviceName) {
		ActivityManager myManager = (ActivityManager) MorningBabyApp.getInstance()
				.getSystemService(Context.ACTIVITY_SERVICE);
		ArrayList<RunningServiceInfo> runningService = (ArrayList<RunningServiceInfo>) myManager
				.getRunningServices(30);
		for (int i = 0; i < runningService.size(); i++) {
			if (runningService.get(i).service.getClassName().toString()
					.equals(serviceName)) {
				return true;
			}
		}
		return false;
	}

	public static void changeSoftKeyboard() {
		InputMethodManager imm = (InputMethodManager) MorningBabyApp.getInstance()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		// 得到InputMethodManager的实例
		if (imm.isActive()) {
			// 如果开启
			imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,
					InputMethodManager.HIDE_NOT_ALWAYS);
			// 关闭软键盘，开启方法相同，这个方法是切换开启与关闭状态的
		}
	}

	public static void openSoftwareLayer(View view) {
		if (Build.VERSION.SDK_INT > 11)
			view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
	}

	public static int getSystemSDKVersion() {
		return Build.VERSION.SDK_INT;
	}

	/**
	 * @return 当前系统语言别名称
	 */
	public static String getLanguageWithLocal() {
		return Locale.getDefault().getCountry();
		// String ret;
		// if ("CN".equals(Locale.getDefault().getCountry()))
		// ret = "zh_CN";
		// else if ("TW".equals(Locale.getDefault().getCountry()))
		// ret = "zh_TW";
		// else
		// ret = "en";
		// return ret;
	}
}
