package com.morningbaby.app.tool;

import android.util.Log;
 

public class LogUtil {

	public static final String TAG = LogUtil.class.getSimpleName();
	
	public static void v(String tag , String info){
		
		if(false){
			Log.v(tag,info);
		}
		
	}
	
	
	public static void d(String tag , String info){
		
		if(false){
			Log.d(tag,info);
		}
		
	}
	
	public static void e(String tag , String info){
		
		if(false){
			Log.e(tag,info);
		}
		
	}
	
	
	public static void i(String tag , String info){
		
		if(false){
			Log.i(tag,info);
		}
		
	}
	
	public static void w(String tag , String info){
		
		if(false){
			Log.w(tag,info);
		}
		
	}
	
}
