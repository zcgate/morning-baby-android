package com.morningbaby.app.tool;

import android.content.SharedPreferences;

import com.morningbaby.app.MorningBabyApp;

/**
 * Created by apple on 15/8/8.
 */
public class Setting {

    public static SharedPreferences getSP(){
        return MorningBabyApp.getInstance().getSharedPreferences("settings", 0);
    }

    public static boolean dlUnderWifi(){
        SharedPreferences sp=getSP();
        boolean flag=sp.getBoolean("need_wifi", true);
        return flag;
    }

    public static void setDlUnderWifi(boolean isChecked){
        SharedPreferences sp=getSP();
        SharedPreferences.Editor editor=sp.edit();
        editor.putBoolean("need_wifi", isChecked);
        editor.commit();
    }
}
