package com.morningbaby.app.tool;

import android.content.Context;
import android.graphics.Paint;
import android.util.DisplayMetrics;

/**
 * Android大小单位转换工具类
 * 
 * @author wader
 * 
 */
public final class DisplayUtil {
    /**
     * 
     * Creates a new instance of DisplayUtil. <br>
     * Created 2014年9月9日 上午9:42:41
     */
    private DisplayUtil() {
        super();
    }

    private static DisplayMetrics mDisplayMetrics;

    /**
     * 将px值转换为dip或dp值，保证尺寸大小不变
     * 
     * @param pxValue
     *            入参
     * @param scale
     *            精度 （DisplayMetrics类中属性density）
     * @return 值
     */
    public static int px2dip(float pxValue, float scale) {
        return (int) (pxValue / scale + 0.5f);
    }

    /**
     * 将dip或dp值转换为px值，保证尺寸大小不变
     * 
     * @param dipValue
     * @param scale
     *            （DisplayMetrics类中属性density）
     * @return 值
     */
    public static int dip2px(float dipValue, float scale) {
        return (int) (dipValue * scale + 0.5f);
    }

    /**
     * 将px值转换为sp值，保证文字大小不变
     * 
     * @param pxValue
     * @param fontScale
     *            （DisplayMetrics类中属性scaledDensity）
     * @return 值
     */
    public static int px2sp(float pxValue, float fontScale) {
        return (int) (pxValue / fontScale + 0.5f);
    }

    /**
     * 将sp值转换为px值，保证文字大小不变
     * 
     * @param spValue
     * @param fontScale
     *            （DisplayMetrics类中属性scaledDensity）
     * @return 值
     */
    public static int sp2px(float spValue, float fontScale) {
        return (int) (spValue * fontScale + 0.5f);
    }

    /**
     * 将sp值转换为px值，保证文字大小不变
     * 
     * @param spValue
     *            （DisplayMetrics类中属性scaledDensity）
     * @return
     */
    public static int sp2px(Context context, float spValue) {
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (spValue * fontScale + 0.5f);
    }

    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return dip2px(dpValue, scale);
    }

    /***
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
     * 
     * <br>
     * Created 2014-9-2 上午11:44:00
     * 
     * @param context
     *            上下文
     * @param pxValue
     *            入参
     * @return 值
     * @author ChenYong
     */
    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return px2dip(pxValue, scale);
    }

    /**
     * 获取屏幕宽度
     * 
     * <br>
     * Created 2014年9月4日 上午11:18:13
     * 
     * @param context
     *            上下文
     * @return 屏幕宽度
     * @author 何贤挺
     */
    public static int getScreenWidth(Context context) {

        if (mDisplayMetrics == null) {
            mDisplayMetrics = context.getResources().getDisplayMetrics();
        }
        return mDisplayMetrics.widthPixels;
    }
    
    /***
     * 获取文本长度
     * 
     * <br>Created 2015-1-15 下午7:32:33
     * @param text
     * @param textSize
     * @return
     * @author       ChenYong
     */
   public static int getTextLength(String text , float textSize){
       Paint pFont = new Paint();
       pFont.setTextSize(textSize);        
       float size = pFont.measureText(text);    
       //直接返回参数字符串所占用的宽度
        return (int)size;
   }
}
