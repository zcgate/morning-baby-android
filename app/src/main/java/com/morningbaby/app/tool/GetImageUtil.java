package com.morningbaby.app.tool;

import android.app.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.widget.Toast;


import com.morningbaby.app.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * 获取图片工具类
 * 
 * <br>
 * Created 2014-9-11 上午11:10:24
 * 
 * @version
 * @author 阮锋冰
 * 
 * @see
 * 
 *
 */
public class GetImageUtil {

    /**
     * 用来标识请求照相功能的activity
     */
    public static final int CAMERA_WITH_DATA = 3023;

    /**
     * 用来标识请求gallery的activity
     */
    private static final int PHOTO_PICKED_WITH_DATA = 3021;

    /**
     * 
     */
    private static final int CROP_PHOTO_WITH_DATA = 3024;

    /**
     * 调用activity上下文
     */
    private Context context = null;

    /**
     * 拍照的照片存储位置
     */
    private static final File PHOTO_DIR = new File(Environment.getExternalStorageDirectory()
            + "/DCIM/Camera");

    private static final String TAG = GetImageUtil.class.getSimpleName();

    /**
     * 图片URI地址
     */
    private Uri cropImgUri = null;

    /**
     * 照相机拍照得到的图片
     */
    private File mCurrentPhotoFile;

    /**
     * 允许没有SD卡的情况下使用相机
     */
    private boolean useCameraWithoutSD;

    /**
     * 允许没有SD卡的情况下使用相册
     */
    private boolean usePhotosWithoutSD;

    /**
     * 允许在低电量情况下使用照相机
     */
    private boolean useCameraInLowBattery;

    private static final String KEY_CURRENT_PHOTO_FILE = "KEY_CURRENT_PHOTO_FILE";

    public GetImageUtil(Context context, Bundle savedInstanceState) {

        this.context = context;

        useCameraWithoutSD = false;
        usePhotosWithoutSD = false;
        useCameraInLowBattery = false;

        if (savedInstanceState != null) {
            mCurrentPhotoFile = new File(savedInstanceState.getString(KEY_CURRENT_PHOTO_FILE));
        }
    }

    /**
     * 在启动相机时，原activity可能被销毁，在销毁时需要把mCurrentPhotoFile存储起来，然后创建activity的时候再读出来，否则这个变量就没了。 <br>
     * Created 2015-1-24 下午3:26:04
     * 
     * @param outState
     * @author 蒋立辉
     */
    public void onSaveInstanceState(Bundle outState) {
        if (mCurrentPhotoFile != null) {
            outState.putString(KEY_CURRENT_PHOTO_FILE, mCurrentPhotoFile.getAbsolutePath());
        }
    }

    /**
     * 设置是否在没有SD卡情况下使用相机
     * 
     * <br>
     * Created 2014-9-26 上午10:55:00
     * 
     * @param useCameraWithoutSD
     * @author 阮锋冰
     */
    public void setUseCameraWithoutSD(boolean useCameraWithoutSD) {

        this.useCameraWithoutSD = useCameraWithoutSD;
    }

    /**
     * 设置是否在没有SD卡的情况下使用相册
     * 
     * <br>
     * Created 2014-9-26 上午10:55:24
     * 
     * @param usePhotosWithoutSD
     * @author 阮锋冰
     */
    public void setUsePhotosWithoutSD(boolean usePhotosWithoutSD) {

        this.usePhotosWithoutSD = usePhotosWithoutSD;
    }

    /**
     * 设置是否在低电量情况下使用SD卡
     * 
     * <br>
     * Created 2014-9-26 上午11:18:11
     * 
     * @param useCameraInLowBattery
     * @author 阮锋冰
     */
    public void setUseCameraInLowBattery(boolean useCameraInLowBattery) {
        this.useCameraInLowBattery = useCameraInLowBattery;
    }

    /**
     * 获取剪切图片的URI
     * 
     * <br>
     * Created 2014-9-22 下午4:08:51
     * 
     * @return
     * @author 阮锋冰
     */
    public Uri getCropImgUri() {
        return cropImgUri;
    }

    /**
     * 检查是否存在SD卡
     * 
     * <br>
     * Created 2014-9-26 上午11:16:41
     * 
     * @return
     * @author 阮锋冰
     */
    private boolean hasSDCard() {

        boolean sdCardExist = Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED); // 判断sd卡是否存在

        return sdCardExist;
    }

    /**
     * 电量是否为低
     * 
     * <br>
     * Created 2014-9-26 上午11:19:14
     * 
     * @return
     * @author 阮锋冰
     */
    private boolean isBatteryLow() {

        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent result = context.registerReceiver(null, intentFilter);
        int level = result.getIntExtra("level", 0);
        int scale = result.getIntExtra("scale", 100);
        if (((level * 100) / scale) < 10) {
            return true;
        }
        return false;
    }

    /**
     * 从相机中获取图片
     * 
     * <br>
     * Created 2014-9-11 上午11:10:46
     * 
     * @author 阮锋冰
     */
    public void doTakePhoto() {

        // 如果SD卡不允许为空，而且没有SD卡的情况下
        if (!useCameraWithoutSD && !hasSDCard()) {
            Toast t = Toast.makeText(context, context.getString(R.string.forum_please_check_sdcard), Toast.LENGTH_LONG);
            t.show();
            return;
        }

        // 如果电量低
        if (!useCameraInLowBattery && isBatteryLow()) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setTitle(context.getString(R.string.forum_warn));
            dialog.setMessage(context.getString(R.string.forum_lottery_lower));
            dialog.setPositiveButton(context.getString(R.string.forum_confirm), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    goToCamera();
                }
            });
            dialog.setNegativeButton(context.getString(R.string.forum_cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    return;
                }
            });
            dialog.show();
        } else {
            goToCamera();
        }

    }

    public void goToCamera() {
        try {
            // Launch camera to take photo for selected contact
            PHOTO_DIR.mkdirs();// 创建照片的存储目录
            mCurrentPhotoFile = new File(PHOTO_DIR, getPhotoFileName());// 给新照的照片文件命名
            Intent intent = getTakePickIntent(mCurrentPhotoFile);
            ((Activity) context).startActivityForResult(intent, CAMERA_WITH_DATA);

        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, context.getString(R.string.forum_no_photo), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * 
     * 
     * <br>
     * Created 2014-9-11 上午11:11:01
     * 
     * @param f
     * @return
     * @author 阮锋冰
     * 
     */
    public static Intent getTakePickIntent(File f) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri uri = Uri.fromFile(f);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        return intent;
    }

    /**
     * 用当前时间给图片命名
     * 
     * <br>
     * Created 2014-9-11 上午11:11:32
     * 
     * @return
     * @author 阮锋冰
     */
    private String getPhotoFileName() {
        Date date = new Date(System.currentTimeMillis());
        return date.getTime() + ".jpg";
    }

    /**
     * 用当前裁剪给图片命名
     * 
     * <br>
     * Created 2014-9-11 上午11:11:32
     * 
     * @return
     * @author 阮锋冰
     */
    private String getCropPhotoFileName() {
        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat dateFormat = new SimpleDateFormat("'CROPIMG'_yyyy-MM-dd-HH-mm-ss");
        return dateFormat.format(date) + ".jpg";
    }

    /**
     * 请求Gallery程序
     * 
     * <br>
     * Created 2014-9-11 上午11:11:53
     * 
     * @author 阮锋冰
     */
    public void doPickPhotoFromGallery() {

        // 如果SD卡不允许为空，而且没有SD卡的情况下
        if (!usePhotosWithoutSD && !hasSDCard()) {
            Toast t = Toast.makeText(context, context.getString(R.string.forum_please_check_sdcard), Toast.LENGTH_LONG);
            t.show();
            return;
        }

        try {
            // Launch picker to choose photo for selected contact
            ToastUtil.showLongToast(context, context.getString(R.string.forum_photo_error_tip));
            final Intent intent = getPhotoPickIntent();
            ((Activity) context).startActivityForResult(intent, PHOTO_PICKED_WITH_DATA);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, context.getString(R.string.forum_gallery_open_fail), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * 封装请求Gallery的intent
     * 
     * <br>
     * Created 2014-9-11 上午11:12:21
     * 
     * @return
     * @author 阮锋冰
     */
    public static Intent getPhotoPickIntent() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT, null);
        intent.setType("image/*");
        return intent;
    }

    /**
     * 启动gallery去剪辑这个照片
     * 
     * <br>
     * Created 2014-9-11 上午11:12:56
     * 
     * @author 阮锋冰
     */
    public void doCropPhoto(Uri u) {
        if (u == null) {
            return;
        }
        try {
            // 需要先解析一遍uri，因为4.4以上系统返回的uri之前的版本不一样，这里需要统一转换成file://形式
            u = Uri.parse("file://" + getPath(context, u));

            Bitmap bmp = decodeUriAsBitmap(u);
            if (bmp.getWidth() < 110 || bmp.getHeight() < 110) {
                Toast.makeText(context, context.getString(R.string.forum_photo_pattern_error), Toast.LENGTH_LONG).show();
                return;
            }

//            gotoCropImageActivity(CROP_PHOTO_WITH_DATA, u.toString(),
//                    (Activity) context, 640, 640);

            // 启动gallery去剪辑这个照片
//            final Intent intent = getCropImageIntent(u);
//            ((Activity) context).startActivityForResult(intent, CROP_PHOTO_WITH_DATA);
        } catch (Exception e) {
            Toast.makeText(context, context.getString(R.string.forum_gallery_open_fail), Toast.LENGTH_LONG).show();
        }
    }

//    public void gotoCropImageActivity(int request, String uri,
//        Activity activity, int width, int height){
//        Intent intent = new Intent(activity, CropImageActivity.class);
//        intent.setClass(activity, CropImageActivity.class);
//        intent.putExtra("outputX", width);
//        intent.putExtra("outputY", height);
//        intent.putExtra("mCropUri", uri);
//        activity.startActivityForResult(intent, request);
//    }

    /**
     * Constructs an intent for image cropping. 调用图片剪辑程序
     * 
     * <br>
     * Created 2014-9-11 上午11:14:54
     * 
     * @param photoUri
     * @return
     * @author 阮锋冰
     */
    public Intent getCropImageIntent(Uri photoUri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(photoUri, "image/*");
        intent.putExtra("crop", "true");

        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", 110);
        intent.putExtra("outputY", 110);
        intent.putExtra("scale", true);
        intent.putExtra("scaleUpIfNeeded", true);
        intent.putExtra("return-data", true);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());

        return intent;
    }

    /**
     * @n<b>函数名称</b> :decodeUriAsBitmap
     * @brief uri 解析出图片
     * @see
     * @since Ver 1.3.0
     * @param @param uri
     * @param @return
     * @return Bitmap
     * @<b>作者</b> : daiyf
     * @<b>创建时间</b> : 2013-11-29下午6:08:53
     */
    public Bitmap decodeUriAsBitmap(Uri uri) {

        InputStream openInputStream = null;
        try {
            openInputStream = context.getContentResolver().openInputStream(uri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return FileUtil.decodeInputStreamAsBitmap(openInputStream);
    }

    /**
     * 保存bitmap 文件
     * 
     * <br>
     * Created 2014-9-26 下午6:11:45
     * 
     * @param bmp
     * @author Administrator
     */
    public void saveBitmap(Bitmap bmp, File f) {

        if (f.exists()) {
            f.delete();
        }
        try {

            FileOutputStream out = new FileOutputStream(f);
            bmp.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 此函数必须放在调用activity的 onActivityResult函数中
     * 
     * <br>
     * Created 2014-9-11 上午11:44:38
     * 
     * @param requestCode
     * @param resultCode
     * @param data
     * @return 获取照片的photo
     * @author Administrator
     * 
     * @warning 函数通过Activity.onActivityResult回调，如果选择通过相机进行图片提取，那么onActivityResult 在完成启动相机的activity
     *          onActivityResult之后，还会调用一次图片截取的activity，二在启动相机结束后返回的photo一定是
     *          null的，所以，在获得返回的bmp之后，一定要判断bmp是否为空才能使用
     */
    public Bitmap onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode != Activity.RESULT_OK) {
            return null;
        }

        Bitmap photo = null;
        switch (requestCode) {
        case PHOTO_PICKED_WITH_DATA: // 调用Gallery返回的

            Uri imgUri = data.getData();
            doCropPhoto(imgUri);
            break;

        case CAMERA_WITH_DATA: // 照相机程序返回的,再次调用图片剪辑程序去修剪图片

            if (mCurrentPhotoFile != null && mCurrentPhotoFile.exists()) {
                doCropPhoto(Uri.fromFile(mCurrentPhotoFile));
            } else if (data != null && data.getData() != null) {
                doCropPhoto(data.getData());
            } else {
                LogUtil.w(TAG, "mCurrentPhotoFile null and data null");
            }
            break;

        case CROP_PHOTO_WITH_DATA:

            // 调用微博模块的裁剪，其返回的结果是uri
            cropImgUri = Uri.parse(data.getStringExtra("mCropUri"));
            // 根据uri解析出bitmap
            photo = BitmapFactory.decodeFile(cropImgUri.getPath());
            // photo = data.getParcelableExtra("data");
            // if (photo.getWidth() < 110 || photo.getHeight() < 110) {
            //
            // photo = null;
            // cropImgUri = null;
            // } else {
            // // save the bitmap and return bitmap uri
            // File cropImgFile = new File(PHOTO_DIR, getCropPhotoFileName());
            // saveBitmap(photo, cropImgFile);
            // cropImgUri = Uri.fromFile(cropImgFile);
            // }
            break;
        default:
            break;
        }

        return photo;
    }

    /**
     * 根据uri获取绝对路径（4.4以上的系统返回的uri以“content”开头） <br>
     * Created 2015-1-13 下午4:47:36
     * 
     * @param context
     * @param uri
     * @return
     * @author 蒋立辉
     */
    @SuppressLint("NewApi")
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            } else if (isDownloadsDocument(uri)) {
                // DownloadsProvider

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            } else if (isMediaDocument(uri)) {
                // MediaProvider
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] { split[1] };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        } else if ("content".equalsIgnoreCase(uri.getScheme())) {
            // MediaStore (and general)

            // Return the remote address
            if (isGooglePhotosUri(uri)) {
                return uri.getLastPathSegment();
            }

            return getDataColumn(context, uri, null, null);
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            // File
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for MediaStore Uris, and other
     * file-based ContentProviders.
     * 
     * @param context
     *            The context.
     * @param uri
     *            The Uri to query.
     * @param selection
     *            (Optional) Filter used in the query.
     * @param selectionArgs
     *            (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
            String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = { column };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    /**
     * @param uri
     *            The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri
     *            The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri
     *            The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri
     *            The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }
}
