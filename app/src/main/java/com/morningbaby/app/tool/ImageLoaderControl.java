package com.morningbaby.app.tool;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.File;

/**
 * Created by apple on 15/6/23.
 */
public class ImageLoaderControl {
    private DisplayImageOptions mUrlOptions;
    private DisplayImageOptions mLocalOptions;
    private boolean cacheInMemory=false;
    public ImageLoaderControl() {
        init(null, Bitmap.Config.RGB_565);
    }

    public ImageLoaderControl(boolean cacheInMemory){
        this.cacheInMemory=cacheInMemory;
        init(null, Bitmap.Config.RGB_565);
    }

    public ImageLoaderControl(Bitmap.Config config){
        init(null, config);
    }

    public ImageLoaderControl(Drawable defPic) {
        init(defPic, Bitmap.Config.RGB_565);
    }

    public ImageLoaderControl(Drawable defPic, boolean cacheInMemory) {
        this.cacheInMemory=cacheInMemory;
        init(defPic,Bitmap.Config.RGB_565);
    }

    private void init(Drawable defPic,Bitmap.Config config) {
        mUrlOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(cacheInMemory).cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .showImageOnLoading(defPic)
                .considerExifParams(true)
                .bitmapConfig(config)
                .build();
        mLocalOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(cacheInMemory).cacheOnDisk(false)
                .imageScaleType(ImageScaleType.EXACTLY)
                .showImageOnLoading(defPic)
                .considerExifParams(true)
                .bitmapConfig(config)
                .build();
    }

    public Bitmap loadImageSync(Object pic){
        return loadImageSync(pic, null);
    }

    public Bitmap loadImageSync(Object pic, ImageSize size) {
        String uri = null;
        if (pic instanceof Integer) {
            uri = "drawable://" + pic;
        } else {
            String image = pic.toString();
            try {
                int temp = Integer.valueOf(image);
                uri = "drawable://" + temp;
            } catch (Exception e) {
                if (!URLUtil.isValidUrl(image))
                    uri = "file://" + image;
            }
        }
        if (uri == null)
            return null;
        return ImageLoader.getInstance().loadImageSync(uri, size, mLocalOptions);
    }

    public void displayImage(Object pic,ImageView view,ImageLoadingListener listener){
        if(Setting.dlUnderWifi()&&!environment.isWifiConnect())
            return;
        if(pic==null)
            return;
        boolean isUrl=false;
        String uri = null;
        if (pic instanceof Integer) {
            uri = "drawable://" + pic;
        } else {
            String image = pic.toString();
            try {
                int temp = Integer.valueOf(image);
                uri = "drawable://" + temp;
            } catch (Exception e) {
                if (!URLUtil.isValidUrl(image)) {
                    uri = "file://" + image;
                }
                else{
                    uri=image;
                    isUrl=true;
                }
            }
        }
        if (uri == null)
            return;
        if(isUrl)
            ImageLoader.getInstance().displayImage(uri, view, mUrlOptions,listener);
        else
            ImageLoader.getInstance().displayImage(uri, view, mLocalOptions,listener);
    }

    public void displayImage(Object pic, ImageView view) {
        displayImage(pic, view, null);
    }


    public static void stop(){
        ImageLoader.getInstance().stop();
    }
}
