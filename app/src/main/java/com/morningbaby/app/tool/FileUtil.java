package com.morningbaby.app.tool;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.morningbaby.app.MorningBabyApp;
import com.morningbaby.app.log.Logger;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class FileUtil {

    public static String getAppExternalCachePath() {
        try {
            return MorningBabyApp.getInstance().getExternalCacheDir().getPath()
                    + File.separator;
        } catch (Exception e) {
            return "";
        }
    }

    public static String getAssetsFileContent(String filePath) {
        try {
            InputStream is = MorningBabyApp.getInstance().getAssets().open(filePath);
            int i = -1;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            while ((i = is.read()) != -1) {
                baos.write(i);
            }
            // Logger.i(AssetsManager.class, baos.toString());
            return baos.toString();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            Logger.i(FileUtil.class, "no file:" + filePath);
        }
        return null;
    }

    public static Bitmap decodeInputStreamAsBitmap(InputStream in) {
        if (in == null) {
            return null;
        }
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(in);
        } catch (OutOfMemoryError e2) {
            BitmapFactory.Options opt = new BitmapFactory.Options();
            opt.inPreferredConfig = Bitmap.Config.RGB_565;
            opt.inPurgeable = true;
            opt.inInputShareable = true;
            // ����
            opt.inSampleSize = 2;
            try {
                BitmapFactory.decodeStream(in, null, opt);
            } catch (Exception e4) {
                e4.printStackTrace();
            }
        } catch (Exception e5) {
            e5.printStackTrace();
        }
        return bitmap;
    }
}
