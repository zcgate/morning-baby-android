package com.morningbaby.app.tool;

import android.content.Context;
import android.location.Location;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.text.DecimalFormat;

public class PublicUtil {
	/**
	 * md5編碼
	 * 
	 * @param source
	 * @return
	 */
	public static String getMD5(byte[] source) {
		String s = null;
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'a', 'b', 'c', 'd', 'e', 'f' };
		try {
			java.security.MessageDigest md = java.security.MessageDigest
					.getInstance("MD5");
			md.update(source);
			byte tmp[] = md.digest();
			char str[] = new char[16 * 2];
			int k = 0;
			for (int i = 0; i < 16; i++) {
				byte byte0 = tmp[i];
				str[k++] = hexDigits[byte0 >>> 4 & 0xf];
				str[k++] = hexDigits[byte0 & 0xf];
			}
			s = new String(str);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	/**
	 * 将px值转换为sp值，保证文字大小不变
	 * 
	 * @param pxValue
	 *            （DisplayMetrics类中属性scaledDensity）
	 * @return
	 */
	public static int px2sp(Context context, float pxValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (pxValue / scale + 0.5f);
	}

	/**
	 * 将sp值转换为px值，保证文字大小不变
	 * 
	 * @param spValue
	 *            （DisplayMetrics类中属性scaledDensity）
	 * @return
	 */
	public static int sp2px(Context context, float spValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (spValue * scale + 0.5f);
	}

	/**
	 * 将dip转换成px
	 * 
	 * @param context
	 * @param dpValue
	 * @return
	 */
	public static int dip2px(Context context, float dpValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dpValue * scale + 0.5f);
	}

	/**
	 * 将px转换成dip
	 * 
	 * @param context
	 * @param pxValue
	 * @return
	 */
	public static float px2dip(Context context, int pxValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return pxValue / scale;
	}

	/**
	 * 将dip转换成sp
	 * 
	 * @param context
	 * @param dpValue
	 * @return
	 */
	public static float dip2sp(Context context, float dpValue) {
		return px2sp(context, dip2px(context, dpValue));
	}

	/**
	 * 将sp转换成dip
	 * 
	 * @param context
	 * @param spValue
	 * @return
	 */
	public static float sp2dip(Context context, float spValue) {
		return px2dip(context, sp2px(context, spValue));
	}

	/**
	 * 取得兩點間的距離，經緯度
	 *
	 * @param lat1
	 * @param lon1
	 * @param lat2
	 * @param lon2
	 * @return
	 */
	public static float getDistance(double lat1, double lon1, double lat2,
									double lon2) {
		return getDistanceInMiles(lat1, lon1, lat2, lon2) / 1000;

	}

	/**
	 *
	 * @param lat1
	 * @param lon1
	 * @param lat2
	 * @param lon2
	 * @return mile
	 */
	public static float getDistanceInMiles(double lat1, double lon1,
										   double lat2, double lon2) {

		float[] results = new float[1];

		Location.distanceBetween(lat1, lon1, lat2, lon2, results);

		return results[0];
	}

	/**
	 * @param distance 单位是米，接收getDistanceInMiles的返回结果
	 * @return
	 */
	public static String getDistanceDisplay(float distance) {
		DecimalFormat df = new DecimalFormat("#.#");
		String dis="";
		if(distance<1000){
			distance=Float.parseFloat(df.format(distance));
			dis=distance+"M";
		}
		else{
			distance=Float.parseFloat(df.format(distance/1000));
			dis=distance+"KM";
		}
		return dis;
	}

	/**
	 * 重新计算ListView的高度(ScorllView 中如果再放入scrollView 是无法计算的，我们可以计算后再赋值)
	 * @param listView
	 */
	public static void setListViewHeightBasedOnChildren(ListView listView) {
		setListViewHeightBasedOnChildren(listView,0);
	}

	public static void setListViewHeightBasedOnChildren(ListView listView,int extraHeight) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null) {
			// pre-condition
			return;
		}

		int totalHeight = 0;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(0, 0);
			totalHeight += listItem.getMeasuredHeight();
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1))+extraHeight;
		listView.setLayoutParams(params);
	}
}
