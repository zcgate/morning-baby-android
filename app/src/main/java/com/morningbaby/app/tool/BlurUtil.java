package com.morningbaby.app.tool;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;

/**
 * 高斯模糊处理工具类
 * 
 * <br>
 * Created 2014-8-6 上午10:31:46
 * 
 * @version
 * @author HuangYK
 * 
 * @see
 */
public final class BlurUtil {

    /**
     * Creates a new instance of BlurUtil. <br>
     * Created 2015-2-9 上午11:54:16
     */
    private BlurUtil() {
    }

    /** 宽度按照比例值进行高斯模糊处理 */
    public static final int BLUR_SCALE_TYPE_W = 0x900;
    /** 高度按照比例值进行高斯模糊处理 */
    public static final int BLUR_SCALE_TYPE_H = 0x901;
    /** 宽度和高度都按照比例值进行高斯模糊处理 */
    public static final int BLUR_SCALE_TYPE_W_H = 0x903;

    /** 默认模糊像素范围 10 */
    public static final int BLUR_DEFAULT_RADIO = 10;

    /**
     * 将整张图进行高斯模糊处理
     * 
     * <br>
     * Created 2014-7-30 下午1:48:48
     * 
     * @param sentBitmap
     *            原图片
     * @param radius
     *            模糊像素范围（一般取10就够了）
     * @return 高斯模糊后的Bitmap对象
     * @author : HuangYK
     */
    public static Bitmap blurFullBitmap(Bitmap sentBitmap, int radius) {
        Bitmap bitmap = copyBitmap(sentBitmap);

        if (bitmap == null) {
            return sentBitmap;
        }

        if (radius < 1) {
            return bitmap;
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        return blurBitmap(bitmap, radius, 0, 0, w, h);
    }

    /**
     * 根据比例对部分图片进行高斯模糊处理
     * 
     * <br>
     * Created 2014-7-30 下午2:00:26
     * 
     * @param sentBitmap
     *            原图
     * @param radius
     *            模糊像素范围（一般取10就够了），至少要>= 1，如果 <1则返回原图
     * @param scale
     *            比例，例如1/3
     * @param blurType
     *            高斯模糊处理类型 {@link #BLUR_SCALE_TYPE_W_H} {@link #BLUR_SCALE_TYPE_H}
     *            {@link #BLUR_SCALE_TYPE_W_H}
     * @return
     * @author : HuangYK
     */
    public static Bitmap blurBitmapByScale(Bitmap sentBitmap, int radius, float scale, int blurType) {

        Bitmap bitmap = copyBitmap(sentBitmap);

        if (radius < 1) {
            return bitmap;
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        switch (blurType) {
        case BLUR_SCALE_TYPE_W:
            w = (int) (w * scale + 0.5);
            break;
        case BLUR_SCALE_TYPE_H:
            h = (int) (h * scale + 0.5);
            break;
        case BLUR_SCALE_TYPE_W_H:
            w = (int) (w * scale + 0.5);
            h = (int) (h * scale + 0.5);
            break;
        default:
            return bitmap;
        }
        return blurBitmap(bitmap, radius, 0, 0, w, h);
    }

    /**
     * 根据给定的大小对图片进行高斯模糊处理
     * 
     * <br>
     * Created 2014-7-30 下午2:11:04
     * 
     * @param sentBitmap
     *            原图
     * @param radius
     *            模糊像素范围（一般取10就够了），至少要>= 1，如果 <1则返回原图
     * @param width
     *            需要高斯模糊处理的宽度 ，<= 0 则返回原图
     * @param height
     *            需要高斯模糊处理的高度，<= 0 则返回原图
     * @return 高斯模糊处理后的图片
     * @author : HuangYK
     */
    public static Bitmap blurBitmapBySize(Bitmap sentBitmap, int radius, int width, int height) {
        Bitmap bitmap = copyBitmap(sentBitmap);

        if (radius < 1 || width <= 0 || height <= 0) {
            return bitmap;
        }
        return blurBitmap(bitmap, radius, 0, 0, width, height);
    }

    /**
     * 根据坐标和给定的宽高对图片进行高斯模糊处理
     * 
     * <br>
     * Created 2014-8-7 下午3:50:56
     * 
     * @param sentBitmap
     *            原图
     * @param radius
     *            模糊像素范围（一般取10就够了），至少要>= 1，如果 <1则返回原图
     * @param topX
     *            顶部起始x坐标
     * @param topY
     *            顶部起始y坐标
     * @param width
     *            模糊宽度
     * @param height
     *            模糊高度
     * @return 高斯模糊处理后的图片
     * @author : HuangYK
     */
    public static Bitmap blurBitmapByCoordinate(Bitmap sentBitmap, int radius, int topX, int topY,
            int width, int height) {
        Bitmap bitmap = copyBitmap(sentBitmap);
        if (radius < 1 || width <= 0 || height <= 0) {
            return bitmap;
        }
        return blurBitmap(bitmap, radius, topX, topY, width, height);
    }

    /**
     * 从原图的位图中拷贝一份
     * 
     * <br>
     * Created 2014-8-6 上午11:01:50
     * 
     * @param sentBitmap
     *            原图
     * @return 拷贝后的图片
     * @author : HuangYK
     */
    private static Bitmap copyBitmap(Bitmap sentBitmap) {
        Config config = sentBitmap.getConfig();
        if (config == null) {
            config = Config.RGB_565;
        }
        return sentBitmap.copy(config, true);
    }

    /**
     * 图片进行高斯模糊处理
     * 
     * <br>
     * Created 2014-8-7 下午3:45:21
     * 
     * @param bitmap
     *            原图
     * @param radius
     *            高斯模糊像素范围
     * @param topX
     *            起始X坐标
     * @param topY
     *            其实Y坐标
     * @param w
     *            需要高斯模糊处理的宽度
     * @param h
     *            需要高斯模糊处理的高度
     * @return 高斯模糊处理后的图片
     * @author : HuangYK
     */
    private static Bitmap blurBitmap(Bitmap bitmap, int radius, int topX, int topY, int w, int h) {

        int[] pix = new int[w * h];
        bitmap.getPixels(pix, 0, w, topX, topY, w, h);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        bitmap.setPixels(pix, 0, w, topX, topY, w, h);
        return (bitmap);
    }

}
