package com.morningbaby.app.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.morningbaby.app.R;

/**
 * 標緻小點view的封裝類
 * @author apple
 *
 */
public class DotView extends LinearLayout implements OnClickListener {
	private int m_dotCount = 0;
	private Context m_context;
	private ImageView[] m_imageViews;
	private int m_curSel;
	public DotView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	//	setBackgroundResource(R.drawable.dot_bg);
		setOrientation(LinearLayout.HORIZONTAL);
		m_context = context;
	}

	public DotView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	//	setBackgroundResource(R.drawable.dot_bg);
		setOrientation(LinearLayout.HORIZONTAL);
		m_context = context;
	}

	public void setDotCount(int p_count) {
		m_dotCount = p_count;
	}

	public void createView(int p_count) {
		if(p_count<=1)
			return;
		m_dotCount = p_count;
		for (int i = 0; i < m_dotCount; i++) {
			ImageView iv = new ImageView(m_context);
			iv.setImageResource(R.drawable.dot_guide);
			iv.setClickable(true);
			iv.setPadding(25, 8, 0, 8);
			if (i == m_dotCount - 1)
				iv.setPadding(25, 8, 25, 8);
			LayoutParams lp = new LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			lp.gravity = Gravity.CENTER_VERTICAL;
			addView(iv, lp);
		}
		m_imageViews = new ImageView[m_dotCount];
		for (int i = 0; i < m_dotCount; i++) {
			m_imageViews[i] = (ImageView) getChildAt(i);
			m_imageViews[i].setEnabled(true);
		    m_imageViews[i].setOnClickListener(this);
			m_imageViews[i].setTag(i);
		}
		m_curSel = 0;
		m_imageViews[m_curSel].setEnabled(false);
	}
	public void setCurPoint(int index)
	{
		if (index < 0 || index > m_dotCount - 1 || m_curSel == index) {
			return;
		}
		m_imageViews[m_curSel].setEnabled(true);
		m_imageViews[index].setEnabled(false);
		m_curSel = index;
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
//		int index = (Integer) (v.getTag());
//		if(m_curSel==index)
//			return;
//		setCurPoint(index);
//		m_ic.handleCallBack(null, null);
	}
	public int getCurSel()
	{
		return m_curSel;
	}
}
