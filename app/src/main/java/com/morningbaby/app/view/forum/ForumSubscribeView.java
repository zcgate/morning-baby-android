package com.morningbaby.app.view.forum;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.viewpagerindicator.UnderlinePageIndicator;

/**
 * 我的群体activity
 * 
 * <br>
 * Created 2014-8-22 下午2:21:46
 * 
 * @version
 * @author zhuhp
 * 
 * @see
 * 
 *
 */
public class ForumSubscribeView extends Fragment implements
		OnClickListener, OnPageChangeListener {

	/**
	 * tab数量
	 */
	public static final int ITEM_SUM = 2;

	/**
	 * 全部tab
	 */
	public static final int ALL = 0;

	/**
	 * 我的管理
	 */
	public static final int MANAGER = 1;



	/**
	 * indicator
	 */
	private UnderlinePageIndicator mIndicator;

	/**
	 * 主viewpager
	 */
	private ViewPager mMainViewPager;

	/**
	 * “全部”
	 */
	private TextView mAllTextView;

	/**
	 * 我管理的
	 */
	private TextView mManagerTextView;

    //热门
//    private TextView mHotTextView;
	
	/**
	 * 适配器
	 */

    private View mRootView;
    private Context mContext;

    public static ForumSubscribeView newInstance(){
        ForumSubscribeView fragment = new ForumSubscribeView();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        mContext = getActivity();
//        mRootView = inflater.inflate(R.layout.forum_activity_square_mygroup,null,false);
//        mMainViewPager = (ViewPager) mRootView.findViewById(R.id.vp_square_mygroup);
//        mIndicator = (UnderlinePageIndicator) mRootView.findViewById(R.id.indicator_square_mygroup);
//        mRootView.findViewById(R.id.rl_square_group_my_send).setOnClickListener(this);
//        mRootView.findViewById(R.id.rl_square_group_my_join).setOnClickListener(this);
//        mRootView.findViewById(R.id.rl_square_group_subscribe).setVisibility(View.GONE);
//        mAdapter=new MyGroupViewAdapter(getFragmentManager());
//        mMainViewPager.setAdapter(mAdapter);
//        mIndicator.setFades(false);
//        Resources res = mContext.getResources();
//        mIndicator.setSelectedColor(res.getColor(
//                R.color.forum_cor_square_header_text_press));
//        mIndicator.setViewPager(mMainViewPager);
//        mIndicator.setOnPageChangeListener(this);
//        mAllTextView = (TextView) mRootView.findViewById(R.id.tv_square_group_my_send);
//        mAllTextView.setText(res.getString(R.string.forum_square_mygroup_tag_all));
//        mManagerTextView = (TextView) mRootView.findViewById(R.id.tv_square_group_my_join);
//        mManagerTextView.setText(res.getString(R.string.forum_square_mygroup_tag_manager));
//        return mRootView;

        return null;
    }



	/**
	 * 
	 * 
	 * <br>
	 * Created 2014-9-2 下午2:14:43
	 * 
	 * @param type type
	 * @author zhuhp
	 */
	private void updateTagView(int type) {
		Resources res = mContext.getResources();
		switch (type) {
//		case ALL:
//			// 点击全部tab
//			mAllTextView.setTextColor(res
//					.getColor(R.color.forum_cor_square_header_text_press));
////            mHotTextView.setTextColor(res.getColor(R.color.cor_square_tag_text_normal));
//			mManagerTextView.setTextColor(res
//					.getColor(R.color.forum_cor_square_tag_text_normal));
//			break;
//		case MANAGER:
//			// 点击我的管理tab
//			mAllTextView.setTextColor(res
//					.getColor(R.color.forum_cor_square_tag_text_normal));
////            mHotTextView.setTextColor(res.getColor(R.color.cor_square_tag_text_normal));
//			mManagerTextView.setTextColor(res
//					.getColor(R.color.forum_cor_square_header_text_press));
//			break;
//        case HOT:
//            mHotTextView.setTextColor(res.getColor(R.color.cor_square_header_text_press));
//            mAllTextView.setTextColor(res
//                    .getColor(R.color.cor_square_tag_text_normal));
//            mManagerTextView.setTextColor(res
//                    .getColor(R.color.cor_square_tag_text_normal));
//            break;
		default:
			break;
		}
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {

	}

	@Override
	public void onPageSelected(int position) {
		// 更新tab的状态
		updateTagView(position);
	}

	/**
	 * view的adapter
	 * 
	 * <br>
	 * Created 2014-8-28 下午7:54:20
	 * 
	 * @version SquareMyGroupActivity
	 * @author zhuhp
	 * 
	 * @see
	 * 
	 *
	 */
//	static class MyGroupViewAdapter extends FragmentPagerAdapter {
//
//		/**
//		 * “全部”tab所对应的fragment
//		 */
//		private BaseForumListFragment mAllView;
//
//		/**
//		 * “我的管理”tab所对应的fragment
//		 */
//		private BaseForumListFragment mManagerView;
//
//        private HotCommunityView mHotView;
//
//
//		/**
//		 *
//		 * Creates a new instance of MyGroupFragmentAdapter. <br>
//		 * Created 2014-9-2 下午3:16:38
//		 *
//		 */
//		public MyGroupViewAdapter(FragmentManager manager) {
//			super(manager);
//		}
//
//        @Override
//        public Fragment getItem(int position) {
//            if (position == ALL) {
//                // 全部tab
//                if (mAllView == null) {
//                    mAllView = MyForumListFragment.newInstance(MyForumListFragment.FOLLOW);
//                }
//                return mAllView;
//            } else if (position == MANAGER) {
//                // 我的管理tab
//                if (mManagerView == null) {
//                    mManagerView = MyForumListFragment.newInstance(MyForumListFragment.MANAGER);
//                }
//                return mManagerView;
//            }  else {
//                return null;
//            }
//        }
//
//		@Override
//		public int getCount() {
//			return ITEM_SUM;
//		}
//
//	}

	@Override
	public void onClick(View view) {
		int id = view.getId();
//		if (id == R.id.rl_square_group_my_send) {
//			// d点击“全部”tab
//			mMainViewPager.setCurrentItem(ALL);
//		} else if (id == R.id.rl_square_group_my_join) {
//			// 点击我的管理tab
//			mMainViewPager.setCurrentItem(MANAGER);
//		}
		
	}

    
}
