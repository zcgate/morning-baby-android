package com.morningbaby.app.view;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.morningbaby.app.R;

public class CustomProgressDialog extends Dialog {
	private TextView m_tv_loading;
	private View m_contentView;
	public CustomProgressDialog(Context context) {
		// TODO Auto-generated constructor stub
		super(context, R.style.CustomDialog);
		m_contentView=LayoutInflater.from(context).inflate(R.layout.progressdialog, null);
		setContentView(m_contentView);
		m_tv_loading = (TextView) findViewById(R.id.tv_loading);
	}

	public void setMessage(CharSequence message) {
		// TODO Auto-generated method stub
		if (m_contentView==null||m_tv_loading == null)
			return;
		if(!TextUtils.isEmpty(message)){
			m_contentView.setBackgroundColor(Color.WHITE);
			m_tv_loading.setText(message);
		}
	}

//	@Override
//	public void onWindowFocusChanged(boolean hasFocus) {
//		ImageView imageView = (ImageView)findViewById(R.id.iv_loading);
//		AnimationDrawable animationDrawable = (AnimationDrawable) imageView
//				.getBackground();
//		animationDrawable.start();
//	}
}
