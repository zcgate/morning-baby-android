package com.morningbaby.app.view;

public final class ChageImgType {

    /**
     * Creates a new instance of ChageImgType. <br>
     * Created 2015-2-9 上午11:55:53
     */
    private ChageImgType() {
    }

    public final static int CHAGE_IMG_CANCEL = 0;
    public final static int CHAGE_IMG_FROM_CAMERA = 1;
    public final static int CHAGE_IMG_FROM_PHOTO = 2;
    public final static int UPLOAD_IMG_FROM_PHOTO = 3;
}
