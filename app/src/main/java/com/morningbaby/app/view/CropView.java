package com.morningbaby.app.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

/**
 * ClassName:CropView
 * Function: 截图区域的绘制
 * Reason:	 
 * 
 * @author   linqm	
 * @version  
 * @since    Ver 1.1
 * @Date	 2014	2014-1-10		上午9:27:43
 *
 * @see 	 
 */
@SuppressLint("DrawAllocation")
public class CropView extends View {
	private final static int CROP_VIEW = 0x67000000;    //cropview区域颜色
	private final static int LINE_COLOR = 0xffffffff;   //线框颜色
	private final DisplayMetrics mDMetrics = getResources().getDisplayMetrics();
    private int mWidth = mDMetrics.widthPixels;       //屏幕宽度
    private int mHeight = mDMetrics.heightPixels;    //屏幕高度
    public final int finallength = mWidth<mHeight?mWidth:mHeight;   //判定截图框的边长
	private int mViewWidth;       //cropView宽度
	private int mViewHeight;      //cropview高度
	public int requestWidth;    //截图框左边第一个节点所在X坐标     
	public int requestHeight;  //截图框左边第一个节点所在Y坐标     
	private boolean isFirst = true;     //view是否第一次绘制
	private Paint mPaint = new Paint();
	private Paint mLinePaint = new Paint();



	public CropView(Context context) {
		super(context);
	}
	
	public CropView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CropView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if (isFirst) {
			mViewWidth = this.getWidth();
			mViewHeight = this.getHeight();
			requestWidth = (mViewWidth - finallength) < 0 ? 0   //当view的宽度小于截图框的边长，设置截图框的节点X坐标为0，
					: (mViewWidth - finallength) / 2;          //否则设置为减去的一半，是的截图框居中对齐
			requestHeight = (mViewHeight - finallength) < 0 ? 0   //当view的高度小于截图框的边长，设置截图框的节点Y坐标为0
					: (mViewHeight/2)-(finallength/2);   //截图框距离状态栏的高度
			isFirst = false;
		}
		//绘制黑色透明区域		
		mPaint.setColor(CROP_VIEW);

			// top
			canvas.drawRect(0, 0, mWidth, requestHeight, mPaint);
			// left
			canvas.drawRect(0, requestHeight, requestWidth, requestHeight+finallength, mPaint);
			// right
			canvas.drawRect(finallength+requestWidth, requestHeight, mWidth,requestHeight+finallength,
					mPaint);
			// bottom
			canvas.drawRect(0,requestHeight+finallength, mWidth, mHeight, mPaint);
            //绘制矩形框
			mLinePaint.setColor(LINE_COLOR);
			mLinePaint.setStrokeWidth(3f);			
			// top
			canvas.drawLine(requestWidth,requestHeight, requestWidth,requestHeight+finallength,
					mLinePaint);
			// left
			canvas.drawLine(requestWidth,requestHeight, requestWidth+finallength, requestHeight,
					mLinePaint);
			// right
			canvas.drawLine(requestWidth+finallength, requestHeight,  requestWidth+finallength,
					requestHeight+finallength, mLinePaint);
			// bottom
			canvas.drawLine(requestWidth,requestHeight+finallength, requestWidth+finallength,
					requestHeight+finallength, mLinePaint);

		}
	

}
