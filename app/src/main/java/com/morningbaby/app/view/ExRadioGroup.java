package com.morningbaby.app.view;

import java.util.Vector;

/**
 * radiobutton group类
 * 
 * <br>Created 2014-9-3 下午8:31:11
 * @version  
 * @author   阮锋冰		
 *
 * @see 	 
 * 
 *
 */
public class ExRadioGroup {

	/*
	 * 按钮组中 按钮队列
	 * */
	private Vector<ExRadioButton>radios = null;
	
	/*
	 * 当前已被设置的按钮
	 */
	private ExRadioButton checkRadio = null;
	
	public ExRadioGroup() {
		
		init();
	}
	
	public ExRadioButton getCheckButton(){
		
		return checkRadio;
	}
	
	/**
	 * 初始化
	 * 
	 * <br>Created 2014-9-3 下午8:32:03
	 * @author       阮锋冰
	 */
	private void init(){
		
		radios = new Vector<ExRadioButton>();
		checkRadio = null;
	}
	
	public int size(){
		
		return radios.size();
	}
	
	/**
	 * 查找按钮在按钮组中的索引
	 * 
	 * <br>Created 2014-9-3 下午8:32:15
	 * @param radio 需要查找的按钮
	 * @return 按钮所在的索引
	 * @author       阮锋冰
	 */
	public int findRadio(ExRadioButton radio){
		
		if(radio == null){
			return -1;
		}
		
		int size = radios.size();
		for(int i = 0; i < size; i++){
			
			ExRadioButton tmpRadio = radios.get(i);
			if(tmpRadio.equals(radio)){
				
				return i;
			}
		}
		
		return -1;
	}

	/**
	 * 添加新按钮
	 * 
	 * <br>Created 2014-9-3 下午8:33:10
	 * @param radio 需要加入的按钮
	 * @author       阮锋冰
	 */
	public void addExRadioButton(ExRadioButton radio){
		
		if(findRadio(radio) > 0){
			
			return;
		}
		
		radios.add(radio);
		radio.setExRadioGroup(this);
	}
	
	/**
	 * 移除按钮
	 * 
	 * <br>Created 2014-9-3 下午8:33:27
	 * @param radio 需要移除的按钮
	 * @author       阮锋冰
	 */
	public void removeRadioButton(ExRadioButton radio){
		
		int find = findRadio(radio);
		if(find < 0){
			
			return;
		}
		
		radio.setExRadioGroup(null);
		radios.remove(find);
	}
	
	/**
	 * 返回radio按钮组的数目
	 * 
	 * <br>Created 2014-9-4 上午8:49:40
	 * @return
	 * @author       Administrator
	 */
	public int radioCount(){
		
		return radios.size();
	}
	
	/**
	 * 获取索引为 i 的radio的状态
	 * 
	 * <br>Created 2014-9-4 上午8:51:09
	 * @param i radio索引
	 * @return 索引为i的按钮状态，如果索引值错误，返回false
	 * @author       Administrator
	 */
	public boolean getRadioCheck(int i){
		
		if(i >= 0 && i < radios.size()){
		
			ExRadioButton radio = radios.get(i);
			return radio.getCheck();
		}
		
		return false;
	}
	
	/**
	 * 通知其他同组中的按钮
	 * 
	 * <br>Created 2014-9-3 下午8:33:55
	 * @param radio 当前被按下的按钮
	 * @author       阮锋冰
	 */
	public void notifyRadioClick(ExRadioButton radio){
		
		if(checkRadio == null){
			
			checkRadio = radio;
			return;
		}
		
		if(checkRadio.equals(radio)){
			return;
		}
		
		checkRadio.setCheck(false);
		checkRadio = radio;
	}
}
