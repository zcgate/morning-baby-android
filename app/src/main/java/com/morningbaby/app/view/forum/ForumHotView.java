package com.morningbaby.app.view.forum;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.morningbaby.app.R;
import com.viewpagerindicator.UnderlinePageIndicator;

/**
 * 我的群体activity
 * 
 * <br>
 * Created 2014-8-22 下午2:21:46
 * 
 * @version
 * @author zhuhp
 * 
 * @see
 * 
 *
 */
public class ForumHotView extends Fragment implements
		OnClickListener, ViewPager.OnPageChangeListener {

	/**
	 * tab数量
	 */
	public static final int ITEM_SUM = 2;

    private static final int HOT_BOARDS = 0;

	public static final int HOT_POSTS = 1;


	/**
	 * indicator
	 */
	private UnderlinePageIndicator mIndicator;

	/**
	 * 主viewpager
	 */
	private ViewPager mMainViewPager;

    private MyGroupViewAdapter mAdapter;

    private View mRootView;

    private TextView mHotBoardsView;
    private TextView mHotPostsView;

    private Context mContext;

    public static ForumHotView newInstance(){
        ForumHotView fragment = new ForumHotView();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContext = getActivity();
        mRootView = inflater.inflate(R.layout.forum_hot, null);
        mMainViewPager = (ViewPager) mRootView.findViewById(R.id.vp_forum_hot);


        mIndicator = (UnderlinePageIndicator) mRootView.findViewById(R.id.indicator_forum_hot);
        mRootView.findViewById(R.id.rl_forum_hot_boards).setOnClickListener(this);
        mRootView.findViewById(R.id.rl_forum_hot_posts).setOnClickListener(this);
        mAdapter = new MyGroupViewAdapter(getFragmentManager());
        mMainViewPager.setAdapter(mAdapter);

        mHotBoardsView = (TextView) mRootView.findViewById(R.id.tv_forum_hot_boards);
        mHotPostsView = (TextView) mRootView.findViewById(R.id.tv_forum_hot_posts);

        mIndicator.setFades(false);
        mIndicator.setSelectedColor(mContext.getResources().getColor(
                R.color.forum_light_text_indicator));
        mIndicator.setOnPageChangeListener(this);
        mIndicator.setViewPager(mMainViewPager);
        return mRootView;
    }



    public void onResult(int request,int result,Intent data){
//        if(mAdapter==null) {
//            return;
//        }
//        SubscribeView view = mAdapter.getView(mCurrentPage);
//        if(view == null){
//            return;
//        }
//        view.result(request, result, data);
    }
	

	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
//		switch (requestCode) {
//		case MenuActivity.MENU_REQUEST_CODE:
//			if(resultCode==MenuActivity.CREATE_COMMUNITY){
//				//创建团体
//				intent=new Intent(this,CreateCommunityActivity.class);
//				startActivity(intent);
//			}else if(resultCode==MenuActivity.CREATE_TEAM){
//			}
//			break;
//
//		default:
//			break;
//		}
	}


    static class MyGroupViewAdapter extends FragmentPagerAdapter {

        private ForumBoardView mHotBoardsView;

        private ForumPostView mHotPostsView;


        public MyGroupViewAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == HOT_BOARDS) {
                if (mHotBoardsView == null) {
                    mHotBoardsView = ForumBoardView.newInstance();
                }
                return mHotBoardsView;
            } else if (position == HOT_POSTS) {
                if (mHotPostsView == null) {
                    mHotPostsView = ForumPostView.newInstance();
                }
                return mHotPostsView;
            } else {
                return null;
            }
        }


        @Override
        public int getCount() {
            return ITEM_SUM;
        }
    }


    @Override
	public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.rl_forum_hot_boards) {
            mMainViewPager.setCurrentItem(HOT_BOARDS);
        }  else if(id == R.id.rl_forum_hot_posts){
            mMainViewPager.setCurrentItem(HOT_POSTS);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        updateTagView(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void updateTagView(int type) {
        Resources res = mContext.getResources();
        switch (type) {
            case HOT_BOARDS:
                mHotBoardsView.setTextColor(res
                        .getColor(R.color.white));
                mHotPostsView.setTextColor(res.getColor(R.color.forum_light_text));
                break;

            case HOT_POSTS:
                mHotBoardsView.setTextColor(res.getColor(R.color.forum_light_text));
                mHotPostsView.setTextColor(res
                        .getColor(R.color.white));
                break;
            default:
                break;
        }
    }

//    /**
//     * 销毁通知
//     */
//    public void onDestroy(){
//        if (mAdapter != null){
//            if(mAdapter.getMySendView()!=null) {
//                mAdapter.getMySendView().onDestroy();
//            }
//            if(mAdapter.getMyJoinView()!=null){
//                mAdapter.getMyJoinView().onDestroy();
//            }
//        }
//    }
    
}
