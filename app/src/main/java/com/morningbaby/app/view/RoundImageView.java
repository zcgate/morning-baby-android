package com.morningbaby.app.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.ImageView;

public class RoundImageView extends ImageView {
	private Path mMaskPath;

	private float mCornerRadiusLeftTop = 0;

	private float mCornerRadiusRightTop = 0;

	private float mCornerRadiusLeftBottom = 0;

	private float mCornerRadiusRightBottom = 0;

	private int mWidth = 0;

	private int mHeight = 0;

	private Paint mMaskPaint;

	public RoundImageView(Context context) {
		super(context);
	}

	public RoundImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public RoundImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	/**
	 根据画布的宽高来设置路径，用于清除掉此路径外的部分
	 */
	private void generateMaskPath() {
		this.mMaskPath = new Path();
		this.mMaskPath.addRoundRect(new RectF(0, 0, mWidth, mHeight),
				new float[] { mCornerRadiusLeftTop, mCornerRadiusLeftTop,
						mCornerRadiusRightTop, mCornerRadiusRightTop,
						mCornerRadiusRightBottom, mCornerRadiusRightBottom,
						mCornerRadiusLeftBottom, mCornerRadiusLeftBottom },
				Path.Direction.CW);
		this.mMaskPath.setFillType(Path.FillType.INVERSE_WINDING);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		if ((w != oldw) || (h != oldh)) {
			this.mMaskPath = null;
			mWidth = w;
			mHeight = h;
		}
	}
	
	private DrawEnd m_drawEnd;
	public void setDrawEnd(DrawEnd drawEnd){
		m_drawEnd=drawEnd;
	}
	public interface DrawEnd{
		public void drawEnd(int width, int height);
	}

	@Override
	protected void onDraw(Canvas canvas) {
	//	Logger.d(getClass(), "onDraw");
		// 保存当前layer的透明橡树到离屏缓冲区。并新创建一个透明度爲255的新layer
		if (mCornerRadiusLeftTop > 0 || mCornerRadiusRightTop > 0
				|| mCornerRadiusRightBottom > 0 || mCornerRadiusLeftBottom > 0) {
			int saveCount = canvas.saveLayerAlpha(0.0F, 0.0F,
					canvas.getWidth(), canvas.getHeight(), 255,
					Canvas.HAS_ALPHA_LAYER_SAVE_FLAG);
			super.onDraw(canvas);
			if (this.mMaskPath == null)
				generateMaskPath();
			if (this.mMaskPath != null) {
				if (mMaskPaint == null) {
//					try {
//						if (android.os.Build.VERSION.SDK_INT >= 11)
//							setLayerType(LAYER_TYPE_SOFTWARE, null);
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
					mMaskPaint = new Paint();
					this.mMaskPaint.setAntiAlias(true);
					this.mMaskPaint.setXfermode(new PorterDuffXfermode(
							PorterDuff.Mode.CLEAR));
				}
				canvas.drawPath(this.mMaskPath, this.mMaskPaint);
			}
			canvas.restoreToCount(saveCount);
		} else{
			super.onDraw(canvas);
			if(m_drawEnd!=null){
				m_drawEnd.drawEnd(getWidth(), getHeight());
			}
		}
	}

	/**
	 * 设置圆角的半径
	 * 
	 * @param r
	 *            半径
	 */
	public void setCornerRadius(float r) {
		setCornerRadius(r, r, r, r);
	}

	/**
	 * 对应左上右上右下左下角进行设置圆角的半径
	 * 
	 * @param lt
	 *            左上半径
	 * @param rt
	 *            右上半径
	 * @param rb
	 *            右下半径
	 * @param lb
	 *            左下半径
	 */
	public void setCornerRadius(float lt, float rt, float rb, float lb) {
		mCornerRadiusLeftTop = lt;
		mCornerRadiusRightTop = rt;
		mCornerRadiusRightBottom = rb;
		mCornerRadiusLeftBottom = lb;
		this.mMaskPath = null;
		invalidate();
	}
}

