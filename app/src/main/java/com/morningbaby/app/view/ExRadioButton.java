package com.morningbaby.app.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.morningbaby.app.R;
import com.morningbaby.app.tool.DisplayUtil;


/**
 * 使用relativlayout 扩张单选按钮
 * 
 * <br>Created 2014-9-3 下午5:48:22
 * @version  
 * @author   阮锋冰		
 *
 * @see 	 
 * 
 *
 */
public class ExRadioButton extends RelativeLayout implements OnClickListener{

	/**
	 * 在relativelayout 索引为0的子view 显示title信息
	 */
	private TextView tvTitle = null;
	
	/**
	 * 用于显示，状态改变信息
	 */
	private ImageView ivCheck = null;

	/**
	 * 当前按钮状态
	 */
	private boolean check;
	
	/**
	 * 上下文
	 */
	private Context mContext;
	
	/**
	 * check状态下 显示的图片信息
	 */
	private int checkImgResId;
	
	/**
	 * uncheck状态下 显示的图片信息
	 */
	private int uncheckImgResId;
	
	/**
	 * check状态改变时候的回调函数
	 */
	private OnExRadioCheckChange mOnExRadioCheckChange = null;
	
	/**
	 * 与radiobutton 相关联的的radiogroup类
	 */
	private ExRadioGroup radioGroup = null;
	
	
	public ExRadioButton(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		
		mContext = context;
	}
	
	public ExRadioButton(Context context, AttributeSet attrs){
		super(context, attrs);
		
		mContext = context;
	}

	public ExRadioButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		mContext = context;
	}
	
	/**
	 * 代码初始化控件
	 * 
	 * <br>Created 2014-9-3 下午5:52:49
	 * @author       阮锋冰
	 */
	public void init(){
		

		
		//初始化imageview控件
		ivCheck = new ImageView(mContext);
		//imageview靠右居中
        LayoutParams checkLayout = new LayoutParams(
                DisplayUtil.dip2px(mContext, 25), DisplayUtil.dip2px(mContext, 25));
		checkLayout.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		checkLayout.addRule(RelativeLayout.CENTER_VERTICAL);
		this.addView(ivCheck, checkLayout);
		
		
        //初始化textview控件
        tvTitle = new TextView(mContext);
        tvTitle.setTextSize(17);
        tvTitle.setTextColor(0xff535353);
        tvTitle.setGravity(Gravity.CENTER);

        //textview 靠左，居中
        LayoutParams titleLayout = new LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        titleLayout.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        titleLayout.addRule(RelativeLayout.CENTER_VERTICAL);
        this.addView(tvTitle, titleLayout);		
		
		check = false;
		
		this.setFocusable(true);
		this.setClickable(true);
		
		//设置监听
		this.setOnClickListener(this);
	}
	
	/**
	 * 通过xml文件，对控件进行初始化后，需要，定义相关的子控件位置
	 * 
	 * <br>Created 2014-9-3 下午5:53:06
	 * @author       阮锋冰
	 */
	public void initFromXml(){
		
		//如果使用xml定义，第一个子view必须诶textview,第二个子类必须为imageview
		tvTitle = (TextView)this.getChildAt(0);
		ivCheck = (ImageView)this.getChildAt(1);
		
		check = false;
		
		this.setFocusable(true);
		this.setClickable(true);
		
		this.setOnClickListener(this);
	}
	
	/**
	 * 修改radiobutton的提示信息
	 * 
	 * <br>Created 2014-9-3 下午5:53:38
	 * @param title 提示信息
	 * @author       阮锋冰
	 */
	public void setTitle(String title){
		
		tvTitle.setText(title);
	}
	
	/**
	 * 获取RadioButton的text信息
	 * 
	 * <br>Created 2014-9-10 下午5:12:59
	 * @return
	 * @author       阮锋冰
	 */
	public String getText(){
		
		return tvTitle.getText().toString();
	}
	
	/**
	 * 设置按钮check状态下的显示图片
	 * 
	 * <br>Created 2014-9-3 下午5:53:52
	 * @param id 图片资源id
	 * @author       阮锋冰
	 */
	public void setCheckImgResId(int id){
		
		this.checkImgResId = id;
	}
	
	
	/**
	 * 设置按钮uncheck状态下的显示图片
	 * 
	 * <br>Created 2014-9-3 下午5:55:20
	 * @param id 图片资源id
	 * @author       阮锋冰
	 */
	public void setUncheckImgResId(int id){
		
		this.uncheckImgResId = id;
	}
	
	/**
	 * 第一次创建后，必须调用，用于跟新图片显示
	 * 
	 * <br>Created 2014-9-3 下午5:55:37
	 * @author       阮锋冰
	 */
	public void updateCheck(){
		
		setCheck(check);
	}
	
	/**
	 * 设置按钮状态改变监听
	 * 
	 * <br>Created 2014-9-4 上午8:47:01
	 * @param mOnExRadioCheckChange 按钮状态改变监听类
	 * @author       阮锋冰
	 */
	public void setOnExRadioCheckChange(OnExRadioCheckChange mOnExRadioCheckChange){
		
		this.mOnExRadioCheckChange = mOnExRadioCheckChange;
	}
	
	/**
	 * 
	 * 
	 * <br>Created 2014-9-3 下午5:55:56
	 * @param check check状态
	 * @author       阮锋冰
	 */
	public void setCheck(boolean check){
		
		//更新check状态
		this.check = check;
		if(check){
			//显示check状态下的图片
			
			if(checkImgResId != 0){
				//有效图片ID
				
				ivCheck.setBackgroundResource(checkImgResId);
			}
			
		}else{
			//显示uncheck状态下的图片
			
			if(uncheckImgResId != 0){
				//无效图片ID
				
				ivCheck.setBackgroundResource(uncheckImgResId);
			}else{
				//默认显示白色
				
				ivCheck.setBackgroundColor(0x00000000);
			}
		}
		
		//状态改变的回调函数
		if(mOnExRadioCheckChange != null){
			mOnExRadioCheckChange.onChangeCheck(check);
		}
		
		//对radio组，进行通知
		if(check && radioGroup!= null){
			radioGroup.notifyRadioClick(this);
		}
	}
	
	/**
	 * 设置radio组
	 * 
	 * <br>Created 2014-9-3 下午5:58:01
	 * @param exRadioGroup radio组
	 * @author       阮锋冰
	 */
	public void setExRadioGroup(ExRadioGroup exRadioGroup){
		
		this.radioGroup = exRadioGroup;
	}
	
	/**
	 * 获取当前的按钮状态
	 * 
	 * <br>Created 2014-9-3 下午5:58:28
	 * @return
	 * @author       Administrator
	 */
	public boolean getCheck(){
		
		return check;
	}
	
	/**
	 * 按钮状态改变的回调函数
	 * 
	 * <br>Created 2014-9-3 下午5:59:02
	 * @version  ExRadioButton
	 * @author   阮锋冰		
	 *
	 * @see 	 
	 * 
	 *
	 */
	public interface OnExRadioCheckChange{
		
		void onChangeCheck(boolean check);
	}

	@Override
	public void onClick(View arg0) {
		
        if (!check) {
            setCheck(true);
        }/*
          * else{
          * 
          * }
          */

	}
	
}
