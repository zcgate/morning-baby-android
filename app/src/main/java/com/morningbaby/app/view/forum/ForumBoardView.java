package com.morningbaby.app.view.forum;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.forum.sdk.bean.section.ForumSectionInfo;
import com.forum.sdk.bean.section.ForumSectionList;
import com.forum.sdk.dao.section.ForumSectionDao;
import com.forum.sdk.service.ForumServiceFactory;
import com.morningbaby.app.R;
import com.morningbaby.app.adapter.ForumBoardAdapter;
import com.morningbaby.app.gui.activity.ForumBoardDetailActivity;
import com.morningbaby.app.logic.ActivityCommand;
import com.morningbaby.app.logic.BaseFragment;
import com.morningbaby.app.view.CustomRefreshLayout;
import com.morningbaby.sdk.Constant;
import com.nd.smartcan.commons.util.logger.Logger;
import com.nd.smartcan.frame.dao.CacheDefine.IListDataRetrieveListener;
import com.nd.smartcan.frame.exception.DaoException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by zchao on 8/18/15.
 */
public class ForumBoardView extends BaseFragment {


    public static final int LIST_SIZE = Constant.PageSize;
    private int mPage = 0;
    private int mCurrentPage = 0;

    private ListView m_lv_boards;
    private CustomRefreshLayout mRefreshLayout;
    private Handler mHandler = new Handler();


    private ForumBoardAdapter mAdapter;
    private ForumSectionList mForumBoards;

    public static ForumBoardView newInstance() {
        ForumBoardView fragment = new ForumBoardView();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        initContent();
        return m_lv_boards;
    }

    public void initContent(){


        m_lv_boards = (ListView) getView().findViewById(R.id.lv_forum_boards);

        mRefreshLayout = (CustomRefreshLayout) getView().findViewById(R.id.swiperefresh);
        mRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.orange));
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData(true);
            }
        });
        mRefreshLayout.setOnLoadListener(new CustomRefreshLayout.OnLoadListener() {
            @Override
            public void onLoad() {
                nextPage();
            }
        });

        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mRefreshLayout.setRefreshing(true);
                loadData(false);
            }
        });

        m_lv_boards.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                position = position - m_lv_boards.getHeaderViewsCount();
                Bundle bundle = new Bundle();
                bundle.putString("forumBoardId", mForumBoards.getItems().get(position).getId());
                ActivityCommand com = new ActivityCommand();
                com.setBundle(bundle);
                gotoActivity(com, ForumBoardDetailActivity.class);
            }
        });
    }

    public void loadData(boolean force) {
        try {
            ForumSectionList forumBoards = ForumServiceFactory.INSTANCE.getForumSectionService().getHotSectionList(mPage, LIST_SIZE, true);
            showForumBoardList(forumBoards.getItems());
            mCurrentPage = 0;
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }


    public void nextPage() {
        try {
            ForumSectionList forumBoards = ForumServiceFactory.INSTANCE.getForumSectionService().getHotSectionList(mCurrentPage, LIST_SIZE, true);
            showForumBoardList(forumBoards.getItems());
            mCurrentPage += 1;
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    private Object sync = new Object();

    private void showForumBoardList(List<ForumSectionInfo> ForumBoards) {
        synchronized (sync) {
            if(isDetached())
                return;
            if (mForumBoards == null) {
                mForumBoards = new ForumSectionList();
            } else {
                mForumBoards.setItems(null);
            }
            if (ForumBoards != null) {
                mForumBoards.setItems(ForumBoards);
            }
            if (mAdapter == null) {
                mAdapter = new ForumBoardAdapter(getActivity(), mForumBoards);
                m_lv_boards.setAdapter(mAdapter);
            } else {
                mAdapter.notifyDataSetChanged();
            }
        }
    }
//
//    IListDataRetrieveListener<ForumSectionInfo> listener = new IListDataRetrieveListener<ForumSectionInfo>() {
//
//        @Override
//        public void onCacheDataRetrieve(List<ForumSectionInfo> ForumBoards, Map<String, Object> stringObjectMap, boolean b) {
//            Logger.i(getClass(), "retrieve ForumBoards list from cache.");
//            showForumBoardList(ForumBoards);
//        }
//
//        @Override
//        public void onServerDataRetrieve(List<ForumSectionInfo> ForumBoards, Map<String, Object> stringObjectMap) {
//            Logger.i(getClass(), "retrieve ForumBoards list from server.");
//            showForumBoardList(ForumBoards);
//        }
//
//        @Override
//        public void done() {
//            mRefreshLayout.setRefreshing(false);
//            mRefreshLayout.setLoading(false);
////            mRefreshLayout.setCanLoadMore(mForumBoards.canLoadMore());
//        }
//
//        @Override
//        public void onException(Exception e) {
//            e.printStackTrace();
//            fail();
//        }
//
//        @Override
//        public Handler getCallBackLooperHandler() {
//            return mHandler;
//        }
//    };

    private void fail() {
        Toast.makeText(getActivity(), R.string.network_error, Toast.LENGTH_SHORT).show();
    }
}
