package com.morningbaby.app.logic;

import android.app.ActionBar;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.morningbaby.app.R;
import com.morningbaby.app.log.Logger;
import com.morningbaby.app.tool.SystemBarTintManager;
import com.morningbaby.app.tool.environment;
import com.nd.smartcan.frame.exception.DaoException;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Iterator;

public class BaseActivity extends FragmentActivity{

    private BroadcastReceiver m_broadcastReceiver;
    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        ActivityStackManager.getInstance().pushActivity(this);
        onBeforeCreate(savedInstanceState);
        super.onCreate(savedInstanceState);
        onCreateContent(savedInstanceState);
     //   initSystemBar();
        createActionBar();
        try {
            onAfterCreate(savedInstanceState);
        } catch (DaoException e) {
            e.printStackTrace();
        }
        registerActions();
    }

    private void registerActions() {
        IntentFilter intentFilter = new IntentFilter();
        addActions(intentFilter);
        Iterator<String> ite = intentFilter.actionsIterator();
        if (ite != null && ite.hasNext()) {
            registerReceiver(getBroadCastReceiver(), intentFilter);
        }
    }

    private BroadcastReceiver getBroadCastReceiver() {
        if (m_broadcastReceiver == null) {
            m_broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    // TODO Auto-generated method stub
                    onReceiveAction(context, intent);
                }
            };
        }
        return m_broadcastReceiver;
    }

    protected void onReceiveAction(Context context, Intent intent) {

    }

    protected void addActions(IntentFilter intentFilter) {

    }

    protected void addAction(String action, IntentFilter intentFilter) {
        if (!TextUtils.isEmpty(action))
            intentFilter.addAction(action);
    }

    private void initSystemBar(){
        SystemBarTintManager tintManager = new SystemBarTintManager(this);
        tintManager.setStatusBarTintEnabled(true);
        tintManager.setTintColor(getResources().getColor(R.color.orange));
    }

    /**
     * 設置當前activity的contentView
     *
     * @param savedInstanceState
     */
    protected void onCreateContent(Bundle savedInstanceState) {
        int rid = getContentViewID();
        if (rid != -1) {
            setContentView(rid);
        } else {
            setContentView(createContentView());
        }
    }

    /**
     * 返回自建的view
     *
     * @return
     */
    protected View createContentView() {
        return null;
    }

    /**
     * 返回res中layou對應的資源id號
     *
     * @return
     */
    protected int getContentViewID() {
        return -1;
    }

    protected String getActionBarTitle() {
        return "";
    }

    protected void onBeforeCreate(Bundle savedInstanceState) {

    }

    protected void onAfterCreate(Bundle savedInstanceState) throws DaoException {

    }

    protected void createActionBar() {
        ActionBar actionBar = getActionBar();
        if (actionBar == null)
            return;
        // 返回箭头（默认不显示）
        actionBar.setDisplayHomeAsUpEnabled(false);
        // 左侧图标点击事件使能
        actionBar.setHomeButtonEnabled(true);
        // 使左上角图标(系统)是否显示
        actionBar.setDisplayShowHomeEnabled(false);
        // 显示标题
        actionBar.setDisplayShowTitleEnabled(false);
        // 显示自定义视图
        actionBar.setDisplayShowCustomEnabled(true);
        setOverflowShowingAlways();

        setActionBarCustomView();
    }

    protected void setActionBarCustomView() {
        View actionbarLayout = LayoutInflater.from(this).inflate(
                R.layout.title, null);
        getActionBar().setCustomView(actionbarLayout);
        setActionBarTitle(getActionBarTitle());
        setCloseImageListener();

        actionbarLayout.getLayoutParams().width= environment.getScreenWidth(this);
    }

    protected void setCloseImageListener() {
        ImageView title_back = (ImageView) findViewById(R.id.title_iv_back);
        title_back.setOnClickListener(getBackClickListener());
    }

    public OnClickListener getBackClickListener() {
        return new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        };
    }

    private TextView m_tvTilte;

    protected TextView getActionBarTitleView() {
        if(m_tvTilte==null)
            m_tvTilte=(TextView) findViewById(R.id.title_tv_title);
        return m_tvTilte;
    }

    protected void setActionBarTitle(String title) {
        getActionBarTitleView().setText(title);
    }

    private void setOverflowShowingAlways() {
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class
                    .getDeclaredField("sHasPermanentMenuKey");
            menuKeyField.setAccessible(true);
            menuKeyField.setBoolean(config, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onMenuOpened(int featureId, Menu menu) {
        if (featureId == Window.FEATURE_ACTION_BAR && menu != null) {
            if (menu.getClass().getSimpleName().equals("MenuBuilder")) {
                try {
                    Method m = menu.getClass().getDeclaredMethod(
                            "setOptionalIconsVisible", Boolean.TYPE);
                    m.setAccessible(true);
                    m.invoke(menu, true);
                } catch (Exception e) {
                }
            }
        }
        return super.onMenuOpened(featureId, menu);
    }

    public void gotoActivity(Class activity) {
        gotoActivity(null, activity);
    }

    public void gotoActivity(Class activity, int requestCode) {
        gotoActivity(activity.getName(), requestCode);
    }

    public void gotoActivity(String activity, int requestCode) {
        Intent intent = new Intent();
        intent.setClassName(this, activity);
        startActivityForResult(intent, requestCode);
    }

    public void gotoActivity(ActivityCommand command, Class activity) {
        gotoActivity(command, activity.getName());
    }

    public void gotoActivity(String activity) {
        gotoActivity(null, activity);
    }

    public void gotoActivity(ActivityCommand command, String activity) {
        Intent intent = new Intent();
        intent.setClassName(this, activity);
        if (command != null) {
            Bundle bundle = command.getBundle();
            if (bundle != null)
                intent.putExtras(bundle);
            int requestCode = command.getRequestCode();
            if (requestCode != -1) {
                startActivityForResult(intent, requestCode);
            } else {
                startActivity(command, intent);
            }
        } else {
            go(intent);
        }
    }

    public void startActivity(ActivityCommand command, Intent intent) {
        if (command == null) {
            go(intent);
            return;
        }
        boolean close = false;
        if (command.isCloseAll()) {
            close = true;
            ActivityStackManager.getInstance().remove(this);
            ActivityStackManager.getInstance().popAllActivity();
        } else {
            String closeActivityUtilOne = command.getCloseActivityUtilOne();
            if (!TextUtils.isEmpty(closeActivityUtilOne)) {
                close = true;
                ActivityStackManager.getInstance().remove(this);
                ActivityStackManager.getInstance().popActivityUtilOne(
                        closeActivityUtilOne);
            }
        }
        if (intent == null)
            return;
        startActivity(intent);
        if (close || command.isClose()) {
            finish();
        }
    }

    private void go(Intent intent) {
        if (intent == null)
            return;
        startActivity(intent);
    }

    @Override
    public void finish() {
        // TODO Auto-generated method stub
        super.finish();
        ActivityStackManager.getInstance().remove(this);
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        if (isFinishing()) {
            if (m_broadcastReceiver != null)
                unregisterReceiver(m_broadcastReceiver);
            setContentView(new View(this));
            System.gc();
            System.runFinalization();
        }
    }
}
