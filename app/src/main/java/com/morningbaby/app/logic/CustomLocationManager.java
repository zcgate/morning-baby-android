package com.morningbaby.app.logic;

import android.app.Activity;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import com.morningbaby.app.MorningBabyApp;
import com.morningbaby.app.log.Logger;

public class CustomLocationManager {
	private LocationManager m_locationManager;
	private LocationListener m_networkListner = null;
	private CustomLocationListener m_listener;
	private boolean success=false;
	public CustomLocationManager(CustomLocationListener listener) {
		m_listener = listener;
	}

	public void startLocationListener() {
		if (m_locationManager == null)
			m_locationManager = (LocationManager) MorningBabyApp.getInstance()
					.getSystemService(Activity.LOCATION_SERVICE);
		if (!m_locationManager
				.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
			Logger.e(getClass(), "locationDisabled");
			if(m_listener!=null){
				m_listener.unsupport();
			}
			return;
		}
		m_networkListner = new MyLocationListner();
		m_locationManager.requestLocationUpdates(
				LocationManager.NETWORK_PROVIDER, 0, 500, m_networkListner);
//		new Thread(new Runnable() {
//			@Override
//			public void run() {
//				try {
//					Thread.sleep(timeout);
//				} catch (InterruptedException e) {
//					e.printStackTrace();
//				}
//				if(!success){
//					m_locationManager.removeUpdates(m_networkListner);
//					if(m_listener!=null){
//						m_listener.timeout();
//					}
//				}
//			}
//		}).start();
	}

	private class MyLocationListner implements LocationListener {
		@Override
		public void onLocationChanged(final Location location) {
			success=true;
			m_locationManager.removeUpdates(this);
			if(m_listener!=null)
				m_listener.onLocationChange(location);
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onProviderDisabled(String provider) {
		}
	};
	
	public interface CustomLocationListener {
		public void onLocationChange(Location location);

		public void unsupport();

		public void timeout();
	}
}
