package com.morningbaby.app.logic;

import android.os.Bundle;

public class ActivityCommand {
	private Bundle bundle; 
	private int requestCode=-1; //如果有设定，调用startActivityForResult
	private boolean isCloseAll=false; //是否关闭前面所有activity之后再跳转
	private boolean isClose=false;
	private String  closeActivityUtilOne; //关闭closeActivityUtilOne之后开启的所有页面
	public Bundle getBundle() {
		return bundle;
	}
	public void setBundle(Bundle bundle) {
		this.bundle = bundle;
	}
	public boolean isClose() {
		return isClose;
	}
	public void setClose(boolean isClose) {
		this.isClose = isClose;
	}
	public int getRequestCode() {
		return requestCode;
	}
	public void setRequestCode(int requestCode) {
		this.requestCode = requestCode;
	}
	public boolean isCloseAll() {
		return isCloseAll;
	}
	public void setCloseAll(boolean isCloseAll) {
		this.isCloseAll = isCloseAll;
	}
	public String getCloseActivityUtilOne() {
		return closeActivityUtilOne;
	}
	public void setCloseActivityUtilOne(Class closeActivityUtilOne) {
		this.closeActivityUtilOne = closeActivityUtilOne.getName();
	}
	public void setCloseActivityUtilOne(String closeActivityUtilOne){
		this.closeActivityUtilOne = closeActivityUtilOne;
	}
}
