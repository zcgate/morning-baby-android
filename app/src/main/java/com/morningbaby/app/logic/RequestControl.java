package com.morningbaby.app.logic;

import android.app.Activity;
import android.os.Handler;

import com.morningbaby.app.MorningBabyApp;

/**
 * Created by apple on 15/8/5.
 */
public class RequestControl {
    private Handler mHandler;
    private boolean showProgress=true; //是否显示进度条
    private boolean async=true; //true，异步执行
    private boolean cancelable=true;
    public void setShowProgress(boolean showProgress) {
        this.showProgress = showProgress;
    }

    public boolean isCancelable() {
        return cancelable;
    }

    public void setCancelable(boolean cancelable) {
        this.cancelable = cancelable;
    }

    public void setAsync(boolean async) {
        this.async = async;
    }
    public RequestControl(Handler handler){
        mHandler=handler;
    }

    public void doRequest(final Activity activity,final RequestCallback callback){
        if(callback==null)
            return;
        if(async){
            if(showProgress){
                MorningBabyApp.getInstance().showProgressDialog(activity,cancelable);
            }
            new Thread(new Runnable() {
                @Override
                public void run() {
                    final Object data=callback.request();
                    if(mHandler!=null)
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if(showProgress){
                                    MorningBabyApp.getInstance().dismissProgressDialog();
                                }
                                if(activity!=null&&!activity.isFinishing())
                                     callback.afterRequest(data);
                            }
                        });
                }
            }).start();
        }else{
            Object data=callback.request();
            callback.afterRequest(data);
        }
    }

    public interface RequestCallback{
        public Object request();
        public void afterRequest(Object result);
    }

}
