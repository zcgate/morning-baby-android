package com.morningbaby.app.logic;

import android.app.Activity;

import java.util.ListIterator;
import java.util.Stack;

public class ActivityStackManager {
    private static Stack<BaseActivity> activityStack;
    private static ActivityStackManager m_self;

    private ActivityStackManager() {

    }

    public boolean has(Class activity) {
        if (activityStack == null || activityStack.size() == 0)
            return false;
        ListIterator<BaseActivity> list = activityStack.listIterator();
        if (list == null)
            return false;
        while (list.hasNext()) {
            BaseActivity temp = list.next();
            if (temp.getClass().getName().equals(activity.getName())) {
                return true;
            }
        }
        return false;
    }

    public static ActivityStackManager getInstance() {
        if (m_self == null) {
            m_self = new ActivityStackManager();
            activityStack = new Stack<BaseActivity>();
        }
        return m_self;
    }

    public void remove(BaseActivity activity) {
        if (activityStack.contains(activity))
            activityStack.remove(activity);
    }

    public void popActivity(BaseActivity activity) {
        if (activity != null) {
            ((Activity) activity).finish();
            activity = null;
        }
    }

    public BaseActivity currentActivity() {
        try {
            BaseActivity activity = activityStack.lastElement();
            return (BaseActivity) activity;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void pushActivity(BaseActivity activity) {
        if (activity != null)
            activityStack.add(activity);
    }

    public void popAllActivity() {
        while (true) {
            try {
                BaseActivity activity = activityStack.lastElement();
                if (activity == null) {
                    break;
                }

                popActivity(activity);
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }
    }

    public void popActivityUtilOne(String classname) {
        while (true) {
            try {
                BaseActivity activity = activityStack.lastElement();
                if (activity == null) {
                    break;
                }
                if (activity.getClass().getName().equals(classname)) {
                    break;
                }
                popActivity(activity);
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }
    }

    public void popActivityUtilOne(Class cls) {
        popActivityUtilOne(cls.getName());
    }


    public void popActivityTillOne(String classname) {
        while (true) {
            try {
                BaseActivity activity = activityStack.lastElement();
                if (activity == null) {
                    break;
                }
                if (activity.getClass().getName().equals(classname)) {
                    popActivity(activity);
                    break;
                }
                popActivity(activity);
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }
    }

    public void popActivityTillOne(Class cls) {
        popActivityTillOne(cls.getName());
    }

}
