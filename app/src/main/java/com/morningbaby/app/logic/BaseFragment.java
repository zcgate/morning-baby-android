package com.morningbaby.app.logic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.morningbaby.app.R;

import java.util.Iterator;

public class BaseFragment extends Fragment {

    private BroadcastReceiver m_broadcastReceiver;

    /**
     * 返回自建的view
     *
     * @return
     */
    protected View createContentView() {
        return null;
    }

    /**
     * 返回res中layou對應的資源id號
     *
     * @return
     */
    protected int getContentViewID() {
        return -1;
    }

    protected boolean showTitle() {
        return true;
    }

    protected String getTitle() {
        return "";
    }

    protected boolean showBack() {
        return false;
    }

    protected TextView getTitleView() {
        return (TextView) getView().findViewById(R.id.title_tv_title);
    }

    protected void setTitle(String title){
        getTitleView().setText(title);
    }

    protected OnClickListener backClick() {
        return new OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        };
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        registerActions();
    }
    @Override
    public void onDestroy(){
        if(m_broadcastReceiver!=null) {
            getActivity().unregisterReceiver(m_broadcastReceiver);
        }
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        int layout = R.layout.basefragment;
        LinearLayout baseView = (LinearLayout) LayoutInflater.from(
                getActivity()).inflate(layout, container, false);
        int rid = getContentViewID();
        View mainView = null;
        if (rid != -1) {
            mainView = LayoutInflater.from(getActivity()).inflate(rid,
                    baseView, true);
        } else {
            mainView = createContentView();
            if (mainView != null) {
                baseView.addView(mainView);
            }
        }
        initTitleView(baseView);
        return baseView;
    }

    private void initTitleView(View baseView){
        if(!showTitle()){
            baseView.findViewById(R.id.ll_title).setVisibility(View.GONE);
        }
        else{
            ImageView back= (ImageView) baseView.findViewById(R.id.title_iv_back);
            if(!showBack()){
                back.setVisibility(View.GONE);
            }else{
                back.setOnClickListener(backClick());
            }
            TextView title=(TextView)baseView.findViewById(R.id.title_tv_title);
            title.setText(getTitle());
        }
    }

    public void gotoActivity(Class activity) {
        gotoActivity(null, activity);
    }

    public void gotoActivity(Class activity, int requestCode) {
        gotoActivity(activity.getName(), requestCode);
    }

    public void gotoActivity(String activity, int requestCode) {
        Intent intent = new Intent();
        intent.setClassName(getActivity(), activity);
        startActivityForResult(intent, requestCode);
    }

    public void gotoActivity(ActivityCommand command, Class activity) {
        gotoActivity(command, activity.getName());
    }

    public void gotoActivity(String activity) {
        gotoActivity(null, activity);
    }

    public void gotoActivity(ActivityCommand command, String activity) {
        Intent intent = new Intent();
        intent.setClassName(getActivity(), activity);
        if (command != null) {
            Bundle bundle = command.getBundle();
            if (bundle != null)
                intent.putExtras(bundle);
            int requestCode = command.getRequestCode();
            if (requestCode != -1) {
                startActivityForResult(intent, requestCode);
            } else {
                if (getActivity() != null)
                    ((BaseActivity) getActivity()).startActivity(command,
                            intent);
            }
        } else {
            if (getActivity() != null)
                ((BaseActivity) getActivity()).startActivity(command, intent);
        }
    }

    private void registerActions() {
        IntentFilter intentFilter = new IntentFilter();
        addActions(intentFilter);
        Iterator<String> ite = intentFilter.actionsIterator();
        if (ite != null && ite.hasNext()) {
            if (getActivity() != null)
                getActivity().registerReceiver(getBroadCastReceiver(),
                        intentFilter);
        }
    }

    private BroadcastReceiver getBroadCastReceiver() {
        if (m_broadcastReceiver == null) {
            m_broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    // TODO Auto-generated method stub
                    onReceiveAction(context, intent);
                }
            };
        }
        return m_broadcastReceiver;
    }

    protected void onReceiveAction(Context context, Intent intent) {

    }

    protected void addActions(IntentFilter intentFilter) {

    }

    protected void addAction(String action, IntentFilter intentFilter) {
        if (!TextUtils.isEmpty(action))
            intentFilter.addAction(action);
    }
}
